<?php 

/**
 *  this is a helper class for writing commond shared code
 */
namespace Elementor;
trait Helper{
    public function getLeague(){
        $response = wp_remote_request( FOREHAND_API_URL.'/getLeague',
            [
                'method'     => 'GET', 
                'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            ]
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
    }
    public function getOptionsAndDefault($leagues){
       
        $options = []; 
		$default = count($leagues) > 0 ? $default = $leagues[0]->id : null;
		foreach ($leagues as $l) { 
			$options[$l->id] = __( $l->leagueName, 'plugin-domain');
        }
        return [$options, $default];
    }
    public function getOptionsAndDefaultForTournament($trs){
        $options = []; 
		$default = count($trs) > 0 ? $default = $trs[0]->id : null;
		foreach ($trs as $l) { 
			$options[$l->id] = __( $l->tournamentName, 'plugin-domain');
        }
        return [$options, $default];
    }
    public function getOptionsAndDefaultForNewsCat($categories){
        $options = []; 
		$default = count($categories) > 0 ? $default = $categories[0]->category : null;
		foreach ($categories as $c) { 
			$options[$c->category] = __( $c->category, 'plugin-domain');
        }
        return [$options, $default];
    }


    
    public function getOptionsAndDefaultValues($data, $fieldName){
        $options = []; 
        $found = false;
        if(isset($_GET['eventId'])){
            // check if id is present in the data set 
            foreach($data as $d){
                if($d->id==$_GET['eventId']){
                    $default = $_GET['eventId'];
                    $found = true;
                    break;
                }
            }
           
        }
        if(!$found){
            $default = count($data) > 0 ? $default = $data[0]->id : null;
        }
       
		
		foreach ($data as $l) { 
			$options[$l->id] = __( $l->{$fieldName}, 'plugin-domain');
        }
        return [$options, $default];
    }
    public function callApi($url){
        $response = wp_remote_request( FOREHAND_API_URL."/$url",
        [
            'method'     => 'GET', 
            'headers' => [
                'forehandclub' => FOREHAND_API_KEY
            ]
        ]
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
    }


}