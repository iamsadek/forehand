<?php
namespace App;

/**
 * Admin Pages Handler
 */
class Admin {

    public function __construct() {
        add_action( 'admin_menu', [ $this, 'admin_menu' ] );
        add_action( 'admin_init', array( $this, 'setup_init' ) );
    }

    /**
     * Register our menu page
     *
     * @return void
     */
    public function admin_menu() {
        global $submenu;

        $capability = 'manage_options';
        $slug       = 'forehand';

        $hook = add_menu_page( __( 'Forehand', 'textdomain' ), __( 'Forehand', 'textdomain' ), $capability, $slug, [ $this, 'settings_page_content' ], 'dashicons-text' );

        // if ( current_user_can( $capability ) ) {
        //     $submenu[ $slug ][] = array( __( 'App', 'textdomain' ), $capability, 'admin.php?page=' . $slug . '#/' );
        //     $submenu[ $slug ][] = array( __( 'Settings', 'textdomain' ), $capability, 'admin.php?page=' . $slug . '#/settings' );
        // }

        add_action( 'load-' . $hook, [ $this, 'init_hooks'] );
    }

    /**
     * Initialize our hooks for the admin page
     *
     * @return void
     */
    public function init_hooks() {
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
    }

    /**
     * Load scripts and styles for the app
     *
     * @return void
     */
    public function enqueue_scripts() {
        wp_enqueue_style( 'baseplugin-admin' );
        wp_enqueue_script( 'baseplugin-admin' );
    }

    /**
     * Render our admin page
     *
     * @return void
     */
    public function plugin_page() {
       // echo '<div class="wrap"><div id="vue-admin-app"></div></div>';
        require_once 'settings/forehandsetting.php';
    }
    
    /* Create the page*/
    public function settings_page_content() { ?>
        <div class="wrap">
            <h2> FOREHAND API KEY </h2>
            <form method="post" action="options.php">
                <?php
                    settings_fields("member_only_fields");
                    do_settings_sections("member_only_fields");
                    submit_button();
                ?>
            </form>
    <?php }
    /* Setup section_callback */
    public function section_callback( $arguments ) {
        /* Set up input*/
        switch( $arguments['id'] ){
            case "forehand_api_key" :
                echo "This key will be used to get your club data! Contact admin if you don't have a key.";
                break;
           
        }
    }
    public function setup_init() {
        register_setting("member_only_fields", "forehand_api_key");

        add_settings_section("forehand_api_key", " ", array($this, 'section_callback'), "member_only_fields");
        add_settings_field( 'forehand_api_key', 'Forehand API Key: ', array( $this, 'field_callback' ), 'member_only_fields', 'forehand_api_key' );

        
    }
    /* Create input fields*/
    public function field_callback ( $arguments ) {
        echo "<input name=\"forehand_api_key\" id=\"forehand_api_key\" type=\"text\" value=\"" .get_option("forehand_api_key"). "\"\>";
    }
}
