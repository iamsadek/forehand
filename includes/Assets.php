<?php
namespace App;

/**
 * Scripts and Styles Class
 */
class Assets {

    function __construct() {

        if ( is_admin() ) {
            add_action( 'admin_enqueue_scripts', [ $this, 'register' ], 25 );
        } else {
            add_action( 'wp_enqueue_scripts', [ $this, 'register' ], 50 );
        }
    }

    /**
     * Register our app scripts and styles
     *
     * @return void
     */
    public function register() {
        if(!is_admin()){
            wp_enqueue_style( 'iview-style', 'https://unpkg.com/view-design/dist/styles/iview.css', null, false );
            wp_enqueue_style( 'style-handle', plugins_url( "adons_assets/css/forehand.css", __FILE__ ), null, BASEPLUGIN_VERSION );
            wp_enqueue_style( 'dopadel-style', plugins_url( "adons_assets/css/dopadel.css", __FILE__ ), null, BASEPLUGIN_VERSION );
            wp_enqueue_style( 'dopadel-mobile-style', plugins_url( "adons_assets/css/dopadel_mobile.css", __FILE__ ), null, BASEPLUGIN_VERSION );
            wp_enqueue_style( 'forehand-tablet-style', plugins_url( "adons_assets/css/forehand_tablet.css", __FILE__ ), null, BASEPLUGIN_VERSION );
            wp_enqueue_style( 'forehand-mobile-style', plugins_url( "adons_assets/css/forehand_mobile.css", __FILE__ ), null, BASEPLUGIN_VERSION );
            wp_enqueue_style( 'forehand-icon', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', null, false );
            wp_enqueue_style( 'forehand-bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css', null, false );
            wp_enqueue_script( 'forehand-axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.9.0/axios.min.js', null, false );
            wp_enqueue_script( 'forehand-loadsh', 'https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js', null, false );
            wp_enqueue_script( 'forehand-moment', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js', null, false );
            wp_enqueue_script( 'script-handle', plugins_url( 'adons_assets/js/forehand.js', __FILE__ ), null, false);
            wp_enqueue_script( 'touch-js', plugins_url( 'adons_assets/js/touch.js', __FILE__ ), null, false);
            wp_enqueue_script( 'iview-js', 'https://unpkg.com/view-design/dist/iview.min.js', null, false);
            wp_enqueue_script( 'stripe-js', 'https://js.stripe.com/v3', null, false);
        }
       
        
        // $this->register_scripts( $this->get_scripts() );
        // $this->register_styles( $this->get_styles() );
    }

    /**
     * Register scripts
     *
     * @param  array $scripts
     *
     * @return void
     */
    private function register_scripts( $scripts ) {
        foreach ( $scripts as $handle => $script ) {
            $deps      = isset( $script['deps'] ) ? $script['deps'] : false;
            $in_footer = isset( $script['in_footer'] ) ? $script['in_footer'] : false;
            $version   = isset( $script['version'] ) ? $script['version'] : BASEPLUGIN_VERSION;

            wp_register_script( $handle, $script['src'], $deps, $version, $in_footer );
        }
    }

    /**
     * Register styles
     *
     * @param  array $styles
     *
     * @return void
     */
    public function register_styles( $styles ) {
        foreach ( $styles as $handle => $style ) {
            $deps = isset( $style['deps'] ) ? $style['deps'] : false;

            wp_register_style( $handle, $style['src'], $deps, BASEPLUGIN_VERSION );
        }
    }

    /**
     * Get all registered scripts
     *
     * @return array
     */
    public function get_scripts() {
        $prefix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '.min' : '';

        $scripts = [
            'baseplugin-runtime' => [
                'src'       => BASEPLUGIN_ASSETS . '/js/runtime.js',
                'version'   => filemtime( BASEPLUGIN_PATH . '/assets/js/runtime.js' ),
                'in_footer' => true
            ],
            'baseplugin-vendor' => [
                'src'       => BASEPLUGIN_ASSETS . '/js/vendors.js',
                'version'   => filemtime( BASEPLUGIN_PATH . '/assets/js/vendors.js' ),
                'in_footer' => true
            ],
            'baseplugin-frontend' => [
               // 'src'       => BASEPLUGIN_ASSETS . '/js/frontend.js',
                'deps'      => [ 'jquery', 'baseplugin-vendor', 'baseplugin-runtime' ],
                'version'   => filemtime( BASEPLUGIN_PATH . '/assets/js/frontend.js' ),
                'in_footer' => true
            ],
            'baseplugin-admin' => [
                'src'       => BASEPLUGIN_ASSETS . '/js/admin.js',
                'deps'      => [ 'jquery', 'baseplugin-vendor', 'baseplugin-runtime' ],
                'version'   => filemtime( BASEPLUGIN_PATH . '/assets/js/admin.js' ),
                'in_footer' => true
            ],
            
        ];

        return $scripts;
    }

    /**
     * Get registered styles
     *
     * @return array
     */
    public function get_styles() {

        $styles = [
            'baseplugin-style' => [
                'src' =>  BASEPLUGIN_ASSETS . '/css/style.css'
            ],
            'baseplugin-frontend' => [
                'src' =>  BASEPLUGIN_ASSETS . '/css/frontend.css'
            ],
            'baseplugin-admin' => [
                'src' =>  BASEPLUGIN_ASSETS . '/css/admin.css'
            ],
        ];

        return $styles;
    }

}
