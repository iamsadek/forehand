<?php

namespace App;

/**
 * Frontend Pages Handler
 */
class Frontend
{
    
    protected $news;
    protected $allowedControls = ['text', 'number', 'textarea', 'wysiwyg', 'repeater','URL'];
    public function __construct()
    {

        // Register widgets
        add_action('elementor/widgets/widgets_registered', [$this, 'register_widgets']);
       

        // add a single page view
        add_filter('template_include', [$this, 'newsSinglePageTemplate']);

    }

    public function register_widgets()
    {
        $dir = BASEPLUGIN_INCLUDES . '/adons';
        $adonsFolder = scandir(BASEPLUGIN_INCLUDES . '/adons');
        foreach ($adonsFolder as $folderName) {
            if (($folderName == '.') || ($folderName == '..')) {
                continue;
            }

            if (is_dir($dir . '/' . $folderName)) {
                $phpFile = $dir . '/' . $folderName . '/' . $folderName . '.php';
                $className = ucfirst($folderName);
                if (file_exists($phpFile)) { // include the php file if exists and declere the classes passing elementor namespaced
                    require $phpFile;
                    $class = "\Elementor\\$className";
                    $a = new $class();
                    \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new $class());
                    // translation logic
                    $name = $a->get_name();
                    
                    $info = $a->get_controls();
                    $allControls = [];
                    
                    foreach ( $info as $n => $control ) {
                        if(in_array($control['type'],$this->allowedControls) && key_exists('section', $control) && $control['section'] == 'section_title'){
                            $allControls[] = $control;
                            ;
                        }
                    }
                    add_filter( 'wpml_elementor_widgets_to_translate', function($widget) use($allControls, $name){
                        return $this->add_wpml_translation($widget, $allControls, $name);
                        
                    });
                    $allControls = [];
                   
                    
                }
            }
        }
    }
    public function add_wpml_translation($widgets, $allControls, $name){
        
       $fieldsData = []; 
        foreach($allControls as $c){
            //error_log(print_r($c, true));
            // if($c['name'] == 'buttonLink') continue;
            //error_log($c['type']);
            $type = $c['type'] == 'wysiwyg' ? 'VISUAL' : 'LINE';
            $fieldsData[] = [
                'field' => $c['name'],
                'type' => __( $c['name'], 'seelos-elementor' ),
                'editor_type' => $type
                // 'editor_type' =>  'VISUAL' 
            ];
        }
        $widgets[ $name ] = [
            'conditions' => [ 'widgetType' => $name ],
            'fields'     => $fieldsData,
         ];
        //error_log(print_r($fieldsData, true));
        
        return $widgets;
    }

    // news single page template render and logics
    public function newsSinglePageTemplate($templates, $content = '')
    {
        global $wp;
        // echo json_encode($wp);
        // die();
        if (isset($wp->query_vars['pagename']) && $wp->query_vars['pagename'] == 'forehand-news') {
            return $this->singleNews();
        }

        return $templates;
    }
    // get the contents of single page for the news..
    public function singleNews()
    {
        global $wp;
        $id = $wp->query_vars['page'];
        $response = wp_remote_request(FOREHAND_API_URL . "/clubNewsSingle/$id",
            array(
                'method' => 'GET',
                'headers' => [
                    'forehandclub' => FOREHAND_API_KEY,
                ],
            )
        );

        $newsData = wp_remote_retrieve_body($response);
        $this->news = json_decode($newsData);
        add_filter('pre_get_document_title', [$this, 'setPageTitleSingel']);
        get_header();
        wp_enqueue_style('baseplugin-frontend');

        // require the news page template
        require 'adons/newsSingle/newsSingleHtml.php';
        get_footer();
    }
    public function setPageTitleSingel($data)
    {
        return $this->news->title;
    }

    public function getDivisionData()
    {
        $response = wp_remote_request("http://backoffice.localhost/widget/web-display-byDivision/36",
            array(
                'method' => 'GET',
                'headers' => [
                    'forehandclub' => FOREHAND_API_KEY,
                ],
            )
        );

        //var_dump($response);
        //echo  $response;
        return $body = wp_remote_retrieve_body($response);
        //echo json_encode($body);
    }

}
