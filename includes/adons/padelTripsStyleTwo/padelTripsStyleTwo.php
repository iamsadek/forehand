<?php
namespace Elementor;

class PadelTripsStyleTwo extends Widget_Base {
	
	public function get_name() {
		return 'forehand-padelTrips-text-style-two';
	}
	
	public function get_title() {
		return 'PADEL TRIPS STYLE 2';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		        
    
		$this->add_control(
			'headingOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'All these places have' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		
		$this->add_control(
			'headingTwo', [
				'label' => __( 'Text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Something in Common' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingTwoColor',
			[
				'label' => __( 'Text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'sectionBgTwo',
			[
				'label' => __( 'Section background two', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$repeater->add_control(
			'headingThree', [
				'label' => __( 'Text two', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Specialised' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'headingThreeColor',
			[
				'label' => __( 'Text two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'headingFour', [
				'label' => __( 'Text three', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Padel Trips' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'headingFourColor',
			[
				'label' => __( 'Text three color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],
			
			]
		);

		
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('padelTripsStyleTwoHtml.php');
    }
   
	
	


	
	
}