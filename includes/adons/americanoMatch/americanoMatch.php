<?php
namespace Elementor;

require_once BASEPLUGIN_INCLUDES . '/common/helperclass.php';
class AmericanoMatch extends Widget_Base
{
	use Helper;
    public function get_name()
    {
        return 'forehand-americano-match';
    }

    public function get_title()
    {
        return 'AMERICANO 1';
    }

    public function get_icon()
    {
        return 'fas fa-heading';
    }

    public function get_categories()
    {
        return ['Forehand'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Settings', 'elementor'),
            ]
        );
        $tournamentLists = $this->callApi('getTournamentsListNamesByType?type=Americano');
        $optinsAndDefault = $this->getOptionsAndDefault($tournamentLists);
        $this->add_control(
            'trId',
            [
                'label' => __( 'Select the tournament', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => $optinsAndDefault[1],
                'options' => $optinsAndDefault[0],
            ]
         );
       
        $this->end_controls_section();
       
    }
   

    // php render.
    protected function render()
    {
		$settings = $this->get_settings_for_display();
		$id = $settings['trId'];
		
        $matchData = $this->getData($id);
		require 'americanoMatchHtml.php';
    }
    public function getData($id){
		
        $response = wp_remote_request( FOREHAND_API_URL."/americanoMatchesAndRankData/$id",
			array(
				'method'     => 'GET', 
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
			)
        );
		$body = wp_remote_retrieve_body($response);
		return json_decode($body);
    }

}
