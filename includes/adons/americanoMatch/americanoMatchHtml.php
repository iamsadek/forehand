<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var americanoSettings='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageUrl='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsmatchData = json_encode($matchData);
echo "<script type='text/javascript'>var jsmatchData='$jsmatchData'</script>";

?>

<americano1>
	<div v-cloak>
    <div>
    <div class="_2tv_all">
      <div class="_2tv_main">
        <!--===== Left =====-->
        <div class="_2tv_left">
          <div class="_2tv_group _text_right">
            <p class="_2tv_group_text">GROUP A</p>
          </div>
          <!-- Players Results -->
          <div class="_2tv_results_all">
            <!-- Items -->
            <div class="_2tv_results _1bg" v-for="(r, i) in rankA" :key="i" v-if="rankA.length">
              <p class="_2tv_results_num">{{i+1}}</p>

              <div class="_2tv_results_player">
                <div class="_2tv_results_player_pic">
                  <img class="_2tv_results_player_img" :src="imageUrl + r.player.profilePic " alt="" title="">
                </div>

                <p class="_2tv_results_player_name _text_overflow">{{r.player.firstName}} {{r.player.lastName}}</p>
              </div>

              <div class="_2tv_results_list">
                <ul class="_2tv_results_list_ul">
                  <li>{{r.round1}}</li>
                  <li>{{r.round2}}</li>
                  <li>{{r.round3}}</li>
                  <li>{{r.round4}}</li>
                  <li>{{r.round5}}</li>
                  <li>{{r.round6}}</li>
                  <li>{{r.round7}}</li>
                </ul>
              </div>

              <div class="_2tv_results_piont">
                <p class="_2tv_results_piont_text">{{r.total}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Players Results -->
          <!-- Teams -->
          <div class="_2tv_teams_all">
            <!-- Items -->
            <div class="_2tv_teams _1bg" v-for="(item, index) in groupA" :key="index" v-if="groupA.length">
              <div class="_2tv_teams_players_all">
                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player1.firstName}} {{item.player1.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player2.firstName}} {{item.player2.lastName}}</p>
                </div>

                <div class="_2tv_teams_vs">
                  <p class="_2tv_teams_vs_text">V/S</p>
                </div>

                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player3.firstName}} {{item.player3.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player4.firstName}} {{item.player4.lastName}}</p>
                </div>
              </div>

              <div class="_2tv_teams_results _2bg">
                <p class="_2tv_teams_results_point">
                  {{item.teamOnePoints}}
                  <span>-</span>
                  {{item.teamTwoPoints}}
                </p>

                <p class="_2tv_teams_court">{{ item.courtName || item.court.courtName}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Teams -->


        </div>
        <!--===== Left =====-->

        <!--===== Center =====-->
        <div class="_2tv_center">
          <div class="_2tv_center_top">
            <img class="_2tv_center_logo" :src="imageUrl + club.logo" alt="" title="" v-if="club">
            <p class="_2tv_center_title">{{trName}}</p>
            <p class="_2tv_center_date" v-if="tr">{{tr.startingTime}} | {{tr.playingTime}}</p>
            <p class="_2tv_center_court" v-if="tr">{{tr.city}}</p>
          </div>

          <div class="_2tv_center_final" v-if="final">
            <p class="_2tv_center_final_title">FINAL</p>
            <!-- Items -->
            <div class="_2tv_teams _1bg">
              <div class="_2tv_teams_players_all">
                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{final.player1.firstName}} {{final.player1.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{final.player2.firstName}} {{final.player2.lastName}}</p>
                </div>

                <div class="_2tv_teams_vs">
                  <p class="_2tv_teams_vs_text">V/S</p>
                </div>

                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{final.player3.firstName}} {{final.player3.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{final.player4.firstName}} {{final.player4.lastName}}</p>
                </div>
              </div>

              <div class="_2tv_teams_results _2bg">
                <p class="_2tv_teams_results_point">
                  {{final.teamOnePoints}}
                  <span>-</span>
                  {{final.teamTwoPoints}}
                </p>

                <!-- <p class="_2tv_teams_court">COURT 3</p> -->
              </div>
            </div>
            <!-- Items -->
          </div>

          <div class="_2tv_center_winner" v-if="final && final.over == 1">
            <p class="_2tv_center_winner_text">Winners</p>

            <div class="_2tv_center_winner_main" v-if="final.teamOnePoints > final.teamTwoPoints">
              <div class="_2tv_center_winner_team">
                <img class="_2tv_center_img" :src="imageUrl + final.player1.profilePic " alt="" title="">
                <p class="_2tv_center_winner_name _text_overflow">{{final.player1.firstName}} {{final.player1.lastName}}</p>
              </div>

              <p class="_2tv_center_winner_and"> & </p>

              <div class="_2tv_center_winner_team">
                <img class="_2tv_center_img" :src="imageUrl + final.player2.profilePic " alt="" title="">
                <p class="_2tv_center_winner_name _text_overflow">{{final.player2.firstName}} {{final.player2.lastName}}</p>
              </div>
            </div>
             <div class="_2tv_center_winner_main" v-else>
              <div class="_2tv_center_winner_team">
                <img class="_2tv_center_img" :src="imageUrl + final.player3.profilePic " alt="" title="">
                <p class="_2tv_center_winner_name _text_overflow">{{final.player3.firstName}} {{final.player3.lastName}}</p>
              </div>

              <p class="_2tv_center_winner_and"> & </p>

              <div class="_2tv_center_winner_team">
                <img class="_2tv_center_img" :src="imageUrl + final.player4.profilePic " alt="" title="">
                <p class="_2tv_center_winner_name _text_overflow">{{final.player4.firstName}} {{final.player4.lastName}}</p>
              </div>
            </div>


          </div>

          <div class="_2tv_center_sponsor">
            <p class="_2tv_center_sponsor_title">sponsors</p>

            <ul class="_2tv_center_sponsor_list">
              <li v-for="(l, i) in sponsorsLogo" :key="i" v-if="sponsorsLogo.length && l.logo !='/uploads/white.png' ">
                <img :src=" imageUrl + l.logo " alt="" title="">
              </li>

            </ul>
          </div>
        </div>
        <!--===== Center =====-->

        <!--===== Right =====-->
        <div class="_2tv_left">
          <div class="_2tv_group _text_left">
            <p class="_2tv_group_text">GROUP B</p>
          </div>
             <!-- Players Results -->
          <div class="_2tv_results_all">
            <!-- Items -->
             <div class="_2tv_results _1bg" v-for="(r, i) in rankB" :key="i" v-if="rankB.length">
              <p class="_2tv_results_num">{{i+1}}</p>

              <div class="_2tv_results_player">
                <div class="_2tv_results_player_pic">
                  <img class="_2tv_results_player_img" :src="imageUrl + r.player.profilePic " alt="" title="">
                </div>

                <p class="_2tv_results_player_name _text_overflow">{{r.player.firstName}} {{r.player.lastName}}</p>
              </div>

              <div class="_2tv_results_list">
                <ul class="_2tv_results_list_ul">
                  <li>{{r.round1}}</li>
                  <li>{{r.round2}}</li>
                  <li>{{r.round3}}</li>
                  <li>{{r.round4}}</li>
                  <li>{{r.round5}}</li>
                  <li>{{r.round6}}</li>
                  <li>{{r.round7}}</li>
                </ul>
              </div>

              <div class="_2tv_results_piont">
                <p class="_2tv_results_piont_text">{{r.total}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Players Results -->
          <!-- Teams -->
         <div class="_2tv_teams_all">
            <!-- Items -->
            <div class="_2tv_teams _1bg" v-for="(item, index) in groupB" :key="index" v-if="groupB.length">
              <div class="_2tv_teams_players_all">
                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player1.firstName}} {{item.player1.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player2.firstName}} {{item.player2.lastName}}</p>
                </div>

                <div class="_2tv_teams_vs">
                  <p class="_2tv_teams_vs_text">V/S</p>
                </div>

                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player3.firstName}} {{item.player3.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player4.firstName}} {{item.player4.lastName}}</p>
                </div>
              </div>

              <div class="_2tv_teams_results _2bg">
                <p class="_2tv_teams_results_point">
                  {{item.teamOnePoints}}
                  <span>-</span>
                  {{item.teamTwoPoints}}
                </p>

                <p class="_2tv_teams_court">{{ item.courtName || item.court.courtName}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Teams -->


        </div>
        <!--===== Right =====-->
      </div>
    </div>
    <div class="_2tv_all" v-if="groupC.length">
      <div class="_2tv_main">
        <!--===== Left =====-->
        <div class="_2tv_left">
          <div class="_2tv_group _text_right">
            <p class="_2tv_group_text">GROUP C</p>
          </div>
          <!-- Players Results -->
          <div class="_2tv_results_all">
            <!-- Items -->
            <div class="_2tv_results _1bg" v-for="(r, i) in rankC" :key="i" v-if="rankC.length">
              <p class="_2tv_results_num">{{i+1}}</p>

              <div class="_2tv_results_player">
                <div class="_2tv_results_player_pic">
                  <img class="_2tv_results_player_img" :src="imageUrl + r.player.profilePic " alt="" title="">
                </div>

                <p class="_2tv_results_player_name _text_overflow">{{r.player.firstName}} {{r.player.lastName}}</p>
              </div>

              <div class="_2tv_results_list">
                <ul class="_2tv_results_list_ul">
                  <li>{{r.round1}}</li>
                  <li>{{r.round2}}</li>
                  <li>{{r.round3}}</li>
                  <li>{{r.round4}}</li>
                  <li>{{r.round5}}</li>
                  <li>{{r.round6}}</li>
                  <li>{{r.round7}}</li>
                </ul>
              </div>

              <div class="_2tv_results_piont">
                <p class="_2tv_results_piont_text">{{r.total}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Players Results -->
          <!-- Teams -->
          <div class="_2tv_teams_all">
            <!-- Items -->
            <div class="_2tv_teams _1bg" v-for="(item, index) in groupC" :key="index" v-if="groupC.length">
              <div class="_2tv_teams_players_all">
                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player1.firstName}} {{item.player1.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player2.firstName}} {{item.player2.lastName}}</p>
                </div>

                <div class="_2tv_teams_vs">
                  <p class="_2tv_teams_vs_text">V/S</p>
                </div>

                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player3.firstName}} {{item.player3.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player4.firstName}} {{item.player4.lastName}}</p>
                </div>
              </div>

              <div class="_2tv_teams_results _2bg">
                <p class="_2tv_teams_results_point">
                  {{item.teamOnePoints}}
                  <span>-</span>
                  {{item.teamTwoPoints}}
                </p>

                <p class="_2tv_teams_court">{{ item.courtName || item.court.courtName}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Teams -->


        </div>
        <!--===== Left =====-->

        <!--===== Center =====-->
        <div class="_2tv_center">
         

         
        </div>
        <!--===== Center =====-->

        <!--===== Right =====-->
        <div class="_2tv_left">
          <div class="_2tv_group _text_left">
            <p class="_2tv_group_text">GROUP D</p>
          </div>
             <!-- Players Results -->
          <div class="_2tv_results_all">
            <!-- Items -->
             <div class="_2tv_results _1bg" v-for="(r, i) in rankD" :key="i" v-if="rankD.length">
              <p class="_2tv_results_num">{{i+1}}</p>

              <div class="_2tv_results_player">
                <div class="_2tv_results_player_pic">
                  <img class="_2tv_results_player_img" :src="imageUrl + r.player.profilePic " alt="" title="">
                </div>

                <p class="_2tv_results_player_name _text_overflow">{{r.player.firstName}} {{r.player.lastName}}</p>
              </div>

              <div class="_2tv_results_list">
                <ul class="_2tv_results_list_ul">
                  <li>{{r.round1}}</li>
                  <li>{{r.round2}}</li>
                  <li>{{r.round3}}</li>
                  <li>{{r.round4}}</li>
                  <li>{{r.round5}}</li>
                  <li>{{r.round6}}</li>
                  <li>{{r.round7}}</li>
                </ul>
              </div>

              <div class="_2tv_results_piont">
                <p class="_2tv_results_piont_text">{{r.total}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Players Results -->
          <!-- Teams -->
         <div class="_2tv_teams_all">
            <!-- Items -->
            <div class="_2tv_teams _1bg" v-for="(item, index) in groupD" :key="index" v-if="groupD.length">
              <div class="_2tv_teams_players_all">
                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player1.firstName}} {{item.player1.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player2.firstName}} {{item.player2.lastName}}</p>
                </div>

                <div class="_2tv_teams_vs">
                  <p class="_2tv_teams_vs_text">V/S</p>
                </div>

                <div class="_2tv_teams_players">
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player3.firstName}} {{item.player3.lastName}}</p>
                  <p class="_2tv_teams_players_name _text_overflow">{{item.player4.firstName}} {{item.player4.lastName}}</p>
                </div>
              </div>

              <div class="_2tv_teams_results _2bg">
                <p class="_2tv_teams_results_point">
                  {{item.teamOnePoints}}
                  <span>-</span>
                  {{item.teamTwoPoints}}
                </p>

                <p class="_2tv_teams_court">{{ item.courtName || item.court.courtName}}</p>
              </div>
            </div>
            <!-- Items -->
          </div>
          <!-- Teams -->


        </div>
        <!--===== Right =====-->
      </div>
    </div>

  </div>
    </div>
</americano1>

<script>
settings  = JSON.parse(americanoSettings)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"

jsmatchData  = JSON.parse(jsmatchData)
var list = document.getElementsByTagName("americano1");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "americano1-app-" + i);
           var app = new Vue({
           el: "#americano1-app-" + i,
                data(){
                    return {
                        groupA : [],
                        groupB : [],
                        groupC : [],
                        groupD : [],
                        final :  null,
                        rankA: [],
                        rankB: [],
                        rankC: [],
                        rankD: [],
                        club: null,
                        trName : '',
                        sponsorsLogo : [],
                        tr: null,
                        indexA: 0,
                        indexB: 0,
                        
                        refreshTime : 5000, 
                        
                    }
                },
                methods : {
                    
                    async getNewData(){
                        
                        const res = await this.callApi('get', `${apiUrl}/refreshAmericanoData/${this.tr.id}`)
                        if(res.status==200){
                            this.groupA = res.data.groupA
                            this.groupB = res.data.groupB
                            this.final =  res.data.final
                            this.rankA = res.data.rankA
                            this.rankB = res.data.rankB
                        }else{
                           alert('There was a an issue occured while fetching new update')
                        }

                        setTimeout(()=>{
                        this.getNewData()
                        }, this.refreshTime)

                    },
                    async callApi(method, url, dataObj) {
                        try {

                            let data = await axios({
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                method: method,
                                url: url,
                                data: dataObj
                            })
                            return data

                        } catch (e) {

                            return e.response
                        }
                    },
                },

                created(){
                    console.log(jsmatchData)
                    this.tr = jsmatchData.tr
                    this.groupA = jsmatchData.groupA
                    this.groupB = jsmatchData.groupB
                    this.groupC = jsmatchData.groupC
                    this.groupD = jsmatchData.groupD
                    this.final =  jsmatchData.final
                    this.rankA = jsmatchData.rankA
                    this.rankB = jsmatchData.rankB
                    this.rankC = jsmatchData.rankC
                    this.rankD = jsmatchData.rankD
                    this.club =  jsmatchData.club
                    this.trName = jsmatchData.trName
                    this.sponsorsLogo = jsmatchData.sponsorsLogo
                   
                    setTimeout(()=>{
                        this.getNewData()
                    }, this.refreshTime)
                }


			})

    }
</script>