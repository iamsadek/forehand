<div class="_pdl_plyr_section image_row_section">

		<!-- ITEAM -->
		<?php if($settings['allImages'] ): ?> 
			<?php foreach($settings['allImages'] as $item ): ?>
				<div class="image_col_3">
					<div class="_pdl_plyr_crd image_row_item">
						<a href="<?php echo $item['url1']['url']?>">
							<img src="<?php echo $item['image1']['url'] ?>">
							<div class="_pdl_plyr_crd_info">
								<h3><?php echo $item['header1'] ?></h3>
								<p><?php echo $item['subHeader1'] ?></p>
							</div>
						</a>
					</div>
				</div>
		<?php endforeach; ?>
        <?php endif; ?>
		<!-- ITEAM -->

	
	</div>