<?php
namespace Elementor;

class ImageRowStyleOne extends Widget_Base {
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name() {
		return 'forehand-image-row-style-one';
	}
	
	public function get_title() {
		return 'IMAGE ROW STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-picture-o';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$repeater = new \Elementor\Repeater();
		
		$repeater->add_control(
			'image1', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/logo.png',
				],
			]
		);
		$repeater->add_control(
			'header1',
			[
				'label' => __( 'Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Series Game'
			]
		);
		$repeater->add_control(
			'subHeader1',
			[
				'label' => __( 'Sub Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => "Men & women's series play"
			]
		);
		$repeater->add_control(
			'url1', [
				'label' => __( 'Image 1 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'allImages',
			[
				'label' => __( 'Courts', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [],
				'title_field' => '{{{ header1 }}}',
			]
		);



		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('imageRowStyleOneHtml.php');
    }
   
	
	


	
	
}