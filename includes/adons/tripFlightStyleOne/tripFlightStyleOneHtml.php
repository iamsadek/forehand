
	<!-- DO PADEL PAGE-4 SECHUDLE-->
	<!-- DO PADEL PAGE-4 SECHUDLE-->
	<div class="_do_pdl4_schdl_sec" style="background-color: <?php echo $settings['sectionBackground'] ?>;">
		<div class="_do_pdl4_schdl_top">
       <h3 style="color : <?php echo $settings['headerOneColor'] ?>
       ;background: <?php echo  $settings['headerOneBackground']?>" >
           <?php echo  $settings['headerOne']?>
        </h3>
		</div>
		<div class="_do_pdl4_schdl_card_main">
			<div class="_do_pdl4_schdl_card_day">
        <p style="color : <?php echo $settings['headerTwoColor'] ?>">
           <?php echo  $settings['headerTwo']?>
        </p>
			</div>
			<div class="_do_pdl4_schdl_card_all">

			
				<!-- CARD -->
        <?php if($settings['list'] ): ?> 
        <?php foreach($settings['list'] as $item ): ?>
					<!-- CARD -->
				<div class="_do_pdl4_schdl_card">
					<div class="_do_pdl4_schdl_card_inner">
          <img src="<?php echo $item['image']['url'] ?>" alt="image">
						<div class="_do_pdl4_schdl_card_top">
            <p  style="color : <?php echo $item['timeColor'] ?>
                 ;background: <?php echo  $item['timeBackground']?> ">
                 <?php echo  $item['time']?>
                </p>
						</div>
						<div class="_do_pdl4_schdl_card_btm">
            <?php if($item['addLogoImage']) : ?>
            <div class="_do_pdl4_schdl_card_logo">
            <img src="<?php echo $item['logoImage']['url'] ?>" alt="Image">
              </div>
              <?php endif; ?>
							<div class="_do_pdl4_schdl_card_btm_inner">
								<div class="_do_pdl4_schdl_card_btm_top"  style="background-color: <?php echo $item['sectionBackgroundTwo'] ?>;">
                <p  style="color : <?php echo $item['typeOfCardColor'] ?>">
                   <?php echo  $item['typeOfCard']?> </p>

									<h4  style="color : <?php echo $item['placeColor'] ?>">
                   <?php echo  $item['place']?> </h4>
									<div class="_do_pdl4_schdl_card_info">
										<div class="_do_pdl4_schdl_card_info_txt">
                    <p style="color : <?php echo $item['textOneColor'] ?>"><?php echo  $item['textOne']?> </p>
										</div>
										<div class="_do_pdl4_schdl_card_tym">
                    <p style="color : <?php echo $item['timeTwoColor'] ?>"><?php echo  $item['timeTwo']?> </p>
										</div>
									</div>                             
								</div>
								<div class="_do_pdl4_schdl_card_btm_txt" style="background-color: <?php echo $item['sectionBackgroundThree'] ?>;" >
								<p style="color : <?php echo $item['textTwoColor'] ?>">
								<?php echo  $item['textTwo']?> <span style="color : <?php echo $item['distancetextColor'] ?>">
								<?php echo  $item['distancetext']?></span></p>
								</div>
							</div>
						</div>   
					</div>
				</div>
				<!-- CARD -->

        <?php endforeach; ?>
            <?php endif; ?>
				<!-- CARD -->
			</div>
		</div>
	</div>
	<!-- DO PADEL PAGE-4 SECHUDLE END-->