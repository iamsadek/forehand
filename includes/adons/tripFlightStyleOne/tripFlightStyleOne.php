<?php
namespace Elementor;

class TripFlightStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-tripsflight-text-style-one';
	}
	
	public function get_title() {
		return 'TRIP FLIGHT STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
	
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
	

		        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Itinenary' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'headerOneBackground',
			[
				'label' => __( 'Title background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$this->add_control(
			'headerTwo', [
				'label' => __( 'Text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Day 1' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerTwoColor',
			[
				'label' => __( 'Text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
	
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'addLogoImage',
			[
				'label' => __( 'Add logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$repeater->add_control(
			'logoImage',
			[
				'label' => __( 'Choose logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
	
		$repeater->add_control(
			'time', [
				'label' => __( 'Time text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '08:00' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'timeColor',
			[
				'label' => __( 'Time text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);
			$repeater->add_control(
				'timeBackground',
				[
					'label' => __( ' Time text background color', 'elementor' ),
					'type' => Controls_Manager::COLOR,
					'default' => ''
				]
			);
        
		$repeater->add_control(
			'sectionBackgroundTwo',
			[
				'label' => __( 'Section two background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'typeOfCard', [
				'label' => __( 'Type of card', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Flight' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'typeOfCardColor',
			[
				'label' => __( 'Type of card text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);

			$repeater->add_control(
				'place', [
					'label' => __( 'Place', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Copenhagen - Mallorca' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'placeColor',
				[
					'label' => __( 'Place text Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
			
			
				$repeater->add_control(
					'textOne', [
						'label' => __( 'Text one', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( 'Lufthansa Airlines' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$repeater->add_control(
					'textOneColor',
					[
						'label' => __( 'Text One Color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
				);
				$repeater->add_control(
					'timeTwo', [
						'label' => __( 'Time two text', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( '08:00 - 10:00' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
			
				
		
				$repeater->add_control(
					'timeTwoColor',
					[
						'label' => __( 'Time two text color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
				);

				$repeater->add_control(
					'sectionBackgroundThree',
					[
						'label' => __( 'Section three background color', 'elementor' ),
						'type' => Controls_Manager::COLOR,
						'default' => ''
					]
				);
				$repeater->add_control(
					'textTwo', [
						'label' => __( 'Text two', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( 'Airport to Hotel distance ' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$repeater->add_control(
					'textTwoColor',
					[
						'label' => __( 'Text two Color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
				);
				$repeater->add_control(
					'distancetext', [
						'label' => __( 'Distance text ', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( '5 miles' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$repeater->add_control(
					'distancetextColor',
					[
						'label' => __( 'Distance text  Color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
				);
		
			
	
			
	

		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
				
					
				],
				
			]
		);

		$this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('tripFlightStyleOneHtml.php');
    }
   
	
	


	
	
}