<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsCalenderData = json_encode($calendarData);
echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";
// print_r($settings['eventsCat']);
// die('trying to see ');
	$color = $settings['isDarkMood'] ? 'black;' : 'white';
	$textColor = $settings['isDarkMood'] ? 'white !important;' : 'black !important';
	$border = $settings['isDarkMood'] ? '1px solid white' : '1px solid black';
?>
<style>
	._trmnt3_crd_top_lft{
		background : <?php echo $settings['dateBgColor'] ?>;
		border : 2px solid <?php echo $settings['dateBorderColor'] ?>;

	}
	._trmnt3_crd_top_p1{
		color: <?php echo $settings['dateTxtColor'] ?> !important;
	}
	._trmnt3_crd_top_h3{
		color: <?php echo $settings['dateTxtColor'] ?> !important;
	}
	._trmnt3_crd_top_p2{
		color: <?php echo $settings['dateTxtColor'] ?> !important;
	}
	._trmnt3_crd_btm_p{
		color : <?php echo $settings['priceColor'] ?>;
		background : <?php echo $settings['priceBgColor'] ?>;
	}
	._trmnt3_crd_btm_h3{
		color : <?php echo $settings['titleColor'] ?>;
	}
	._trmnt3_crd_loctn_p{
		color : <?php echo $settings['locColor'] ?>;
	}
	._trmnt3_crd_loctn i{
		color : <?php echo $settings['locColor'] ?>;
	}
	._clndr2_main_sec {
		background : <?php echo $color ?>
	}
	._clndr2_mnu_ul li p {
		color : <?php echo $textColor ?> ;
	}
	.headercolor {
		color : <?php echo $textColor ?> ;
	}
	._clndr2_mnu_active{
		border: <?php echo $border ?> ;
	}

</style>


<calendarslider >
    <div v-cloak>
    <div class="_clndr2_main_sec">
	    <div class="_clndr2_mnu">
			<div class="_do_pdl_nws_titl">
				<h3 class="headercolor">
				<?php echo  $settings['headerOne']?>
				</h3> 
			</div>
	    	<ul class="_clndr2_mnu_ul">

	    		<li :class="catIndex==i? '_clndr2_mnu_active' : '' " v-for="(eventCat, i) in headerStyleOneSetting.eventsCat" v-if="headerStyleOneSetting.eventsCat.length" @click="changeCat(eventCat, i)">
	    			<p>{{eventCat == 'All' ? 'Alla' : eventCat}}</p>
	    		</li>


			</ul>
			<!-- <div class="_right">
				<button class="next_btn" @click="next(false)" v-if="prev>0">Prev</button>
				<button class="next_btn" @click="next(true)">Next</button>
			</div> -->
	    </div>
	    <div class="_clndr2_dte_lst" ref="sl">
	    	<!-- ITEAM -->
	    <div class="_clndr2_dte_itm" v-for="(calendar, key) in eventDates" v-if="eventDates" >
				<p class="_clndr2_dte_itm_p1 _clndr2_dte_active" v-if="month==calendar[0].monthName"  :style="`color : ${headerStyleOneSetting.isDarkMood ? 'white' : 'black'} `">
					{{calendar[0].monthName}}
				</p>
				<p class="_clndr2_dte_itm_p1 _clndr2_dte_active" v-if="month!=calendar[0].monthName"  :style="`color : ${headerStyleOneSetting.isDarkMood ? 'gray' : 'gray'} `">
					{{calendar[0].monthName}}
				</p>
				<div class="_clndr2_dte_itm_flx">
				    <div v-for="(c,j) in calendar" v-if="calendar.length" class="days" @click="getData(c, `${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`)">

							<ul class="_clndr2_dte_itm_ul">
								<li>

									<p class="_clndr2_dte_itm_p2 _clndr2_dte_active" v-if="monthName==`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`"  :style="`color : ${headerStyleOneSetting.isDarkMood ? 'white' : 'black'} `">
										{{c.dayOfTheMonth}}
									</p>
									<p class="_clndr2_dte_itm_p2 _clndr2_dte_active" v-if="monthName!=`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`" :style="`color : ${headerStyleOneSetting.isDarkMood ? 'gray' : 'gray'} `">
										{{c.dayOfTheMonth}}
									</p>
									<p class="_clndr2_dte_itm_p3 _clndr2_dte_active" v-if="monthName==`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`"   :style="`color : ${headerStyleOneSetting.isDarkMood ? 'white' : 'black'} `">
										{{c.dayName}}
									</p>
									<p class="_clndr2_dte_itm_p3 _clndr2_dte_active" v-if="monthName!=`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`" :style="`color : ${headerStyleOneSetting.isDarkMood ? 'gray' : 'gray'} `">
										{{c.dayName}}
									</p>
								</li>


							</ul>
						</div>
					</div>
		    	</div>
	    	<!-- ITEAM -->



	    </div>


		<div class="_padel10_main_all">
			<div class="_padel10_main" :style="{ transform: 'translate3d('+translateValue+'%, 0px, 0px)'  }">
				<div class="_trmnt3_card_all" v-for="(c, key) in eventData" v-if="eventData.length">

					<div class="_trmnt3_crd">
						<img :src="imageSource+c.eventImg" alt="image">
						<div class="_trmnt3_crd_top">
						<div class="_trmnt3_crd_top_lft">
							<p class="_trmnt3_crd_top_p1">
							{{c.monthName.substring(0,3)}}
							</p>
							<h3 class="_trmnt3_crd_top_h3">
							{{c.dayOfTheMonth}}
							</h3>
							<p class="_trmnt3_crd_top_p2">
							{{c.eventTime}}
							</p>
						</div>
						<div class="_trmnt3_crd_top_icon">
							<span>
							<i class="fa fa-share"></i>
							</span>
						</div>
						</div>

						<div class="_trmnt3_crd_btm" @click="goNext(c)">
							<p class="_trmnt3_crd_btm_p">
							{{c.eventFee}} kr
							</p>
							<h3 class="_trmnt3_crd_btm_h3">
							{{c.eventName}}
							</h3>
							<div class="_trmnt3_crd_loctn">
								<span>
									<i class="fas fa-map-marker-alt"></i>
								</span>
									<p class="_trmnt3_crd_loctn_p">
									{{c.eventLocation}}
									</p>

							</div>
							<div class="_trmnt3_crd_loctn">
								<span>
									<i class="fa fa-user"></i>
								</span>
									<p class="_trmnt3_crd_loctn_p">
										{{c.__meta__.singedup_count}}/{{c.eventLimit}} platser kvar
									</p>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
   <!-- ARROW -->
		<div class="_padel10_arrow_lft" @click="goBack" v-if="eventData.length > 4">
			<i class="fa fa-arrow-left"></i>
		</div>
		<div class="_padel10_arrow_r8" @click="sliderRight" v-if="eventData.length > 4">
			<i class="fa fa-arrow-right"></i>
		</div>
		<!-- ARROW -->




  	</div>
    </div>
</calendarslider>



<script type="text/javascript">

    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)

	jsCalenderData = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    jsCalenderData = JSON.parse(jsCalenderData)

    // eventData  = JSON.parse(eventData)
    // console.log('data is', eventData)


    var list = document.getElementsByTagName("calendarslider");
    console.log(list)

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "calendarslider-app-" + i);
            var app = new Vue({
            el: "#calendarslider-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting,
                        eventDates: jsCalenderData.eventDates,
                        eventData: [],
                        items: [2,2,3,4,5],
                        isModal: false,
                        c : false,
                        imageSource: imageSource,
						index: 0,
						eventItems: [],
						keyName: '',
						prev: 0,
                        nextEl: 0,
                        isDone: false,
						apiUrl : apiUrl,
						catIndex: 0,
						dateIndex : 0,
						monthName : '',
						month : '',
						s: 0,
						translateValue: 0, 
                      	clickNumbers: 1, 

                    }
                },
                methods: {
					
                    async getData(event, dateString){
						this.monthName = dateString
						this.month = dateString.split("-")[0]
						const res = await axios.get(`${apiUrl}/getEventByDate?date=${event.eventDate}`)
						if(res.status==200){
							this.translateValue = 0
							let repeatedImages =[]
							if(res.data.length > 4){
								repeatedImages = new Array(3).fill(res.data).flat();
							}else{
								repeatedImages = res.data
							}
							console.log('images', repeatedImages)
                            this.eventData = repeatedImages
						}else{
							alert('Something went wrong while loading the data, please try again or contact us!')
						}
					},
                    async changeCat(cat, i){
						this.catIndex = i
						const res = await axios.get(`${apiUrl}/calendarThree?cat=${cat}`)
						if(res.status==200){
							this.translateValue = 0
							this.eventDates = res.data.eventDates
							let repeatedImages =[]
							if(res.data.eventData.length > 4){
								repeatedImages = new Array(3).fill(res.data.eventData).flat();
							}else{
								repeatedImages = res.data.eventData
							}
							console.log(repeatedImages)
							console.log(res.data.eventData)
                            this.eventData = repeatedImages
                            
							if(this.eventData.length){
								let c = this.eventData[0]
								this.monthName = `${c.monthName}-${c.dayOfTheMonth}-${c.dayName}`
								//this.month = c.monthName
							}
						}else{
							alert('Something went wrong while loading the data, please try again or contact us!')
						}
					},
					goNext(c){
						window.location = c.landingPage
					},
					sliderRight(){ // increase 
						if(this.clickNumbers == this.eventData.length - 4){
							this.translateValue = 0
							this.clickNumbers = 1
							return

						} 
						
						this.translateValue = -this.clickNumbers*25
						this.clickNumbers++
						
					},
					goBack(){ // going back
						if(this.clickNumbers == 1) return 
						let newNumber = this.translateValue + 25
					
						this.translateValue = newNumber
						console.log('decreament',this.translateValue)
						this.clickNumbers--
					},

                },
                created(){
					
					let repeatedImages =[]
					if(jsCalenderData.eventData.length > 4){
						repeatedImages = new Array(3).fill(jsCalenderData.eventData).flat();
					}else{
						repeatedImages = jsCalenderData.eventData
					}
					console.log(repeatedImages)
					this.eventData = repeatedImages

					if(this.eventData.length){
						let c = this.eventData[0]
						this.monthName = `${c.monthName}-${c.dayOfTheMonth}-${c.dayName}`
						//this.month = c.monthName
					}
					
                },


            })

        }
 </script>

