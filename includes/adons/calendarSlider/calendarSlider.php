<?php
namespace Elementor;


class CalendarSlider extends Widget_Base {
	
	public function get_name() {
		return 'forehand-calendar-slider';
	}
	
	public function get_title() {
		return 'CALENDAR SLIDER';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		// call the server for data... 
		$eventCats = $this->getEventCat();
		

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Events'
			]
		);

		$options = []; 
		$default = ['All']; 
		$options['All'] = __( 'All', 'plugin-domain');
		
		foreach ($eventCats as $cat) { 
			$options[$cat->category] = __( $cat->category, 'plugin-domain');
			array_push($default, $cat->category);
		}
		
		
		
		// multiple selects 
		$this->add_control(
			'eventsCat',
			[
				'label' => __( 'Show Elements', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $options,
				'default' => $default,
			]
		);





		// news title
		$this->add_control(
			'isDarkMood',
			[
				'label' => __( 'Dark Mood', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::SWITCHER,
				'placeholder' => __( 'Dark Mood', 'elementor' ),
				'default' => false
			]
		);

		
		// $this->add_control(
		// 	'selectedTxtColor',
		// 	[
		// 		'label' => __( 'Selected text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'black'
		// 	]
		// );
		// $this->add_control(
		// 	'unselectedTxtColor',
		// 	[
		// 		'label' => __( 'Unselected text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'gray'
		// 	]
		// );
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Kalender' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'dateBgColor',
			[
				'label' => __( 'Date background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'transparent'
			]
		);
		$this->add_control(
			'dateBorderColor',
			[
				'label' => __( 'Date border color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'dateTxtColor',
			[
				'label' => __( 'Date text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'titleColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'locColor',
			[
				'label' => __( 'Location text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'priceBgColor',
			[
				'label' => __( 'Price background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'priceColor',
			[
				'label' => __( 'Price text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $calendarData = $this->getCalendarData($settings['eventsCat']);
        require('calendarSliderHtml.php');
    }
    public function getCalendarData($eventsCat){
		$cat = 'All'; 
		if(count($eventsCat)>0){
			$cat = $eventsCat[0]; 
		}
		
        $response = wp_remote_request( FOREHAND_API_URL."/calendarThree?cat=$cat",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
	}
	
	public function getEventCat(){
		$response = wp_remote_request( FOREHAND_API_URL.'/eventCategories',
			array(
				'method'     => 'GET', 
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
			)
		);
		$body = wp_remote_retrieve_body($response);
		//return $body;
		return json_decode($body);
	}
	

	// js render
	
	


	
	
}