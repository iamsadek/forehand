<?php
namespace Elementor;

require_once BASEPLUGIN_INCLUDES . '/common/helperclass.php';
class DivisionRankMatch extends Widget_Base
{
	use Helper;
    public function get_name()
    {
        return 'forehand-division-rank-match';
    }

    public function get_title()
    {
        return 'DIVISION RANK MATCH 1';
    }

    public function get_icon()
    {
        return 'fas fa-heading';
    }

    public function get_categories()
    {
        return ['Forehand'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Settings', 'elementor'),
            ]
        );
        $tournamentLists = $this->callApi('getTournamentsListNamesByType?type=Divisions');
        $optinsAndDefault = $this->getOptionsAndDefault($tournamentLists);
        $divsArray = [
            ['id' => 1, 'name' => "Division 1"],
            ['id' => 2, 'name' => "Division 2"],
            ['id' => 3, 'name' => "Division 3"],
            ['id' => 4, 'name' => "Division 4"],
            ['id' => 5, 'name' => "Division 5"],
            ['id' => 6, 'name' => "Division 6"],
            ['id' => 7, 'name' => "Division 7"],
            ['id' => 8, 'name' => "Division 8"],
            ['id' => 9, 'name' => "Division 9"],
            ['id' => 10, 'name' => "Division 10"],
            ['id' => 11, 'name' => "Division 11"],
            ['id' => 12, 'name' => "Division 12"],
            ['id' => 13, 'name' => "Division 13"],
            ['id' => 14, 'name' => "Division 14"],
            ['id' => 15, 'name' => "Division 15"],
            ['id' => 16, 'name' => "Division 16"],
            ['id' => 17, 'name' => "Division 17"],
            ['id' => 18, 'name' => "Division 18"],
            ['id' => 19, 'name' => "Division 19"],
            ['id' => 20, 'name' => "Division 20"],
        ]; 
       
       
        $this->add_control(
            'trId',
            [
                'label' => __( 'Select the tournament', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => $optinsAndDefault[1],
                'options' => $optinsAndDefault[0],
            ]
        );
       

        $divOptions = []; 
        $divDefault = count($divsArray) > 0 ? $default = $divsArray[0]['id'] : null;
        foreach ($divsArray as $l) { 
			$divOptions[$l['id']] = __( $l['name'], 'plugin-domain');
        }
        $this->add_control(
            'divIndex',
            [
                'label' => __( 'Select the division', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => $divDefault,
                'options' => $divOptions,
            ]
        );
       

       
       
       $this->end_controls_section();
       
    }
   

    // php render.
    protected function render()
    {
		$settings = $this->get_settings_for_display();
		$id = $settings['trId'];
		$divIndex = $settings['divIndex'];
        $data = $this->getData($id, $divIndex);
		require 'divisionRankMatchHtml.php';
    }
    public function getData($id, $divIndex){
		
        $response = wp_remote_request( FOREHAND_API_URL."/divisionRankAndMatch/$id/$divIndex",
			array(
				'method'     => 'GET', 
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
			)
        );
		$body = wp_remote_retrieve_body($response);
		return json_decode($body);
    }

}
