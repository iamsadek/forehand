<style>
    ._nws2_bnr_crd{
        background: <?php echo $settings['featuredNewsBoxBg'] ?>;
        opacity: <?php echo $settings['featuredNewsBoxOp'] ?>;
    }
    ._nws2_bnr_dte{
        background: <?php echo $settings['featuredNewsBoxDateBg'] ?>;
        color: <?php echo $settings['featuredNewsBoxDateTxtColor'] ?>;
    }
    ._nws2_crd_clr1{
        background: <?php echo $settings['newsDateBg'] ?> !important;
       
    }
    ._nws2_crd_top_p{
        color: <?php echo $settings['newsDateTxtColor'] ?> !important;
       
    }
</style>

<newsstyletwo>
    <?php if(count($news)>0): ?>
        <div class="_nws2_banner_sec" style="background-image: url(<?php echo FOREHAND_IMAGE_SOURCE.'/'.$news[0]->image; ?>);">
            <div class="_nws2_bnr_crd">
                <p class="_nws2_bnr_dte">
                    <?php echo $news[0]->newsDate; ?>
                </p>
                <h3 class="_nws2_bnr_crd_h3"><?php echo $news[0]->title; ?></h3>
                <p class="_nws2_bnr_crd_txt"><?php echo $news[0]->exerpt; ?></p>
                <div class="_nws2_bnr_crd_btm">
                    <div class="_nws2_bnr_crd_btm_lft">
                        <img src="<?php echo FOREHAND_IMAGE_SOURCE.''.$news[0]->author->profilePic; ?>" alt="image">
                    </div>
                    <div class="_nws2_bnr_crd_btm_r8">
                        <p class="_nws2_bnr_crd_btm_r8_p"><?php echo $news[0]->author->firstName; ?> <?php echo $news[0]->author->lastName; ?></p>
                    </div>
                </div>
                <?php 
                    $tags = json_decode($news[0]->tags);
                ?>
                <ul class="_nws2_bnr_crd_btm_r8_ul">
                    <?php foreach ($tags as $key => $t) :  ?>
                        <li ><?php echo $t->tag; ?></li>
                    <?php endforeach; ?>
                </ul>
                <a href="/forehand-news/<?php echo $news[0]->id; ?>">Läs nyheten</a>
            </div>
        </div>
    <?php endif; ?>
    <div class="_nws2_crd_sec">
		<div class="_nws2_crd_all_main">
			<!-- CARD -->
            <?php foreach($news as $key=>$n): ?>
                <?php if($key==0) continue; ?>
                <div class="_nws2_crd_main">
                    <a href="/forehand-news/<?php echo $n->id; ?>" class="hre">
                        <div class="_nws2_crd">
                            <img src="<?php echo FOREHAND_IMAGE_SOURCE.'/'.$n->image; ?>" alt="image">
                            <div class="_nws2_crd_top">
                                <p class="_nws2_crd_top_p _nws2_crd_clr1">
                                    <?php echo $n->newsDate; ?>
                                </p>
                            </div>
                            <div class="_nws2_crd_btm">
                                <?php 
                                    $tags = json_decode($n->tags);
                                ?>
                                <ul class="_nws2_crd_btm_ul">
                                <?php foreach ($tags as $key => $t) :  ?>
                                    <li ><?php echo $t->tag; ?></li>
                                <?php endforeach; ?>
                                </ul>
                                <h4 class="_nws2_crd_btm_h4"><?php echo $n->title; ?></h4>
                                <p class="_nws2_crd_btm_p"><?php echo $n->exerpt; ?></p>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- CARD -->
            <?php endforeach; ?>
			
		</div>


	</div>
</newsstyletwo>
