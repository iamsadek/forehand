<?php
namespace Elementor;

class NewsStyletwo extends Widget_Base {
	public function get_name() {
		return 'forehand-news-two';
	}
	
	public function get_title() {
		return 'NEWS STYLE 2';
	}
	
	public function get_icon() {
		return 'fa fa-newspaper-o';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Latest news'
			]
		);
		
		$this->add_control(
			'featuredNewsBoxBg',
			[
				'label' => __( 'Featured news box background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'featuredNewsBoxOp',
			[
				'label' => __( 'Featured news box opacity', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '0.8'
			]
		);
		$this->add_control(
			'featuredNewsBoxDateBg',
			[
				'label' => __( 'Featured news date background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'yellow'
			]
		);
		$this->add_control(
			'featuredNewsBoxDateTxtColor',
			[
				'label' => __( 'Featured news date text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#444444;'
			]
		);
		$this->add_control(
			'newsDateBg',
			[
				'label' => __( 'News date background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#53ffeb;'
			]
		);
		$this->add_control(
			'newsDateTxtColor',
			[
				'label' => __( 'News date text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000;'
			]
		);


		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $news = $this->getNewsData();
       
        require('newsStyleTwoHtml.php');
    }
    public function getNewsData(){
        $response = wp_remote_request( FOREHAND_API_URL . "/clubNews?limit=30",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
       
        //var_dump($response);
        //echo  $response;
		 $body = wp_remote_retrieve_body($response);
		 //echo json_encode($body);
         return json_decode($body);
    }

	// js render
	
	


	
	
}