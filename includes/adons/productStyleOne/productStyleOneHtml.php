<div class="_shop2_sec">
          <!-- LEFT -->
          <div class="_shop2_lft">
            <img src="<?php echo $settings['image']['url'] ?>" alt="image">
          </div>
          <!-- LEFT -->

          <!-- RIGHT -->
          <div class="_shop2_r8">
            <p class="_shop2_r8_p1">
				<?php echo $settings['heading1'] ?>
            </p>
            <h3 class="_shop2_r8_h3">
			<?php echo $settings['heading2'] ?>
            </h3>
            <p class="_shop2_r8_p2">
			<?php echo $settings['heading3'] ?>
            </p>
            <p class="_shop2_r8_p3">
			<?php echo $settings['text'] ?>
            </p>
			
			<?php if($settings['addButton']=='yes') : ?>
				<h3 class="_shop2_r8_price" style="background:<?php echo $settings['buttonBg'] ?>;color:<?php echo $settings['buttonTxtColor'] ?>">
					<?php echo $settings['buttonTxt'] ?>
				</h3>
			<?php endif;?>
			
          </div>
          <!-- RIGHT -->
      </div>