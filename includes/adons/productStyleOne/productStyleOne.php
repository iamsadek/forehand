<?php
namespace Elementor;

class ProductStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-product-style-one';
	}
	
	public function get_title() {
		return 'PRODUCT STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-shopping-cart';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		// $this->add_control(
		// 	'imageDirection',
		// 	[
		// 		'label' => __( 'Image placement', 'plugin-domain' ),
		// 		'type' => \Elementor\Controls_Manager::SELECT,
		// 		'default' => 'left',
		// 		'options' => [
		// 			'left'  => __( 'Left', 'plugin-domain' ),
		// 			'right' => __( 'Right', 'plugin-domain' ),
					
		// 		],
		// 	]
		// );
		


		$this->add_control(
			'image', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/logo.png',
				],
			]
		);
		$this->add_control(
			'heading1',
			[
				'label' => __( 'Heading 1', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your headline', 'elementor' ),
				'default' => "BULLPADEL NOX"
			]
		);
		$this->add_control(
			'heading1Color',
			[
				'label' => __( 'Heading1 text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#444444'
			]
		);

		$this->add_control(
			'heading2',
			[
				'label' => __( 'Heading 2', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your headline', 'elementor' ),
				'default' => "Padel Nyhet"
			]
		);
		$this->add_control(
			'heading2Color',
			[
				'label' => __( 'Heading2 text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#444444'
			]
		);
		$this->add_control(
			'heading3',
			[
				'label' => __( 'Heading 3', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your headline', 'elementor' ),
				'default' => "BullPadel Nox"
			]
		);
		$this->add_control(
			'heading3Color',
			[
				'label' => __( 'Heading3 text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'c8e831'
			]
		);


		$this->add_control(
			'text',
			[
				'label' => __( 'Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Text', 'elementor' ),
				'default' => "Nike Lima är en sko speciellt framtagen för padelspelare och har kommit att bli vår mest populära padelsko. Denna modell används av den meriterade padelspelaren Nike Lima från Brasilien. Nu släpps den i denna stilrena design som vi på Racketspecialisten tycker att Nike lyckats väldigt bra med!"
			]
		);

		

		
		$this->add_control(
			'addButton',
			[
				'label' => __( 'Add a button', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);
		$this->add_control(
			'buttonTxt',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '1295 kr'
			]
		);
		// $this->add_control(
		// 	'buttonLink',
		// 	[
		// 		'label' => __( 'Button link', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => ''
		// 	]
		// );
		// $this->add_control(
		// 	'buttonRds',
		// 	[
		// 		'label' => __( 'Button border radius', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => '30px'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonBorderColor',
		// 	[
		// 		'label' => __( 'Button border color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'darkgray'
		// 	]
		// );



		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#d8f54e'
			]
		);
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);





		// $this->add_control(
		// 	'marginTop',
		// 	[
		// 		'label' => __( 'Margin top', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Margin top', 'elementor' ),
		// 		'default' => '120px'
		// 	]
		// );
		// $this->add_control(
		// 	'marginLeft',
		// 	[
		// 		'label' => __( 'Margin left', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Margin left', 'elementor' ),
		// 		'default' => '45px'
		// 	]
		// );


		// $this->add_control(
		// 	'padingBottom',
		// 	[
		// 		'label' => __( 'Padding bottom', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Padding bottom', 'elementor' ),
		// 		'default' => '34px'
		// 	]
		// );
		// $this->add_control(
		// 	'padingLeft',
		// 	[
		// 		'label' => __( 'Padding left', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Padding left', 'elementor' ),
		// 		'default' => '20px'
		// 	]
		// );
		// $this->add_control(
		// 	'textAlign',
		// 	[
		// 		'label' => __( 'Text alignment', 'plugin-domain' ),
		// 		'type' => \Elementor\Controls_Manager::SELECT,
		// 		'default' => 'center',
		// 		'options' => [
		// 			'left'  => __( 'Left', 'plugin-domain' ),
		// 			'center' => __( 'Center', 'plugin-domain' ),
		// 			'right' => __( 'Right', 'plugin-domain' ),
					
		// 		],
		// 	]
		// );


		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		// text color 
		$this->add_control(
			'headingColor',
			[
				'label' => __( 'Heading color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'listColor',
			[
				'label' => __( 'List item color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		// $this->add_control(
		// 	'addButton',
		// 	[
		// 		'label' => __( 'Add a button', 'elementor' ),
		// 		'type' => Controls_Manager::SWITCHER,
		// 		'default' => 'yes'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonTxt',
		// 	[
		// 		'label' => __( 'Button text', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => 'Contact us'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonLink',
		// 	[
		// 		'label' => __( 'Button link', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => ''
		// 	]
		// );
		// $this->add_control(
		// 	'buttonRds',
		// 	[
		// 		'label' => __( 'Button border radius', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => '30px'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonBorderColor',
		// 	[
		// 		'label' => __( 'Button border color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'darkgray'
		// 	]
		// );



		// $this->add_control(
		// 	'buttonBg',
		// 	[
		// 		'label' => __( 'Background color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'transparent'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonTxtColor',
		// 	[
		// 		'label' => __( 'Button text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'black'
		// 	]
		// );



		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('productStyleOneHtml.php');
    }
   
	
	


	
	
}