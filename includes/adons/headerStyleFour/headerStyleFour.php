<?php
namespace Elementor;

class HeaderStyleFour extends Widget_Base {
	
	public function get_name() {
		return 'forehand-header-style-four';
	}
	
	public function get_title() {
		return 'HEADER STYLE 4';
	}
	
	public function get_icon() {
		return 'fas fa-heading';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Box background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ecececf2'
			]
		);

		// MONTH
		$this->add_control(
			'monthBg',
			[
				'label' => __( 'Month background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		$this->add_control(
			'monthTxtColor',
			[
				'label' => __( 'Month background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'monthTxt',
			[
				'label' => __( 'Month', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your month', 'elementor' ),
				'default' => '1 juni'
			]
		);
	
	
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Inbjudan till en turnering i världsklass'
			]
		);
		$this->add_control(
			'description',
			[
				'label' => __( 'Description text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Enter description', 'elementor' ),
				'default' => 'The resistive exercises & concepts discussed in this article are key to developing a successful tennis-specific strength & conditioning program'
			]
		);
		
	
		//registration text
		$this->add_control(
			'regTxt',
			[
				'label' => __( 'Registration text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Till anmälan'
			]
		);
		$this->add_control(
			'regLink',
			[
				'label' => __( 'Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'regBgColor',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'regTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'allTxtColor',
			[
				'label' => __( 'All text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		// image 
		$this->add_control(
			'banner',
			[
				'label' => __( 'Choose Banner', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),

				],
			]
		);
		// image 
		$this->add_control(
			'trainerImg',
			[
				'label' => __( 'Choose trainer profile picture', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$this->add_control(
			'trainerName',
			[
				'label' => __( 'Trainer name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Michael Dunmatch'
			]
		);
		$this->add_control(
			'proTxt',
			[
				'label' => __( 'Pro text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'PRO'
			]
		);

		$this->add_control(
			'proTxtColor',
			[
				'label' => __( 'Pro text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'proTxtBg',
			[
				'label' => __( 'Pro text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ce5353'
			]
		);
		$this->add_control(
			'expert',
			[
				'label' => __( 'expert level', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Tävlingsansvarig'
			]
		);
		$this->add_control(
			'AttendeeLimitTxt',
			[
				'label' => __( 'Attendee limit text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Max antal deltagare'
			]
		);
		$this->add_control(
			'AttendeeLimitNumber',
			[
				'label' => __( 'Attendee limit', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => '40 st'
			]
		);
		$this->add_control(
			'attendeeLimitUrl',
			[
				'label' => __('Attendee limit url', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::URL,
				'default' => __('A nice course', 'plugin-domain'),
				'label_block' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);


		$this->add_control(
			'prizeMoneyTxt',
			[
				'label' => __( 'Prize money text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Prispengar'
			]
		);
		$this->add_control(
			'prizeMoney',
			[
				'label' => __( 'Attendee limit', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => '40 000 kr'
			]
		);
		$this->add_control(
			'prizeMoneyUrl',
			[
				'label' => __('Prize money url', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::URL,
				'default' => __('A nice course', 'plugin-domain'),
				'label_block' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'spaceTxt',
			[
				'label' => __( 'Space left text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Anmälan öppen tom'
			]
		);
		$this->add_control(
			'spaceTxtNumber',
			[
				'label' => __( 'Attendee limit', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => '27 april'
			]
		);

		$this->add_control(
			'spaceTxtNumberUrl',
			[
				'label' => __('space text url', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::URL,
				'default' => __('A nice course', 'plugin-domain'),
				'label_block' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		
		
		


		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('headerStyleFourHtml.php');
    }
   
	
	


	
	
}