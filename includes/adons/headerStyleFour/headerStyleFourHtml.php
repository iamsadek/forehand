<style>
	a{
		color: <?php echo $settings['allTxtColor'] ?>
	}
</style>
<div class="_event1_banner_sec" style="background: url(<?php echo $settings['banner']['url'] ?>) no-repeat center center;">
			<div class="_event1_bnr_card_main">
				<div class="_event1_bnr_top" style="color: <?php echo $settings['monthTxtColor'] ?>;background: <?php echo $settings['monthBg'] ?>">
					<p class="_event1_bnr_top_p" ><?php echo $settings['monthTxt'] ?></p>
					
				</div>
				<div class="_event1_bnr_card" style="background: <?php echo $settings['boxBgColor'] ?>">
					<div class="_event1_bnr_titl"> 
						<h3 class="_event1_bnr_titl_h3" style="color: <?php echo $settings['allTxtColor'] ?>"><?php echo $settings['title'] ?></h3>
						<p class="_event1_bnr_titl_p">
							<?php echo $settings['description'] ?>
						</p>
					</div>
					<div class="_event1_bnr_img_all">
						<div class="_event1_crd_btm_img_top">
							<div class="_event1_crd_btm_img">
								<div class="_bnr_atnd">
									<img src="<?php echo $settings['trainerImg']['url'] ?>" alt="image">
								</div>
							</div>
							<div class="_event1_crd_btm_txt">
								<p class="_event1_crd_btm_txt_p" style="color: <?php echo $settings['allTxtColor'] ?>" >
								  <?php echo $settings['trainerName'] ?> <span style="background: <?php echo $settings['proTxtBg'] ?>; color: <?php echo $settings['proTxtColor']?>"><?php echo $settings['proTxt'] ?></span>
								</p>
							</div>
							<div class="_event1_crd_btm_txt_r8">
								<p class="_event1_crd_btm_txt_r8_p" >
									<span style="color: <?php echo $settings['allTxtColor'] ?>"><?php echo $settings['expert'] ?></span>
								</p>
							</div>
						</div>
					</div>
					<div class="_event1_bnr_btm">
						<ul class="_event1_bnr_btm_ul">
							<li style="color: <?php echo $settings['allTxtColor'] ?>"><a href="<?php echo $settings['attendeeLimitUrl']['url'] ?>"><?php echo $settings['AttendeeLimitTxt'] ?> <span><?php echo $settings['AttendeeLimitNumber'] ?> </span></a></li>
							<li style="color: <?php echo $settings['allTxtColor'] ?>"><a href="<?php echo $settings['prizeMoneyUrl']['url'] ?>"><?php echo $settings['prizeMoneyTxt'] ?> <span><?php echo $settings['prizeMoney'] ?></span></a></li>
							<li style="color: <?php echo $settings['allTxtColor'] ?>"> <a href="<?php echo $settings['spaceTxtNumberUrl']['url'] ?>"><?php echo $settings['spaceTxt'] ?> <span><?php echo $settings['spaceTxtNumber'] ?></span></a></li>
						</ul>
					</div>
				</div>
				<?php if($settings['regTxt'] !='') : ?>
					<div class="_event1_bnr_read">
					<a href="<?php echo $settings['regLink']['url'] ?>" style="background: <?php echo $settings['regBgColor'] ?>; color: <?php echo $settings['regTxtColor']?>">
						<?php echo $settings['regTxt'] ?>
					</a>
				</div>
				<?php endif;?>
				
			</div>
		</div>