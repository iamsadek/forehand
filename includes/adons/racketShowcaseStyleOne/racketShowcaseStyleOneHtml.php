<!-- DO PADEL PAGE-4 FACILITES -->
<?php 
	$jsSettings = json_encode($settings); 
	echo "<script> var jsSettings =  $jsSettings  </script>";

?>

<racketshowcasestyleone> 
<div v-cloak>	
	<!-- DO PADEL PAGE-1 RACKET -->
	<div class="_do_pdl_all_racket_sec" style="background-color: <?php echo $settings ['sectionBackground'] ?>;">
		<div class="_do_pdl_all_rckt_lft">
			<div class="_do_pdl_all_rckt_top">
				<ul>
				   
					<li>
					    <a href="<?php echo $settings['buttonLink'] ?>" style="color : <?php echo $settings['buttonTextColor'] ?> ;background: <?php echo $settings['buttonBackground'] ?>">
                         <?php echo  $settings['buttonText']?></a>
					</li>

					<li>
					     <a href="<?php echo $settings['buttonLinkTwo'] ?>" style="color : <?php echo $settings['buttonTextColorTwo'] ?> ;background: <?php echo $settings['buttonBackgroundTwo'] ?>">
                         <?php echo  $settings['buttonTextTwo']?></a>
					</li>
						<li>
						<a href="<?php echo $settings['buttonLinkThree'] ?>" style="color : <?php echo $settings['buttonTextColorThree'] ?> ;background: <?php echo $settings['buttonBackgroundThree'] ?>">
                         <?php echo  $settings['buttonTextThree']?></a>
					</li>
				</ul>
			</div>
			<div class="_do_pdl_all_rckt_btm">
			    <p style="color: <?php echo $settings['textColor'] ?>">
                  <?php echo $settings['text']?></p>
				  <h3 style="color: <?php echo $settings['headingColor'] ?>">
				  <?php echo $settings['heading']?></h3>
				   
				  <?php if($settings['textTwo'] ): ?>
                  
				  <div class="_do_pdl_all_rckt_btm_txt">
				  <p style="color: <?php echo $settings['textColorTwo'] ?>">
                  <?php echo $settings['textTwo']?></p>
                 </div>
				 <?php endif; ?>
				 

				  <a href="<?php echo $settings['buttonLinkFour'] ?>" style="color : <?php echo $settings['buttonTextColorFour'] ?> ;background: <?php echo $settings['buttonBackgroundFour'] ?>">
                    <?php echo  $settings['buttonTextFour']?></a>
			</div>
		  </div>
		  <div class="_do_pdl_all_rckt_r8">
			<!-- ITEAM COLUMN -->
		  <div class="_do_pdl_all_rckt_clm" >
			<div v-if="settings.imageSelection.length" v-for="(colOne, i)  in settings.imageSelection" >
				<!-- IMAGE CARD -->
				<a :href="colOne.link.url">
					<div class="_do_pdl_all_rckt_img_sngl _all_rckt_clr_2" :style="{background:colOne.imageCardBg} ">
						<div class="_do_pdl_all_rckt_logo">
						<img :src="colOne.imageLogo.url" alt="image">
						</div>
						<div class="_do_pdl_all_rckt_img">
						<img :src="colOne.imageCourts.url" alt="image">
						</div>
					</div>
				</a>
			</div>
		</div>
		  <!-- ITEAM COLUMN -->
		  <div class="_do_pdl_all_rckt_clm" >
			<div v-if="settings.imageSelection2.length" v-for="(colTwo, i)  in settings.imageSelection2" >
				<!-- IMAGE CARD -->
				<a :href="colTwo.link.url">
					<div class="_do_pdl_all_rckt_img_sngl _all_rckt_clr_2" :style="{background:colTwo.imageCardBg2} ">
						<div class="_do_pdl_all_rckt_logo">
						<img :src="colTwo.imageLogo2.url" alt="image">
						</div>
						<div class="_do_pdl_all_rckt_img">
						<img :src="colTwo.imageCourts2.url" alt="image">
						</div>
					</div>
				</a>
			</div>
		  </div>
		  <div class="_do_pdl_all_rckt_clm" >
			<div v-if="settings.imageSelection3.length" v-for="(colThree, i)  in settings.imageSelection3" >
				<!-- IMAGE CARD -->
				<a :href="colThree.link.url">
					<div class="_do_pdl_all_rckt_img_sngl _all_rckt_clr_2" :style="{background:colThree.imageCardBg3} ">
						<div class="_do_pdl_all_rckt_logo">
						<img :src="colThree.imageLogo3.url" alt="image">
						</div>
						<div class="_do_pdl_all_rckt_img">
						<img :src="colThree.imageCourts3.url" alt="image">
						</div>
					</div>
				</a>
			</div>
		  </div>
		  <div class="_do_pdl_all_rckt_clm" >
			<div v-if="settings.imageSelection4.length" v-for="(colFour, i)  in settings.imageSelection4" >
				<!-- IMAGE CARD -->
				<a :href="colFour.link.url">
					<div class="_do_pdl_all_rckt_img_sngl _all_rckt_clr_2" :style="{background:colFour.imageCardBg4} ">
						<div class="_do_pdl_all_rckt_logo">
						<img :src="colFour.imageLogo4.url" alt="image">
						</div>
						<div class="_do_pdl_all_rckt_img">
						<img :src="colFour.imageCourts4.url" alt="image">
						</div>
					</div>
				</a>
			</div>
		  </div>
		 
	    </div>
	 </div>  
	<!--  DO PADEL PAGE-1 RACKET END-->
</div>
	</racketshowcasestyleone>  
	<script type="text/javascript">



	

	var imageSelection = document.getElementsByTagName("racketshowcasestyleone");
	
    

        for (var i = 0; i < imageSelection.length; i++) {
            imageSelection[i].setAttribute("id", "racketshowcasestyleone-app-" + i);
            var app = new Vue({
            el: "#racketshowcasestyleone-app-" + i,
                data(){
                    return {
						settings: jsSettings,
						bigPic : '' ,
						
                    }
					
				},
              
            })
        }
		
 </script>                            


