<?php
namespace Elementor;

class racketShowcaseStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-showcaseracket-text-style-one';
	}
	
	public function get_title() {
		return 'RACKET SHOWCASE STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
			
		$this->add_control(
			'heading', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Awsome Rackets' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		
		$this->add_control(
			'text',
			[
				'label' => __( 'Text one', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'default' => "We are showcasing some"
			]
		);
		$this->add_control(
			'textColor',
			[
				'label' => __( 'Text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);$this->add_control(
			'textTwo',
			[
				'label' => __( 'Text two', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'default' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacinia quam sit amet dolor tristique, sed vulputate risus ultrices"
			]
		);
		$this->add_control(
			'textColorTwo',
			[
				'label' => __( 'Text two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$this->add_control(
			'buttonText', [
				'label' => __( 'Button one text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Carbon Fiber' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button one link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '#'
			]
		);
		$this->add_control(
			'buttonTextColor',
			[
				'label' => __( 'Button one text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonBackground',
			[
				'label' => __( 'Button one background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
				
		$this->add_control(
			'buttonTextTwo', [
				'label' => __( 'Button two text ', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Nylon Material' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'buttonLinkTwo',
			[
				'label' => __( 'Button two link ', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '#'
			]
		);
		$this->add_control(
			'buttonTextColorTwo',
			[
				'label' => __( 'Button two text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonBackgroundTwo',
			[
				'label' => __( 'Button two background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);		
		$this->add_control(
			'buttonTextThree', [
				'label' => __( 'Button three text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Something Else' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'buttonLinkThree',
			[
				'label' => __( 'Button three link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '#'
			]
		);
		$this->add_control(
			'buttonTextColorThree',
			[
				'label' => __( 'Button three text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonBackgroundThree',
			[
				'label' => __( 'Button three background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);		
		$this->add_control(
			'buttonTextFour', [
				'label' => __( 'Button four text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'VIEW ALL RACKETS' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'buttonLinkFour',
			[
				'label' => __( 'Button four link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '#'
			]
		);
		$this->add_control(
			'buttonTextColorFour',
			[
				'label' => __( 'Button four text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonBackgroundFour',
			[
				'label' => __( 'Button four background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
      
		
	

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'imageCourts',
			[
				'label' => __( 'Choose  image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'imageLogo',
			[
				'label' => __( 'Choose logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'link',
				[
					'label' => __( 'Link', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::URL,
					'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
					'show_external' => true,
					'default' => [
						'url' => '',
						'is_external' => true,
						'nofollow' => true,
					],
				]
		);
      	
		$repeater->add_control(
			'imageCardBg',
			[
				'label' => __( 'Image card background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'imageSelection',
			[
				'label' => __( 'First type of image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [],
			]
		);
		
		$repeater2 = new \Elementor\Repeater();
		$repeater2->add_control(
			'imageCourts2',
			[
				'label' => __( 'Choose  image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater2->add_control(
			'imageLogo2',
			[
				'label' => __( 'Choose logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$repeater2->add_control(
			'link',
				[
					'label' => __( 'Link', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::URL,
					'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
					'show_external' => true,
					'default' => [
						'url' => '',
						'is_external' => true,
						'nofollow' => true,
					],
				]
		);

		$repeater2->add_control(
			'imageCardBg2',
			[
				'label' => __( 'Image card background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'imageSelection2',
			[
				'label' => __( 'Second type of image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],

			]
		);



		$repeater3 = new \Elementor\Repeater();


		$repeater3->add_control(
			'imageCourts3',
			[
				'label' => __( 'Choose  image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater3->add_control(
			'imageLogo3',
			[
				'label' => __( 'Choose logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater3->add_control(
			'link',
				[
					'label' => __( 'Link', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::URL,
					'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
					'show_external' => true,
					'default' => [
						'url' => '',
						'is_external' => true,
						'nofollow' => true,
					],
				]
		);
		$repeater3->add_control(
			'imageCardBg3',
			[
				'label' => __( 'Image card background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'imageSelection3',
			[
				'label' => __( 'Third type of image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],

			]
		);
		$repeater4 = new \Elementor\Repeater();
		$repeater4->add_control(
			'imageCourts4',
			[
				'label' => __( 'Choose  image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater4->add_control(
			'imageLogo4',
			[
				'label' => __( 'Choose logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater4->add_control(
			'link',
				[
					'label' => __( 'Link', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::URL,
					'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
					'show_external' => true,
					'default' => [
						'url' => '',
						'is_external' => true,
						'nofollow' => true,
					],
				]
		);
		$repeater4->add_control(
			'imageCardBg4',
			[
				'label' => __( 'Image card background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
   
		$this->add_control(
			'imageSelection4',
			[
				'label' => __( 'Fourth type of image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],

			]
		);

		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('racketShowcaseStyleOneHtml.php');
    }
   
	
	


	
	
}