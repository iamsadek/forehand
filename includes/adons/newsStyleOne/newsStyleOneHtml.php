<newsStyleOne>
    <section class="_news_section">
          <div class="_news_sec_top">
              <h2 class="_news_sec_top_h2">
                <?php echo $settings['title'] ?>
              </h2>
              <a href="<?php echo $settings['allNewsLink']['url'] ?>"><?php echo $settings['allNewsTxt'] ?></a>
          </div>
          <div class="_news_card_all">
              <!-- NEWS CARD LEFT SIDE -->
              <?php if(count($news)>0): ?>
                
                    <div class="_news_card_lft">
                        <img class="_news_card_lft_img" src=" <?php echo FOREHAND_IMAGE_SOURCE.'/'.$news[0]->image; ?>" alt="image">
                        <div class="_news_card_top">
                            <p class="_news_card_top_dte">
                            <?php echo $news[0]->newsDate; ?>
                            </p>
                        </div>
                        <a href="/forehand-news/<?php echo $news[0]->id; ?>">
                            <div class="_news_card_btm">
                                <div class="_news_card_btm_top">
                                    <div class="_news_card_btm_img">
                                        <img class="_news_btm_img" src="<?php echo FOREHAND_IMAGE_SOURCE.''.$news[0]->author->profilePic; ?>" alt="image">
                                    </div>
                                        <h4 class="_news_card_btm_h4">
                                            <?php echo $news[0]->author->firstName; ?> <?php echo $news[0]->author->lastName; ?>
                                        </h4>
                                </div>
                                <div class="_news_card_btm_dtls">
                                    <h3 class="_news_card_dtls_h3">
                                        <?php echo $news[0]->title; ?>
                                    </h3>
                                    <p class="_nws2_crd_btm_p"><?php echo $news[0]->exerpt; ?></p>   
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                
              <?php endif; ?>
                <!-- NEWS CARD LEFT SIDE -->

              <!-- NEWS CARD RIGHT SIDE -->
              <div class="_news_card_r8_all">
                <!--RIGHT CARD ITEAM -->
                <?php foreach($news as $key=>$n): ?>
                    <?php if($key==0) continue; ?>
                    <a href="/forehand-news/<?php echo $n->id; ?>">
                    <div class="_news_card_r8">
                        <div class="_news_card_r8_img">
                            <img src="<?php echo FOREHAND_IMAGE_SOURCE.'/'.$n->image; ?>" alt="image">
                        </div>
                        <div class="_news_card_top">
                        <p class="_news_card_top_dte">
                        <?php echo $n->newsDate; ?>
                        </p>
                        </div>
                        <div class="_news_card_r8_btm">
                            <h4 class="_news_card_dtls_h4">
                                <?php echo $n->title; ?>
                            </h4>
                            <p class="_nws2_crd_btm_p"><?php echo $n->exerpt; ?></p>                   
                        </div>
                    </div>
                    </a>
                  <?php endforeach; ?>
                 <!--RIGHT CARD ITEAM -->

                     
              </div>
              <!-- NEWS CARD RIGHT SIDE -->
          </div>
      </section>
</newsStyleOne>
