<?php
namespace Elementor;
require_once(BASEPLUGIN_INCLUDES . '/common/helperclass.php');
class NewsStyleOne extends Widget_Base {
	use Helper;
	public function get_name() {
		return 'forehand-news-one';
	}
	
	public function get_title() {
		return 'NEWS STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-newspaper-o';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);

		$categories = $this->callApi('newsCat');
		$options = []; 
		$default = []; 
		
		foreach ($categories as $cat) { 
			$options[$cat->category] = __( $cat->category, 'plugin-domain');
			array_push($default, $cat->category);
		}
		
		// multiple selects 
		$this->add_control(
			'cat',
			[
				'label' => __( 'Show Elements', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $options,
				'default' => $default,
			]
		);


		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Latest news'
			]
		);
		// news title
		$this->add_control(
			'allNewsTxt',
			[
				'label' => __( 'All news text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'All News'
			]
		);
		$this->add_control(
			'allNewsLink',
			[
				'label' => __( 'News landing page link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->end_controls_section();
	}
	
	protected function render() {
		$settings = $this->get_settings_for_display();
		$news = $this->getNewsData($settings['cat']);
		require('newsStyleOneHtml.php');
	}
	public function getNewsData($cat){
		
        $response = wp_remote_request( FOREHAND_API_URL . "/getFilterNewsWithLimit",
            array(
				'method'     => 'POST',
				'body' => [
					'cat' => $cat
				],
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
       
        //var_dump($response);
        //echo  $response;
         $body = wp_remote_retrieve_body($response);
         return json_decode($body);
    }
   

	// js render
	
	


	
	
}