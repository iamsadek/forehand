<?php
namespace Elementor;

class PricingStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-pricing-1';
	}
	
	public function get_title() {
		return 'PRICING';
	}
	
	public function get_icon() {
		return 'fa fa-sort-amount-asc';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'sectionBg',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Banhyra'
			]
		);

		$repeater = new \Elementor\Repeater();
		
		$repeater->add_control(
			'cardTitle',
			[
				'label' => __( 'Sub title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your subtitle', 'elementor' ),
				'default' => 'Vardagar'
			]
		);
		$repeater->add_control(
			'time',
			[
				'label' => __( 'Time', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your time', 'elementor' ),
				'default' => '06:00 - 16:00'
			]
		);
		$repeater->add_control(
			'price',
			[
				'label' => __( 'Price', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your price', 'elementor' ),
				'default' => '360 kr'
			]
		);
		$repeater->add_control(
			'boxOneBg',
			[
				'label' => __( 'Box one background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$repeater->add_control(
			'boxOneTxtColor',
			[
				'label' => __( 'Box one text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$repeater->add_control(
			'boxTwoBg',
			[
				'label' => __( 'Box two background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#304f6e'
			]
		);
		$repeater->add_control(
			'boxTwoTxtColor',
			[
				'label' => __( 'Box two text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);




		$this->add_control(
			'list',
			[
				'label' => __( 'Sponsor logos', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'cardTitle' => 'Vardagar',
						'time' => '06:00 - 16:00',
						'price' => '360 kr',
						'boxOneBg' => 'white',
						'boxOneTxtColor' => 'black',
						'boxTwoBg' => '#304f6e',
						'boxTwoTxtColor' => 'white',
						
					],
					[
						'cardTitle' => 'Vardagar',
						'time' => '06:00 - 16:00',
						'price' => '360 kr',
						'boxOneBg' => 'white',
						'boxOneTxtColor' => 'black',
						'boxTwoBg' => '#304f6e',
						'boxTwoTxtColor' => 'white',
						
					],
					[
						'cardTitle' => 'Vardagar',
						'time' => '06:00 - 16:00',
						'price' => '360 kr',
						'boxOneBg' => 'white',
						'boxOneTxtColor' => 'black',
						'boxTwoBg' => '#304f6e',
						'boxTwoTxtColor' => 'white',
						
					],
				],
				'title_field' => '{{{ cardTitle }}}',
			]
		);




		
		

		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        
		$settings = $this->get_settings_for_display();
		// title
		$this->add_inline_editing_attributes( 'title', 'none' );
		$this->add_render_attribute('title',['class' => ['_price_title'],]);
		// cardTitle
		$this->add_inline_editing_attributes( 'cardTitle', 'none' );
        $this->add_render_attribute('cardTitle',['class' => ['_price_card_title'],]);
        // price
		$this->add_inline_editing_attributes( 'price', 'none' );
        $this->add_render_attribute('price',['class' => ['_price_card_price'],]);
        // time
		$this->add_inline_editing_attributes( 'time', 'none' );
        $this->add_render_attribute('time',['class' => ['_price_card_time'],]);
		
		require('pricingStyleOneHtml.php');
  
      

	}

	// js render
	
	


	
	
}