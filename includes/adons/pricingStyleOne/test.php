
<!--======== Price Section ========--->
<div class="_price" style="background: <?php echo $settings['sectionBg'] ?>">
        <div class="_price_all">
          <h2 class="_price_title"><?php echo $settings['title']?></h2>

          <div class="_price_main">
              <?php if($settings['list'] ): ?> 
                <?php foreach($settings['list'] as $item ): ?>
                    <!-- Items -->             
                      <div class="_price_card">
                        <div class="_price_card_left" style="background: <?php echo $item['boxOneBg'] ?>">
                          <p class="_price_card_title" style="color: <?php echo $item['boxOneTxtColor'] ?>"><?php echo $item['cardTitle'] ?></p>

                          <p class="_price_card_time" style="color: <?php echo $item['boxOneTxtColor'] ?>"><?php echo $item['time'] ?></p>
                        </div>

                        <div class="_price_card_right blue" style="background: <?php echo $item['boxTwoBg'] ?>">
                          <h2 class="_price_card_price" style="color: <?php echo $item['boxTwoTxtColor'] ?>"><?php echo $item['price'] ?></h2>
                        </div>
                      </div>            
                    <!-- Items -->
                  <?php endforeach; ?>
              <?php endif; ?>
             
          </div>
        </div>
      </div>
      <!--======== Price Section ========--->
	