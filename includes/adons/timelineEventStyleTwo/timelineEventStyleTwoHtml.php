<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsCalenderData = json_encode($calendarData);

echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";

?>

<style>
	._evnt1_schdl_top_dte {
		background : <?php echo $settings['dateBg'] ?>
	}
	._evnt1_schdl_top_dte_p1{
		color: <?php echo $settings['dateTxtColor'] ?>
	}
	._evnt1_schdl_top_dte_p2{
		color: <?php echo $settings['dateTxtColor'] ?>
	}
	
	._evnt1_schdl_crd_main_all{
		background : <?php echo $settings['boxBackground'] ?>
	}
	._evnt1_schdl_crd_top:after{
		background: linear-gradient(0deg, <?php echo $settings['shadingColor'] ?> 8%, rgba(34,34,34,0.26094187675070024) 50%);
	}
</style>


<timelineeventstyletwo>
    <div v-cloak>
	<div class="_pgrm_schdl_sec">
	<div class="_pgrm_schdl_lft" v-for="(event, i) in jsCalenderData" v-if="jsCalenderData.length">
		<div class="newDiv">
				<div class="_pgrm_schdl_card">
					<div class="_pgrm_schdl_dte">
						<p class="_pgrm_schdl_dte_p">
							{{event.eventName}}
						</p>
						<h3 class="_pgrm_schdl_dte_h3">
						{{event.dayName}}, {{event.dayOfTheMonth}} <span class="_small" style="font-weight: bold">{{event.monthName}} </span>
						</h3>
					</div>
					<div class="_pgrm_schdl_card_dtls">


						<!-- ITEAM -->
						<div class="_pgrm_schdl_dtls_itm" v-for="(t, j) in event.timeLine" v-if="event.timeLine.length">
							<div class="_pgrm_schdl_dtls_lft">
								<p class="_pgrm_schdl_dtls_lft_p">
								{{t.time}}
								</p>
							</div>
							<div class="_pgrm_schdl_dtls_r8_all">
								<div class="_pgrm_schdl_dtls_r8">
									<div class="_pgrm_schdl_dtls_r8_one_all">
										<div class="_pgrm_schdl_dtls_r8_one">
											<p class="_pgrm_schdl_dtls_r8_titl _pgrm_schdl_line">
											{{t.timelineName}}
											</p>
										</div>
									</div>
									<div class="_pgrm_schdl_dtls_r8_one_all">
										<div class="_pgrm_schdl_dtls_r8_one">
											<ul class="_pgrm_schdl_r8_list_ul _pgrm_schdl_line" v-for="(sub,k) in t.subTimelines" v-if="t.subTimelines.length">
												<li class="_crcl_lft1">{{sub.subTimelineName}}</li>
											</ul>

										</div>
									</div>
								</div>
							</div>

						</div>
						<!-- ITEAM -->
					</div>
				</div>
				</div>
				</div>
		</div>
	</div>
</timelineeventstyletwo>



<script type="text/javascript">

axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
	var jsdata = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");

    jsCalenderData  = JSON.parse(jsdata)
    var list = document.getElementsByTagName("timelineeventstyletwo");
    console.log(list)

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "timelineeventstyletwo-app-" + i);
            var app = new Vue({
            el: "#timelineeventstyletwo-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting,
                        jsCalenderData: jsCalenderData,
						imageSource: imageSource,
                        apiUrl : apiUrl,


                    }
                },

                created(){
					console.log('data is', jsCalenderData)
				},


            })

        }
 </script>

