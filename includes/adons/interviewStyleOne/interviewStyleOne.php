<?php
namespace Elementor;

class InterviewStyleOne extends Widget_Base {

	public function get_name() {
		return 'forehand-interview-style-one';
	}
	
	public function get_title() {
		return 'INTERVIEW STYLE 1';
	}
	
	public function get_icon() {
		return 'far fa-images';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'txtColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'heading',
			[
				'label' => __( 'Heading', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter Heading', 'elementor' ),
				'default' => 'Interviews'
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'image', [
				'label' => __( 'Video thumbnail', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/logo.png',
				],
			]
		);
		$repeater->add_control(
			'videoLink', [
				'label' => __( 'Youtube embed video link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => 'https://www.youtube.com/embed/DxC7pJevEIw',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$repeater->add_control(
			'tagLine', [
				'label' => __( 'Tagline', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'The Late Night Show with Tony Stevens' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'firstName', [
				'label' => __( 'Firstname', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Firstname' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'lastName', [
				'label' => __( 'Lastname', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Lastname' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		// $repeater->add_control(
		// 	'frameColor', [
		// 		'label' => __( 'Frame color', 'plugin-domain' ),
		// 		'type' => \Elementor\Controls_Manager::COLOR,
		// 		'default' => __( 'Frame color' , 'plugin-domain' ),
		// 		'label_block' => true,
		// 	]
		// );
		$this->add_control(
			'list',
			[
				'label' => __( 'Images', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [],
				'title_field' => '{{{ firstName }}}',
			]
		);


		
		


		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('interviewStyleOneHtml.php');
    }
   
	
	


	
	
}