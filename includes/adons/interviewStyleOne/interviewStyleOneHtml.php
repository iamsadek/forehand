<?php
// settings to js data
$jsSettings = json_encode($settings);
echo "<script type='text/javascript'>var interviewstyleone='$jsSettings'</script>";
?>

<interviewstyleone>
<div class="_intrvw_sec" style="background: <?php echo $settings['boxBgColor'] ?>" v-cloak>
		<div class="_intrvw_lft">
			<h3 class="_intrvw_sec_h3" style="color: <?php echo $settings['txtColor'] ?>">
					<?php echo $settings['heading'] ?>
			</h3>
		</div>
		<div class="_intrvw_r8">
			<!-- CARD -->
		
			<div class="_intrvw_crd" v-for="(v, i) in interviewstyleone.list" v-if="interviewstyleone.list.length">
				<div class="_intrvw_crd_vdo">
					<div class="_intrvw_crd_line2" @click="playVideo(v)">
						<img :src="v.image.url" alt="image">
						<span><i class="fa fa-play"></i></span>
					</div>
				</div>
				<div class="_intrvw_crd_nme">
					<h3 class="_intrvw_crd_nme_h3" style="color: <?php echo $settings['txtColor'] ?>">
						{{v.firstName}} <br>
						<span>{{v.lastName}}</span>
					</h3>
				</div>
				<div class="_intrvw_crd_btm">
					<p class="_intrvw_crd_btm_p" style="color: <?php echo $settings['txtColor'] ?>">
						{{v.tagLine}}
					</p>
					<h3 class="_intrvw_crd_btm_h3" style="color: <?php echo $settings['txtColor'] ?>">
					{{v.firstName}} {{v.lastName}}
					</h3>
				</div>
			</div>
			<div class="_vdo_modal_sec" v-if="isVideo">
				<div class="_vdo_modal_crd">
					<iframe  :src="url"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ></iframe>
					<div class="_vdo_modal_icon">
						<span @click="isVideo = false">
							<i class="fa fa-times"></i>
						</span>
					</div>
				</div>
			</div>
			

		</div>
	</div>
</interviewstyleone>



<script type="text/javascript">
   interviewstyleone  = JSON.parse(interviewstyleone)
   var list = document.getElementsByTagName("interviewstyleone");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "interviewstyleone-app-" + i);
           var app = new Vue({
           el: "#interviewstyleone-app-" + i,
               data(){
                   return {
						interviewstyleone: interviewstyleone,
						isVideo : false, 
						url : ''
					}
			   },
			   methods: {
					playVideo(v){
						this.url = v.videoLink.url
						this.isVideo = true
					}
			   },
			   
			   

           })
	}
</script>

