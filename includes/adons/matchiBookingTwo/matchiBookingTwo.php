<?php
namespace Elementor;


class MatchiBookingTwo extends Widget_Base {
	
	public function get_name() {
		return 'MATCHI-BOOKING-2';
	}
	
	public function get_title() {
		return 'MATCHi BOOKING TWO';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Matchi' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// news title
		$this->add_control(
			'facilityId',
			[
				'label' => __( 'Facility ID', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::NUMBER,
				'placeholder' => __( 'Enter your Facility ID', 'elementor' ),
				'default' => 1
			]
		);
		
		//$courts = $this->getCourts();
		
		//echo json_encode($courts);
		//die();
		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $calendarData = $this->getCalendarData($settings['facilityId']);
        require('matchiBookingTwoHtml.php');
    }
    public function getCalendarData($id){
		
		$date =  date("Y-m-d");
        $response = wp_remote_request( "https://dev-api.matchi.se/pub/v1/rest/facility/$id/slots/date/$date",
            array(
				'method'     => 'GET',
				'headers' => [
					'Authorization' => 'Basic ' . base64_encode( 'michael@forehand.se:cgzy4nJwCuqVisqDQGey' ),
				]
            )
		);
		if($response['response']['code']!=200){
			return [];
		}
		$body = wp_remote_retrieve_body($response);
		$s=json_decode($body, true);
		$arr = $s['slots'];
		$new_arr =[];
		foreach($arr as $key => $val) {
			$new_arr[] = $val['court']['name'];
		}
		$courts = array_unique($new_arr);
		
		$this->start_controls_section(
			'section_title_2',
			[
				'label' => __( 'Court logos', 'elementor' ),
			]
		);
		foreach($courts as $c){
			$plainCourtName =str_replace(' ', '', $c);
			
			$this->add_control(
				$plainCourtName, [
					'label' => __( "Logo for $plainCourtName", 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::MEDIA,
					'default' => [
						'url' => get_template_directory_uri().'/assets/img/logo.png',
					],
				]
			);
		}
		$this->end_controls_section();
        return json_decode($body);
	}
	
	
	
	


	
	
}