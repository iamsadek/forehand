<?php 
    // settings to js data 
    $jsSettings = json_encode($settings);
    
    // calendar data in js var... 
    $jsCalenderData = json_encode($calendarData);
    echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";
    echo "<script type='text/javascript'>var jsSettings='$jsSettings'</script>";

   

?>
<matchib2 >
   <div v-cloak>
  
   <div class="_ply_tim_section">
		<div class="_ply_tim_top">
			<p>Available courts for</p>
			<ul>
				<li>{{runningDate | formateDate}}</li>
				<li>
					<span>14:00</span>
				</li>
			</ul>
		</div>
		<div class="_ply_tim_day">
			<ul>
				<li>Next Day</li>
				<li>Calender</li>
			</ul>
		</div>
		<div class="_ply_tim_all">
			<ul>
		
				<li v-if="courts" v-for="(c,i) in courts" :class="i==currentSlot? '_ply_tim_clr_blck' : ''  " @click="changeCourt(i)">
					 {{i | getStart}}
				</li>
		
			</ul>
		</div>

		<div class="_ply_tim_crd_all">
			<!-- ITEAM -->
			<div class="_ply_tim_crd" v-for="(c, index) in initData" v-if="initData.length">
				<div class="_ply_tim_crd_inner ">
					<div class="_ply_tim_crd_top">
						<h3 class="__courtName">{{c.court.name}}</h3>
						<div class="_ply_tim_crd_top_img">
							<img :src="c.logo.url" alt="court logo">
						</div>
					</div>
					<div class="_ply_tim_crd_mdl">
						<p>{{((new Date(c.end)-new Date(c.start))/1000)/60}} min</p>
						<ul>
							<li>{{c.court.sport.type}}</li>
							<li>{{c.court.surface}}</li>
						</ul>
					</div>
					<div class="_ply_tim_crd_btm">
						<p>320sek</p>
						<a href="">book now</a>
					</div>
				</div>
			</div>
			<!-- ITEAM -->

			

		</div>
	</div>
        
      
   
   </div>
</matchib2>



<script type="text/javascript">
   
	
	let data2 = JSON.parse(jsCalenderData)
  
  
	var list = document.getElementsByTagName("matchib2");

 
        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "matchib2-app-" + i);
            var app = new Vue({
            el: "#matchib2-app-" + i,
                data(){
                    return {

                         courts : false, 
                         initData: [], 
                         settings : JSON.parse(jsSettings), 
                         currentSlot: '', 
                         runningDate : null
                       
                    }
                }, 
                methods: {
                  groupByKey(array, key) {
                    return array
                      .reduce((hash, obj) => {
                        if(obj[key] === undefined) return hash; 
                        return Object.assign(hash, { [obj[key]]:( hash[obj[key]] || [] ).concat(obj)})
                      }, {})
                  }, 
                  addLogo(){
                     for(let c of this.initData){
                        let logo = c.court.name.replace(/\s/g,'')
                        if(this.settings[logo]){
                          c.logo = this.settings[logo]
                        }else{
                          c.logo = {url: '', id: null}
                        }
                        
                       
                     }
                    
                  }, 
                  changeCourt(i){
                    this.initData = this.courts[i]
                    this.currentSlot = i
                    this.addLogo()
                  }, 
                  getDate(){
                    var today = new Date();
                    var d = String(today.getDate()).padStart(2, '0');
                    var m = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var y = today.getFullYear();

                    today = `${y}-${m}-${d}`
                    this.runningDate = today
                  }
                   
                },
                created(){
                   this.getDate()
                   if(data2.slots){
                       this.courts = this.groupByKey(data2.slots, 'start')
                      for(let i in this.courts){
                         this.initData = this.courts[i]
                         this.currentSlot = i
                         break
                      }
                   }
                   this.addLogo()
                   
                   
                }, 
                filters: {
                  getStart(key){
                    let d = new Date(key)
                    let h = d.getHours() < 10 ? `0${d.getHours()}` : d.getHours()
                    let m = d.getMinutes() < 10 ? `0${d.getMinutes()}` : d.getMinutes()
                    return `${h}:${m}`
                  }, 
                  formateDate(key){
                    let d = new Date(key)
                    let names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    
                   
                    
                    let str = `${String(d.getDate()).padStart(2, '0')} ${names[String(d.getMonth())]}, ${d.getFullYear()}`
                    
                    return str
                  
                }
              }
               
                
            })

        }
 </script>

