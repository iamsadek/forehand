<div class="_do_pdl2_trvl_spot_sec" style="background-color: <?php echo $settings['sectionBackground']?> ;">
		<div class="_do_pdl2_trvl_spot_top">
			<div class="_do_pdl2_trvl_top_lft">
      <h3 style="color : <?php echo $settings['headerOneColor'] ?>">
            <?php echo  $settings['headerOne']?>
      </h3> 
      <p href="<?php echo $item['buttonOneLink'] ?>" style="color : <?php echo $settings['buttoOneColor'] ?> ;background: <?php echo $settings['buttoOneBgColor'] ?>">
           <?php echo  $settings['buttoOne']?>
      </p> 
			</div>
			<div class="_do_pdl2_trvl_top_r8">
				<ul>
          <li class="_do_pdl2_trvl_actv_grd">
          <?php echo  $settings['listOne']?></li>
          <li > <?php echo  $settings['listTwo']?>
          </li>
				</ul>
			</div>
		</div>

		<div class="_do_pdl2_trvl_crd_all">
      <!-- CARD -->
      <?php if($settings['list'] ): ?> 
        <?php foreach($settings['list'] as $item ): ?>

			<div class="_do_pdl2_trvl_crd">
				<div class="_do_pdl2_trvl_crd_inner">
				<img src="<?php echo $item['image']['url'] ?>" alt="image">
					<div class="_do_pdl2_trvl_crd_info">
						<div class="_do_pdl2_trvl_crd_info_lft">
            <p style="color : <?php echo $item['titleThreeColor'] ?>">
               <?php echo  $item['titleThree']?>
            </p> 

             <h3 style="color : <?php echo $item['PlaceColor'] ?>">
               <?php echo  $item['Place']?>
             </h3>
              <a href="<?php echo $item['buttonTwoLink'] ?>" style="color : <?php echo $item['buttonTwoColor'] ?> ;background: <?php echo $item['buttonTwoBgColor'] ?>">
               <?php echo  $item['buttonTwo']?></a>
						</div>
						<div class="_do_pdl2_trvl_crd_info_r8">
							<div class="_do_pdl2_trvl_info_r8_top">
              <p style="color : <?php echo $item['durationColor'] ?>">
               <?php echo  $item['duration']?>
              </p>
                <p style="color : <?php echo $item['costColor'] ?>"><span >from</span> 
                  <?php echo  $item['cost']?></p>              
							</div>
							<div class="_do_pdl2_trvl_info_r8_btm">
								<ul>
                <li style="color : <?php echo $item['facilitesOneColor'] ?>"> <?php echo  $item['facilitesOne']?></li>
                <li style="color : <?php echo $item['facilitesTwoColor'] ?>"> <?php echo  $item['facilitesTwo']?></li>
                <li style="color : <?php echo $item['facilitesThreeColor'] ?>"> <?php echo  $item['facilitesThree']?></li>
                <li style="color : <?php echo $item['facilitesFourColor'] ?>"> <?php echo  $item['facilitesFour']?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

      <?php endforeach; ?>
            <?php endif; ?>
	
		</div>
	</div>
	<!-- DO PADEL PAGE-2 TRAVEL SPOT END-->