<?php
namespace Elementor;

class UpcomingTripsstyleTwo extends Widget_Base {
	
	public function get_name() {
		return 'forehand-upcomingtrips-text-style-two';
	}
	
	public function get_title() {
		return 'UPCOMING TRIPS STYLE 2';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
	
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
	

		        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Where do you want to go?' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttoOne', [
				'label' => __( 'Button one text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Filter' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'buttoOneColor',
			[
				'label' => __( 'Button one text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttoOneBgColor',
			[
				'label' => __( 'Button one backgournd color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$this->add_control(
			'buttonOneLink',
			[
				'label' => __( 'Button one link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'listOne', [
				'label' => __( 'View text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Grid View ' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$this->add_control(
			'listOneColor',
			[
				'label' => __( 'View text one Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);

			$this->add_control(
				'listTwo', [
					'label' => __( 'View text two', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Expanded View' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$this->add_control(
				'listTwoColor',
				[
					'label' => __( ' View text two Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
		
		
	
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		
		$repeater->add_control(
			'titleThree', [
				'label' => __( 'Text two ', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Premium paddle trip to' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'titleThreeColor',
			[
				'label' => __( 'Text two  color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);
			$repeater->add_control(
				'Place', [
					'label' => __( 'Place text ', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Valencia' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'PlaceColor',
				[
					'label' => __( 'Place text color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
        $repeater->add_control(
			'buttonTwo', [
				'label' => __( 'Button two text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'EXPLORE' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'buttonTwoColor',
			[
				'label' => __( 'Button two text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'buttonTwoBgColor',
			[
				'label' => __( 'Button two background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'buttonTwoLink',
			[
				'label' => __( 'Button two link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$repeater->add_control(
			'duration', [
				'label' => __( 'Days', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '4 Days' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'durationColor',
			[
				'label' => __( 'Days text color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);
			$repeater->add_control(
				'cost', [
					'label' => __( 'Cost', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( '400KR' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'costColor',
				[
					'label' => __( 'Cost text color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);


			
				$repeater->add_control(
					'list_title', [
						'label' => __( 'Title', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( 'Madrid' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$repeater->add_control(
					'list_color',
					[
						'label' => __( 'Title Color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
				);
		
		
		$repeater->add_control(
			'facilitesOne', [
				'label' => __( 'Facilities', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Hotel Stay' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'facilitesOneColor',
			[
				'label' => __( 'Facilities one text color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);
			$repeater->add_control(
				'facilitesTwo', [
					'label' => __( 'Facilities two', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Breakfast' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'facilitesTwoColor',
				[
					'label' => __( 'Facilities two text color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
				$repeater->add_control(
					'facilitesThree', [
						'label' => __( 'Facilities three', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( 'club Charges' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$repeater->add_control(
					'facilitesThreeColor',
					[
						'label' => __( 'Facilities three text color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
					);
					$repeater->add_control(
						'facilitesFour', [
							'label' => __( 'Facilities four', 'plugin-domain' ),
							'type' => \Elementor\Controls_Manager::TEXT,
							'default' => __( 'Transists' , 'plugin-domain' ),
							'label_block' => true,
						]
					);
			
					
			
					$repeater->add_control(
						'facilitesFourColor',
						[
							'label' => __( 'Facilities four text color', 'plugin-domain' ),
							'type' => \Elementor\Controls_Manager::COLOR,
							'default' => '',
						]
						);

		

		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
				
					
				],
				
			]
		);

		$this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('upcomingTripsstyleTwoHtml.php');
    }
   
	
	


	
	
}