<?php
namespace Elementor;

class EventHeaderStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-event-header-1';
	}
	
	public function get_title() {
		return 'EVENT HEADER 1';
	}
	
	public function get_icon() {
		return 'fas fa-heading';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Box background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ecececf2'
			]
		);

		// MONTH
		$this->add_control(
			'monthBg',
			[
				'label' => __( 'Month background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		$this->add_control(
			'monthTxtColor',
			[
				'label' => __( 'Month text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'timeBg',
			[
				'label' => __( 'Time background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'transparent'
			]
		);
		$this->add_control(
			'timeTxtColor',
			[
				'label' => __( 'Time text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'eventNameTxtColor',
			[
				'label' => __( 'Event name text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		$this->add_control(
			'locationTxtColor',
			[
				'label' => __( 'Location text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		$this->add_control(
			'attendeeTxtColor',
			[
				'label' => __( 'Attendee text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		$this->add_control(
			'feeTxtColor',
			[
				'label' => __( 'Fee text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		
	
	
		$this->add_control(
			'showEvents',
			[
				'label' => __( 'Show event text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Kommande event'
			]
		);
		
	
		//registration text
		$this->add_control(
			'regTxt',
			[
				'label' => __( 'Registration text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Läs mer'
			]
		);
		$this->add_control(
			'regLink',
			[
				'label' => __( 'Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'regBgColor',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'regTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'reserveSpotTxt',
			[
				'label' => __( 'Reserve spot/signedup text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		
		
		
		
		
		
		
		
		
		
		



		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$calendarData = $this->getCalendarData();
        require('eventHeaderStyleOneHtml.php');
	}
	public function getCalendarData(){
        $response = wp_remote_request( FOREHAND_API_URL.'/eventheader',
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
    }
   
	
	


	
	
}