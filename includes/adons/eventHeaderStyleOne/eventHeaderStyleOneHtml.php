<?php
// settings to js data
	$jsSettings = json_encode($settings);
	echo "<script type='text/javascript'>var eventheaderstyleone='$jsSettings'</script>";
	$imageSource = FOREHAND_IMAGE_SOURCE;
	$apiUrl = FOREHAND_API_URL;

	echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
	echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
	// calendar data in js var...
	$events = json_encode($calendarData);

	echo "<script type='text/javascript'>var events='$events'</script>";
?>

<style>
	._evnt5_bnr_crd1 {
		background : <?php echo $settings['boxBgColor'] ?>;
	}
	._evnt5_bnr_titl_h3 {
		color : <?php echo $settings['eventNameTxtColor'] ?>;
	}
	._evnt5_bnr_titl_ul li {
		color : <?php echo $settings['locationTxtColor'] ?>;
	}
	.ate {
		color : <?php echo $settings['attendeeTxtColor'] ?> !important;
	}
	.fee {
		color : <?php echo $settings['feeTxtColor'] ?> !important;
	}
	._evnt5_crd1_btm_txt_p {
		color : <?php echo $settings['reserveSpotTxt'] ?> !important;
	}


	._evnt5_bnr_dte {
		background : <?php echo $settings['monthBg'] ?> !important;
		color : <?php echo $settings['monthTxtColor'] ?> !important;
	}
	.timeTxt{
		background : <?php echo $settings['timeBg'] ?> !important;
		color : <?php echo $settings['timeTxtColor'] ?> !important;
	}

</style>


<eventheaderstyleone>

<div class="_evnt5_banner_sec" :style="{'background-image': `url(${imageSource+currentEvent.eventImg})`}" v-cloak v-if="events.length">
			<div class="_evnt5_bnr_crd1_main">
				<div class="_evnt5_bnr_crd1">
					<div class="_evnt5_bnr_crd1_top">
						<ul class="_evnt5_bnr_crd1_top_ul">
							<li class="_evnt5_bnr_dte">{{currentEvent.dayOfTheMonth}} {{currentEvent.monthName.substring(0,3)}}</li>
							<li class="timeTxt">{{currentEvent.eventTime}}</li>
						</ul>
					</div>
					<div class="_evnt5_bnr_titl">
						<h3 class="_evnt5_bnr_titl_h3">{{currentEvent.eventName}}</h3>
						<ul class="_evnt5_bnr_titl_ul">
							<li class="_evnt5_bnr_titl_1">{{currentEvent.eventLocation}}</li>
							<!-- <li>Stockholm, Sweden</li> -->
						</ul>
					</div>
					<div class="_evnt5_bnr_img_all">
						<div class="_evnt5_crd1_btm_img_top">
							<div class="_evnt5_crd1_btm_itm">
								<div class="_evnt5_crd1_btm_img" v-for="(u, i) in currentEvent.accepted" v-if="currentEvent.accepted">
									<img :src="imageSource+u.user.profilePic" alt="image">
								</div>
								
							</div>
							<div class="_evnt5_crd1_btm_txt" v-if="currentEvent.accepted.length < currentEvent.__meta__.singedup_count">
								<p class="_evnt5_crd1_btm_txt_p">
									+{{currentEvent.__meta__.singedup_count-currentEvent.accepted.length}} signed up
								</p>
							</div>
							<div class="_evnt5_crd1_btm_txt"  v-if="currentEvent.accepted.length == currentEvent.__meta__.singedup_count && currentEvent.__meta__.singedup_count>0">
								<p class="_evnt5_crd1_btm_txt_p">
									Attended
								</p>
							</div>
							<!-- <div class="_evnt5_crd1_btm_txt"  v-if="currentEvent.__meta__.singedup_count==0">
								<p class="_evnt5_crd1_btm_txt_p">
									Reserve your spot!
								</p>
							</div> -->
						</div>
					</div>
					<div class="_evnt5_bnr_btm">
						<ul class="_evnt5_bnr_btm_ul">
							<li class="ate"><span>{{currentEvent.__meta__.singedup_count}}/{{currentEvent.eventLimit}}</span> platser kvar</li>
							<!-- <li class="ate">Attendee limit <span>{{currentEvent.eventLimit}}</span></li> -->
							<li class="fee">Anmälningsavgift <span>{{currentEvent.eventFee}} kr</span></li>
							<!-- <li>Prize money <span>4000 kr</span></li> -->
							
						</ul>
					</div>
				</div>
				
				<div class="_evnt5_bnr_read" >
					<a :href="currentEvent.landingPage"  style="background: <?php echo $settings['regBgColor'] ?>; color: <?php echo $settings['regTxtColor']?>"><?php echo $settings['regTxt'] ?></a>
				</div>
				
			</div>
			<div class="_evnt5_btm_bnr_sec">
				<div class="_evnt5_btm_bnr_lft">
					<p class="_evnt5_btm_bnr_lft_p"><?php echo $settings['showEvents'] ?></p>
				</div>
				<div class="_evnt5_btm_bnr_r8">
					<ul class="_evnt5_btm_bnr_r8_ul" v-if="events.length">
						<li :class="currentIndex == i? '_evnt5_bnr_actv_evnt' : '' " v-for="(event, i) in events" @click="changeEvent(event, i)">
							<img :src="imageSource+event.eventImg" alt="image">
							<div class="_evnt5_bnr_btm_img_text">
								<p class="_evnt5_dte_mnth">{{event.monthName.substring(0,3)}}</p>
								<p class="_evnt5_dte_day_p eventHeader">
									{{event.dayName}} 
									<span>{{event.dayOfTheMonth}}</span>
								</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div v-cloak v-if="!events.length">
			<h1>You don't have any events data. Please recheck the date for events.</h1>
		</div>


</eventheaderstyleone>


<script type="text/javascript">
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
   eventheaderstyleone  = JSON.parse(eventheaderstyleone)
   //console.log(events)
   events  = events.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
   events = JSON.parse(events)
   var list = document.getElementsByTagName("eventheaderstyleone");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "eventheaderstyleone-app-" + i);
           var app = new Vue({
           el: "#eventheaderstyleone-app-" + i,
               data(){
                   return {
						eventheaderstyleone: eventheaderstyleone,
						events: [], 
						currentEvent : null, 
						currentIndex : 0
					   
                   }
			   },
			   methods: {
					changeEvent(ev, i){
						// console.log(ev)
						// console.log(i)
						this.currentEvent = ev 
						this.currentIndex = i
					}
				  
			   },
			   created(){
				  if(typeof events !=null){
					 this.events = events 
					 this.currentEvent = this.events[0]
				  }
				  console.log(this.events)
				   
			   }

           })
	}
</script>

