<?php
namespace Elementor;

class SixImageStyleOne extends Widget_Base {
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name() {
		return 'forehand-six-image-style-one';
	}
	
	public function get_title() {
		return 'SIX IMAGE STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-picture-o';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222222'
			]
		);
	
		
		$this->add_control(
			'image1', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header1',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Sponsors'
			]
		);
		$this->add_control(
			'url1', [
				'label' => __( 'Image 1 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		
		$this->add_control(
			'image2', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header2',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Event'
			]
		);
		$this->add_control(
			'url2', [
				'label' => __( 'Image 2 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$this->add_control(
			'image3', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header3',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Business club'
			]
		);
		$this->add_control(
			'url3', [
				'label' => __( 'Image 3 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		
		$this->add_control(
			'image4', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header4',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Seriespel'
			]
		);

		$this->add_control(
			'url4', [
				'label' => __( 'Image 4 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'image5', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header5',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Träning'
			]
		);
		$this->add_control(
			'url5', [
				'label' => __( 'Image 5 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'image6', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header6',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Gemenskap'
			]
		);
		$this->add_control(
			'url6', [
				'label' => __( 'Image 6 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);






		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('sixImageStyleOneHtml.php');
    }
   
	
	


	
	
}