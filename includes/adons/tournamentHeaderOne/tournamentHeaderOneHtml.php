 <style>
   ._2banner_card{
      background: <?php echo $settings['boxBgColor'] ?>;
   }
 </style>
 <?php 
    $imageSource = FOREHAND_IMAGE_SOURCE;
    
 ?>

 
 <!-- BANNER -->
 <div class="_2banner" style="background-image: url(<?php echo $imageSource.$tournamentInfo->league->img->image ?>)">
       <div class="_2banner_card">
          <div class="_2banner_card_dte">
            <p class="_2banner_card_dte_txt1"><?php echo date('M', strtotime($tournamentInfo->startingTime)) ?></p>
            <p class="_2banner_card_dte_txt2"><?php echo date('d', strtotime($tournamentInfo->startingTime)) ?></p>
            <p class="_2banner_card_dte_txt3"><?php echo date('Y', strtotime($tournamentInfo->startingTime)) ?></p>
          </div>
          <div class="_2banner_card_mdl">
            <h3 class="_2banner_card_mdl_h3"><?php echo $tournamentInfo->tournamentName ?></h3>
            <ul class="_2banner_card_mdl_ul">
              <li class="_line_aftr"><?php echo $tournamentInfo->league->club->clubName; ?></li>
              <li><?php echo $tournamentInfo->league->club->city; ?></li>
            </ul>
          </div>
          <div class="_2banner_card_btm">
          
            <ul class="_2banner_card_btm_ul">
              <li>
               <a href="" class="_2banner_card_btn _s_btn1"><?php echo $settings['signUpTextBtn'] ?></a>
              </li>
                <li>
                <a href="" class="_2banner_card_btn _s_btn2"><?php echo $settings['shareTextBtn'] ?></a>
              </li>
            </ul>
          </div>
        </div>
    </div>
     <!-- BANNER END -->
       <!-- TOURNAMENT INFO -->
       <div class="_trnmnt_info" style="background: <?php echo $settings['bannerBg'] ?>">
          <!-- ITEAM -->
          <?php if($settings['list'] ): ?> 
            <?php foreach($settings['list'] as $item ): ?>
              <div class="_trnmnt_info_itm">
                <p class="_trnmnt_info_itm_txt" style="color: <?php echo $settings['bannerFooterTxtColor'] ?> ">
                  <?php echo $item['headingOne'] ?>
                </p>
                <h4 class="_trnmnt_info_itm_h4" style="color: <?php echo $settings['bannerFooterTxtColor'] ?> ">
                <?php echo $item['headingTwo'] ?>
                </h4>
              </div>
            <?php endforeach; ?>
            <?php endif; ?>
          <!-- ITEAM -->
    </div>
     <!-- TOURNAMENT INFO -->