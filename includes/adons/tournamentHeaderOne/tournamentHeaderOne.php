<?php
namespace Elementor;

require_once BASEPLUGIN_INCLUDES . '/common/helperclass.php';
class TournamentHeaderOne extends Widget_Base
{
	use Helper;
    public function get_name()
    {
        return 'forehand-tournament-header-one';
    }

    public function get_title()
    {
        return 'TOURNAMENT HEADER 1';
    }

    public function get_icon()
    {
        return 'fas fa-heading';
    }

    public function get_categories()
    {
        return ['Forehand'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Settings', 'elementor'),
            ]
        );
        $tournamentLists = $this->callApi('getTournamentsListNames');
        $optinsAndDefault = $this->getOptionsAndDefault($tournamentLists);
        $this->add_control(
            'trId',
            [
                'label' => __( 'Select the tournament', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => $optinsAndDefault[1],
                'options' => $optinsAndDefault[0],
            ]
        );

        // background color
        $this->add_control(
            'boxBgColor',
            [
                'label' => __('Box background color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '#2b436efa',
            ]
        );

        // MONTH
        $this->add_control(
            'month',
            [
                'label' => __('Month', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your month', 'elementor'),
                'default' => 'May',
            ]
        );
        // date
        $this->add_control(
            'date',
            [
                'label' => __('Date', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::NUMBER,
                'placeholder' => __('Enter your date', 'elementor'),
                'default' => '12',
            ]
        );
        // year
        $this->add_control(
            'year',
            [
                'label' => __('Year', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your year', 'elementor'),
                'default' => '2020',
            ]
        );
        // title
        $this->add_control(
            'title',
            [
                'label' => __('Title', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your title', 'elementor'),
                'default' => 'French Open',
            ]
        );
        //club name
        $this->add_control(
            'clubName',
            [
                'label' => __('Club name', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your text', 'elementor'),
                'default' => 'Hede Padelcenter',
            ]
        );
        //club name
        $this->add_control(
            'address',
            [
                'label' => __('Address', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your text', 'elementor'),
                'default' => 'Stockham, Hanige',
            ]
        );
        //registration text
        $this->add_control(
            'regTxt',
            [
                'label' => __('Registration text', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your text', 'elementor'),
                'default' => 'Registration until 29 oct',
            ]
        );
        //sign up text
        $this->add_control(
            'signUpTextBtn',
            [
                'label' => __('Signup text', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your text', 'elementor'),
                'default' => 'SIGN UP',
            ]
        );
        //share this text
        $this->add_control(
            'shareTextBtn',
            [
                'label' => __('Share this text', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your text', 'elementor'),
                'default' => 'SHARE THIS',
            ]
        );
        // image
        $this->add_control(
            'banner',
            [
                'label' => __('Choose Banner', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => get_template_directory_uri() . '/assets/img/bg2.jpg',
                ],
            ]
        );

        // background color for footer
        $this->add_control(
            'bannerBg',
            [
                'label' => __('Banner footer background color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '#d8e5f5',
            ]
        );
        $this->add_control(
            'bannerFooterTxtColor',
            [
                'label' => __('Banner footer text color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => 'black',
            ]
        );

        //  repeater starts ....

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'title', [
                'label' => __('Title', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('Title 1', 'plugin-domain'),
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'headingOne', [
                'label' => __('Heading one', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your text', 'elementor'),
                'default' => 'Prize Money',
            ]
        );
        $repeater->add_control(
            'headingTwo', [
                'label' => __('Heading two', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your text', 'elementor'),
                'default' => '38 000KR',
            ]
        );

        $this->add_control(
            'list',
            [
                'label' => __('Banner footer', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'title' => __('Title #1', 'plugin-domain'),
                        'headingOne' => __('Prize Money', 'plugin-domain'),
                        'headingTwo' => __('38 000KR'),
                    ],
                    [
                        'title' => __('Title #2', 'plugin-domain'),
                        'headingOne' => __('Classes', 'plugin-domain'),
                        'headingTwo' => __('Herr & Dam A,B'),
                    ],
                    [
                        'title' => __('Title #3', 'plugin-domain'),
                        'headingOne' => __('Organiser', 'plugin-domain'),
                        'headingTwo' => __('Action Padel'),
                    ],
                    [
                        'title' => __('Title #4', 'plugin-domain'),
                        'headingOne' => __('Result and Schedule', 'plugin-domain'),
                        'headingTwo' => __('TBA'),
                    ],
                    [
                        'title' => __('Title #5', 'plugin-domain'),
                        'headingOne' => __('Tournament Sanctioned by', 'plugin-domain'),
                        'headingTwo' => __('Swedish Padel Federation'),
                    ],

                ],
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();
    }

    // php render.
    protected function render()
    {
		$settings = $this->get_settings_for_display();
		$id = $settings['trId'];
		$tournamentInfo = $this->callApi("getTrInfoByLeagueId/$id");
		//echo date('Y', strtotime($tournamentInfo->startingTime));
		//echo date('M', strtotime($tournamentInfo->startingTime));
		// echo date('d', strtotime($tournamentInfo->startingTime));
		// die();
		require 'tournamentHeaderOneHtml.php';
    }

}
