<div class="_do_pdl2_trips" style="background-color: <?php echo $settings['sectionBackground'] ?>;">
		<div class="_do_pdl2_trips_top">
        <h3 style="color : <?php echo $settings['headerOneColor'] ?>">
           <?php echo  $settings['headerOne']?>
        </h3> 
		</div>
		<div class="_do_pdl2_trips_crd_all">
        <?php if($settings['list'] ): ?> 
        <?php foreach($settings['list'] as $item ): ?>

			<!-- CARD -->                                       
			<div class="_do_pdl2_trips_crd">                           
				<div class="_do_pdl2_trips_crd_inner">
        <img src="<?php echo $item['image']['url'] ?>" alt="image">
					<div class="_do_pdl2_trips_crd_dte" style="background-color: <?php echo  $item['backgroundOne']?> ;" >
           <h3 style="color : <?php echo $item['dateColor'] ?>">
               <?php echo  $item['date']?>
          </h3> 

          <p class="_do_pdl2_trips_mnth" style="color : <?php echo $item['dateMonthColor'] ?>">
           <?php echo  $item['dateMonth']?>
          </p> 
           <p class="_do_pdl2_trips_day" style="color : <?php echo $item['dateDayColor'] ?>">
           <?php echo  $item['dateDay']?>
          </p> 
           
					</div>
					<div class="_do_pdl2_trips_crd_btm" style="background-color: <?php echo  $item['backgroundTwo']?> ;">
						<div class="_do_pdl2_trips_name">
              <h3 style="color : <?php echo $item['list_color'] ?>">
               <?php echo  $item['list_title']?>
              </h3>
							<ul>
                  <li style="color : <?php echo $item['listOneColor'] ?>"> <?php echo  $item['listOne']?></li>
                  <li style="color : <?php echo $item['listTwoColor'] ?>"> <?php echo  $item['listTwo']?></li>
							</ul>
						</div>
						<div class="_do_pdl2_trips_pymnt">                          
               <a href="<?php echo $item['buttonLink'] ?>" style="color : <?php echo $item['buttonTextColor'] ?> ;background: <?php echo $item['buttonBackground'] ?>">
               <?php echo  $item['buttonText']?></a>
						</div>
					</div>
				</div>
			</div>
			<!-- CARD -->

       <?php endforeach; ?>
            <?php endif; ?>
		</div>
	</div>
	<!-- DO PADEL PAGE-2 UPCOMING TRIPS END-->

	