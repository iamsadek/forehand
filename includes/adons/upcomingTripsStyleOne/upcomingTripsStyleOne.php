<?php
namespace Elementor;

class UpcomingTripsStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-upcomingtrips-text-style-one';
	}
	
	public function get_title() {
		return 'UPCOMING TRIPS STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
	
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
	

		        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Upcoming trips' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
	
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$repeater->add_control(
			'backgroundOne',
			[
				'label' => __( 'Background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'date', [
				'label' => __( 'Date', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '4' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'dateColor',
			[
				'label' => __( 'Date text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);


		$repeater->add_control(
			'dateDay', [
				'label' => __( 'Days', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Fri' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'dateDayColor',
			[
				'label' => __( 'Day text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);
			$repeater->add_control(
				'dateMonth', [
					'label' => __( 'Month', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Jun' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'dateMonthColor',
				[
					'label' => __( 'Month text Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
				$repeater->add_control(
					'backgroundTwo',
					[
						'label' => __( 'Background color', 'elementor' ),
						'type' => Controls_Manager::COLOR,
						'default' => ''
					]
				);
			
				$repeater->add_control(
					'list_title', [
						'label' => __( 'Title', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( 'Madrid' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$repeater->add_control(
					'list_color',
					[
						'label' => __( 'Title Color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
				);
		
		$repeater->add_control(
			'buttonText', [
				'label' => __( 'Button text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'CHECK OUT' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$repeater->add_control(
			'buttonTextColor',
			[
				'label' => __( 'Button  text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'buttonBackground',
			[
				'label' => __( 'Button  background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'listOne', [
				'label' => __( 'Days', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '4 Days' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'listOneColor',
			[
				'label' => __( 'Day text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);

			$repeater->add_control(
				'listTwo', [
					'label' => __( 'Amount', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( '400 kr' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'listTwoColor',
				[
					'label' => __( 'Amount text color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
		

		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
				
					
				],
				
			]
		);

		$this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('upcomingTripsStyleOneHtml.php');
    }
   
	
	


	
	
}