<?php
namespace Elementor;

class specRacketStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-specsrackets-text-style-one';
	}
	
	public function get_title() {
		return 'SPEC RACKET STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		
		
		$this->add_control(
			'sectionBackgroundOne',
			[
				'label' => __( 'Section background one', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

	        
    
		$this->add_control(
			'headingOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Specifications' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

	
		$this->add_control(
			'headingOneBgColor',
			[
				'label' => __( 'Title background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
			
		$this->add_control(
			'sectionBackgroundTwo',
			[
				'label' => __( 'Section background two', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		
		
	


		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'specTitle', [
				'label' => __( 'Specification  title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Thickness' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'specTitleColor',
			[
				'label' => __( 'Specification  title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$repeater->add_control(
			'specText', [
				'label' => __( 'Text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '38mm' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'specTextFColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
   
		
		
		$this->add_control(
			'specList',
			[
				'label' => __( 'Specification lists', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],

			]
		);
	
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('specRacketStyleOneHtml.php');
    }
   
	
	


	
	
}