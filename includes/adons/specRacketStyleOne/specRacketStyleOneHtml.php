
	<!-- DO PADEL PAGE-3 RACKET DETAILS-->
	<div class="_do_pdl3_rckt_dtls" style="background-color: <?php echo $settings['sectionBackgroundOne'] ?>;">
		<div class="_do_pdl3_rckt_dtls_top">
			  <p style="color: <?php echo $settings['headingOneColor'] ?>;
			  background:<?php echo $settings['headingOneBgColor'] ?>;">
                     <?php echo $settings['headingOne']?></p>
		</div>
		<div class="_do_pdl3_rckt_dtls_inner" style="background-color: <?php echo $settings['sectionBackgroundTwo'] ?>;">
			<ul>
			   <?php if($settings['specList'] ): ?> 
               <?php foreach($settings['specList'] as $item ): ?>
				<li>
					<div class="_do_pdl3_rckt_dtls_box">
					<p style="color: <?php echo $item['specTitleColor'] ?>;">
                     <?php echo $item['specTitle']?></p>
					 <h3 style="color: <?php echo $item['specTextFColor'] ?>;">
                     <?php echo $item['specText']?></h3>
					</div>
				</li> 
				<?php endforeach; ?>
                 <?php endif; ?>
			</ul>
		</div>
	</div>
	<!-- DO PADEL PAGE-3 RACKET DETAILS END-->
                                                       
	<!-- DO PADEL PAGE-3 RACKET DETAILS TXT-->
	