<?php
namespace Elementor;

class TwoImageStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-two-image-2';
	}
	
	public function get_title() {
		return 'TWO IMAGE STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-picture-o';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'image1', [
				'label' => __( 'Image one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/logo.png',
				],
			]
		);
		$this->add_control(
			'image2', [
				'label' => __( 'Image two', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/logo.png',
				],
			]
		);
		$this->add_control(
			'image1Title',
			[
				'label' => __( 'Image one title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter description', 'elementor' ),
				'default' => 'Seriespel'
			]
		);
		$this->add_control(
			'image1Txt',
			[
				'label' => __( 'Image one sub texts', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Enter description', 'elementor' ),
				'default' => '<p><span style="color: #ff6600;"><a style="color: #ff6600;" href="http://forehand.test:8080/wp-admin/post.php?post=6&amp;action=elementor#">VISIT PAGE</a>&nbsp; |&nbsp; <a style="color: #ff6600;" href="http://forehand.test:8080/wp-admin/post.php?post=6&amp;action=elementor#">SIGN UP NOW</a></span></p>'
			]
		);
		$this->add_control(
			'image2Title',
			[
				'label' => __( 'Image two title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter description', 'elementor' ),
				'default' => 'Seriespel'
			]
		);
		$this->add_control(
			'image2Txt',
			[
				'label' => __( 'Image two sub texts', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Enter description', 'elementor' ),
				'default' => '<p><span style="color: #ff6600;"><a style="color: #ff6600;" href="http://forehand.test:8080/wp-admin/post.php?post=6&amp;action=elementor#">VISIT PAGE</a>&nbsp; |&nbsp; <a style="color: #ff6600;" href="http://forehand.test:8080/wp-admin/post.php?post=6&amp;action=elementor#">SIGN UP NOW</a></span></p>'
			]
		);
		
		
        
       
		
		
		
		
	
		
		
		

		





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('twoImageStyleOneHtml.php');
    }
   
	
	


	
	
}