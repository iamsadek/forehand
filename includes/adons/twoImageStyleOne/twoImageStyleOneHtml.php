<!-- PADEL SECTION -->
<div class="_pdl_man_sec" style="background: <?php echo $settings['boxBgColor'] ?>">
				<!-- ITEAM -->
			<div class="_pdl_man_sngl">
				<img src="<?php echo $settings['image1']['url'] ?>" alt="image">
				<div class="_pdl_man_sngl_info">
					<h3><?php echo $settings['image1Title'] ?></h3>
					<p><?php echo $settings['image1Txt'] ?></p>
				</div>
			</div>
			<!-- ITEAM -->

				<!-- ITEAM -->
			<div class="_pdl_man_sngl">
				<img src="<?php echo $settings['image2']['url'] ?>" alt="image">
				<div class="_pdl_man_sngl_info">
					<h3><?php echo $settings['image2Title'] ?></h3>
					<p><?php echo $settings['image2Txt'] ?></p>
				</div>
			</div>
			<!-- ITEAM -->
		</div>
	<!-- PADEL SECTION END-->