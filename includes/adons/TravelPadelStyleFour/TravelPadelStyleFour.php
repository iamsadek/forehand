<?php
namespace Elementor;

class TravelPadelStyleFour extends Widget_Base {
	
	public function get_name() {
		return 'forehand-pTravel-text-style-four';
	}
	
	public function get_title() {
		return 'TRAVEL PADEL STYLE 4';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
	
     
		$this->add_control(
			'sectionBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
	

		        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Top Notch' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'subMenue', [
				'label' => __( 'Sub menue text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Sub menue' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'subMenueColor',
			[
				'label' => __( 'Sub menue text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'paragraphText',
			[
				'label' => __( 'Description', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 10,
				'default' => __( 'We can provide you with great padel travel packages. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque opsa quae ab illo', 'plugin-domain' ),
				'placeholder' => __( 'Type your description here', 'plugin-domain' ),
			]
		);
	
		$this->add_control(
			'paragraphTextColor',
			[
				'label' => __( 'Description text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$this->add_control(
			'addButton',
			[
				'label' => __( 'Add button', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		
		$this->add_control(
			'buttonText', [
				'label' => __( 'Button text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Explore All Packages' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonTextColor',
			[
				'label' => __( 'Button  text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonBackground',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		
		
		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		

		$repeater->add_control(
			'list_title', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Puerto Banus' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'list_color',
			[
				'label' => __( 'Title color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
		);
		$repeater->add_control(
			'buttonTextTwo', [
				'label' => __( 'Button text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Läs mer' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'btnLink',
			[
				'label' => __( 'Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$repeater->add_control(
			'buttonTextColorTwo',
			[
				'label' => __( 'Button  text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'buttonBackground',
			[
				'label' => __( 'Button  background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'listOne', [
				'label' => __( 'Days', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '5 dagar' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'listOneColor',
			[
				'label' => __( 'Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);

			$repeater->add_control(
				'listTwo', [
					'label' => __( 'Cost text', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( '4000 kr' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'listTwoColor',
				[
					'label' => __( 'Cost text color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
		

		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'list_title' => __( 'Puerto Banus', 'plugin-domain' ),
						
					],
					
				],
				'title_field' => '{{{ list_title }}}',
			]
		);

		$this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('TravelPadelStyleFourHtml.php');
    }
   
	
	


	
	
}