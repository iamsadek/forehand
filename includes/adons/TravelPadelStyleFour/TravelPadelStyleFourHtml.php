
	<div class="_do_pdl_trvl_sec" style="background-color: <?php echo $settings['sectionBgColor']?> ;">
		<div class="_do_pdl_trvl_lft">
			<div class="_do_pdl_trvl_lft_inr">
			  <h3 style="color : <?php echo $settings['headerOneColor'] ?>">
           <?php echo  $settings['headerOne']?>
        </h3> 
        <h4 style="color : <?php echo $settings['subMenueColor'] ?>">
           <?php echo  $settings['subMenue']?>
        </h4>
        <p style="color : <?php echo $settings['paragraphTextColor'] ?>">
           <?php echo  $settings['paragraphText']?>
        </p> 
        <?php if($settings['addButton']) : ?>
        <a href="<?php echo $settings['buttonLink'] ?>" style="color : <?php echo $settings['buttonTextColor'] ?>
         ;background: <?php echo $settings['buttonBackground'] ?>">
        <?php echo  $settings['buttonText']?></a>
        <?php endif; ?>
       
			</div>
    </div>
    

		<div class="_do_pdl_trvl_r8">

    <?php if($settings['list'] ): ?> 
    <?php foreach($settings['list'] as $item ): ?>
			<!-- CARD -->
			<div class="_do_pdl_trvl_card">
      <img src="<?php echo $item['image']['url'] ?>" alt="image"> 
				<div class="_do_pdl_trvl_info">
        
        <h3 style="color : <?php echo $item['list_color'] ?>">
           <?php echo  $item['list_title']?>
        </h3>

					<ul>
						<li style="color : <?php echo $item['listOneColor'] ?>"> <?php echo  $item['listOne']?></li>
            <li style="color : <?php echo $item['listTwoColor'] ?>"> <?php echo  $item['listTwo']?></li>
					</ul>
				</div>
				<div class="_do_pdl_trvl_card_btn">
        <a href=" <?php echo  $item['btnLink']['url']?>" style="color : <?php echo $item['buttonTextColorTwo'] ?> ;background: <?php echo $item['buttonBackground']?>">
              <?php echo  $item['buttonTextTwo']?></a>
				</div>
      </div>
      
			<!-- CARD -->
      <?php endforeach; ?>
            <?php endif; ?>
		
		</div>
	</div>