<?php
namespace Elementor;

class SponsorStyleTwo extends Widget_Base {
	
	public function get_name() {
		return 'forehand-sponsor-style-two';
	}
	
	public function get_title() {
		return 'SPONSOR STYLE 2';
	}
	
	public function get_icon() {
		return 'far fa-images';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'textColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ccaf80'
			]
        );
		// background color 
		$this->add_control(
			'sponsorTxt',
			[
				'label' => __( 'Headline', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Sponsorer'
			]
		);

		
        
        $repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'logoTitle', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'logo' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'sponsorLogo', [
				'label' => __( 'Sponsor Logo', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'sponsorLink',
			[
				'label' => __( 'Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		

		$this->add_control(
			'list',
			[
				'label' => __( 'Sponsor logos', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[],
					[],
					[],
				],
				'title_field' => '{{{ logoTitle }}}',
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('sponsorStyleTwoHtml.php');
    }
   
	
	


	
	
}