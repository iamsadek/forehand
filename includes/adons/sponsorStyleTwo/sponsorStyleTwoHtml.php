 
     <!-- Sponser Section -->
     <div class="_tour_sponsors _home_sponsors" style="background: <?php echo $settings['boxBgColor'] ?>">
            <div class="_tour_teams_left">
                <h1 class="_tour_sponsors_title" style="color: <?php echo $settings['textColor'] ?>;"> <?php echo $settings['sponsorTxt'] ?></h1>
            </div>

            <div class="_tour_teams_right">
                <div class="_tour_sponsors_card_all">
                    <!-- Item -->
                    <?php if($settings['list'] ): ?> 
                        <?php foreach($settings['list'] as $item ): ?>
            
                    <div class="_tour_sponsors_card">
                       
                        <a href="<?php  echo $item['sponsorLink']['url'] ?>" class="_tour_sponsors_card_pic">
                            <img class="_tour_sponsors_card_img" src="<?php echo $item['sponsorLogo']['url'] ?>" alt="" title="">
                            
                        </a>
                           
                        
                    </div>
                    <?php endforeach; ?>
                     <?php endif; ?>
            
                   
                </div>
            </div>
        </div>