<?php
namespace Elementor;

class FourImageStyleOne extends Widget_Base {
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name() {
		return 'forehand-four-image-style-one';
	}
	
	public function get_title() {
		return 'FOUR IMAGE STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-picture-o';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
	
		
		$this->add_control(
			'image1', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header1',
			[
				'label' => __( 'Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Träning'
			]
		);
		$this->add_control(
			'subHeader1',
			[
				'label' => __( 'Sub Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => "Herrar, Damer och Juniorer"
			]
		);
		$this->add_control(
			'url1', [
				'label' => __( 'Image 1 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'image2', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header2',
			[
				'label' => __( 'Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Matcher'
			]
		);
		$this->add_control(
			'subHeader2',
			[
				'label' => __( 'Sub Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => "Flera olika klasser"
			]
		);
		$this->add_control(
			'url2', [
				'label' => __( 'Image 2 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'image3', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header3',
			[
				'label' => __( 'Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Seriespel'
			]
		);
		$this->add_control(
			'subHeader3',
			[
				'label' => __( 'Sub Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => "10 divisioner"
			]
		);
		$this->add_control(
			'url3', [
				'label' => __( 'Image 3 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'image4', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'header4',
			[
				'label' => __( 'Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Event'
			]
		);
		$this->add_control(
			'subHeader4',
			[
				'label' => __( 'Sub Header', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => "Gå med i vår businessklubb"
			]
		);

		$this->add_control(
			'url4', [
				'label' => __( 'Image 4 url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);




		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('fourImageStyleOneHtml.php');
    }
   
	
	


	
	
}