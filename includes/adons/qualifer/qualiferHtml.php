<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var qualifersetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageUrl='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsmatchData = json_encode($matchData);
echo "<script type='text/javascript'>var jsmatchData=$jsmatchData</script>";

?>

<qualifer>
	<div v-cloak>

    <div class="_2tree">
            <ul class="_2tree_list" v-if="matchesData.settings.length">
                <li :class="selectedSettingIndex == i?'_active' : ''" v-for="(c, i) in matchesData.settings" :key="i" @click="featchNewClass(c,i)">{{c.className}}</li>
               
            </ul>
            <ul class="_2tree_list" v-if="matchesData.settings[selectedSettingIndex].hasLoserBracket=='yes'">
                <li :class="isWinner?'_active' : ''" @click="changeBracket(true)">A-Slutspel</li>
                <li :class="!isWinner?'_active' : ''" @click="changeBracket(false)">B-Slutspel</li>
            </ul>
            <div class="_2tree_errow">
                <p class="_2tree_errow_icon _pre" style="font-family: Inter, Bangla846, sans-serif;" @click="moveToLeft"><svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.70801 10.585L3.12302 6L7.70801 1.41502L6.29304 -6.18503e-08L0.293038 6L6.29304 12L7.70801 10.585Z" fill="#5D6C71"></path></svg></p>
                <p class="_2tree_errow_icon _next" style="font-family: Inter, Bangla846, sans-serif;" @click="moveToRight"><svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.291992 1.41502L4.87698 6L0.291992 10.585L1.70696 12L7.70696 6L1.70696 8.91824e-07L0.291992 1.41502Z" fill="#FDFDFD"></path></svg></p>
            </div>
            <div class="_2tree_main_all">
                <!---->
                <div class="_2tree_main" v-for="(matches, i) in allMatches" v-if="i>lowerLimit && i<upperLimit">
                    <div class="_2tree_pair" v-for="(m, j) in matches">
                        <div :class=" (m.homeTeamPoint > m.awayTeamPoint) ?  `_2tree_card  ` : ` _2tree_card  _loser`  ">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">{{m.home.teamName}} <span v-if="m.isQualified" class="qualifed">Q</span></p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="`${imageUrl}${m.home.player1.profilePic}`" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.home.player1.firstName}} {{m.home.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="`${imageUrl}${m.home.player1.profilePic}`"  alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.home.player2.firstName}} {{m.home.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;" >{{m.setOne ? m.setOne.split('-')[0] : ''}}</p>
                                    
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setTwo ? m.setTwo.split('-')[0] : ''}}</p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setThree ? m.setThree.split('-')[0] : ''}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="_2tree_details" v-if="m.round!='1'">
                            <p class="_2tree_details_step" style="font-family: ab, Bangla846, sans-serif;">{{m.roundName}}</p>
                            <p class="_2tree_details_match" style="font-family: ab, Bangla846, sans-serif;">Match {{m.id}}</p>
                            <p class="_2tree_details_match" style="font-family: lato, Bangla846, sans-serif;">{{m.playingDate.split('T')[0]}} | {{m.playingTime}} <span v-if="m.playingTime">| {{m.courtName}}</span></p>
                        </div>
                        <div :class="(m.awayTeamPoint > m.homeTeamPoint) ?  `_2tree_card  ` : `_2tree_card  _loser ` " v-if="m.round!='1'" >
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">{{m.away.teamName}} <span v-if="m.isQualified" class="qualifed">Q</span></p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="`${imageUrl}${m.away.player1.profilePic}`" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.away.player1.firstName}} {{m.away.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="`${imageUrl}${m.away.player2.profilePic}`" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.away.player2.firstName}} {{m.away.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;" >{{m.setOne ? m.setOne.split('-')[1] : ''}}</p>
                                    
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setTwo ? m.setTwo.split('-')[1] : ''}}</p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setThree ? m.setThree.split('-')[1] : ''}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!---->


            </div>
        </div>
    
    </div>
</qualifer>

<script>
settings  = JSON.parse(qualifersetting)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"


console.log('matches', jsmatchData)
var list = document.getElementsByTagName("qualifer");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "qualifer-app-" + i);
           var app = new Vue({
           el: "#qualifer-app-" + i,
                data(){
                    return {
                        matchesData : jsmatchData, 
                        upperLimit: 5,
                        lowerLimit: -1,
                        numberOfRightClicks : 0,
                        isWinner : true,
                        allMatches : jsmatchData.matches,
                        selectedSettingIndex : 0
                        
                    }
                },
               
              
               methods: {
                moveToRight(){
                    if(this.allMatches.length-this.numberOfRightClicks == 5) return 
                    this.lowerLimit++
                    this.upperLimit++
                    this.numberOfRightClicks++
                },
                moveToLeft(){
                    if(this.lowerLimit == -1) return 
                    this.lowerLimit--
                    this.upperLimit--
                    this.numberOfRightClicks--
                },
                changeBracket(type){
                    this.isWinner = type
                    if(type){
                        return this.allMatches = this.matchesData.matches
                    }
                    return this.allMatches = this.matchesData.loserMatches
                },
                async featchNewClass(c,i){
                    if(i == this.selectedSettingIndex) return 
                    this.selectedSettingIndex = i
                    const res = await this.callApi('get', `${apiUrl}/qualiferMatchesTree/${c.tournament_id}/${c.id}`)
                    if(res.status==200){
                        this.matchesData.matches = res.data.matches
                        this.matchesData.loserMatches = res.data.loserMatches
                        this.allMatches = res.data.matches
                        this.isWinner = true 
                    }else{
                        console.log('couldnt delete event')
                    }
                },
                async callApi(method, url, dataObj) {
                      try {

                          let data = await axios({
                              headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              method: method,
                              url: url,
                              data: dataObj
                          })
                          return data

                      } catch (e) {

                          return e.response
                      }
                  },



                },
                async created(){
                    
                },
                filters : {
                  
                }



			})

    }
</script>