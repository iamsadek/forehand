<?php
namespace Elementor;

class HeaderStyleFive extends Widget_Base {
	
	public function get_name() {
		return 'forehand-header-5';
	}
	
	public function get_title() {
		return 'HEADER STYLE 5';
	}
	
	public function get_icon() {
		return 'fas fa-heading';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);

		$this->add_control(
			'image', [
				'label' => __( 'Background image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/logo.png',
				],
			]
		);
		
		$this->add_control(
			'welcomeMsg',
			[
				'label' => __( 'Welcome text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'WELCOME TO THE'
			]
		);

		$this->add_control(
			'welcomeMsgColor',
			[
				'label' => __( 'Welcome text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		



		$this->add_control(
			'clubName',
			[
				'label' => __( 'Club name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'World of Padel'
			]
		);
		$this->add_control(
			'clubNameColor',
			[
				'label' => __( 'Club name text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#333333'
			]
		);
		$this->add_control(
			'welcomeMsgBg',
			[
				'label' => __( 'Welcome text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);

		$this->add_control(
			'newsTitle',
			[
				'label' => __( 'News title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'It is a long established fact.'
			]
		);
		$this->add_control(
			'newsBg',
			[
				'label' => __( 'News background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'newsTitleTxtColor',
			[
				'label' => __( 'News title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'authorImage', [
				'label' => __( 'Author profile image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/logo.png',
				],
			]
		);
		$this->add_control(
			'authorName',
			[
				'label' => __( 'Author name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'David Watson'
			]
		);
		$this->add_control(
			'authorTxtColor',
			[
				'label' => __( 'Author text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);


		
		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        
		$settings = $this->get_settings_for_display();
	
        require('headerStyleFiveHtml.php');
  
      

	}

	// js render
	
	


	
	
}