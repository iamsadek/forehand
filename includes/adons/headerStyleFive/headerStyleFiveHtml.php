<!-- BANNER SECTION -->
<div class="_bnr_wrld_section" style="background: url(<?php echo $settings['image']['url'] ?>) no-repeat center center;">

<div class="_bnr_wrld_top" style="background : <?php echo $settings['welcomeMsgBg'] ?>">
    <p style="color : <?php echo $settings['welcomeMsgColor'] ?>"><?php echo $settings['welcomeMsg'] ?></p>
    <h3 style="color : <?php echo $settings['clubNameColor'] ?>"><?php echo $settings['clubName'] ?></h3>
</div>

<div class="_bnr_wrld_info" style="background : <?php echo $settings['newsBg'] ?>">
    <div class="_bnr_wrld_info_txt">
        <p style="color : <?php echo $settings['newsTitleTxtColor'] ?>"><?php echo $settings['newsTitle'] ?></p>
    </div>
    <div class="_bnr_wrld_info_img">
        <div class="_bnr_wrld_img">
            <img src="<?php echo $settings['authorImage']['url'] ?>" alt="image">
        </div>
        <div class="_bnr_wrld_img_nme">
            <p style="color : <?php echo $settings['authorTxtColor'] ?>"><?php echo $settings['authorName'] ?></p>
        </div>
    </div>
</div>
</div>
<!-- BANNER SECTION -->
