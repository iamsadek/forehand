<?php
namespace Elementor;
require_once(BASEPLUGIN_INCLUDES . '/common/helperclass.php');

class TimelineEventStyleOne extends Widget_Base {
	use Helper;
	public function get_name() {
		return 'forehand-timeline-event-style-oamne';
	}
	
	public function get_title() {
		return 'EVENT TIMELINE STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		$events = $this->callApi('getTimeLineNames');
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$optinsAndDefault = $this->getOptionsAndDefaultValues($events, 'eventName');
		
		$this->add_control(
			'event_timeline_1_id',
			[
				'label' => __( 'Select the event', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => $optinsAndDefault[1],
				'options' => $optinsAndDefault[0],
			]
		);

		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Schedule'
			]
		);
		$this->add_control(
			'subtitle',
			[
				'label' => __( 'Sub Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => '1 day program'
			]
		);
		$this->add_control(
			'headline',
			[
				'label' => __( 'Headline', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Day 1'
			]
		);
		
		// $this->add_control(
		// 	'dateBg',
		// 	[
		// 		'label' => __( 'Date background color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'white'
		// 	]
		// );
		
		// $this->add_control(
		// 	'dateTxtColor',
		// 	[
		// 		'label' => __( 'Date text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'black'
		// 	]
		// );
		// $this->add_control(
		// 	'primaryTxtColor',
		// 	[
		// 		'label' => __( 'Primary text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'white'
		// 	]
		// );
		// $this->add_control(
		// 	'secondaryTxtColor',
		// 	[
		// 		'label' => __( 'Secondary text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => '#9c9b9b;'
		// 	]
		// );
		// $this->add_control(
		// 	'boxBackground',
		// 	[
		// 		'label' => __( 'Box background color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => '#222222;'
		// 	]
		// );
		// $this->add_control(
		// 	'shadingColor',
		// 	[
		// 		'label' => __( 'Box shading color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'rgb(34, 34, 34);'
		// 	]
		// );
		// $this->add_control(
		// 	'timeLineDotColor',
		// 	[
		// 		'label' => __( 'Timeline dot background color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'white'
		// 	]
		// );
		// $this->add_control(
		// 	'timeLineLineColor',
		// 	[
		// 		'label' => __( 'Timeline line color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => '#9e9e9e;'
		// 	]
		// );
		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $calendarData = $this->getCalendarData($settings['event_timeline_1_id']);
        require('timelineEventStyleOneHtml.php');
    }
    public function getCalendarData($id){
        $response = wp_remote_request( FOREHAND_API_URL."/timelineSingle/$id",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
    }

	// js render
	
	


	
	
}