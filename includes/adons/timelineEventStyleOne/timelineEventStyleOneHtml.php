<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
// $imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsCalenderData = json_encode($calendarData);

echo "<script type='text/javascript'>var jsCalenderData=$jsCalenderData</script>";

?>




<timelineeventstyleone>
    <div v-cloak>
	<!-- TIMELINE SHEDULE -->
	<div class="_tmln_shdl_sec">
		<div class="_tmln_shdl_top">
			<h3><?php echo $settings['title'] ?></h3>
			<p><?php echo $settings['subtitle'] ?></p>
		</div>

		

		<div class="_tmln_shdl_crd_main">
			<div class="_tmln_shdl_crd_all">
				<!-- CARD -->
				<div class="_tmln_shdl_crd">
					<div class="_tmln_shdl_crd_inner">
						<div class="_tmln_shdl_crd_day">
							<p><?php echo $settings['headline'] ?></p>
						</div>
						

						<!-- ITEAM -->
						<div class="_tmln_shdl_crd_itm" v-for="(t, i) in timeLine" v-if="timeLine.length">
							<div class="_tmln_shdl_itm_lft">
								<p :style="`color:${t.color}`">{{t.time}}</p>
							</div>
							<!-- <div v-html="filterColor(t.color,`before-${i}`)"></div> -->
							
							<div class="_tmln_shdl_itm_r8">
								<div class="_tmln_shdl_itm_r8_inner">
									<div class="_tmln_shdl_itm_r8_one _title">
										<h4 :style="`color:${t.color}`" :class="`before-${i}`">
											<span class="bigDot" :style="`background: ${t.color}`"></span>	{{t.title}}
										</h4>
										<p class="_tmln_shdl_itm_txt2" v-if="t.titleSummary !=''">
											{{t.titleSummary}}
										</p>
									</div>
									
								
									<div class="_tmln_shdl_itm_r8_one" v-for="(s,k) in t.subTimelines" v-if="t.subTimelines.length && s.subTitle!=''">
										<ul class="_tmln_shdl_itm_ul_one" v-if="s.subTitleSummary ==''">
											<li class="subtitle">{{s.subTitle}}</li>
											<li v-if="s.subTitleInfo">{{s.subTitleInfo}}</li>
											
											
										</ul>
										<div class=""  v-if="s.subTitleSummary !=''">
											<p class="_tmln_shdl_itm_txt1">
												{{s.subTitle}}
											</p>
											<p class="_tmln_shdl_itm_txt2">
												{{s.subTitleSummary}}
											</p>
										</div>	
										
									</div>
								</div>
							</div>
						</div>
						<!-- ITEAM -->

						

						<!-- ITEAM -->
						<!-- <div class="_tmln_shdl_crd_itm _tmln_shdl_dnr">
							<div class="_tmln_shdl_itm_lft">
								<p>12:30</p>
							</div>
							<div class="_tmln_shdl_itm_r8">
								<div class="_tmln_shdl_itm_r8_inner">
									<div class="_tmln_shdl_itm_r8_one">
										<h4>Dinner with mingle</h4>
									</div>	
								</div>
							</div>
						</div> -->
						<!-- ITEAM -->
					</div>
				</div>
				<!-- CARD -->
			</div>

			
		</div>
	</div>

	<!-- TIMELINE SHEDULE -->    
    </div>
</timelineeventstyleone>



<script type="text/javascript">

axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
	// console.log(jsCalenderData)
	// var jsdata = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    
    // jsCalenderData  = JSON.parse(jsdata)
    var list = document.getElementsByTagName("timelineeventstyleone");
    console.log(list)

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "timelineeventstyleone-app-" + i);
            var app = new Vue({
            el: "#timelineeventstyleone-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting,
                        jsCalenderData: jsCalenderData,
						timeLine : jsCalenderData[0].timeLine,
						imageSource: imageSource, 
                        apiUrl : apiUrl, 
                       

                    }
                },
				methods: {
					filterColor(color,className){
						console.log(color)
						console.log(className)
						return `
							<style>
							.${className}:after{
								background : ${color};
							}
							</style>
						`
					}
				},
               
                created(){
					console.log('data is', jsCalenderData)
				},


            })

        }
 </script>

