<?php
namespace Elementor;

class TripCoachStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-tripoach-text-style-one';
	}
	
	public function get_title() {
		return 'TRIP COACH STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
	
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Coaches' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'headerOneBackground',
			[
				'label' => __( 'Title background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

	  
		$repeater = new \Elementor\Repeater();



        $repeater->add_control(
			'sectionBackgroundTwo',
			[
				'label' => __( 'Section background two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'headerTwo', [
				'label' => __( 'Text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'HEAD PRO' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'headerTwoColor',
			[
				'label' => __( 'Text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'headerTwoBackground',
			[
				'label' => __( 'Text one background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'firstName', [
				'label' => __( 'First name', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Gabriel' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'firstNameTextColor',
			[
				'label' => __( 'First name text  color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'lastName', [
				'label' => __( 'Last name', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'McGrath' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'lastNameTextColor',
			[
				'label' => __( 'Last name text  color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'phoneNumber', [
				'label' => __( 'Phone number', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '0718-900453' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'phoneNumberColor',
			[
				'label' => __( 'Phone number color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'phoneNumberBg',
			[
				'label' => __( 'Phone number background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'emailText', [
				'label' => __( 'Email', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'gabrialmcgrath@gmail.com' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'emailTextColor',
			[
				'label' => __( 'Email text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'emailTextBg',
			[
				'label' => __( 'Email text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'titleCoachDetails', [
				'label' => __( 'Title coach details', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'About Gabriel' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'titleCoachDetailsColor',
			[
				'label' => __( 'Title coach details text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachDetails', [
				'label' => __( 'Coach details', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'default' => __( 'Gabriel is one of the top coach in our club. Last highlight was the Master Final of the WPT (World Padel Tour) year celebrated in Barcelona. Most famous clubs all around the world host the WPT tournaments where best players collect points for ranking.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		$repeater->add_control(
			'coachEducationTitle', [
				'label' => __( 'Title coach education', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Education' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachEducationTitleColor',
			[
				'label' => __( 'Title coach education text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachEduTextOne', [
				'label' => __( 'Education list one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Magna Cum Laude' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachEduTextOneColor',
			[
				'label' => __( 'Education list one text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachEduTextTwo', [
				'label' => __( 'Education list two', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Harvard' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachEduTextTwoColor',
			[
				'label' => __( 'Education list two text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachEduTextThree', [
				'label' => __( 'Education list three', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Finance' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachEduTextThreeColor',
			[
				'label' => __( 'Education list three text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachEduTextFour', [
				'label' => __( 'Education list four', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Magna Cum Laude' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachEduTextFourColor',
			[
				'label' => __( 'Education list four text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachEduTextFive', [
				'label' => __( 'Education list five', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Harvard' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachEduTextFiveColor',
			[
				'label' => __( 'Education list five text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachEduTextSix', [
				'label' => __( 'Education list six', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Finance' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachEduTextSixColor',
			[
				'label' => __( 'Education list six text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$repeater->add_control(
			'', [
				'label' => __( 'Title coach accomplishment', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Accomplishments' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachAccTitleColor',
			[
				'label' => __( 'Title coach accomplishment text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachAccTextOne', [
				'label' => __( 'Accomplishment list one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Magna Cum Laude' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachAccTextOneColor',
			[
				'label' => __( 'Accomplishment list one text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachAccTextTwo', [
				'label' => __( 'Accomplishment list two', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Harvard' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachAccTextTwoColor',
			[
				'label' => __( 'Accomplishment list two text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'coachAccTextThree', [
				'label' => __( 'Accomplishment list three', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Finance' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'coachAccTextThreeColor',
			[
				'label' => __( 'Accomplishment list three text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'subSection', [
				'label' => __( 'Sub section', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'This is the model of a sub section if we have to add anything else.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'subSectionColor',
			[
				'label' => __( 'Sub section text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
	
		
		$this->add_control(
			'coachDetails1',
			[
				'label' => __( 'Coach details', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
				
					
				],
				
			]
		);

		$this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('tripCoachStyleOneHtml.php');
    }
   
	
	


	
	
}