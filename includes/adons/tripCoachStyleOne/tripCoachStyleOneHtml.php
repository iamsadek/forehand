<?php
$jsSettings = json_encode($settings);
echo "<script> var jsSettings =  $jsSettings  </script>";

?>
<tripcoachstyleone>

<div v-cloak>

<div>
     <div class="_do_pdl4_coach"  style="background-color: <?php echo $settings['sectionBackground'] ?>;">
		<div class="_do_pdl4_coach_top">
		<p style="color : <?php echo $settings['headerOneColor'] ?>
       ;background: <?php echo $settings['headerOneBackground'] ?>" >
           <?php echo $settings['headerOne'] ?>
        </p>
		</div>
		<div class="_do_pdl4_coach_main_all">
      <div class="_do_pdl4_coach_silder_all">
        <div class="_do_pdl4_coach_silder">
          <div class="_do_pdl4_coach_main" :style="{ transform: 'translate3d('+translateValue+'%, 0px, 0px)'  }" >
            <div class="_do_pdl4_coach_lft_main"  v-if="settings.coachDetails1.length" v-for="(img, i)  in settings.coachDetails1" >

              <div class="_do_pdl4_coach_lft">
                <div class="_do_pdl4_coach_lft_img" >
				         <img :src="img.image.url" alt="image">
                </div>
                <div class="_do_pdl4_coach_dtls" :style="{background: img.sectionBackgroundTwo}">
                  <div class="_do_pdl4_coach_dtls_one">
                    <div class="_do_pdl4_coach_dtls_top">
                      <p :style="{color: img.headerTwoColor, background: img.headerTwoBackground}" >{{img.headerTwo}}</p>
                      <h3 :style="{color: img.firstNameTextColor}">
                      {{img.firstName}} <span :style="{color: img.lastNameTextColor}">{{img.lastName}}</span>
                      </h3>
                      <ul>
                        <li :style="{color: img.phoneNumberColor, background: img.phoneNumberBg}" v-if="img.phoneNumber" >{{img.phoneNumber}}</li>
                        <li :style="{color: img.emailTextColor, background: img.emailTextBg}" v-if="img.emailText">{{img.emailText}}</li>
                      </ul>
                    </div>

                    <div class="_do_pdl4_coach_dtls_txt">

                      <p class="_do_pdl4_coach_dtls_txt_p" v-html="img.coachDetails">
                        </p>
                        <div class="_do_pdl4_coach_dtls_titl">
                          <p class="_do_pdl4_coach_dtls_titl_p" :style="{color: img.titleCoachDetailsColor}" v-html="img.titleCoachDetails"></p>
                        </div>
                    </div>
                        </div>
                  <div class="_do_pdl4_coach_dtls_two">
                    <div class="_do_pdl4_coach_dtls_itm_all">
                      <div class="_do_pdl4_coach_dtls_itm">
                        <div class="_do_pdl4_coach_dtls_sngl" v-if="img.coachEduTextOne">
                          <ul>
                            <li :style="{color: img.coachEduTextOneColor}" v-if="img.coachEduTextOne">{{img.coachEduTextOne}}</li>
                            <li :style="{color: img.coachEduTextTwoColor}" v-if="img.coachEduTextTwo">{{img.coachEduTextTwo}}</li>
                            <li :style="{color: img.coachEduTextThreeColor}" v-if="img.coachEduTextThree">{{img.coachEduTextThree}}</li>
                          </ul>
                          </div>
                        <div class="_do_pdl4_coach_dtls_sngl">
                          <ul>
                            <li :style="{color: img.coachEduTextFourColor}" v-if="img.coachEduTextFour">{{img.coachEduTextFour}} </li>
                            <li :style="{color: img.coachEduTextFiveColor}" v-if="img.coachEduTextFive">{{img.coachEduTextFive}}</li>
                            <li :style="{color: img.coachEduTextSixColor}" v-if="img.coachEduTextSix">{{img.coachEduTextSix}}</li>
                          </ul>
                        </div>
                      </div>
                      <div class="_do_pdl4_coach_dtls_sngl_titl">
                          <p class="_do_pdl4_coach_lft_rcnt" :style="{color: img.coachEducationTitleColor}">{{img.coachEducationTitle}}</p>
                        </div>
                    </div>
                    <div class="_do_pdl4_coach_dtls_itm_all">
                      <div class="_do_pdl4_coach_dtls_itm">
                        <div class="_do_pdl4_coach_dtls_sngl" v-if="img.coachAccTextOne">
                          <ul>
                            <li :style="{color: img.coachAccTextOneColor}">{{img.coachAccTextOne}}</li>
                          </ul>
                        </div>
                        <div class="_do_pdl4_coach_dtls_sngl" v-if="img.coachAccTextTwo">
                          <ul>
                            <li :style="{color: img.coachAccTextTwoColor}">{{img.coachAccTextTwo}} </li>
                          </ul>
                        </div>
                        <div class="_do_pdl4_coach_dtls_sngl" v-if="img.coachAccTextThree">
                          <ul>
                            <li :style="{color: img.coachAccTextThreeColor}">{{img.coachAccTextThree}}</li>
                          </ul>
                          <p :style="{color: img.subSectionColor}">{{img.subSection}}</p>
                        </div>
                      </div>
                      <div class="_do_pdl4_coach_dtls_sngl_titl" v-if="img.coachAccTitle">
                          <p class="_do_pdl4_coach_lft_rcnt" :style="{color: img.coachAccTitleColor}">{{img.coachAccTitle}}</p>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- next pre -->
        <div  @click="changeImagePre" class="_next_pre _pre" v-if="settings.coachDetails1.length > 1">
          <
        </div>

        <div class="_next_pre _next" @click="changeImage"  v-if="settings.coachDetails1.length > 1" >
          >
        </div>
        <!-- next pre -->
      </div>

			<!-- <div class="_do_pdl4_coach_btm">
				<ul>
					<li class="_do_pdl4_coach_actv"></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div> -->
		</div>
	</div>
  </div>


</div>

</tripcoachstyleone>


	<script type="text/javascript">

	var coachDetails1 = document.getElementsByTagName("tripcoachstyleone");



        for (var i = 0; i < coachDetails1.length; i++) {
            coachDetails1[i].setAttribute("id", "tripcoachstyleone-app-" + i);
            var app = new Vue({
            el: "#tripcoachstyleone-app-" + i,
                data(){
                    return {
                        settings: jsSettings,

                        clickNumbers : 1,
                        translateValue: 0,




                      }

                  },






	 methods : {
       changeImage(s1,h1,h2,h3,f1,f2,l1,l2,p1,p2,p3){ // increase
          if(this.clickNumbers == this.settings.coachDetails1.length - 1){
              this.translateValue = 0
              this.clickNumbers = 1
              return


          }
          this.translateValue = -this.clickNumbers*69
          this.clickNumbers++



          console.log('increament',this.translateValue)
       },
       changeImagePre(){ // going back
         if(this.clickNumbers == 1) return
         let newNumber = this.translateValue + 69

          this.translateValue = newNumber
          console.log('decreament',this.translateValue)
          this.clickNumbers--
       }
     },
               created (){

				if(this.settings.coachDetails1.length > 2){
					var repeatedImages = new Array(3).fill(this.settings.coachDetails1).flat();
            this.settings.coachDetails1 = repeatedImages
            this.settings.coachDetails1.push(repeatedImages[0])
					}

					}
            })
        }

 </script>

