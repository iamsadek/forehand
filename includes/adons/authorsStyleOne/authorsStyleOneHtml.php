<div class="_evnt2_trnr_sec" style="background: <?php echo $settings['boxBgColor']?>">
            <div class="_evnt2_trnr_lft">
                <h3 class="_evnt2_trnr_lft_h3" style="color: <?php echo $settings['txtColor']?>">
                    <?php echo $settings['headingTxt'] ?>
                </h3>
                <!-- <p class="_evnt2_trnr_lft_p">
                    14
                </p> -->
            </div>
            <div class="_evnt2_trnr_r8">
            <?php if($settings['list'] ): ?> 
                <?php foreach($settings['list'] as $item ): ?>
                    <!-- ITEAM -->
                    <div class="_evnt2_trnr_itm">
                        <div class="_evnt2_trnr_itm_img">
                            <a href="<?php echo $item['authorLink']['url'] ?>">
                                <img src="<?php echo $item['authorImg']['url'] ?>" alt="image">
                            </a>
                        </div>
                        <div class="_evnt2_trnr_itm_info">
                            <h3 class="_evnt2_trnr_itm_h3" style="color: <?php echo $settings['txtColor']?>">
                                <?php echo $item['authorName']?>
                            </h3>
                            <p class="_evnt2_trnr_itm_p" style="color: <?php echo $settings['proTxtColor']?>; background:<?php echo $settings['proTxtBg']?>; ">
                                <?php echo $item['proTxt']?>
                            </p>
                            <div class="textDiv">
                                <p class="_normaltxt" style="color: <?php echo $settings['emailColors']?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $item['email']?></p>
                                <p class="_normaltxt" style="color: <?php echo $settings['emailColors']?>"><i class="fa fa-phone-square" aria-hidden="true"></i> <?php echo $item['phone']?></p>
                            </div>
                            


                        </div>
                    </div>
                    <!-- ITEAM -->
                <?php endforeach; ?>
            <?php endif; ?>
              
            </div>
        </div>