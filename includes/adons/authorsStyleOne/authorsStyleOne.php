<?php
namespace Elementor;

class AuthorsStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-author-style-one';
	}
	
	public function get_title() {
		return 'AUTHOR STYLE 1';
	}
	
	public function get_icon() {
		return 'far fa-images';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// background color 
		$this->add_control(
			'headingTxt',
			[
				'label' => __( 'Headline', 'elementor' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => 'Våra tränare'
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		$this->add_control(
			'txtColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#545353;'
			]
		);
		$this->add_control(
			'emailColors',
			[
				'label' => __( 'Email and phone colors', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#545353;'
			]
		);
		$this->add_control(
			'proTxtColor',
			[
				'label' => __( 'Pro text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'proTxtBg',
			[
				'label' => __( 'Pro text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
        
        $repeater = new \Elementor\Repeater();

		
		
		$repeater->add_control(
			'authorImg', [
				'label' => __( 'Author profile', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'authorLink',
			[
				'label' => __( 'Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$repeater->add_control(
			'authorName', [
				'label' => __( 'Pro text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Author Jone' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'proTxt', [
				'label' => __( 'Pro text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Pro' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		$repeater->add_control(
			'email', [
				'label' => __( 'Email', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'example@domain.com' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'phone', [
				'label' => __( 'Phone', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '+123223434' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		
		
		

		$this->add_control(
			'list',
			[
				'label' => __( 'Courses', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'authorImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorLink' => '',
						'authorName' => 'Author Jone',
						'proTxt' => 'Pro',
						'email' => 'example@domain.com',
						'phone' => '+123223434',

					],
					[
						'authorImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorLink' => '',
						'authorName' => 'Author Jone',
						'proTxt' => 'Pro',
						'email' => 'example@domain.com',
						'phone' => '+123223434',

					],
					[
						'authorImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorLink' => '',
						'authorName' => 'Author Jone',
						'proTxt' => 'Pro',
						'email' => 'example@domain.com',
						'phone' => '+123223434',

					],
				],
				'title_field' => '{{{ authorName }}}',
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('authorsStyleOneHtml.php');
    }
   
	
	


	
	
}