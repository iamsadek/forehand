<?php 
    // settings to js data 
    $jsSettings = json_encode($settings);
    
    // calendar data in js var... 
    $jsCalenderData = json_encode($calendarData);
    echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";

   

?>
<calendarstyletwo >
   <div v-cloak>
   <div class="_matchi1_section">
        <!-- ITEAM -->
        <div class="_matchi1_panel" v-if="timeArray.length">
          
		
		
           <!-- right -->
          <div class="_matchi1_panel_r8">
            <div class="_matchi1_panel_r8_box">
              <ul>
                <li v-for="(s, i) in timeArray" :class="s.isSelected ? '_matchi1_box_active' : '' " @click="changeCourt(s,i)">
                  <div class="_matchi1_box">
                    <p>{{s.startHour}} <span>{{s.startMin}}</span> </p>
                  </div>
                </li>
               
              </ul>
            </div>
            <div class="_matchi1_panel_r8_dtls" v-if="courts.length">
              <ul>
                <li>
                  <h3>Available times <?php echo date("l"); ?>, <?php echo date("j"); ?> <?php echo date("M"); ?> <?php echo date("Y"); ?> at {{startHour}}<span>{{startMin}}</span></h3>
                </li>
                <li v-for="(c, index) in courts" v-if="courts.length">
                  <table width="100%">
                    <tbody>
                      <tr class="itemss">
                        <td width="45%">      
                            {{c.court.name}}
                        </td>
                        <td width="15%">{{((new Date(c.end)-new Date(c.start))/1000)/60}} min</td>
                        <td width="20%">
                          <p>{{c.court.sport.type}}</p>
                          <p><small>{{c.court.surface}}</small></p>
                        </td>
                        <td width="20%" class="_matchi1_txt_r8">
                          <p>320 SEK</p>
                           <a :href="c.href" target="_blank" class="btn btn-success btn-sm">
                              Book
                           </a>                  
                        </td>         
                      </tr>
                    </tbody>
                  </table>
                </li>
                
              </ul>
            </div>
            <!-- <div class="_matchi1_panel_r8_btm">
              <i class="fas fa-info-circle text-primary" aria-hidden="true"></i> 
              <p>Can't find the slot you are looking for? </p>
              <strong>
                <a href="">Enter waiting list</a>
              </strong>
            </div> -->
         </div>
           <!-- right -->
		
		</div>
		

        </div>  
         <!-- ITEAM -->
		 <div v-if="!timeArray.length">
			<h1>There is no booking data found. Check if Facility ID is correct.</h1>
		</div>

        
      </div>
   
   </div>
</calendarstyletwo>



<script type="text/javascript">
   
	
	let data = JSON.parse(jsCalenderData)
	let id = 1
	let timeArray = []
	let courts = []
	let startHour
	let startMin
	for(let key in data){
		let d = new Date(key)
		let obj = {
			isSelected :  false,
			startHour: d.getHours() < 10 ? `0${d.getHours()}` : d.getHours(), 
			startMin: d.getMinutes() < 10 ? `0${d.getMinutes()}` : d.getMinutes(),
			key: key, 
			courtsData: data[key], 
			key: key
		}
		
		timeArray.push(obj)
		if(id==1){
			//courts =  data[key]
			startHour = obj.startHour
			startMin = obj.startMin 
		}
		id++
	}
	console.log(timeArray)
	var list = document.getElementsByTagName("calendarstyletwo");
    //console.log(list)
 
        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "calendarstyletwo-app-" + i);
            var app = new Vue({
            el: "#calendarstyletwo-app-" + i,
                data(){
                    return {
                        timeArray : timeArray,
						courts : courts,
						lastIndex : 0, 
						data: data, 
						startHour: startHour,
						startMin: startMin,
                       
                    }
                }, 
                methods: {
					changeCourt(s,i){
						console.log(this.timeArray[i])
						this.startHour = this.timeArray[i].startHour
						this.startMin = this.timeArray[i].startMin
						this.courts = s.courtsData
						this.timeArray[this.lastIndex].isSelected = false 
						this.timeArray[i].isSelected = true 
						this.lastIndex = i 
					}
                   
                },
                created(){
                  
                }, 
               
                
            })

        }
 </script>

