<?php
namespace Elementor;


class MatchiBookingOne extends Widget_Base {
	
	public function get_name() {
		return 'MATCHI-BOOKING-1';
	}
	
	public function get_title() {
		return 'MATCHi BOOKING ONE';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Matchi' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// news title
		$this->add_control(
			'facilityId',
			[
				'label' => __( 'Facility ID', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::NUMBER,
				'placeholder' => __( 'Enter your Facility ID', 'elementor' ),
				'default' => 1
			]
		);
		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $calendarData = $this->getCalendarData($settings['facilityId']);
        require('matchiBookingOneHtml.php');
    }
    public function getCalendarData($id){
		$date =  date("Y-m-d");
        $response = wp_remote_request( "https://dev-api.matchi.se/pub/v1/rest/facility/$id/slots/date/$date",
            array(
				'method'     => 'GET',
				'headers' => [
					'Authorization' => 'Basic ' . base64_encode( 'michael@forehand.se:cgzy4nJwCuqVisqDQGey' ),
				]
            )
		);
		if($response['response']['code']!=200){
			return [];
		}
		$body = wp_remote_retrieve_body($response);
		$s=json_decode($body, true);
		
		
		if(count($s) < 1){
			return [];
		}
		$result = array();
		$key = 'start';
		
		$s= $s['slots'];
		
		$arr = array();
        
		foreach ($s as $k => $item) {
			$arr[$item[$key]][] = $item;
			
		}
		
		return $arr;
        return json_decode($result);
    }

	// js render
	
	


	
	
}