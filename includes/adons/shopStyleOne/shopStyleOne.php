<?php
namespace Elementor;

class ShopStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-shop-style-one';
	}
	
	public function get_title() {
		return 'SHOP STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-shopping-basket';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'headingTxt',
			[
				'label' => __( 'Headline', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Shop'
			]
		);
		$this->add_control(
			'headingTxtColor',
			[
				'label' => __( 'Heading text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'viewmore',
			[
				'label' => __( 'View all text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'View All'
			]
		);
		$this->add_control(
			'viewmoreLink',
			[
				'label' => __( 'View all Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'viewmoreColor',
			[
				'label' => __( 'View more text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#4f8989'
			]
		);
		
		
		
		
        
        $repeater = new \Elementor\Repeater();

		
		
		$repeater->add_control(
			'productImg', [
				'label' => __( 'Product image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'productTitle', [
				'label' => __( 'Product title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Padel Nyhet' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		$repeater->add_control(
			'productSubTitle', [
				'label' => __( 'Product sub title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Nike Lima' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		$repeater->add_control(
			'price', [
				'label' => __( 'Product price', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'default' => __( 500 , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'position',
			[
				'label' => __( 'Price alignment', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'top-right',
				'options' => [
					'top-right'  => __( 'Top right', 'plugin-domain' ),
					'bottom-right' => __( 'Bottom right', 'plugin-domain' ),
					
					
				],
			]
		);


		$repeater->add_control(
			'productBg',
			[
				'label' => __( 'Product background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#3c3a3a'
			]
		);
		$repeater->add_control(
			'productTitleColor',
			[
				'label' => __( 'Product title color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		$repeater->add_control(
			'productSubTitleColor',
			[
				'label' => __( 'Product sub title color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$repeater->add_control(
			'productPriceColor',
			[
				'label' => __( 'Product price text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'red'
			]
		);
		$repeater->add_control(
			'productPriceBgColor',
			[
				'label' => __( 'Product price text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		
		
		

		$this->add_control(
			'list',
			[
				'label' => __( 'Products', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'productImg' => \Elementor\Utils::get_placeholder_image_src(),
						'productTitle' => 'Padel Nyhet',
						'productSubTitle' => 'Nike Lima',
						'price' => 500,
						'position' => 'top-right',
						'productBg' => '#3c3a3a',
					],
					[
						'productImg' => \Elementor\Utils::get_placeholder_image_src(),
						'productTitle' => 'Padel Nyhet',
						'productSubTitle' => 'Nike Lima',
						'price' => 500,
						'position' => 'top-right',
						'productBg' => '#3c3a3a',
					],
					[
						'productImg' => \Elementor\Utils::get_placeholder_image_src(),
						'productTitle' => 'Padel Nyhet',
						'productSubTitle' => 'Nike Lima',
						'price' => 500,
						'position' => 'top-right',
						'productBg' => '#3c3a3a',
					],
					[
						'productImg' => \Elementor\Utils::get_placeholder_image_src(),
						'productTitle' => 'Padel Nyhet',
						'productSubTitle' => 'Nike Lima',
						'price' => 500,
						'position' => 'top-right',
						'productBg' => '#3c3a3a',
					],
				],
				'title_field' => '{{{ productTitle }}}',
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('shopStyleOneHtml.php');
    }
   
	
	


	
	
}