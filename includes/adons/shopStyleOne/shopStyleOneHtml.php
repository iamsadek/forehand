<div class="_shop_sec" style="background: <?php echo $settings['boxBgColor'] ?>">
        <div class="_shop_sec_top">
            <h3 class="_shop_top_h3" style="color: <?php echo $settings['headingTxtColor'] ?>">
                <?php echo $settings['headingTxt'] ?>
            </h3>
           
        </div>
        <div class="_shop_crd_all">

            <?php if ($settings['list']): ?>
                <?php foreach ($settings['list'] as $item): ?>
                    <!-- CARD -->
                    <div class="_shop_crd _shop_crd_clr1" style="background: <?php echo $item['productBg'] ?> !important">
                        <div class="_shop_crd_btm_r8">
                            <?php if($item['position']=='top-right') : ?>
                                <h3 class="_shop_crd_r8_btm_h3" style="background: <?php echo $item['productPriceBgColor'] ?>;color: <?php echo $item['productPriceColor'] ?>;">
                                    <?php echo $item['price'] ?> kr
                                </h3>
                            <?php endif;?>
                            <?php if($item['position']=='bottom-right') : ?>
                                <h3 class="_shop_crd_r8_btm_h3" style="background: <?php echo $item['productPriceBgColor'] ?>;color: <?php echo $item['productPriceColor'] ?>;">
                                    <?php echo $item['price'] ?> kr
                                </h3>
                            <?php endif;?>
                            


                        </div>
                        <div class="_shop_crd_img" >
                            <img src="<?php echo $item['productImg']['url'] ?>" alt="image">
                        </div>

                        <div class="_shop_crd_btm">
                            <div class="_shop_crd_btm_lft">
                                <h3 class="_shop_crd_btm_h3" style="color: <?php echo $item['productTitleColor'] ?>">
                                    <?php echo $item['productTitle'] ?>
                                </h3>
                                <p class="_shop_crd_btm_p" style="color: <?php echo $item['productSubTitleColor'] ?>">
                                    <?php echo $item['productSubTitle'] ?>
                                </p>
                            </div>
                            
                        </div>
                    </div>
                    <!-- CARD -->
            <?php endforeach;?>
            <?php endif;?>






        </div>
    </div>