<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
$eventData =addslashes(json_encode($eventData,JSON_HEX_APOS | JSON_HEX_QUOT));
echo "<script type='text/javascript'>var eventData='$eventData'</script>";


?>



<bracketsingups>
<div v-cloak>

<div class="_tour_teams" >
    <ul class="_2tree_list classes_lists" v-if="classes.length">
        <li :class="classIndex == c.id?'_active' : ''" v-for="(c, i) in classes" :key="i" @click="changeClass(c)">{{c.className}}</li>
    </ul>

    <div class="_tour_teams_left" v-if="trData">
        <h1 class="_tour_teams_left_title">{{trData[classIndex][0].totalTeams}} {{trData[classIndex][0].totalTeams == 1 ? 'Team' : 'Teams'}}</h1>
        <p class="_tour_teams_left_sign">signed up for</p>
        <p class="_tour_teams_left_tour">{{trData[classIndex][0].className}}</p>

        <div class="_tour_teams_date">
           
            <p class="_tour_teams_date_text" >{{trData[classIndex][0].remainingSlots}} slots <span>remaining  </span></p>
        </div>
    </div>
    <div class="_tour_teams_right" v-if="trData">
        <div class="_tour_teams_errow _pre qft qftSingup" @click="goLeft(trData[classIndex])" v-if="trData[classIndex].length > 5"><i class="fas fa-chevron-left"></i></div>
        <div class="_tour_teams_errow _next qft" @click="goRight(trData[classIndex])" v-if="trData[classIndex].length > 5"><i class="fas fa-chevron-right"></i></div>
        <div class="_tour_teams_card_all" :style="{ transform: 'translate3d('+trData[classIndex][0].translateValue+'%, 0px, 0px)'  }">

            <!-- Item -->
            <div class="_tour_teams_card" v-for="(t,i) in trData[classIndex]" >
                <p class="_tour_teams_card_title">{{t.team.teamName}}</p>
                <div class="_tour_teams_card_pic_main">
                    <div class="_tour_teams_card_pic">
                        <img class="_tour_teams_card_img" :src="imageSource+t.team.player1.profilePic" alt="" title="">
                    </div>

                    <div class="_tour_teams_card_pic">
                        <img class="_tour_teams_card_img" :src="imageSource+t.team.player2.profilePic" alt="" title="">
                    </div>
                </div>

                <div class="_tour_teams_card_name_main">
                    <p class="_tour_teams_card_name">
                        <strong>{{t.team.player1.firstName}}</strong>
                        <span>{{t.team.player1.lastName}}</span>
                    </p>

                    <p class="_tour_teams_card_and">&amp;</p>

                    <p class="_tour_teams_card_name _two">
                        <strong>{{t.team.player2.firstName}}</strong>
                        <span>{{t.team.player2.lastName}}</span>
                    </p>
                </div>

                <!-- <p class="_tour_teams_card_class">{{t.class.className}}</p> -->
            </div>
            <!-- Item -->

        </div>
    </div>

    
</div>

      
</bracketsingups>


  
<script type="text/javascript">
    console.log(' ok doc')
    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
    //eventData = eventData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    //console.log('bregore',eventData);
    eventData = JSON.parse(eventData)
    console.log('after',eventData);
    
    
    
    var list = document.getElementsByTagName("bracketsingups");
    

        for (var j = 0; j < list.length; j++) {
            list[j].setAttribute("id", "bracketsingups-app-" + j);
            var app = new Vue({
            el: "#bracketsingups-app-" + j,
                data(){
                    return {
                      trData: null, 
                      classes: eventData.classes, 
                      translateValue: 0, 
                      clickNumbers: 1, 
                      pxToChnage: 20,
                      offestToKeep: 5, 
                      classIndex : null,
                      availableKeys : []
                     
                    }
                },
                methods: {
                   goLeft(trs){
                    if(trs[0].clickNumbers == 1) return 
                     let newNumber = trs[0].translateValue + this.pxToChnage
                      trs[0].translateValue = newNumber
                      trs[0].clickNumbers--
                   },
                   goRight(trs){
                     console.log(trs[0].clickNumbers)
                     console.log(trs.length - this.offestToKeep)
                     if(trs[0].clickNumbers == trs.length - this.offestToKeep){
                        // trs[0].translateValue = 0
                        // trs[0].clickNumbers = 1
                        return

                      } 
                      
                      trs[0].translateValue = -trs[0].clickNumbers*this.pxToChnage
                      trs[0].clickNumbers++
                      
                   },
                   changeClass(c){
                       console.log(c.id)
                       let index = this.availableKeys.findIndex(el => el == c.id)
                       console.log(index)
                       if(index < 0) return this.info('Det finns inga anmälningar i denna klass')
                       this.classIndex=c.id
                   },
                   info(msg){
                    this.$Notice.info({
                      title: 'Hey!',
                      desc: msg
                    });
                  },
                   checkMobile(){
                      return ( ( window.innerWidth <= 800 ) && ( window.innerHeight <= 900 ) );
                    },
                    
                       
                },
                created(){
                    console.log('da', eventData)
                    if(Object.keys(eventData.allClass).length === 0 && eventData.allClass.constructor === Object){
                        this.trData = null
                    }else{
                        let keys = []
                        for(let i in eventData.allClass){
                            keys.push(i)
                        }
                        this.availableKeys = keys 
                        if(this.availableKeys.length){
                            this.classIndex = this.availableKeys[0]
                        }
                        this.trData = eventData.allClass
                        //console.log(this.trData)
                        
                       
                        
                    }
                    if(this.checkMobile()){
                        this.pxToChnage = 65
                        this.offestToKeep = 1
                    }
                }
                

                


            })

        }
 </script>

