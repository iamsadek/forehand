<?php
namespace Elementor;

class racketVideoStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-racketsvedio-text-style-one';
	}
	
	public function get_title() {
		return 'RACKET VEDIO STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		

		$this->add_control(
			'sectionBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'Black'
			]
		);

		        
    
		$this->add_control(
			'heading', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Great Academy' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		$this->add_control(
			'text',
			[
				'label' => __( 'Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => "Need lot of actual text here."
			]
		);
		$this->add_control(
			'textColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => "white"
			]
		);
		
		$this->add_control(
			'buttonText',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'View All Rackets'
			]
		);
		
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
	
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Choose image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('racketVideoStyleOneHtml.php');
    }
   
	
	


	
	
}