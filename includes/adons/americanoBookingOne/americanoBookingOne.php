<?php
namespace Elementor;


class AmericanoBookingOne extends Widget_Base {
	
	public function get_name() {
		return 'Americano-Booking-1';
	}
	
	public function get_title() {
		return 'AMERICANO BOOKING ONE';
	}
	
	public function get_icon() {
		return 'fa fa-user-plus';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'signupkey',
			[
				'label' => __( 'Tournament name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'TÄVLINGSANMÄLAN', 'elementor' ),
				'default' => 'TÄVLINGSANMÄLAN'
			]
		);
		$this->add_control(
			'clubName',
			[
				'label' => __( 'Club name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Globen Arena', 'elementor' ),
				'default' => 'KLUBBNAMN'
			]
		);
		$this->add_control(
			'city',
			[
				'label' => __( 'City', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'PADELSTADEN', 'elementor' ),
				'default' => 'PADELSTADEN'
			]
		);

		$this->add_control(
			'trDate',
			[
				'label' => __( 'Tournament date', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Day number', 'elementor' ),
				'default' => '25'
			]
		);
		$this->add_control(
			'monthName',
			[
				'label' => __( 'Month name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Month name', 'elementor' ),
				'default' => 'Jun'
			]
		);
		$this->add_control(
			'dayName',
			[
				'label' => __( 'Day name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Day name', 'elementor' ),
				'default' => 'Fre'
			]
		);
		$this->add_control(
			'enterDetailsTxt',
			[
				'label' => __( 'Header text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your details', 'elementor' ),
				'default' => 'Tävlingsanmälan'
			]
		);
		$this->add_control(
			'sectionHeight',
			[
				'label' => __( 'Widget height', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Height', 'elementor' ),
				'default' => '550px'
			]
		);
		$this->add_control(
			'formPaddingTop',
			[
				'label' => __( 'Form padding top', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Height', 'elementor' ),
				'default' => '90px'
			]
		);


		$this->add_control(
			'leftBg',
			[
				'label' => __( 'Left background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		$this->add_control(
			'rightBg',
			[
				'label' => __( 'Right background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'player1Bg',
			[
				'label' => __( 'Form background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => '#F4F2F0'
			]
		);
		
		


		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'className',
			[
				'label' => __('Class name', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Men class', 'plugin-domain'),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'maxInClass',
			[
				'label' => __( 'Maximum registration for a class', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::NUMBER,
				'placeholder' => __( 'Enter your maximum number of registraions', 'elementor' ),
				'default' => 60
			]
		);
		$this->add_control(
			'allClasses',
			[
				'label' => __('Classes', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'className' => 'Men class', 
						'maxInClass' => 60
					]
				],
				'title_field' => '{{{ className }}}',
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'shirtSize',
			[
				'label' => __('T-shirt sizes', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Small', 'plugin-domain'),
				'label_block' => true,
			]
		);

		
		$this->add_control(
			'allTShirtSizes',
			[
				'label' => __('T-shirt size', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'shirtSize' => 'Small', 
						
					],
					[
						'shirtSize' => 'Medium', 
						
					],
					[
						'shirtSize' => 'Large', 
						
					],
					[
						'shirtSize' => 'Extra Large', 
						
					],

				],
				'title_field' => '{{{ shirtSize }}}',
			]
		);

		$this->add_control(
			'showSize',
			[
				'label' => __( 'Show T-shirt size?', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);




		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        
        require('americanoBookingOneHtml.php');
    }
   

	// js render
	
	


	
	
}