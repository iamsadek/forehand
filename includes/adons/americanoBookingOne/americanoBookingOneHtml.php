<?php 
    // settings to js data 
    $jsSettings = json_encode($settings);
    
    // // calendar data in js var... 
    // $jsCalenderData = json_encode($calendarData);
    echo "<script type='text/javascript'>var jsSettings='$jsSettings'</script>";

    $apiUrl = FOREHAND_API_URL;

    echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";

?>

<style>
  .ivu-select-large .ivu-select-input, .ivu-select-large.ivu-select-multiple .ivu-select-input {
    font-size: 16px;
    height: 39px;
    line-height: 32px;
    top: 0px;
}
</style>
<americanobookingone >
<div>
   <div v-cloak>
  
		
   <div class="_pdl_frm2_sec" style="background: <?php echo $settings['sectionBg'] ?>;height: <?php echo $settings['sectionHeight'] ?>; ">
  
		<!-- LEFT -->
		<div class="_pdl_frm2_lft"  style="background: <?php echo $settings['leftBg'] ?>;">
			<div class="_pdl_frm2_lft_main">
				
				<div class="_pdl_frm2_lft_crd_all">
					<div class="_pdl_frm2_lft_crd">
						<h4><?php echo $settings['trDate'] ?></h4>
						<p class="_pdl_frm2_lft_crd_mnth"><?php echo $settings['monthName'] ?></p>
						<p class="_pdl_frm2_lft_crd_day"><?php echo $settings['dayName'] ?></p>
            
					</div>

					<div class="_pdl_frm2_lft_info">
						<h2><?php echo $settings['signupkey'] ?></h2>
						<ul>
							<li><?php echo $settings['clubName'] ?></li>
							<li><?php echo $settings['city'] ?></li>
						</ul>
					</div>
				</div>

			</div>
		
		</div>
		<!-- LEFT END -->

		<!-- RIGHT -->
		<div class="_pdl_frm2_r8" style="background: <?php echo $settings['rightBg'] ?>; padding-top: <?php echo $settings['formPaddingTop'] ?>; ">
			<h2><?php echo $settings['enterDetailsTxt'] ?></h2>

			<div class="_pdl_frm2_r8_main _mr_t55" style="background: <?php echo $settings['player1Bg'] ?>;">
				<div class="_pdl_frm2_r8_sngl">
					
          <i-input  type="text"  placeholder="E-post" v-model="data.email" @on-blur="searchUser">
				</div>

				<div class="_pdl_frm2_r8_sngl_all">
					<div class="_pdl_frm2_r8_sngl">
          <i-input class="_firstname" type="text"  v-model="data.firstName" :disabled="isDisabled" placeholder="Förnamn">
					</div>

					<div class="_pdl_frm2_r8_sngl">
          <i-input class="_lastname" type="text"  v-model="data.lastName" :disabled="isDisabled" placeholder="Efternamn">
					</div>
				</div>

				<div class="_pdl_frm2_r8_sngl_all">
					<div class="_pdl_frm2_r8_sngl">
          <i-input class="_phone" type="text"  v-model="data.phone" :disabled="isDisabled" placeholder="Mobil">
					</div>

					<div class="_pdl_frm2_r8_sngl">
            <i-select class="_class" filterable placeholder="Klass"  v-model="data.class" @on-change="changeClass">
                <i-option v-for="k in allClasses" :value="k.className">{{k.className}}</i-option>
             </i-select>
					</div>
					<div class="_pdl_frm2_r8_sngl">
              <i-select class="_tshirt" filterable placeholder="Välj storlek"  v-model="data.shirtSize">
                <i-option v-for="k in shirtSizes" :value="k.shirtSize">{{k.shirtSize}}</i-option>
             </i-select>
					</div>
				</div>
			</div>

			<div class="_pdl_frm2_r8_btn">
        <i-button  @click="signup"   :disabled = "isSending" :loading="isSending">{{isSending ? 'Please wait...' : 'Anmälan'}} </i-button>
			</div>
		</div>
		<!-- RIGHT END -->

	</div>
  </div>
</americanobookingone>



<script type="text/javascript">
   
   axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
	

	var list = document.getElementsByTagName("americanobookingone");
    //console.log(list)
        var settings = JSON.parse(jsSettings)
        console.log(settings.allClasses)
        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "americanobookingone-app-" + i);
            var app = new Vue({
            el: "#americanobookingone-app-" + i,
                data(){
                    return {
                       isDisabled : true, 
                       searching: false,
                       allClasses: settings.allClasses,
                       shirtSizes: settings.allTShirtSizes,
                       data: {
                         email : '', 
                         firstName : '', 
                         lastName : '', 
                         phone : '', 
                         class: '', 
                         shirtSize: '', 
                         
                       }, 
                       signupkey: settings.signupkey,
                       maxSignup : settings.allClasses[0].maxInClass, 
                       hasFound: false, 
                       isSending: false,
                       done: false,
                       
                    }
                }, 
                methods: {
                    changeClass(m){
                       
                       for(let d of this.allClasses){
                          if(m == d.className){
                             this.maxSignup = d.maxInClass
                             break
                          }
                       }
                       
                    },
                    async searchUser(){
                       this.searching = true 
                       const res = await this.callApi('post', `${apiUrl}/get-user-info`, {email: this.data.email})
                       if(res.status==200){
                           this.data = res.data
                           //this.data.class = settings.allClasses[0].className
                           this.maxSignup = settings.allClasses[0].maxInClass
                           //this.data.shirtSize = settings.allTShirtSizes[0].shirtSize, 
                           
                           this.isDisabled = true 
                           this.searching = false 
                           this.hasFound = true 
                        }else{
                           this.isDisabled = false 
                           this.searching = false 
                           this.hasFound = false 
                        }
                    }, 
                    async signup(){
                       this.data.signupkey = this.signupkey
                       this.data.maxSignup = this.maxSignup
                       console.log(this.data.maxSignup)
                       if(this.data.email.trim()=='') return this.i('Email is required')
                       if(this.data.firstName.trim()=='') return this.i('Firstname is required')
                       if(this.data.lastName.trim()=='') return this.i('Lastname is required')
                       if(!this.hasFound){
                          if(this.data.phone.trim()=='') return this.i('Phone is required')
                       }
                       if(this.data.class.trim()=='') return this.i('Classname is required')
                       
                       this.isSending = true 
                       const res = await this.callApi('post', `${apiUrl}/americano-register`, this.data)
                       if(res.status==200){
                         this.isSending = false 
                         this.done = true
                         this.s('You have registered successfully! Thank you.')
                       }else if(res.status==402){
                          this.i(res.data.msg)
                       }
                       else{
                          this.i("There went wrong while registering. Please try again or contact us!")
                          
                       }
                       this.isSending = false 
                       
                    },
                    async callApi(method, url, dataObj) {
                      try {

                          let data = await axios({
                              headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              method: method,
                              url: url,
                              data: dataObj
                          })
                          return data

                      } catch (e) {

                          return e.response
                      }
                  },
                  i(msg){
                    this.$Notice.info({
                      title: 'Hey!',
                      desc: msg
                    });
                  },
                  s(msg){
                    this.$Notice.success({
                      title: 'Great!',
                      desc: msg
                    });
                  },
                   
                },
                created(){
                  
                }, 
               
                
            })

        }
 </script>

