<?php
namespace Elementor;

class tripHotelStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-triphotel-text-style-one';
	}
	
	public function get_title() {
		return 'TRIP HOTEL STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		
		

	

		        
    
		$this->add_control(
			'heading', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'The Four Seasons' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		
		$this->add_control(
			'text',
			[
				'label' => __( 'Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'default' => "Hotels"
			]
		);
		$this->add_control(
			'textColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textBgColor',
			[
				'label' => __( 'Text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
	
		$this->add_control(
			'imageLogo',
			[
				'label' => __( 'Choose logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$this->add_control(
			'sectionBackgroundTwo',
			[
				'label' => __( 'Section background two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$this->add_control(
			'sectionBackgroundThree',
			[
				'label' => __( 'Section background three color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]

		);
		$this->add_control(
			'imageTitleOne', [
				'label' => __( 'Image title text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'OUTDOOR' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'imageTitleOneColor',
			[
				'label' => __( 'Image title text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$this->add_control(
			'imageTitleTwo', [
				'label' => __( 'Image title text two', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'INDOOR' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'imageTitleTwoColor',
			[
				'label' => __( 'Image title text two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$this->add_control(
			'imageTitleThree', [
				'label' => __( 'Image title text three', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'AMBIENCE' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'imageTitleThreeColor',
			[
				'label' => __( 'Image title text three color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textThreeColor',
			[
				'label' => __( 'Text three color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textDistanceColor',
			[
				'label' => __( 'Distance text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textTimeColor',
			[
				'label' => __( 'Time text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'imageCourts',
			[
				'label' => __( 'Choose  image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'textF', [
				'label' => __( 'Text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Rooms have air conditioning, a flat screen TV and a lovely terrace. There is a fitness center with the opportunity to work out outdoors. One of the 4 pools has a glass bottom and looks like it is hanging in the air.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'textFColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'textThree', [
				'label' => __( 'Distance text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Hotel to Court' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		$repeater->add_control(
			'textDistance', [
				'label' => __( 'Distance text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '4.9m' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'textTime', [
				'label' => __( 'Time text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '13 min' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'imageSelection',
			[
				'label' => __( 'First type of image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],
			]
		);
		
		$repeater2 = new \Elementor\Repeater();
		$repeater2->add_control(
			'imageCourts2',
			[
				'label' => __( 'Choose  image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater2->add_control(
			'textF2', [
				'label' => __( 'Text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Rooms have air conditioning, a flat screen TV and a lovely terrace. There is a fitness center with the opportunity to work out outdoors. One of the 4 pools has a glass bottom and looks like it is hanging in the air.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater2->add_control(
			'textFColor2',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater2->add_control(
			'textThree2', [
				'label' => __( 'Distance text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Hotel to Court' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		$repeater2->add_control(
			'textDistance2', [
				'label' => __( 'Distance text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '4.9m' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater2->add_control(
			'textTime2', [
				'label' => __( 'Time text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '13 min' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'imageSelection2',
			[
				'label' => __( 'Second type of image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],

			]
		);



		$repeater3 = new \Elementor\Repeater();


		$repeater3->add_control(
			'imageCourts3',
			[
				'label' => __( 'Choose  image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater3->add_control(
			'textF3', [
				'label' => __( 'Text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Rooms have air conditioning, a flat screen TV and a lovely terrace. There is a fitness center with the opportunity to work out outdoors. One of the 4 pools has a glass bottom and looks like it is hanging in the air.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater3->add_control(
			'textFColor3',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater3->add_control(
			'textThree3', [
				'label' => __( 'Distance text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Hotel to Court' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		
		$repeater3->add_control(
			'textDistance3', [
				'label' => __( 'Distance text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '4.9m' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater3->add_control(
			'textTime3', [
				'label' => __( 'Time text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '13 min' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'imageSelection3',
			[
				'label' => __( 'Third type of image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],

			]
		);
	
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('tripHotelStyleOneHtml.php');
    }
   
	
	


	
	
}