<?php
namespace Elementor;
require_once(BASEPLUGIN_INCLUDES . '/common/helperclass.php');

class EventRegisterForm extends Widget_Base {
	use Helper;
	public function get_name() {
		return 'forehand-event-register-form';
	}
	
	public function get_title() {
		return 'EVENT REGISTER FORM';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		$events = $this->callApi('getClubEvents?timeline=ok');
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$optinsAndDefault = $this->getOptionsAndDefaultValues($events, 'eventName');
		
		$this->add_control(
			'event_register_form_id',
			[
				'label' => __( 'Select the event', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => $optinsAndDefault[1],
				'options' => $optinsAndDefault[0],
			]
		);

		$this->add_control(
			'signupkey',
			[
				'label' => __( 'Event name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'TÄVLINGSANMÄLAN', 'elementor' ),
				'default' => 'TÄVLINGSANMÄLAN'
			]
		);

		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Events'
			]
		);
		$this->add_control(
			'clubName',
			[
				'label' => __( 'Club name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Globen Arena', 'elementor' ),
				'default' => 'KLUBBNAMN'
			]
		);
		$this->add_control(
			'city',
			[
				'label' => __( 'City', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'PADELSTADEN', 'elementor' ),
				'default' => 'PADELSTADEN'
			]
		);

		$this->add_control(
			'trDate',
			[
				'label' => __( 'Tournament date', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Day number', 'elementor' ),
				'default' => '25'
			]
		);
		$this->add_control(
			'monthName',
			[
				'label' => __( 'Month name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Month name', 'elementor' ),
				'default' => 'Jun'
			]
		);
		$this->add_control(
			'dayName',
			[
				'label' => __( 'Day name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Day name', 'elementor' ),
				'default' => 'Fre'
			]
		);
		$this->add_control(
			'enterDetailsTxt',
			[
				'label' => __( 'Header text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your details', 'elementor' ),
				'default' => 'Tävlingsanmälan'
			]
		);
		$this->add_control(
			'sectionHeight',
			[
				'label' => __( 'Widget height', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Height', 'elementor' ),
				'default' => '630px'
			]
		);
		$this->add_control(
			'formPaddingTop',
			[
				'label' => __( 'Form padding top', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Height', 'elementor' ),
				'default' => '90px'
			]
		);


		$this->add_control(
			'leftBg',
			[
				'label' => __( 'Left background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		$this->add_control(
			'rightBg',
			[
				'label' => __( 'Right background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		$this->add_control(
			'player1Bg',
			[
				'label' => __( 'From background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => '#F4F2F0'
			]
		);
		// $this->add_control(
		// 	'player2Bg',
		// 	[
		// 		'label' => __( 'Player2 background color', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => '#dcdbd9'
		// 	]
		// );
		$this->add_control(
			'boxBackground',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222222;'
			]
		);

		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$eventData = $this->getCalendarData($settings['event_register_form_id']);
		
        require('eventRegisterFormHtml.php');
	}
	public function getCalendarData($id){
		
        $response = wp_remote_request( FOREHAND_API_URL."/eventForm/$id",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
		$body = wp_remote_retrieve_body($response);
		
	
        return json_decode($body);
    }
   

	// js render
	
	


	
	
}