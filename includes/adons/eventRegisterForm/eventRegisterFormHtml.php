<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
$eventData = json_encode($eventData);
echo "<script type='text/javascript'>var eventData='$eventData'</script>";

?>



<eventregisterform>
      <div v-cloak>
      <div class="_pdl_frm2_sec" style="background: <?php echo $settings['sectionBg'] ?>;height: <?php echo $settings['sectionHeight'] ?>;">

<!-- LEFT -->
<div class="_pdl_frm2_lft" style="background: <?php echo $settings['leftBg'] ?>">
  <div class="_pdl_frm2_lft_main">

    <div class="_pdl_frm2_lft_crd_all">
      <div class="_pdl_frm2_lft_crd">
        <h4><?php echo $settings['trDate'] ?></h4>
        <p class="_pdl_frm2_lft_crd_mnth"><?php echo $settings['monthName'] ?></p>
        <p class="_pdl_frm2_lft_crd_day"><?php echo $settings['dayName'] ?></p>
      </div>

      <div class="_pdl_frm2_lft_info">
        <h2><?php echo $settings['signupkey'] ?></h2>
        <ul>
          <li><?php echo $settings['clubName'] ?></li>
          <li><?php echo $settings['city'] ?></li>
        </ul>
      </div>
    </div>

  </div>

</div>
<!-- LEFT END -->

<!-- RIGHT -->
<div class="div_form" style="background: <?php echo $settings['rightBg'] ?>; padding-top: <?php echo $settings['formPaddingTop'] ?>; ">
  <h2><?php echo $settings['enterDetailsTxt'] ?></h2>
  <div class="main_form">
  <div class="_pdl_frm2_r8_main_top">
        <ul>
          <li  @click="playerChange(false)"  style="background: <?php echo $settings['player1Bg'] ?>">Jag har inte Forehand ID</li>
          <li  @click="playerChange(true)"  style="background: <?php echo $settings['player2Bg'] ?>">Jag har Forehand ID</li>
        </ul>
      </div>
  <div class="_pdl_frm2_r8_main " style="background: <?php echo $settings['player1Bg'] ?>" v-if="!showPlayer2">
    <div  >
      <div class="_pdl_frm2_r8_sngl">
        <p>E-postadress</p>
        <i-input  type="text"  v-model="data.email">
      </div>

      <div class="_pdl_frm2_r8_sngl_all">
        <div class="_pdl_frm2_r8_sngl">
        <p>Förnamn</p>
        <i-input class="_firstname" type="text"  v-model="data.firstName"  >
        </div>

        <div class="_pdl_frm2_r8_sngl">
        <p>Efternamn</p>
        <i-input class="_lastname" type="text"  v-model="data.lastName"  >
        </div>
      </div>
      <div class="_pdl_frm2_r8_sngl_all">
        <div class="_pdl_frm2_r8_sngl">
          <p>Mobiltel</p>
          <i-input class="_phone" type="text"  v-model="data.phone"  >
        </div>
        
        
      </div>
      
    </div>
    
  </div>
  <div class="_pdl_frm2_r8_main" v-if="showPlayer2" style="background: <?php echo $settings['player1Bg'] ?>">
      
      <div class="_pdl_frm2_r8_sngl">
      <p>E-postadress</p>
        <i-input  type="text"   v-model="data.email" >
      </div>

      <div class="_pdl_frm2_r8_sngl_all">
        <div class="_pdl_frm2_r8_sngl">
        <p>Password</p>
        <i-input class="_firstname" type="password"  v-model="data.password"  >
        </div>

        
      </div>

     
      </template>

    </div>
    <div class="pa" v-if="eventData.key !='' ">
      <form id="payment-form">
          <div class="_stripe_input_details">

              <div id="card-element" class="_stripe_input_input"></div>

          </div>
          <div id="card-errors" role="alert"></div>

      </form>
    </div>
    
    
  
  
  <div class="_pdl_frm2_r8_btn">
    <i-button  @click="save" :disabled="isSending" :loading="isSending" v-if="eventData.key !=''">Registrera & Betala {{eventData.eventFee}} kr </i-button>
    <i-button  @click="save" :disabled="isSending" :loading="isSending" v-if="eventData.key ==''">Registrera</i-button>
  </div>
</div>
<!-- RIGHT END -->

</div>
      </div>



    
	
</eventregisterform>


  <script>
      var stripe = Stripe('pk_live_51HeILUJktwxI8vVnQOpnSaAdWciOR0bufVorpsDAga3BrC28TUsH4xarmDKnKTYTUDbolFv9XmHxXVEuPrndrtsR00MrfFkHwZ');
      var elements = stripe.elements();
      console.log('elements is ', elements)
  </script>

<script type="text/javascript">
    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
    eventData = eventData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    eventData = JSON.parse(eventData)
    console.log(eventData);
    
    
    
    var list = document.getElementsByTagName("eventregisterform");
    

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "eventregisterform-app-" + i);
            var app = new Vue({
            el: "#eventregisterform-app-" + i,
                data(){
                    return {
                      eventData: eventData, 
                      headerStyleOneSetting: headerStyleOneSetting,
                      phone : 'Radio option 2', 
                      data: {
                          firstName : '', 
                          lastName : '', 
                          email : '', 
                          phone : '', 
                          password: ''
                      }, 
                      firstNameError: false,
                      lastNameError: false,
                      emailError: false,
                      phoneError: false,
                      passError: false,
                      hasForehandId : false,
                      isSending: false, 
                      showPlayer2: false, 
                      card: null,
                      formInfo : null
                    }
                },
                methods: {
                  playerChange(m){
                      this.showPlayer2 = m
                      console.log(this.showPlayer2)
                      console.log(m)
                  },
                  async save(){
                      // validate the input 
                      let hasError = false 
                      
                      this.hasForehandId = this.showPlayer2
                      if(!this.hasForehandId){
                          console.log('no id')
                          if(this.data.firstName.trim() == ''){
                            this.firstNameError = true
                            hasError = true 
                          }else{
                            this.firstNameError = false
                          }
                        
                          if(this.data.lastName.trim() == ''){
                            this.lastNameError = true
                            hasError = true 
                          }else{
                            this.lastNameError = false
                          }
                          if(this.data.phone.trim() == ''){
                            this.phoneError = true
                            hasError = true 
                          }else{
                            this.phoneError = false
                          }
                          if(this.data.email.trim() == ''){
                              this.emailError = true
                              hasError = true 
                            }else{
                              this.emailError = false
                            }
                      }else{
                        console.log('with id')
                        if(this.data.password.trim() == ''){
                            this.passError = true
                            hasError = true 
                          }else{
                            this.passError = false
                          }
                      }
                      if(hasError) return 
                      this.saveRegisterInfo()

                      
                  }, 
                  async saveRegisterInfo(){
                     let data = {
                         hasForehandId : this.hasForehandId, 
                         userInfo : this.data, 
                         extraFormData : this.eventData.formData, 
                         totalFromPlayer: this.eventData.totalFromPlayer,
                         totalFeeAmount: this.eventData.totalFeeAmount,
                     }
                     this.formInfo = data
                     this.isSending = true 
                     const res = await this.callApi('post', `${apiUrl}/register_event/${this.eventData.id}`, data)
                     if(res.status==200){
                         if(this.eventData.key !=''){
                            if(await this.charge()){
                              alert(res.data.msg)
                            }else{
                                alert("We couldn't complete the payment, please try again later.")
                            }
                         }else{
                            alert(res.data.msg)
                         }
                         
                      }else{
                        alert(res.data.msg)
                     }
                     this.isSending = false 
                  },
                  async charge(){
                    const {paymentIntent} = await stripe.confirmCardPayment(this.eventData.key, {
                        payment_method: {
                        card: this.card,
                        billing_details: {
                            name: 'Sadek hossain',
                            email: 'event@test.com',

                        }
                        }
                    })

                    console.log(paymentIntent)
                    if(paymentIntent && paymentIntent.status=='succeeded'){
                        alert('payment successeded')
                        return true 
                      }else{
                        // delete the event 
                        const res = await this.callApi('post', `${apiUrl}/delete_event/${this.eventData.id}`, this.formInfo)
                        if(res.status==200){
                          console.log('event deleted')
                        }else{
                          console.log('couldnt delete event')
                        }
                        return false
                      
                    }
                  },

                  async callApi(method, url, dataObj) {
                      try {

                          let data = await axios({
                              headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              method: method,
                              url: url,
                              data: dataObj
                          })
                          return data

                      } catch (e) {

                          return e.response
                      }
                  },
                },
                

                created(){
                  console.log('event data ', this.eventData)
                },
                mounted(){
                  if(this.eventData.key !=''){
                      var style = {
                        base: {
                            'color': "#435467",
                            "fontSize": '16px'
                        },
                        hideIcon: true
                    };

                    var card = elements.create("card", { style: style });

                    card.mount("#card-element");
                    this.card = card
                  }
                 
              },


            })

        }
 </script>

