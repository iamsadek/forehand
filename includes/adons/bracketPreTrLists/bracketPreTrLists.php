<?php
namespace Elementor;
require_once(BASEPLUGIN_INCLUDES . '/common/helperclass.php');

class BracketPreTrLists extends Widget_Base {
	use Helper;
	public function get_name() {
		return 'forehand-bracket-pre-tr-lists';
	}
	
	public function get_title() {
		return 'BRACKET PRE TOURNAMENTS';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);

		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'ViaPlay'
			]
		);
		$this->add_control(
			'subTitle',
			[
				'label' => __( 'Sub Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'ELITSERIEN MATCHES'
			]
		);
	

		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$eventData = $this->getData();
		
        require('bracketPreTrListsHtml.php');
	}
	public function getData(){
		
        $response = wp_remote_request( FOREHAND_API_URL."/bracketPreTrs",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
		$body = wp_remote_retrieve_body($response);
		
	
        return json_decode($body);
    }
   

	// js render
	
	


	
	
}