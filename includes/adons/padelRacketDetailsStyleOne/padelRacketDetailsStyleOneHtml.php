<!-- DO PADEL PAGE-3 RACKET DETAILS LIST-->
<div class="_do_pdl3_rckt_dtls_list" style="background-color: <?php echo $settings ['sectionBackgroundOne'] ?>;">
		<ul>
		   <?php if($settings['list'] ): ?> 
                 <?php foreach($settings['list'] as $item ): ?>
			<li>
			   <div class="_do_pdl3_rckt_dtls_list_img">
	           <img src="<?php echo $item['imageLogo']['url'] ?>" alt="image">
             </div>
			   <p style="color: <?php echo $item['outsideListColor'] ?>">
                  <?php echo $item['outsideList']?>
			</li>
			 <?php endforeach; ?>
               <?php endif; ?>
			
		</ul>
	</div>
	<!-- DO PADEL PAGE-3 RACKET DETAILS LIST END-->