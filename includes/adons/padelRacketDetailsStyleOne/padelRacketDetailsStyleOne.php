<?php
namespace Elementor;

class PadelRacketDetailsStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-detailsrackets-text-style-one';
	}
	
	public function get_title() {
		return 'PADEL RACKET DETAILS STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'sectionBackgroundOne',
			[
				'label' => __( 'Section background ', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
	
		$repeater = new \Elementor\Repeater();

        $repeater->add_control(
			'imageLogo',
			[
				'label' => __( 'Choose logo image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$repeater->add_control(
			'outsideList', [
				'label' => __( 'Outside List ', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Hack' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'outsideListColor',
			[
				'label' => __( 'Outside list color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		
		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],
			
			]
		);

		
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('padelRacketDetailsStyleOneHtml.php');
    }
   
	
	


	
	
}