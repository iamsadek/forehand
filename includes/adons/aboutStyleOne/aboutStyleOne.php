<?php
namespace Elementor;

class AboutStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-about-style-one';
	}
	
	public function get_title() {
		return 'ABOUT STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-info-circle';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// news title
		$this->add_control(
			'trTitle',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Om tävlingen'
			]
		);
		$this->add_control(
			'addButton',
			[
				'label' => __( 'Add top section', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);
		$this->add_control(
			'buttonHeading',
			[
				'label' => __( 'Button Heading', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Viaplay Padel Open'
			]
		);

		$this->add_control(
			'button1Txt',
			[
				'label' => __( 'Button 1 text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'May 12 | 2020'
			]
		);
		$this->add_control(
			'button1Background',
			[
				'label' => __( 'Button 1 background', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'black'
			]
		);
		$this->add_control(
			'button1TxtColor',
			[
				'label' => __( 'Button 1 text color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'white'
			]
		);


		$this->add_control(
			'button2Txt',
			[
				'label' => __( 'Button 2 text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Dam Herr'
			]
		);
		$this->add_control(
			'button2Background',
			[
				'label' => __( 'Button 2 background', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'grey'
			]
		);
		$this->add_control(
			'button2TxtColor',
			[
				'label' => __( 'Button 2 text color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'black'
			]
		);

		$this->add_control(
			'button3Txt',
			[
				'label' => __( 'Button 3 text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Prispengar 38 000 kr'
			]
		);
		$this->add_control(
			'button3Background',
			[
				'label' => __( 'Button 3 background', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => '#4749de'
			]
		);
		$this->add_control(
			'button3TxtColor',
			[
				'label' => __( 'Button 3 text color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'white'
			]
		);




		// text
		$this->add_control(
			'text',
			[
				'label' => __( 'Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => "To get the next level in tennis, a middle school player, an adult league player, a senior recreational player, and a professional tennis player all need to work on their overall fitness. The use of powerful strokes, the repetitive nature of the game, the various court surfaces, individual game styles, and the variety of movement and stroke patterns and stances in tennis call for a proper tennis workout program.
			  				 "
			]
		);

		$this->add_control(
			'addFooterButton',
			[
				'label' => __( 'Add link button', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);
		$this->add_control(
			'buttonTxt',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Till anmälan'
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonRds',
			[
				'label' => __( 'Button border radius', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '30px'
			]
		);
		$this->add_control(
			'buttonBorderColor',
			[
				'label' => __( 'Button border color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'darkgray'
			]
		);



		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'transparent'
			]
		);
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);






		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		// text color 
		$this->add_control(
			'textColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);


		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('aboutStyleOneHtml.php');
    }
	public function settingsOfWidget() {
       return $this->get_settings_for_display();
    }
   
	
	


	
	
}