

<div class="_trnmnt_txt2_sec" style="background: <?php  echo $settings['boxBgColor']?>">
        <div class="_trnmnt_txt2_lft">
          <h3 class="_trnmnt_txt2_lft_h3" style="color: <?php  echo $settings['textColor']?>">
          <?php echo $settings['trTitle'] ?>
          </h3>
        </div>
        <div class="_trnmnt_txt2_r8">
         <?php if($settings['addButton']) : ?>
          <h3 class="_trnmnt_txt2_r8_h3" style="color: <?php  echo $settings['textColor']?>">
             <?php echo $settings['buttonHeading'] ?>
          </h3>
          <div class="_trnmnt_txt2_r8_top">
              <ul class="_trnmnt_txt2_r8_ul">
                <li style="background: <?php echo $settings['button1Background'] ?>; color:  <?php echo $settings['button1TxtColor'] ?>">
                  <?php echo $settings['button1Txt'] ?>
                </li>
                <li style="background: <?php echo $settings['button2Background'] ?>; color:  <?php echo $settings['button2TxtColor'] ?>">
                <?php echo $settings['button2Txt'] ?>
                </li>
              </ul>
              <p class="_trnmnt_txt2_r8_p1" style="background: <?php echo $settings['button3Background'] ?>; color:  <?php echo $settings['button3TxtColor'] ?>">
                <?php echo $settings['button3Txt'] ?>
              </p>
          </div>
         <?php endif;?>
         


            <p class="_trnmnt_txt2_r8_txt" style="color: <?php  echo $settings['textColor']?>">
              <?php   echo $settings['text'] ?>
            </p>

            <?php if($settings['addFooterButton'] == 'yes' ): ?> 
                <div class="forehand_button">
                    <a href="<?php echo $settings['buttonLink'] ?>">
                        <button class="fbutton"
                            style="color:<?php echo $settings['buttonTxtColor'] ?>;background:<?php echo $settings['buttonBg'] ?>;
                            border: 1px solid <?php echo $settings['buttonBorderColor'] ?>;border-radius: <?php echo $settings['buttonRds'] ?>;"
                        >
                            <?php echo $settings['buttonTxt'] ?>
                        </button>
                    </a>
                </div>
            <?php endif; ?>

        </div>
      </div> 
 
 
