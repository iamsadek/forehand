<!-- DO PADEL PAGE-4 FACILITES -->
<?php 
	$jsSettings = json_encode($settings); 
	echo "<script> var jsSettings =  $jsSettings  </script>";

?>
	
<tripsbannerstyletwo>

<div v-cloak>	

	<div class="_do_pdl4_fclts_sec"  :style="{'background-image': `url(${bigPic})`}">
		<div class="_do_pdl4_fclts_info">
		<p style="color: <?php echo $settings['textColor'] ?> 
		;background:<?php echo $settings['textBgColor'] ?>"><?php echo $settings['text']?></p>         
		<h3 style="color: <?php echo $settings['headingColor'] ?>">
            <?php echo $settings['heading']?></h3>
		</div>
		<!-- CARD -->         
		<div class="_do_pdl4_fclts_card" style="background-color: <?php echo $settings['sectionBackground'] ?> ;">
			<p class="_do_pdl4_fclts_card_p" style="color: <?php echo $settings['textTwoColor'] ?>">
			<span style="color: <?php echo $settings['textTwoColorFirst'] ?>"><?php echo $settings['textTwoFirst']?>
		    </span><?php echo $settings['textTwo']?>
			</p>
			<div class="_do_pdl4_fclts_card_inner" style="background-color: <?php echo $settings['sectionBackgroundTwo'] ?> ;">
				<div class="_do_pdl4_fclts_card_top">
					<div class="_do_pdl4_fclts_card_lft">
					  <p style="color: <?php echo $settings['textThreeColor'] ?>">
					  {{textArea1}}</p>
					</div>
					<div class="_do_pdl4_fclts_card_r8">
						<ul>
						 <li style="color: <?php echo $settings['textDistanceColor'] ?>">
						 {{textArea2}}</li>
				         <li style="color: <?php echo $settings['textTimeColor'] ?>">
						 {{textArea3}}</li>                                          
						</ul>            
					</div>                        
				</div>
				<div class="_do_pdl4_fclts_card_txt">
				  <p :style="{color:`${textFAcolor}`}">
				    {{textArea}}
                   
                   </p>
				</div>                        
			</div>
		</div>
		<!-- CARD -->
	</div>
	<div class="_do_pdl4_fclts_btm"  style="background-color: <?php echo $settings['sectionBackgroundThree'] ?> ;">
		<div class="_do_pdl4_fclts_btm_top">
			<ul>
			    <li>Prev</li>             
				<li>Next</li>
			</ul>                           
		</div>                                                                                      
		<div class="_do_pdl4_fclts_main">
			<!-- ITEAM -->              
			<div class="_do_pdl4_fclts_main_itm">
				<div class="_do_pdl4_fclts_main_itm_txt" >
				   <p style="color: <?php echo $settings['imageTitleOneColor'] ?>">
                   <?php echo $settings['imageTitleOne']?></p>
				</div>
				<ul>
				           <li :class="bigPic == img.imageCourts.url ? '_do_pdl4_fclts_actv' : '' " v-if="settings.imageSelection.length" v-for="(img, i)  in settings.imageSelection"  @click="changePic(img.imageCourts.url,img.textF,img.textFColor,img.textThree,img.textDistance,img.textTime)">
								<img :src="img.imageCourts.url" alt="image">	
							</li>     
				
				</ul>                                           
			</div>

			<!-- ITEAM -->
			<div class="_do_pdl4_fclts_main_itm">
				<div class="_do_pdl4_fclts_main_itm_txt">
				   <p style="color: <?php echo $settings['imageTitleTwoColor'] ?>">
                   <?php echo $settings['imageTitleTwo']?></p>
				</div>
				<ul>
				<li :class="bigPic == img.imageCourts2.url ? '_do_pdl4_fclts_actv' : '' " v-if="settings.imageSelection2.length" v-for="(img, i)  in settings.imageSelection2"  @click="changePic(img.imageCourts2.url,img.textF2,img.textFColor2,img.textThree2,img.textDistance2,img.textTime2)">
						<img :src="img.imageCourts2.url" alt="image">
				</li> 
				</ul>
			</div>
			<!-- ITEAM -->
            <!-- ITEAM -->
			<div class="_do_pdl4_fclts_main_itm">
				<div class="_do_pdl4_fclts_main_itm_txt">
				   <p style="color: <?php echo $settings['imageTitleThreeColor'] ?>">
                   <?php echo $settings['imageTitleThree']?></p>
				</div>
				<ul>
				   <li :class="bigPic == img.imageCourts3.url ? '_do_pdl4_fclts_actv' : '' " v-if="settings.imageSelection3.length" v-for="(img, i)  in settings.imageSelection3"  @click="changePic(img.imageCourts3.url,img.textF3,img.textFColor3,img.textThree3,img.textDistance3,img.textTime3)">
						<img :src="img.imageCourts3.url" alt="image">
				   </li> 
				</ul>
			</div>
			<!-- ITEAM -->

		

		
		</div>                                          
	</div>

	</div>

</tripsbannerstyletwo>  
	<!-- DO PADEL PAGE-4 FACILITES END-->

	<script type="text/javascript">



	

	var imageSelection = document.getElementsByTagName("tripsbannerstyletwo");
	
    

        for (var i = 0; i < imageSelection.length; i++) {
            imageSelection[i].setAttribute("id", "tripsbannerstyletwo-app-" + i);
            var app = new Vue({
            el: "#tripsbannerstyletwo-app-" + i,
                data(){
                    return {
						settings: jsSettings,
						bigPic : '' ,
						textArea: '',
						textArea1:'',
						textArea2:'',
						textArea3:'',
						textFAcolor:'',
						
                    }
					
				},
				methods: {
					changePic(pic,text,color,text1,text2,text3){

						this.bigPic = pic
						this.textArea= text
						this.textFAcolor= color
						this.textArea1=text1
						this.textArea2=text2
						this.textArea3=text3

					},
					
				},	
               created (){

				if(this.settings.imageSelection.length){
						this.bigPic = this.settings.imageSelection[0].imageCourts.url
						this.textArea = this.settings.imageSelection[0].textF
						this.textFAcolor = this.settings.imageSelection[0].textFColor
						this.textArea1 = this.settings.imageSelection[0].textThree
						this.textArea2 = this.settings.imageSelection[0].textDistance
						this.textArea3 = this.settings.imageSelection[0].textTime
					}
			        console.log('Here is color',this.textFAcolor)
					} 
            })
        }
		
 </script>                            




