<?php
namespace Elementor;


class CalendarStyleTwo extends Widget_Base {
	
	public function get_name() {
		return 'forehand-calendar-two';
	}
	
	public function get_title() {
		return 'CALENDAR STYLE 2';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// news title
		$this->add_control(
			'name',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Kommande event'
			]
		);
		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $calendarData = $this->getCalendarData();
        require('calendarStyleTwoHtml.php');
    }
    public function getCalendarData(){
        $response = wp_remote_request( FOREHAND_API_URL.'/calendarData?limit=50&c=4',
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
    }

	// js render
	
	


	
	
}