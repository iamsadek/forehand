<?php 
    // settings to js data 
    $jsSettings = json_encode($settings);
    $imageSource = FOREHAND_IMAGE_SOURCE;
    echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
    echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
    // calendar data in js var... 
    $jsCalenderData = json_encode($calendarData);
    echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";

   

?>
<calendarstyletwo >
    <div v-cloak>
        <!-- ALL PROGRAMS -->
	<div class="_evnt2_pgrm_sec">
		<div class="_evnt2_pgrm_top">
			<h3 class="_evnt2_pgrm_top_h3">
				<?php echo $settings['name'] ?>
			</h3>
		</div>
		<div class="_evnt2_pgrm_itm" v-for="(calendar, key) in jsCalenderData" v-if="jsCalenderData">
			<div class="_evnt2_pgrm_itm_top">
				<ul class="_evnt2_pgrm_itm_ul">
					<!-- <li>This month</li> -->
					<li>{{calendar[0].dayOfTheMonth}} {{calendar[0].monthName}} <span> | {{calendar[0].dayName}}</span></li>
				</ul>
			</div>
			<div class="_evnt2_pgrm_itm_crd_all">
				<!-- CARD -->
				<a :href="c.landingPage" v-for="(c , i) in calendar" v-if="calendar">
					<div class="_evnt2_pgrm_itm_crd_main" >
					
						<div class="_evnt2_pgrm_itm_crd">
							<img :src="imageSource+c.eventImg" alt="image">
							<div class="_evnt2_pgrm_crd_one">
								<ul class="_evnt2_pgrm_one_ul">
									<li>{{c.eventTime}}</li>
									<!-- <li>Wed <span>24</span></li> -->
								</ul>
							</div>
							<div class="_evnt2_pgrm_crd_two">
								<h3 class="_evnt2_pgrm_two_h3">
									{{c.eventName}}
								</h3>
							</div>
							<div class="_evnt2_pgrm_crd_three">
								<!-- <div class="_evnt2_pgrm_crd_three_lft">
									<img :src="imageSource+c.eventImg" alt="image">
									
								</div> -->
								<!-- <div class="_evnt2_pgrm_crd_three_r8">
									<p class="_evnt2_pgrm_crd_r8_p">
										Arthur Curry <span class="_evnt2_pgrm_pro_clr1">Pro</span>
									</p>
								</div> -->
							</div>
						</div>
						
					</div>
				</a>
				<!-- CARD -->

				

				
			</div>
		</div>
	</div>
	<!-- ALL PROGRAMS -->

	
   
    </div>
</calendarstyletwo>



<script type="text/javascript">
   
   axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
	
	var jsdata = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
	console.log('data is', jsCalenderData)
    jsCalenderData  = JSON.parse(jsdata)
    

    var list = document.getElementsByTagName("calendarstyletwo");
    //console.log(list)
 
        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "calendarstyletwo-app-" + i);
            var app = new Vue({
            el: "#calendarstyletwo-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting, 
                        jsCalenderData: jsCalenderData, 
                        isModal: false,
                        eventInfo : false, 
                        imageSource: imageSource
                       
                    }
                }, 
                methods: {
                    showEventInfo(c){
                        this.eventInfo = c
                        this.isModal = true
                    }
                   
                },
                created(){
                   console.log('data is', this.jsCalenderData)
                }, 
               
                
            })

        }
 </script>

