<?php
namespace Elementor;
require_once(BASEPLUGIN_INCLUDES . '/common/helperclass.php');
class NewsSliderStyleOne extends Widget_Base {
	use Helper;
	public function get_name() {
		return 'forehand-pnews-text-style-Three';
	}
	
	public function get_title() {
		return 'NEWS SLIDER';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		 
		$trs = $this->callApi('allTrLists');
		
		$optinsAndDefault = $this->getOptionsAndDefaultForTournament($trs);
		//array_unshift($optinsAndDefault[0], 'Alla');
		
        $this->add_control(
            'tournament_id',
            [
                'label' => __( 'Select the tournament', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::SELECT,
				'default' => $optinsAndDefault[1],
                'options' => $optinsAndDefault[0],
            ]
         );




		$this->add_control(
			'sectionBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);	        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'News' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
	   $this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		
		$calendarData = $this->getData($settings['tournament_id']);
        require('newsSliderStyleOneHtml.php');
	}
	public function getData($id){
		
        $response = wp_remote_request( FOREHAND_API_URL."/newByTr/$id",
			array(
				'method'     => 'GET', 
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
			)
        );
		$body = wp_remote_retrieve_body($response);
		return json_decode($body);
    }
   
	
	


	
	
}