<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsCalenderData = json_encode($calendarData);
echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";

?>



<newspadel>
    <div v-cloak>
      <div class="_do_pdl_nws_sec" style="background-color: <?php echo $settings['sectionBgColor']?> ;">
        <div class="_do_pdl_nws_titl">
        <h3 style="color : <?php echo $settings['headerOneColor'] ?>">
              <?php echo  $settings['headerOne']?>
            </h3> 
        </div>
        <div class="outer_div">
          <div class="_do_pdl_nws_main">
            <!-- CARD -->
            <div class="_padel10_main_all">
             <div class="_padel10_main" :style="{ transform: 'translate3d('+translateValue+'%, 0px, 0px)'  }">
              <div class="_do_pdl_nws_card" v-for="(n, i) in news" v-if="news.length" >
              <div class="_news_card_top">
                    <p class="event_date" >
                      {{n.newsDate}}               
                  </p>
                </div>
                  <img :src="imageSource+n.image" alt="image" @click="goNext(n.id)"> 
                  <div class="_do_pdl_nws_crd_info" @click="goNext(n.id)">
                    <p class="_do_pdl_nws_dte" style="color : <?php echo $item['listOneColor'] ?>" >
                        {{n.title}}
                    </p>

                    <!-- <p class="_do_pdl_nws_txt"  >
                        {{n.exerpt}}
                    </p> -->
                  </div>
              </div>
            </div>
            </div>
            <!-- CARD -->
          

          
            
          </div>
          <!-- ARROW -->
          <div class="_padel10_arrow_lft_news commonarrowclass" @click="goBack" v-if="news.length > 4">
              <i class="fa fa-arrow-left"></i>
            </div>
            <div class="_padel10_arrow_rght_news commonarrowclass" @click="sliderRight" v-if="news.length > 4"> 
              <i class="fa fa-arrow-right"></i>
            </div>
        <!-- ARROW -->
        </div>
      </div>
      </div>
</newspadel>


<script type="text/javascript">

    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)

	  jsCalenderData = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
   
    let newsPadelThree = JSON.parse(jsCalenderData)
    console.log('settings from eleme', headerStyleOneSetting)
    // eventData  = JSON.parse(eventData)
    


    var list = document.getElementsByTagName("newspadel");
        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "newspadel-app-" + i);
            var app = new Vue({
            el: "#newspadel-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting,
                       
                        news: [],
                        translateValue: 0, 
                      	clickNumbers: 1, 
                        pxToChnage : 27

                    }
                },
                methods: {
					          goNext(id){
                      window.location = `/forehand-news/${id}`
                    },
                    sliderRight(){ // increase 
                      if(this.clickNumbers == this.news.length - 4){
                        this.translateValue = 0
                        this.clickNumbers = 1
                        return

                      } 
                      
                      this.translateValue = -this.clickNumbers*this.pxToChnage
                      this.clickNumbers++
                      
                    },
                    goBack(){ // going back
                      if(this.clickNumbers == 1) return 
                      let newNumber = this.translateValue + this.pxToChnage
                    
                      this.translateValue = newNumber
                      console.log('decreament',this.translateValue)
                      this.clickNumbers--
                    },
                    checkMobile(){
                      console.log(window.innerWidth)
                      console.log(window.innerHeight)
                      return ( ( window.innerWidth <= 800 ) && ( window.innerHeight <= 900 ) );
                    }

                },
                created(){
                  if(this.checkMobile()){
                     this.pxToChnage = 106
                  }
                  console.log('created', this.checkMobile())
                  let repeatedImages =[]
                  if(newsPadelThree.length > 4){
                    repeatedImages = new Array(3).fill(newsPadelThree).flat();
                  }else{
                    repeatedImages = newsPadelThree
                  }
                  this.news = repeatedImages
                  console.log('news is', this.news)
                },


            })

        }
 </script>

