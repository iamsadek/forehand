<?php
// settings to js data
$jsSettings = json_encode($settings);

// // calendar data in js var...
// $jsCalenderData = json_encode($calendarData);
echo "<script type='text/javascript'>var jsSettings='$jsSettings'</script>";

$apiUrl = FOREHAND_API_URL;

echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";

?>

<style>
  .ivu-select-large .ivu-select-input, .ivu-select-large.ivu-select-multiple .ivu-select-input {
    font-size: 16px;
    height: 39px;
    line-height: 32px;
    top: 0px;
}
</style>
<divisionsignupone >
<div>
   <div v-cloak>


   <div class="_pdl_frm2_sec" style="background: <?php echo $settings['sectionBg'] ?>;height: <?php echo $settings['sectionHeight'] ?>;">

		<!-- LEFT -->
		<div class="_pdl_frm2_lft" style="background: <?php echo $settings['leftBg'] ?>">
			<div class="_pdl_frm2_lft_main">

				<div class="_pdl_frm2_lft_crd_all">
					<div class="_pdl_frm2_lft_crd">
						<h4><?php echo $settings['trDate'] ?></h4>
						<p class="_pdl_frm2_lft_crd_mnth"><?php echo $settings['monthName'] ?></p>
						<p class="_pdl_frm2_lft_crd_day"><?php echo $settings['dayName'] ?></p>
					</div>

					<div class="_pdl_frm2_lft_info">
						<h2><?php echo $settings['signupkey'] ?></h2>
						<ul>
							<li><?php echo $settings['clubName'] ?></li>
							<li><?php echo $settings['city'] ?></li>
						</ul>
					</div>
				</div>

			</div>

		</div>
		<!-- LEFT END -->

		<!-- RIGHT -->
		<div class="div_form" style="background: <?php echo $settings['rightBg'] ?>; padding-top: <?php echo $settings['formPaddingTop'] ?>; ">
			<h2><?php echo $settings['enterDetailsTxt'] ?></h2>
      <div class="main_form">
      <div class="_pdl_frm2_r8_main_top">
            <ul>
              <li  @click="playerChange(false)"  style="background: <?php echo $settings['player1Bg'] ?>">Spelare 1</li>
              <li  @click="playerChange(true)"  style="background: <?php echo $settings['player2Bg'] ?>"> Spelare 2</li>
            </ul>
          </div>
			<div class="_pdl_frm2_r8_main " style="background: <?php echo $settings['player1Bg'] ?>" v-if="!showPlayer2">
        <div  >
          <div class="_pdl_frm2_r8_sngl">
            <p>E-postadress</p>
            <i-input  type="text"  v-model="data.email" @on-blur="searchUser">
          </div>

          <div class="_pdl_frm2_r8_sngl_all">
            <div class="_pdl_frm2_r8_sngl">
            <p>Förnamn</p>
            <i-input class="_firstname" type="text"  v-model="data.firstName"  >
            </div>

            <div class="_pdl_frm2_r8_sngl">
            <p>Efternamn</p>
            <i-input class="_lastname" type="text"  v-model="data.lastName"  >
            </div>
          </div>
          <div class="_pdl_frm2_r8_sngl_all">
            <div class="_pdl_frm2_r8_sngl">
              <p>Mobiltel</p>
              <i-input class="_phone" type="text"  v-model="data.phone"  >
            </div>
            <div class="_pdl_frm2_r8_sngl">
              <p>Ranking</p>
              <i-input class="_phone" type="text"  v-model="data.ranking"  >
              
            </div>
            
          </div>
          <div class="_pdl_frm2_r8_sngl_all">
            <div class="_pdl_frm2_r8_sngl" v-if="allClasses.length">
                <p>Class</p>
                <i-select class="_class" filterable placeholder="Class"  v-model="data.className" @on-change="changeClass">
                  <i-option v-for="k in allClasses" :value="k.className">{{k.className}}</i-option>
                </i-select>
            </div>
            <div class="_pdl_frm2_r8_sngl">
              <p>Lagnamn</p>
              <i-input class="_phone" type="text"  v-model="data.teamName"  >
            </div>
          </div>
        </div>
        
      </div>
      <div class="_pdl_frm2_r8_main" v-if="showPlayer2" style="background: <?php echo $settings['player2Bg'] ?>">
          
          <div class="_pdl_frm2_r8_sngl">
          <p>E-postadress</p>
            <i-input  type="text"  placeholder="Partner email" v-model="data.email2" @on-blur="searchUser2">
          </div>

          <div class="_pdl_frm2_r8_sngl_all">
            <div class="_pdl_frm2_r8_sngl">
            <p>Förnamn</p>
            <i-input class="_firstname" type="text"  v-model="data.firstName2"  >
            </div>

            <div class="_pdl_frm2_r8_sngl">
            <p>Efternamn</p>
            <i-input class="_lastname" type="text"  v-model="data.lastName2"  >
            </div>
          </div>

          <div class="_pdl_frm2_r8_sngl_all">
            <div class="_pdl_frm2_r8_sngl">
            <p>Mobiltel</p>
              <i-input class="_phone" type="text"  v-model="data.phone2"  >
            </div>
            <div class="_pdl_frm2_r8_sngl">
              <p>Ranking</p>
              <i-input class="_phone" type="text"  v-model="data.ranking2"  >
            </div>

          </div>
          </template>

        </div>
			<!-- <div class="_pdl_frm2_r8_main _mr_t55">

			</div> -->

			<div class="_pdl_frm2_r8_btn">
        <i-button  @click="signup"   :disabled = "isSending" :loading="isSending">{{isSending ? 'Please wait...' : 'Anmäl'}} </i-button>
			</div>
		</div>
		<!-- RIGHT END -->

	</div>
  </div>
  
</divisionsignupone>



<script type="text/javascript">

   axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"


	var list = document.getElementsByTagName("divisionsignupone");
    //console.log(list)
        var settings = JSON.parse(jsSettings)
        console.log(settings.allClasses)
        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "divisionsignupone-app-" + i);
            var app = new Vue({
            el: "#divisionsignupone-app-" + i,
                data(){
                    return {
                       isDisabled : true,
                       searching: false,
                       allClasses: settings.allClasses,
                       shirtSizes: settings.allTShirtSizes,
                       data: {
                         email : '',
                         firstName : '',
                         lastName : '',
                         phone : '',
                         email2 : '',
                         firstName2 : '',
                         lastName2 : '',
                         phone2 : '',
                         teamName : '',
                         ranking : '',
                         ranking2 : '',
                         className: '', 
                       },
                       showPlayer2: false, 
                       signupkey: settings.signupkey,
                      
                       hasFound: false,
                       isSending: false,
                       done: false,

                    }
                },
                methods: {
                  changeClass(m){
                    // for(let d of this.allClasses){
                    //       if(m == d.className){
                    //          this.maxSignup = d.maxInClass
                    //          break
                    //       }
                    //    }
                       
                    },
                  playerChange(m){
                      this.showPlayer2 = m
                      console.log(this.showPlayer2)
                      console.log(m)
                  },
                    async searchUser(){
                       this.searching = true
                       const res = await this.callApi('post', `${apiUrl}/get-user-info`, {email: this.data.email})
                       if(res.status==200){
                           this.data.email = res.data.email
                           this.data.firstName = res.data.firstName
                           this.data.lastName = res.data.lastName
                           this.data.phone = res.data.phone
                          
                        }else{
                           this.isDisabled = false
                           this.searching = false
                           this.hasFound = false
                        }
                    },
                    async searchUser2(){
                       this.searching = true
                       const res = await this.callApi('post', `${apiUrl}/get-user-info`, {email: this.data.email2})
                       if(res.status==200){
                           this.data.email2 = res.data.email
                           this.data.firstName2 = res.data.firstName
                           this.data.lastName2 = res.data.lastName
                           this.data.phone2 = res.data.phone


                           this.isDisabled = true
                           this.searching = false
                           this.hasFound = true
                        }else{
                           this.isDisabled = false
                           this.searching = false
                           this.hasFound = false
                        }
                    },
                    async signup(){
                       this.data.signupkey = this.signupkey
                       console.log(this.data)
                       if(this.data.email.trim()=='') return this.i('Email is required')
                       if(this.data.firstName.trim()=='') return this.i('Firstname is required')
                       if(this.data.lastName.trim()=='') return this.i('Lastname is required')
                       if(this.data.teamName.trim()=='') return this.i('Team name is required')
                       if(!this.hasFound){
                          if(this.data.phone.trim()=='') return this.i('Phone is required')
                       }
                       if(this.data.email2.trim()=='') return this.i('Player 2 email is required')
                       if(this.data.firstName2.trim()=='') return this.i('Player 2 firstname is required')
                       if(this.data.lastName2.trim()=='') return this.i('Player 2 lastname is required')



                       this.isSending = true
                       const res = await this.callApi('post', `${apiUrl}/div-register`, this.data)
                       if(res.status==200){
                         this.isSending = false
                         this.done = true
                         this.s(res.data.msg)
                       }else if(res.status==401){
                          this.i(res.data.msg)
                       }
                       else{
                          this.i("There went wrong while registering. Please try again or contact us!")

                       }
                       this.isSending = false

                    },
                    async callApi(method, url, dataObj) {
                      try {

                          let data = await axios({
                              headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              method: method,
                              url: url,
                              data: dataObj
                          })
                          return data

                      } catch (e) {

                          return e.response
                      }
                  },
                  i(msg){
                    this.$Notice.info({
                      title: 'Hey!',
                      desc: msg
                    });
                  },
                  s(msg){
                    this.$Notice.success({
                      title: 'Great!',
                      desc: msg
                    });
                  },

                },
                created(){

                },


            })

        }
 </script>
