<?php
namespace Elementor;


class DivisionSignupOne extends Widget_Base {
	
	public function get_name() {
		return 'Division-signup-1';
	}
	
	public function get_title() {
		return 'TEAM SIGNUP';
	}
	
	public function get_icon() {
		return 'fa fa-user-plus';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'signupkey',
			[
				'label' => __( 'Tournament name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'TÄVLINGSANMÄLAN', 'elementor' ),
				'default' => 'TÄVLINGSANMÄLAN'
			]
		);
		$this->add_control(
			'clubName',
			[
				'label' => __( 'Club name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Globen Arena', 'elementor' ),
				'default' => 'KLUBBNAMN'
			]
		);
		$this->add_control(
			'city',
			[
				'label' => __( 'City', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'PADELSTADEN', 'elementor' ),
				'default' => 'PADELSTADEN'
			]
		);

		$this->add_control(
			'trDate',
			[
				'label' => __( 'Tournament date', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Day number', 'elementor' ),
				'default' => '25'
			]
		);
		$this->add_control(
			'monthName',
			[
				'label' => __( 'Month name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Month name', 'elementor' ),
				'default' => 'Jun'
			]
		);
		$this->add_control(
			'dayName',
			[
				'label' => __( 'Day name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Day name', 'elementor' ),
				'default' => 'Fre'
			]
		);
		$this->add_control(
			'enterDetailsTxt',
			[
				'label' => __( 'Header text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your details', 'elementor' ),
				'default' => 'Tävlingsanmälan'
			]
		);
		$this->add_control(
			'sectionHeight',
			[
				'label' => __( 'Widget height', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Height', 'elementor' ),
				'default' => '670px'
			]
		);
		$this->add_control(
			'formPaddingTop',
			[
				'label' => __( 'Form padding top', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Height', 'elementor' ),
				'default' => '90px'
			]
		);


		$this->add_control(
			'leftBg',
			[
				'label' => __( 'Left background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		$this->add_control(
			'rightBg',
			[
				'label' => __( 'Right background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		$this->add_control(
			'player1Bg',
			[
				'label' => __( 'Player1 background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => '#F4F2F0'
			]
		);
		$this->add_control(
			'player2Bg',
			[
				'label' => __( 'Player2 background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => '#dcdbd9'
			]
		);
		


		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'className',
			[
				'label' => __('Class name', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Men class', 'plugin-domain'),
				'label_block' => true,
			]
		);

		
		$this->add_control(
			'allClasses',
			[
				'label' => __('Classes', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [],
				'title_field' => '{{{ className }}}',
			]
		);

		




		
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        
        require('divisionSignupOneHtml.php');
    }
   

	// js render
	
	


	
	
}