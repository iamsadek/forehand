<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsCalenderData = json_encode($calendarData);
echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";
// print_r($settings['eventsCat']); 
// die('trying to see ');


?>

<style>
	._trmnt3_crd_top_lft{
		background : <?php echo $settings['dateBgColor'] ?>;
		border : 2px solid <?php echo $settings['dateBorderColor'] ?>;

	}
	._trmnt3_crd_top_p1{
		color: <?php echo $settings['dateTxtColor'] ?> !important;
	}
	._trmnt3_crd_top_h3{
		color: <?php echo $settings['dateTxtColor'] ?> !important;
	}
	._trmnt3_crd_top_p2{
		color: <?php echo $settings['dateTxtColor'] ?> !important;
	}
	._trmnt3_crd_btm_p{
		color : <?php echo $settings['priceColor'] ?>;
		background : <?php echo $settings['priceBgColor'] ?>;
	}
	._trmnt3_crd_btm_h3{
		color : <?php echo $settings['titleColor'] ?>;
	}
	._trmnt3_crd_loctn_p{
		color : <?php echo $settings['locColor'] ?>;
	}
	._trmnt3_crd_loctn i{
		color : <?php echo $settings['locColor'] ?>;
	}
	
</style>


<calendarstylethree >
    <div v-cloak>
    <div class="_clndr2_main_sec">
	    <div class="_clndr2_mnu">
	    	<ul class="_clndr2_mnu_ul">
				
	    		<li :class="catIndex==i? '_clndr2_mnu_active' : '' " v-for="(eventCat, i) in headerStyleOneSetting.eventsCat" v-if="headerStyleOneSetting.eventsCat.length" @click="changeCat(eventCat, i)">
	    			<p>{{eventCat == 'All' ? 'Alla' : eventCat}}</p>
	    		</li>
	    		

			</ul>
			<!-- <div class="_right">
				<button class="next_btn" @click="next(false)" v-if="prev>0">Prev</button>
				<button class="next_btn" @click="next(true)">Next</button>
			</div> -->
	    </div>
	    <div class="_clndr2_dte_lst" ref="sl">
	    	<!-- ITEAM -->
	    <div class="_clndr2_dte_itm" v-for="(calendar, key) in eventDates" v-if="eventDates" >
				<p class="_clndr2_dte_itm_p1 _clndr2_dte_active" v-if="month==calendar[0].monthName"  :style="{color: headerStyleOneSetting.selectedTxtColor}">
					{{calendar[0].monthName}}
				</p>
				<p class="_clndr2_dte_itm_p1 _clndr2_dte_active" v-if="month!=calendar[0].monthName"  :style="{color: headerStyleOneSetting.unselectedTxtColor}">
					{{calendar[0].monthName}}
				</p>
				<div class="_clndr2_dte_itm_flx">
				    <div v-for="(c,j) in calendar" v-if="calendar.length" class="days" @click="getData(c, `${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`)">
						
							<ul class="_clndr2_dte_itm_ul">
								<li>
								   
									<p class="_clndr2_dte_itm_p2 _clndr2_dte_active" v-if="monthName==`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`"  :style="{color: headerStyleOneSetting.selectedTxtColor}">
										{{c.dayOfTheMonth}}
									</p>
									<p class="_clndr2_dte_itm_p2 _clndr2_dte_active" v-if="monthName!=`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`" :style="{color: headerStyleOneSetting.unselectedTxtColor}">
										{{c.dayOfTheMonth}}
									</p>
									<p class="_clndr2_dte_itm_p3 _clndr2_dte_active" v-if="monthName==`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`"  :style="{color: headerStyleOneSetting.selectedTxtColor}">
										{{c.dayName}}
									</p>
									<p class="_clndr2_dte_itm_p3 _clndr2_dte_active" v-if="monthName!=`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`" :style="{color: headerStyleOneSetting.unselectedTxtColor}">
										{{c.dayName}}
									</p>
								</li>


							</ul>
						</div>
					</div>
		    	</div>
	    	<!-- ITEAM -->

	    	  <!-- arrow -->
	    	  <!-- <div class="_clndr2_dte_arrow_all">
		           <div class="_clndr2_dte_arrow _clndr2_arrow_lft" @click="slide('prev')">
		              <span>
		               <i class="fa fa-arrow-left"></i>
		              </span>
		            </div>
		            <div class="_clndr2_dte_arrow _clndr2_arrow_r8" @click="slide('next')">
		              <span>
		               <i class="fa fa-arrow-right"></i>
		              </span>
		            </div>
            </div> -->
           <!-- arrow -->

	    </div>
	    <!-- CARD SECTION -->
		<div class="_trmnt3_crd_sec">
			<div class="_trmnt3_card_all" v-for="(c, key) in eventData" v-if="eventData.length">
	
				<div class="_trmnt3_crd">
					<img :src="imageSource+c.eventImg" alt="image">
					<div class="_trmnt3_crd_top">
					<div class="_trmnt3_crd_top_lft">
						<p class="_trmnt3_crd_top_p1">
						{{c.monthName.substring(0,3)}}
						</p>
						<h3 class="_trmnt3_crd_top_h3">
						{{c.dayOfTheMonth}}
						</h3>
						<p class="_trmnt3_crd_top_p2">
						{{c.eventTime}}
						</p>
					</div>
					<div class="_trmnt3_crd_top_icon">
						<span>
						<i class="fa fa-share"></i>
						</span>
					</div>
					</div>

					<div class="_trmnt3_crd_btm" @click="goNext(c)">
						<p class="_trmnt3_crd_btm_p">
						{{c.eventFee == 0 ? 'GRATIS' : c.eventFee }} {{c.eventFee > 0 ? 'kr' : ''}}
						</p>
						<h3 class="_trmnt3_crd_btm_h3">
						{{c.eventName}}
						</h3>
						<div class="_trmnt3_crd_loctn">
							<span>
								<i class="fas fa-map-marker-alt"></i>
							</span>
								<p class="_trmnt3_crd_loctn_p">
								{{c.eventLocation}}
								</p>
								
						</div>
						<div class="_trmnt3_crd_loctn">
							<span>
								<i class="fa fa-user"></i>
							</span>
								<p class="_trmnt3_crd_loctn_p">
									{{c.__meta__.singedup_count}}/{{c.eventLimit}} platser kvar
								</p>
								
						</div>
					</div>
				</div>
		
			</div>
		</div>
	    <!-- CARD SECTION -->
  	</div>
    </div>
</calendarstylethree>



<script type="text/javascript">

    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
	
	jsCalenderData = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    jsCalenderData = JSON.parse(jsCalenderData)
	
    // eventData  = JSON.parse(eventData)
    // console.log('data is', eventData)
    

    var list = document.getElementsByTagName("calendarstylethree");
    console.log(list)

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "calendarstylethree-app-" + i);
            var app = new Vue({
            el: "#calendarstylethree-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting,
                        eventDates: jsCalenderData.eventDates,
                        eventData: [],
                        items: [2,2,3,4,5],
                        isModal: false,
                        c : false,
                        imageSource: imageSource,
						index: 0,
						eventItems: [],
						keyName: '',
						prev: 0,
                        nextEl: 0,
                        isDone: false,
						apiUrl : apiUrl,
						catIndex: 0,
						dateIndex : 0, 
						monthName : '', 
						month : '', 
						s: 0,

                    }
                },
                methods: {
					slide(type) {
						let el = this.$refs.sl;
						console.log(el);
						//el.scrollLeft = this.s;
						if (type == "next") {
							this.s += 500;
						} else {
							this.s -= 500;
						}
					},
                    async getData(event, dateString){
						this.monthName = dateString
						this.month = dateString.split("-")[0]
						const res = await axios.get(`${apiUrl}/getEventByDate?date=${event.eventDate}`)
						if(res.status==200){
                            this.eventData = res.data
						}else{
							alert('Something went wrong while loading the data, please try again or contact us!')
						}
					},
                    async changeCat(cat, i){
						this.catIndex = i
						const res = await axios.get(`${apiUrl}/calendarThree?cat=${cat}`)
						if(res.status==200){
                            this.eventDates = res.data.eventDates
                            this.eventData = res.data.eventData
							if(this.eventData.length){
								//let c = this.eventData[0]
								//this.monthName = `${c.monthName}-${c.dayOfTheMonth}-${c.dayName}`
								//this.month = c.monthName
							}
						}else{
							alert('Something went wrong while loading the data, please try again or contact us!')
						}
					},
					goNext(c){
						window.location = c.landingPage
					}

                },
                created(){
					console.log(jsCalenderData)
					this.eventData = jsCalenderData.eventData

					if(this.eventData.length){
						//let c = this.eventData[0]
						//this.monthName = `${c.monthName}-${c.dayOfTheMonth}-${c.dayName}`
						//this.month = c.monthName
					}
                },


            })

        }
 </script>

