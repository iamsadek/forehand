 <style>
   ._2banner_card{
      background: <?php echo $settings['boxBgColor'] ?>;
   }
 </style>
 
 <header3 >
 <!-- BANNER -->
 <div class="_2banner" style="background-image: url(<?php echo $settings['banner']['url'] ?>)">
       <div class="_2banner_card">
          <div class="_2banner_card_dte">
            <p class="_2banner_card_dte_txt1"><?php echo $settings['month'] ?></p>
            <p class="_2banner_card_dte_txt2"><?php echo $settings['date'] ?></p>
            <p class="_2banner_card_dte_txt3"><?php echo $settings['year'] ?></p>
          </div>
          <div class="_2banner_card_mdl">
            <h3 class="_2banner_card_mdl_h3"><?php echo $settings['title'] ?></h3>
            <ul class="_2banner_card_mdl_ul">
              <li class="_line_aftr"><?php echo $settings['clubName'] ?></li>
              <li><?php echo $settings['address'] ?></li>
            </ul>
          </div>
          <div class="_2banner_card_btm">
            <p class="_2banner_card_btm_txt">
               <?php echo $settings['regTxt'] ?> 
            </p>
            <ul class="_2banner_card_btm_ul">
              <li>
               <a href="<?php echo $settings['buttonLink']['url'] ?>" class="_2banner_card_btn _s_btn1"><?php echo $settings['signUpTextBtn'] ?></a>
              </li>
                <li onclick="copyLink()">
                  <p  class="_2banner_card_btn _s_btn2" ><?php echo $settings['shareTextBtn'] ?></p>
              </li>
            </ul>
          </div>
        </div>
    </div>
     <!-- BANNER END -->
       <!-- TOURNAMENT INFO -->
       <div class="_trnmnt_info" style="background: <?php echo $settings['bannerBg'] ?>">
          <!-- ITEAM -->
          <?php if($settings['list'] ): ?> 
            <?php foreach($settings['list'] as $item ): ?>
              <div class="_trnmnt_info_itm">
                <p class="_trnmnt_info_itm_txt" style="color: <?php echo $settings['bannerFooterTxtColor'] ?> ">
                  <?php echo $item['headingOne'] ?>
                </p>
                <h4 class="_trnmnt_info_itm_h4" style="color: <?php echo $settings['bannerFooterTxtColor'] ?> ">
                <?php echo $item['headingTwo'] ?>
                </h4>
              </div>
            <?php endforeach; ?>
            <?php endif; ?>
          <!-- ITEAM -->
    </div>
     <!-- TOURNAMENT INFO -->
     </header3>

<script>
function copyLink(){
  var input = document.body.appendChild(document.createElement("input"));
  input.value = window.location.href;
  input.focus();
  input.select();
  document.execCommand('copy');
  input.parentNode.removeChild(input);
  alert('Link copied')
}
</script>