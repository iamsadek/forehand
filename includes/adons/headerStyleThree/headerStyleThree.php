<?php
namespace Elementor;

class HeaderStyleThree extends Widget_Base {
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name() {
		return 'forehand-header-style-three';
	}
	
	public function get_title() {
		return 'HEADER STYLE 3';
	}
	
	public function get_icon() {
		return 'fas fa-heading';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Box background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#2b436efa'
			]
		);

		// MONTH
		$this->add_control(
			'month',
			[
				'label' => __( 'Month', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your month', 'elementor' ),
				'default' => 'Maj'
			]
		);
		// date
		$this->add_control(
			'date',
			[
				'label' => __( 'Date', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::NUMBER,
				'placeholder' => __( 'Enter your date', 'elementor' ),
				'default' => '12'
			]
		);
		// year
		$this->add_control(
			'year',
			[
				'label' => __( 'Year', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your year', 'elementor' ),
				'default' => '2020'
			]
		);
		// title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Viasat Padel Open'
			]
		);
		//club name
		$this->add_control(
			'clubName',
			[
				'label' => __( 'Club name', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Padelcenter'
			]
		);
		//club name
		$this->add_control(
			'address',
			[
				'label' => __( 'Address', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Globen | Stockholm'
			]
		);
		//registration text
		$this->add_control(
			'regTxt',
			[
				'label' => __( 'Registration text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Anmälan öppen till och med 7 maj'
			]
		);
		//sign up text
		$this->add_control(
			'signUpTextBtn',
			[
				'label' => __( 'Signup text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Anmälan'
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		//share this text
		$this->add_control(
			'shareTextBtn',
			[
				'label' => __( 'Share this text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Dela'
			]
		);
		// image 
		$this->add_control(
			'banner',
			[
				'label' => __( 'Choose Banner', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/bg2.jpg',
				],
			]
		);

		// background color for footer 
		$this->add_control(
			'bannerBg',
			[
				'label' => __( 'Banner footer background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#d8e5f5'
			]
		);
		$this->add_control(
			'bannerFooterTxtColor',
			[
				'label' => __( 'Banner footer text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);

		//  repeater starts .... 

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'title', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Title 1' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'headingOne', [
				'label' => __( 'Heading one', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Prispengar'
			]
		);
		$repeater->add_control(
			'headingTwo', [
				'label' => __( 'Heading two', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => '38 000 kr'
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'Banner footer', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'title' => __( 'Title #1', 'plugin-domain' ),
						'headingOne' => __( 'Prispengar', 'plugin-domain' ),
						'headingTwo' => __( '38 000 kr' ),
					],
					[
						'title' => __( 'Title #2', 'plugin-domain' ),
						'headingOne' => __( 'Klasser', 'plugin-domain' ),
						'headingTwo' => __( 'Herr & Dam' ),
					],
					[
						'title' => __( 'Title #3', 'plugin-domain' ),
						'headingOne' => __( 'Origanisatör', 'plugin-domain' ),
						'headingTwo' => __( 'Forehand' ),
					],
					[
						'title' => __( 'Title #4', 'plugin-domain' ),
						'headingOne' => __( 'Lottning & Matcher', 'plugin-domain' ),
						'headingTwo' => __( '3 dagar innan' ),
					],
					[
						'title' => __( 'Title #5', 'plugin-domain' ),
						'headingOne' => __( 'Turneringen sanktionerad av', 'plugin-domain' ),
						'headingTwo' => __( 'Svenska Padel Förbundet'),
					],
					
				],
				'title_field' => '{{{ title }}}',
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('headerStyleThreeHtml.php');
    }
   
	
	


	
	
}