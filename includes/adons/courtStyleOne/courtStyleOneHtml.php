<div class="_do_pdl_event_sec" style="background: <?php  echo $settings['boxBgColor'] ?>">
		
            <!-- ITEAM -->
        <?php if($settings['courts'] ): ?> 
            <?php foreach($settings['courts'] as $item ): ?>
                <div class="_do_pdl_event_crd">
                    <img src="<?php echo $item['courtImg']['url'] ?>" alt="image">
                    <div class="_do_pdl_event_crd_btm" style="background: <?php  echo $item['courtTxtBg'] ?>">
                        <div class="_do_pdl_event_crd_btm_lft">
                            <h3 style="color: <?php  echo $item['header1Color'] ?>"><?php echo $item['header1'] ?></h3>
                            <p style="color: <?php  echo $item['header2Color'] ?>"><?php echo $item['header2'] ?></p>
                        </div>
                        <div class="_do_pdl_event_crd_btm_r8">
                            <img src="<?php echo $item['sponsorImg']['url'] ?>" alt="image">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
		<!-- ITEAM -->

		
	</div>


