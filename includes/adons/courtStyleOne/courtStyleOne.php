<?php
namespace Elementor;

class CourtStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-court-style-one';
	}
	
	public function get_title() {
		return 'COURT STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-quote-left';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		
		
		
        
        $repeater = new \Elementor\Repeater();

		
		
		$repeater->add_control(
			'courtImg', [
				'label' => __( 'Court background image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'header1', [
				'label' => __( 'Header 1', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Bana 1' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'header1Color',
			[
				'label' => __( 'Header 1 background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		$repeater->add_control(
			'header2', [
				'label' => __( 'Header 2', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Padelcenter' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'header2Color',
			[
				'label' => __( 'Header 1 background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#c7c7c7'
			]
		);
		$repeater->add_control(
			'sponsorImg', [
				'label' => __( 'Sponsor image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		
		
		$repeater->add_control(
			'courtTxtBg',
			[
				'label' => __( 'Court text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		
		
		

		$this->add_control(
			'courts',
			[
				'label' => __( 'Courts', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'header1' => 'Bana 1'
					],
					[
						'header1' => 'Bana 2'
					],
					[
						'header1' => 'Bana 3'
					],
				],
				'title_field' => '{{{ header1 }}}',
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('courtStyleOneHtml.php');
    }
   
	
	


	
	
}