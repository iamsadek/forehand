<?php
namespace Elementor;


class CalendarStyleFour extends Widget_Base {
	
	public function get_name() {
		return 'forehand-calendar-four';
	}
	
	public function get_title() {
		return 'CALENDAR STYLE 4';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'landingUrl', [
				'label' => __( 'Landing page url', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);



		// news title
		// $this->add_control(
		// 	'title',
		// 	[
		// 		'label' => __( 'Title', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Enter your title', 'elementor' ),
		// 		'default' => 'Events'
		// 	]
		// );
		// $this->add_control(
		// 	'selectedTxtColor',
		// 	[
		// 		'label' => __( 'Selected text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'black'
		// 	]
		// );
		// $this->add_control(
		// 	'unselectedTxtColor',
		// 	[
		// 		'label' => __( 'Unselected text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'gray'
		// 	]
		// );

		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $calendarData = $this->getCalendarData();
        require('calendarStyleFourHtml.php');
    }
    public function getCalendarData(){
		
        $response = wp_remote_request( FOREHAND_API_URL.'/calendarData?limit=50&c=4',
            array(
				'method'     => 'GET', 
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
		$body = wp_remote_retrieve_body($response);
		
        return json_decode($body);
    }

	// js render
	
	


	
	
}