<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsCalenderData = json_encode($calendarData);

echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";

?>
<calendarstylefour >
    <div v-cloak>
    <div class="_evnt5_main_sec">
			<!-- <div class="_evnt5_mnu">
				<ul class="_evnt5_mnu_ul">
					<li class="_evnt5_mnu_actv">all events</li>
					<li>padel</li>
					<li>tennis</li>
					<li>badminton</li>
					<li>table tennis</li>
					<li>squash</li>
				</ul>
			</div> -->
			<div class="_evnt5_card2_all" v-for="(calendar, key) in jsCalenderData" v-if="jsCalenderData">
				<h3 class="_evnt5_main_sec_h3"><span>{{calendar[0].monthName}}</span></h3>
				<!-- CARD -->
               
				<div class="_evnt5_card2" v-for="(c , i) in calendar" v-if="calendar" >
					<div class="_evnt5_card2_lft">
						<img :src="imageSource+c.eventImg" alt="image">
						
					</div>
					<div class="_evnt5_card2_r8">
						<div class="_evnt5_card2_r8_top">
							<ul class="_evnt5_card2_r8_top_ul">
								<li class="_evnt5_mtch_dte_li">
									<p class="_evnt5_mtch_dte_p _devider">{{calendar[0].dayOfTheMonth}} {{calendar[0].monthName}}</p>
									<p class="_evnt5_mtch_dte_p">{{calendar[0].dayName}}</p>
								</li>
								<li>
									{{c.eventTime}}
								</li>
							</ul>
						</div>
						<div class="_evnt5_card2_r8_titl">
							<h3 class="_evnt5_card2_r8_titl_h3">{{c.eventName}}</h3>
							<p class="_evnt5_card2_r8_titl_p _desktoponly">{{c.eventFee}} kr</p>
						</div>
						<div class="_evnt5_card2_r8_txt">
							<p class="_evnt5_card2_r8_txt_p">{{c.eventDescription}}</p>
						</div>
						<div class="_evnt5_card2_r8_btm">
							<!-- <div class="_evnt5_crd2_btm_lst2">
								<div class="_evnt5_crd2_btm_img2" v-for="(u, i) in c.accepted" v-if="c.accepted">
									<img :src="imageSource+u.user.profilePic" alt="image">
								</div>
								
							</div> -->
							<div class="_evnt5_crd2_btm_txt">
								<!-- <p class="_evnt5_crd2_btm_txt_p" v-if="c.accepted.length < c.__meta__.singedup_count">
									<span>+{{c.__meta__.singedup_count-c.accepted.length}}</span> Others attending
								</p> -->
								<!-- <p class="_evnt5_crd2_btm_txt_p" v-if="c.accepted.length == c.__meta__.singedup_count && c.__meta__.singedup_count>0">
                                    Attended
								</p> -->
								<!-- <p class="_evnt5_crd2_btm_txt_p" v-if="c.__meta__.singedup_count==0">
                                    Reserve your spot!
								</p> -->
							</div>


							<div class="_evnt5_crd2_btm_txt">
								<p class="_evnt5_crd2_btm_txt_p">
                                    <i class="fa fa-user gray_color" aria-hidden="true" ></i> <span>{{c.__meta__.singedup_count}}/{{c.eventLimit}}</span> platser kvar
                                    <p class="_evnt5_card2_r8_titl_p _onlymobile">{{c.eventFee}} kr</p>
								</p>
							</div>
							<div class="_evnt5_crd2_btm_txt">
                            <div class="forehand_button signup_btn">
                                    <a :href="`${headerStyleOneSetting.landingUrl.url}?eventId=${c.id}`">
                                        <button class="fbutton">
                                            Anmälan
                                        </button>
                                    </a>
                                </div>
							</div>
						</div>
					</div>
				</div>
                
				<!-- CARD -->
            </div>




			</div>
		</div>
    </div>
</calendarstylefour>



<script type="text/javascript">

axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
    var jsdata = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    console.log('this is wow')
    jsCalenderData  = JSON.parse(jsdata)
   
    


    var list = document.getElementsByTagName("calendarstylefour");
    console.log(list)

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "calendarstylefour-app-" + i);
            var app = new Vue({
            el: "#calendarstylefour-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting,
                        jsCalenderData: jsCalenderData,
                        items: [2,2,3,4,5],
                        isModal: false,
                        eventInfo : false,
                        imageSource: imageSource,
                        apiUrl : apiUrl,
                        prev: 0,
                        nextEl: 0,
                        isDone: false

                    }
                },
                methods: {
                    showEventInfo(c){
                        this.eventInfo = c
                        this.isModal = true
                    },
                    next(type){
                        let id
                        for(let k in this.jsCalenderData){
                            if(type){
                                id = this.jsCalenderData[k][0].lastId
                            }else{
                                id = this.jsCalenderData[k][0].firstId
                            }
                            break
                        }
                        this.getData(id,type)

                    },
                    async getData(id,type){

                        if(type){
                            this.prev++
                            this.nextEl++
                        }else{
                            this.prev--
                            this.nextEl--
                            this.isDone = false
                        }
                        const res = await axios.get(`${apiUrl}/calendarData?next=${id}&type=${type}`)
						if(res.status==200){
                            console.log(res.data)
                            if(window._.isEmpty(res.data)){
                                this.isDone = true
                                alert('No more future events found!')
                            }else{
                                this.jsCalenderData = res.data

                            }

						}else{
							alert('Something went wrong while loading the data, please try again or contact us!')
						}
                    }

                },
                created(){
                   console.log('data is', this.jsCalenderData)
                },


            })

        }
 </script>
