<?php
namespace Elementor;

class PadelTripsStyleThree extends Widget_Base {
	
	public function get_name() {
		return 'forehand-padelTrips-text-style-three';
	}
	
	public function get_title() {
		return 'PADEL TRIPS STYLE 3';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'White'
			]
		);
	        
    
		$this->add_control(
			'headingOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Great Academy' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		$this->add_control(
			'description',
			[
				'label' => __( 'Description', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 10,
				'default' => __( 'We can provide you with great padel training. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem ape riam, eaqueopsa quae ab illo.', 'plugin-domain' ),
				'placeholder' => __( 'Type your description here', 'plugin-domain' ),
			]
		);
		$this->add_control(
			'descriptionColor',
			[
				'label' => __( 'Description text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'buttonText',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'VIEW ALL RACKETS'
			]
		);
		
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],
			
			]
		);

		
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('padelTripsStyleThreeHtml.php');
    }
   
	
	


	
	
}