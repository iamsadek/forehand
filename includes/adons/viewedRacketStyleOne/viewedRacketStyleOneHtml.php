<div class="_do_pdl3_pepl_view_sec" style="background-color: <?php echo $settings['sectionBackground'] ?>;">
		<div class="_do_pdl3_pepl_view_top">
		  <h3  style="color : <?php echo $settings['headerOneColor'] ?>">
            <?php echo  $settings['headerOne']?></h3>
		</div>
		<div class="_do_pdl3_pepl_view_card_all">
           
        <?php if($settings['list'] ): ?> 
        <?php foreach($settings['list'] as $item ): ?>
		 
				<div class="_do_pdl3_pepl_view_card">
				<a href="<?php echo $item['link']['url'] ?>">
					<div class="_do_pdl3_pepl_view_card_inner">
						<div class="_do_pdl3_pepl_view_card_top">
			<img src="<?php echo $item['image']['url'] ?>" alt="image">
						</div>
						<div class="_do_pdl3_pepl_view_card_info">
							<div class="_do_pdl3_pepl_view_info_lft" style="background-color: <?php echo $item['backgroundColorOne'] ?>;" >
				<p class="_do_pdl3_pepl_view_info_txt1" style="color : <?php echo $item['textOneColor'] ?>">
				<?php echo  $item['textOne']?></p>
								<h3  style="color : <?php echo $item['textTwoColor'] ?>">
				<?php echo  $item['textTwo']?></h3>
								<p class="_do_pdl3_pepl_view_info_txt2"style="color : <?php echo $item['textThreeColor'] ?>">
				<?php echo  $item['textThree']?></p>
							</div>                                       
							<div class="_do_pdl3_pepl_view_info_r8"   style="background-color: <?php echo $item['backgroundColorTwo'] ?>;">
				<h3  style="color : <?php echo $item['textFourColor'] ?>">
				<?php echo  $item['textFour']?></h3>
							</div>
						</div>
					</div>
					</a>
				</div>
			
			<!-- CARD -->
       
      <?php endforeach; ?>
            <?php endif; ?>
		</div>
	</div>

	