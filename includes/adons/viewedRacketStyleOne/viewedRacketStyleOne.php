<?php
namespace Elementor;

class ViewedRacketStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-viewedracket-text-style-one';
	}
	
	public function get_title() {
		return 'VIEWED RACKETS STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
	
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
	

		        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'People also viewed' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
	
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		$repeater->add_control(
			'backgroundColorOne',
			[
				'label' => __( 'Background color one', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		$repeater->add_control(
			'textOne', [
				'label' => __( 'Text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'nox' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'textOneColor',
			[
				'label' => __( 'Text one Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => 'black',
			]
			);
			$repeater->add_control(
				'textTwo', [
					'label' => __( 'Text two', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Tempo World Padel Tour' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$repeater->add_control(
				'textTwoColor',
				[
					'label' => __( 'Text two Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => 'black',
				]
				);
				$repeater->add_control(
					'textThree', [
						'label' => __( 'Text three', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => __( 'Official Racket 2020' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$repeater->add_control(
					'textThreeColor',
					[
						'label' => __( 'Text three Color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => 'black',
					]
					);

					$repeater->add_control(
						'backgroundColorTwo',
						[
							'label' => __( 'Background color two', 'elementor' ),
							'type' => Controls_Manager::COLOR,
							'default' => ''
						]
					);
					$repeater->add_control(
						'textFour', [
							'label' => __( 'Text four', 'plugin-domain' ),
							'type' => \Elementor\Controls_Manager::TEXT,
							'default' => __( '€249.95' , 'plugin-domain' ),
							'label_block' => true,
						]
					);
			
					
			
					$repeater->add_control(
							'textFourColor',
							[
								'label' => __( 'Text four Color', 'plugin-domain' ),
								'type' => \Elementor\Controls_Manager::COLOR,
								'default' => 'black',
							]
					);
					$repeater->add_control(
							'link',
							[
								'label' => __( 'Link', 'plugin-domain' ),
								'type' => \Elementor\Controls_Manager::URL,
								'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
								'show_external' => true,
								'default' => [
									'url' => '',
									'is_external' => true,
									'nofollow' => true,
								],
							]
					);

         
		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
				
					
				],
				
			]
		);

		$this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('viewedRacketStyleOneHtml.php');
    }
   
	
	


	
	
}