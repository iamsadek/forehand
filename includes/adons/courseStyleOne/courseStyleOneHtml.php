<div class="_evnt2_all_pgrm_sec" style="background: <?php echo $settings['boxBgColor'] ?>">
		<div class="_evnt2_pgrm_itm">
			<div class="_evnt2_pgrm_itm_top">
				<ul class="_evnt2_pgrm_itm_ul">
					<li><?php echo $settings['headingTxt'];?></li>
				</ul>
			</div>
			<div class="_evnt2_pgrm_itm_crd_all">
                <?php if($settings['list'] ): ?> 
                    <?php foreach($settings['list'] as $item ): ?>
                        <!-- CARD -->
                        <a href="<?php echo $item['courseLink']['url'] ?>">
                            <div class="_evnt2_pgrm_itm_crd_main">

                                <div class="_evnt2_pgrm_itm_crd">
                                    <img src="<?php echo $item['courseImg']['url']; ?>" alt="image">
                                    <?php if($item['weekday'] ): ?> 
                                        <div class="_evnt2_pgrm_crd_one">
                                            <ul class="_evnt2_pgrm_one_ul">
                                                <li><?php echo $item['weekday']; ?></li>
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                                    
                                    <div class="_evnt2_pgrm_crd_two">
                                        <h3 class="_evnt2_pgrm_two_h3">
                                            <?php echo $item['courseTitle']; ?>
                                        </h3>
                                    </div>
                                    <div class="_evnt2_pgrm_crd_three">
                                        <div class="_evnt2_pgrm_crd_three_lft">
                                            <img src="<?php echo $item['authorImg']['url']; ?>" alt="image">
                                        </div>
                                        <div class="_evnt2_pgrm_crd_three_r8">
                                            <p class="_evnt2_pgrm_crd_r8_p">
                                                <?php echo $item['authorName']; ?> 
                                                <span class="_evnt2_pgrm_pro_clr3" style="background: <?php echo $settings['proTxtBg'] ?> !important; color: <?php echo $settings['proTxtColor'] ?>"><?php echo $item['proTxt']; ?> </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <!-- CARD -->
                    <?php endforeach; ?>
                <?php endif; ?>
			


						
				
			</div>
		</div>
	</div>