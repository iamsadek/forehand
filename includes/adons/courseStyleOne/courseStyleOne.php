<?php

namespace Elementor;

class CourseStyleOne extends Widget_Base
{
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name()
	{
		return 'forehand-course-style-one';
	}

	public function get_title()
	{
		return 'COURSE STYLE 1';
	}

	public function get_icon()
	{
		return 'far fa-images';
	}

	public function get_categories()
	{
		return ['Forehand'];
	}

	protected function _register_controls()
	{

		$this->start_controls_section(
			'section_title',
			[
				'label' => __('Settings', 'elementor'),
			]
		);
		// background color 
		$this->add_control(
			'headingTxt',
			[
				'label' => __('Headline', 'elementor'),
				'type' => Controls_Manager::WYSIWYG,
				'default' => 'Jan 2020'
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __('Section background color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);


		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'courseTitle',
			[
				'label' => __('Course Title', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('A nice course', 'plugin-domain'),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'courseLink',
			[
				'label' => __('Course link', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::URL,
				'default' => __('A nice course', 'plugin-domain'),
				'label_block' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$repeater->add_control(
			'courseImg',
			[
				'label' => __('Course image', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'authorImg',
			[
				'label' => __('Author profile', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'authorName',
			[
				'label' => __('Pro text', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Author Jone', 'plugin-domain'),
				'label_block' => true,
			]
		);


		$repeater->add_control(
			'proTxt',
			[
				'label' => __('Pro text', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Pro', 'plugin-domain'),
				'label_block' => true,
			]
		);
		// background color 
		$this->add_control(
			'proTxtBg',
			[
				'label' => __('Pro text background color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'default' => '#ce5353'
			]
		);
		$this->add_control(
			'proTxtColor',
			[
				'label' => __('Pro text color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$repeater->add_control(
			'weekday',
			[
				'label' => __('Weekday', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Tor 24', 'plugin-domain'),
				'label_block' => true,
			]
		);



		$this->add_control(
			'list',
			[
				'label' => __('Courses', 'plugin-domain'),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'courseTitle' => "Course title",
						'courseImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorName' => "Auhor Jone",
						'proTxt' => "Pro",
					],
					[
						'courseTitle' => "Course title",
						'courseImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorName' => "Auhor Jone",
						'proTxt' => "Pro",
					],
					[
						'courseTitle' => "Course title",
						'courseImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorImg' => \Elementor\Utils::get_placeholder_image_src(),
						'authorName' => "Auhor Jone",
						'proTxt' => "Pro",
						'weekday' => 'Tor 24'
					],
				],
				'title_field' => '{{{ courseTitle }}}',
			]
		);





		$this->end_controls_section();
	}

	// php render. 
	protected function render()
	{
		$settings = $this->get_settings_for_display();
		require('courseStyleOneHtml.php');
	}
}
