<?php
namespace Elementor;

class specDetailsRacketSTyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-specsdetailsrackets-text-style-one';
	}
	
	public function get_title() {
		return 'SPEC DETAILS RACKET STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		
		
		$this->add_control(
			'sectionBackgroundOne',
			[
				'label' => __( 'Section background', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

	
		$this->add_control(
			'headingOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Specifications' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

	
		$this->add_control(
			'headingOneBgColor',
			[
				'label' => __( 'Title background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'specTextOne', [
				'label' => __( 'Specifications text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'L' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'specTextOneColor',
			[
				'label' => __( 'Specifications text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'specTextTwo', [
				'label' => __( 'Specifications text two', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( ' Rorem orem ipsum dolor sit amet, consectetuer adipiscing elit. Aen commodo ligula e get dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'specTextTwoColor',
			[
				'label' => __( 'Specifications text two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);$this->add_control(
			'specTextThree', [
				'label' => __( 'Specifications text three', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Lorem orem ipsum dolor sit amet, consectetuer adipiscing elit. Aen commodo ligula e get dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'specTextThreeColor',
			[
				'label' => __( 'Specifications text three color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
	
		
		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('specDetailsRacketSTyleOneHtml.php');
    }
   
	
	


	
	
}