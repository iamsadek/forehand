<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsmatchData = json_encode($matchData);
echo "<script type='text/javascript'>var jsmatchData='$jsmatchData'</script>";

?>

<trlisting>
	<div v-cloak>
   <div class="_padel10_section">
  

<div class="_padel9_list">
  <ul>
    <li :class="current == 'all' ?  '_padel9_activ' : '' "  @click="changeTr('all')">
      <p><?php echo $settings['allTxt'] ?></p>
    </li>
    <li :class="current == 'Division' ?  '_padel9_activ' : '' " @click="changeTr('Division')">
      <p><?php echo $settings['divTxt'] ?></p>
    </li>
    <li :class="current == 'Bracket' ?  '_padel9_activ' : '' " @click="changeTr('Bracket')">
      <p><?php echo $settings['brTxt'] ?></p>
    </li>
    <li :class="current == 'Americano' ?  '_padel9_activ' : '' " @click="changeTr('Americano')">
      <p><?php echo $settings['amTxt'] ?></p>
    </li>
    <!-- <li>
      <p>mixed</p>
    </li> -->
  </ul>
</div>


  <div class="_padel10_main_all">
     <div class="_padel10_main" :style="{ transform: 'translate3d('+translateValue+'%, 0px, 0px)'  }">
        <!-- ITEAM -->
  			<div class="_padel10_card" v-for="(t, i) in currentTrs" v-if="currentTrs.length" @click="goNext(t)">
  				<img :src="imageSource+t.img.image" alt="image" v-if="!t.standingPic">
  				<img :src="imageSource+t.standingPic" alt="image" v-if="t.standingPic">
  				<div class="_padel10_card_top" v-if="t.competitionType !='Divisions'">
                 <p>{{t.dayName}}</p>
                 <ul>
                    <li>{{t.dayOfTheMonth}} {{t.monthName}}</li>
                    
                 </ul>
  				</div>

  				<div class="_padel10_card_top_tab">
                 <p style="color : <?php echo $settings['sportTypeTxtColor'] ?>; background : <?php echo $settings['sportTypeTxtBgColor'] ?>;">{{t.competitionType | translateBr}}</p>
  				</div>

  				<div class="_padel10_card_logo">
  					<img :src="imageSource+t.club.logo" alt="image">
  				</div>

  				<div class="_padel10_card_btm">
  					<div class="_padel10_card_btm_one">
                    <p v-if="t.trs.brs.length" v-for="(s, j) in t.trs.brs" :key="j" >{{s.className}}</p>
  					</div>
  					<div class="_padel10_card_btm_two">
                    <h3>{{t.leagueName}}</h3>
  						<ul>
                       <li>{{t.club.clubName}}</li>
  							<li>{{t.club.city}}</li>
  						</ul>
  					</div>
  				</div>
  			</div>
     </div>
  </div>
   <!-- ARROW -->
		<div class="_padel10_arrow_lft" @click="goBack" v-if="currentTrs.length > 4">
			<i class="fa fa-arrow-left"></i>
		</div>
		<div class="_padel10_arrow_r8" @click="sliderRight" v-if="currentTrs.length > 4"> 
			<i class="fa fa-arrow-right"></i>
		</div>
		<!-- ARROW -->
</div>


		
	</div>
  <div>
</trlisting>


<script>
settings  = JSON.parse(headerStyleOneSetting)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
jsmatchData  = JSON.parse(jsmatchData)
var list = document.getElementsByTagName("trlisting");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "trlisting-app-" + i);
           var app = new Vue({
           el: "#trlisting-app-" + i,
               data(){
                   return {
                      settings : settings,
                      trLists : jsmatchData,
                      imageSource: imageSource,
                      currentTrs : [], 
                      current : 'all', 
                      divs : [], 
                      br : [], 
                      ame: [], 
                      translateValue: 0, 
                      clickNumbers: 1, 
                      value : 35,
                      pxToChnage: 25

                   }
               },
			   methods : {
            changeTr(d){
               this.translateValue = 0
               this.clickNumbers = 1
               let trArray = []
               if(d == 'Bracket'){
                     trArray = this.br
                  }else if(d == 'Americano'){
                     trArray = this.ame
                  }else if(d == 'Division'){
                     trArray = this.divs
                  }
                  else{
                     trArray = this.trLists
                     
                  }
                  if(trArray.length > 4){
                      trArray = new Array(3).fill(trArray).flat();
                  }
                  
                  this.currentTrs = trArray
                  console.log('trarray is', this.currentTrs)
                  console.log('d', d)
                  this.current = d
               }, 
               goNext(t){
                  window.location = t.trs.landingPage
               },
               sliderRight(){ // increase 
                  if(this.clickNumbers == this.currentTrs.length - 4){
                    this.translateValue = 0
                    this.clickNumbers = 1
                    return

                  } 
                 
                  this.translateValue = -this.clickNumbers* this.pxToChnage
                  this.clickNumbers++
                 
               },
               goBack(){ // going back
                     if(this.clickNumbers == 1) return 
                     let newNumber = this.translateValue + this.pxToChnage
                  
                     this.translateValue = newNumber
                     console.log('decreament',this.translateValue)
                     this.clickNumbers--
               },
               checkMobile(){
                  console.log(window.innerWidth)
                  console.log(window.innerHeight)
                  return ( ( window.innerWidth <= 800 ) && ( window.innerHeight <= 900 ) );
               }
			   },

			   created(){
               if(this.checkMobile()){
                     this.pxToChnage = 103
                  }
               for(let d of this.trLists){
                  if(d.competitionType == 'Bracket'){
                     this.br.push(d)
                  }else if(d.competitionType == 'Americano'){
                     this.ame.push(d)
                  }else{
                     this.divs.push(d)
                  }
               } 
               var repeatedImages = new Array(3).fill(this.trLists).flat();
               this.currentTrs = repeatedImages
               console.log('trLists is ',this.currentTrs)
               console.log('br is ',this.br)
               console.log('ame is ',this.ame)
               console.log('divs is ',this.divs)
			   }, 
            filters: {
               translateBr(v){
                   if(v=='Bracket') return 'Gruppspel & Slutspel'
                   if(v=='Divisions') return 'Seriespel'
                   return 'Americano'
               }
            }


			})

    }
</script>
