<?php
namespace Elementor;
require_once(BASEPLUGIN_INCLUDES . '/common/helperclass.php');

class HeaderStyleSix extends Widget_Base {
	use Helper;
	public function get_name() {
		return 'forehand-header-style-six-6';
	}
	
	public function get_title() {
		return 'HEADER STYLE SIX QUALIFIER';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		$trs = $this->callApi('qualiferTrListsWithClass');
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$optinsAndDefault = $this->getOptionsAndDefaultForTournament($trs);
		
		$this->add_control(
			'qualifier_form_id',
			[
				'label' => __( 'Select the tournament', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => $optinsAndDefault[1],
				'options' => $optinsAndDefault[0],
			]
		);

		

		// news title
		
		$this->add_control(
			'regTxt',
			[
				'label' => __( 'Registration text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Registration until Sat 1 August, 2021'
			]
		);
		$this->add_control(
			'txt1',
			[
				'label' => __( 'Text 1', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Prize'
			]
		);
		$this->add_control(
			'txt2',
			[
				'label' => __( 'Text 2', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Classes'
			]
		);
		$this->add_control(
			'txt3',
			[
				'label' => __( 'Text 3', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Lottning & Matcher'
			]
		);

		$this->add_control(
			'lottery',
			[
				'label' => __( 'Text 3 value', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => '3 dagar innar'
			]
		);
	
		
		$this->add_control(
			'txt4',
			[
				'label' => __( 'Text 4', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Organiser'
			]
		);
		$this->add_control(
			'organiser',
			[
				'label' => __( 'Text 4 value', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Forehand'
			]
		);

		$this->add_control(
			'txt5',
			[
				'label' => __( 'Text 5', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Anmälningsavgift'
			]
		);
		$this->add_control(
			'fee',
			[
				'label' => __( 'Text 5 value', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => '800 kr / lag'
			]
		);
		$this->add_control(
			'txt6',
			[
				'label' => __( 'Text 6', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Svenska Padel Forbundet'
			]
		);
		$this->add_control(
			'trSanctioned',
			[
				'label' => __( 'Text 6 value', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Svenska Padel Forbundet'
			]
		);
	

		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$eventData = $this->getCalendarData($settings['qualifier_form_id']);
		
        require('headerStyleSixHtml.php');
	}
	public function getCalendarData($id){
		
        $response = wp_remote_request( FOREHAND_API_URL."/qualiferDetails/$id",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
		$body = wp_remote_retrieve_body($response);
		
	
        return json_decode($body);
    }
   

	// js render
	
	


	
	
}