<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
$eventData = json_encode($eventData);
echo "<script type='text/javascript'>var eventData='$eventData'</script>";

?>



<headerstylesix>

<div v-cloak class="header_style_six_all" v-if="trData">

        <div class="_banner_pic">
            <img class="_banner_img" :src="imageSource+trData.league.img.image" alt="" title="">
        </div>

        

        <div class="_banner_main">
            <div class="_banner_cal">
                <p class="_banner_cal_mon">{{trData.monthName.substring(0,3)}}</p>
                <p class="_banner_cal_date">{{trData.dayOfTheMonth}}</p>
                <p class="_banner_cal_year">{{trData.year}}</p>
            </div>

            <div class="_banner_main_deatils">
                <h1 class="_banner_main_title">{{trData.tournamentName}}</h1>
                <p class="_banner_main_gol">{{trData.league.club.clubName}} <span>| {{trData.city}}</span></p>

                <p class="_banner_main_regi"><?php echo $settings['regTxt'] ?></p>

                <div class="_banner_main_button">
                    <button class="_banner_main_signUp"  @click="goToForm">SIGN UP</button>
                    <!-- <button class="_banner_main_share">SHARE</button> -->
                </div>
            </div>
        </div>
        <div class="_2banner_bottom">
                <div class="_2banner_bottom_ro">
                    <p class="_2banner_bottom_ro_text">Details</p>
                </div>
                <div class="_2banner_bottom_con" v-if="trData.league.prizeMoney > 0">
                    <p class="_2banner_bottom_title"><?php echo $settings['txt1'] ?></p>
                    <h1 class="_2banner_bottom_val">{{trData.league.prizeMoney}} <span>kr</span></h1>
                </div>
                <div class="_2banner_bottom_con">
                    <p class="_2banner_bottom_title"><?php echo $settings['txt2'] ?> {{trData.league.prizeMoney}} </p>
                    <h1 class="_2banner_bottom_val">
                        <template v-for="(c,i) in trData.qfclasses" class="_2banner_bottom_val">{{c.className}}<span v-if="i<trData.qfclasses.length - 2">,</span> <span v-if="i==trData.qfclasses.length - 2"> &amp; </span></template>
                    </h1>
                </div>
                <div class="_2banner_bottom_con">
                    <p class="_2banner_bottom_title"><?php echo $settings['txt3'] ?></p>
                    <h1 class="_2banner_bottom_val"><?php echo $settings['lottery'] ?></h1>
                </div>
                <div class="_2banner_bottom_con">
                    <p class="_2banner_bottom_title"><?php echo $settings['txt4'] ?></p>
                    <h1 class="_2banner_bottom_val"><?php echo $settings['organiser'] ?></h1>
                </div>
                <div class="_2banner_bottom_con">
                    <p class="_2banner_bottom_title"><?php echo $settings['txt5'] ?></p>
                    <h1 class="_2banner_bottom_val"><?php echo $settings['fee'] ?></h1>
                </div>
                <div class="_2banner_bottom_con">
                    <p class="_2banner_bottom_title"><?php echo $settings['txt6'] ?></p>
                    <h1 class="_2banner_bottom_val"><?php echo $settings['trSanctioned'] ?></h1>
                </div>
              
            </div>
       
    </div>

</div>
      
</headerstylesix>


  
<script type="text/javascript">
    
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
    eventData = eventData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    eventData = JSON.parse(eventData)
    console.log(eventData);
    
    
    
    var list = document.getElementsByTagName("headerstylesix");
    

        for (var j = 0; j < list.length; j++) {
            list[j].setAttribute("id", "headerstylesix-app-" + j);
            var app = new Vue({
            el: "#headerstylesix-app-" + j,
                data(){
                    return {
                      trData : eventData
                     
                    }
                },
                methods : {
                    goToForm(){
                        const el = document.getElementById('qualifer_register_form');
                        console.log(el)
                        if(el){
                            el.scrollIntoView({behavior: "smooth"});
                        }
                        
                        
                    }
                }
                
            })

        }
 </script>

