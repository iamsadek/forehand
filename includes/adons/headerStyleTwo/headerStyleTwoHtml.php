<headerStyleTwo>
    <div class="_banners" style="background-image: url(<?php echo $settings['headerStyleOneBanner']['url'] ?>)">
        <div class="_1header_content_cards">
            <p class="_1header_content_card_lasts"><?php echo $settings['newsTitle1']?></p>

            <h3 class="_1header_content_card_titles"><?php echo $settings['newsHeading1']?></h3>

            <p class="_1header_content_card_texts"><?php echo $settings['newsShortDesc1']?></p>

            <a class="_1header_content_reads" href="<?php echo $settings['newsLink1']['url']?>"><?php echo $settings['newsLinkName1']?></a>
        </div>
        
    </div>
</headerStyleTwo>
<?php 
    $jsSettings = json_encode($settings);
    echo "<script type='text/javascript'>var headerStyleTwoSetting='$jsSettings'</script>";
?>

<script type="text/javascript">
   

    headerStyleTwoSetting  = JSON.parse(headerStyleTwoSetting)

    var list = document.getElementsByTagName("headerStyleTwo");
    console.log(list)
 
        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "app-" + i);
            var app = new Vue({
            el: "#app-" + i,
                data(){
                    return {
                        headerStyleTwoSetting: headerStyleTwoSetting, 
                    }
                }, 
             })

        }
 </script>

