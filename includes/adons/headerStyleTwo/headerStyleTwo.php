<?php
namespace Elementor;

class HeaderStyleTwo extends Widget_Base {
	
	public function get_name() {
		return 'forehand-header-style-two';
	}
	
	public function get_title() {
		return 'HEADER STYLE 2';
	}
	
	public function get_icon() {
		return 'fas fa-heading';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// banner 
		$this->add_control(
			'headerStyleOneBanner',
			[
				'label' => __( 'Choose Banner 1', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => get_template_directory_uri().'/assets/img/bg2.jpg',
				],
			]
		);
		// banner one text info 
		// news title
		$this->add_control(
			'newsTitle1',
			[
				'label' => __( 'News title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter news title', 'elementor' ),
				'default' => 'Latest news'
			]
		);
		// news headHeading
		$this->add_control(
			'newsHeading1',
			[
				'label' => __( 'News headline', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter news headline', 'elementor' ),
				'default' => 'This is the news headline. should be in one line.'
			]
		);
		// news newsShortDesc
		$this->add_control(
			'newsShortDesc1',
			[
				'label' => __( 'News short description', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter short description', 'elementor' ),
				'default' => 'This is are some news heppening around. A short descriptions of the news..'
			]
		);
		// news newsLinkName
		$this->add_control(
			'newsLinkName1',
			[
				'label' => __( 'Link text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter link text', 'elementor' ),
				'default' => 'Read more...'
			]
		);
		// news newsLinkName
		$this->add_control(
			'newsLink1',
			[
				'label' => __( 'Link', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'Enter news link', 'elementor' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        
		$settings = $this->get_settings_for_display();
		require('headerStyleTwoHtml.php');
  
      

	}

	// js render
	
	


	
	
}