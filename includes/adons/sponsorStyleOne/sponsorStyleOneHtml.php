 <!-- Sponser Section -->
 <div class="_sponser_sec" style="background: <?php echo $settings['boxBgColor'] ?>">
    <div class="_content" style="margin-top:<?php echo $settings['marginTop'] ?>; margin-bottom:<?php echo $settings['marginBottom'] ?> ">
        <h3 class="_sponser_sec_h4" style="color: <?php echo $settings['textColor'] ?>; text-align:<?php echo $settings['textAlign'] ?>; ">
            <?php echo $settings['sponsorTxt'] ?>
        </h3>
        <ul class="_sponser_sec_ul">
        <?php if($settings['list'] ): ?> 
            <?php foreach($settings['list'] as $item ): ?>
            
                <li>
                    <a href="<?php  echo $item['sponsorLink']['url'] ?>">
                        <img src="<?php echo $item['sponsorLogo']['url'] ?>" alt="image">
                    </a>
                
                </li>

            <?php endforeach; ?>
            <?php endif; ?>
            
        </ul>
       </div>
     </div>
     <!-- Sponser Section -->