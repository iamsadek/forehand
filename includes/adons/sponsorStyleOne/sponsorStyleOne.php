<?php
namespace Elementor;

class SponsorStyleOne extends Widget_Base {
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name() {
		return 'forehand-sponsor-style-one';
	}
	
	public function get_title() {
		return 'SPONSOR STYLE 1';
	}
	
	public function get_icon() {
		return 'far fa-images';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// background color 
		$this->add_control(
			'sponsorTxt',
			[
				'label' => __( 'Headline', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Sponsorer'
			]
		);

		$this->add_control(
			'textAlign',
			[
				'label' => __( 'Headline alignment', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left'  => __( 'Left', 'plugin-domain' ),
					'center' => __( 'Center', 'plugin-domain' ),
					'right' => __( 'Right', 'plugin-domain' ),
					
				],
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Text background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		$this->add_control(
			'marginTop',
			[
				'label' => __( 'Margin top', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '0px'
			]
		);
		$this->add_control(
			'marginBottom',
			[
				'label' => __( 'Margin bottom', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '0px'
			]
		);
		// text color 
		$this->add_control(
			'textColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
        );
        
        $repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'logoTitle', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'logo' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'sponsorLogo', [
				'label' => __( 'Sponsor Logo', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'sponsorLink',
			[
				'label' => __( 'Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		

		$this->add_control(
			'list',
			[
				'label' => __( 'Sponsor logos', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[],
					[],
					[],
				],
				'title_field' => '{{{ logoTitle }}}',
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('sponsorStyleOneHtml.php');
    }
   
	
	


	
	
}