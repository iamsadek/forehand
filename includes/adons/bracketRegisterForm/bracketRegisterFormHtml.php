<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL_PADELCOMP;
$forehandMode = FOREHAND_MODE;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
echo "<script type='text/javascript'>var forehandMode='$forehandMode'</script>";
// $eventData = json_encode($eventData);
// echo "<script type='text/javascript'>var eventData='$eventData'</script>";

?>



<bracketregisterform>
<div v-cloak>
<div class="_eventLan_regi" id="qualifer_register_form" v-if="showFrom && clubInfo" >
      <div class="_eventLan_regi_left">
            <h1 class="_eventLan_regi_left_title"><?php echo $settings['title'] ?> </h1>
            <p class="cityName">{{clubInfo.city}}</p>
        </div>
      

      <div class="_eventLan_regi_right">
        <ul class="_eventLan_regi_menu">
          <li
            :class="tab == 'player1' ? '_active' : ''"
            @click="tab = 'player1'"
          >
            Player 1
          </li>
          <li
            :class="tab == 'player2' ? '_active' : ''"
            @click="tab = 'player2'"
          >
            Player 2
          </li>
          <li :class="tab == 'payment' ? '_active' : ''" @click="showPayment" v-if="trData">
            Payment
          </li>
        </ul>

        <div class="_eventLan_regi_main">
          <div class="row">
            <template v-if="tab == 'player1'">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">E-mail</p>
                  <input
                    class="_eventLan_regi_int"
                    v-model="data.player1.email"
                    placeholder=""
                  />
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">First name</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.player1.firstName"
                  />
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Last name</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.player1.lastName"
                  />
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Mobile</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.player1.phone"
                  />
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Password</p>
                  <input
                    class="_eventLan_regi_int"
                    type="password"
                    placeholder=""
                    v-model="data.player1.password"
                  />
                </div>
              </div>
              <!-- team info -->
              <div class="team_devider"></div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Team name</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.team.teamName"
                  />
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Select class</p>
                  <select
                    class="_eventLan_regi_select"
                    v-model="data.team.classId"
                    @change="classChanged"
                  >
                    <option v-for="(c, i) in classes" :value="c.id" :key="i">
                      {{ c.className }}
                    </option>
                  </select>
                </div>
              </div>
              <!-- coupon info -->
              <div class="team_devider"></div>
              <div class="col-12 col-md-6 col-lg-6" v-if="trData">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Coupon code</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="code"
                  />
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6" v-if="code != ''">
                <div class="coupon_btn_box">
                  <button
                    class="coupon_btn _2btn _btn_long"
                    @click="checkCoupon"
                    :disable="isCoupon"
                  >
                    {{ isCoupon ? "PLEASE WAIT..." : "APPLY" }}
                  </button>
                </div>
              </div>
            </template>
            <template v-if="tab == 'player2'">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">E-mail</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.player2.email"
                  />
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Firstname</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.player2.firstName"
                  />
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Lastname</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.player2.lastName"
                  />
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Mobile</p>
                  <input
                    class="_eventLan_regi_int"
                    placeholder=""
                    v-model="data.player2.phone"
                  />
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="_eventLan_regi_input">
                  <p class="_eventLan_regi_label">Password</p>
                  <input
                    class="_eventLan_regi_int"
                    type="password"
                    placeholder=""
                    v-model="data.player2.password"
                  />
                </div>
              </div>
            </template>

            <!-- team info and registration for  -->

            <div v-show="tab == 'payment' && trData">
              <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                  <div class="_eventLan_regi_input">
                    <p class="_eventLan_regi_label">Cardholder name</p>
                    <input
                      class="_eventLan_regi_int"
                      placeholder=""
                      v-model="data.payment.name"
                    />
                  </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                  <div class="_eventLan_regi_input">
                    <p class="_eventLan_regi_label">Cardholder email</p>
                    <input
                      class="_eventLan_regi_int"
                      placeholder=""
                      v-model="data.payment.email"
                    />
                  </div>
                </div>
                <div class="col-12 col-md-12 col-lg-12">
                  <div class="_eventLan_regi_input card_input">
                    <form id="payment-form">
                      <div class="_stripe_input_details">
                        <div
                          id="card-element"
                          class="_stripe_input_input"
                        ></div>
                      </div>
                      <div id="card-errors" role="alert"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-12 col-md-12 col-lg-12">
              <div class="_eventLan_regi_bottom">
                <div class="row">
                  <div class="col-12 col-md-6 col-lg-6">
                    <div class="_eventLan_regi_button">
                      <button
                        class="_2btn _btn_long"
                        @click="save"
                        :disable="isSending"
                      >
                        {{ isSending ? "PLEASE WAIT..." : "SIGNUP" }}
                      </button>
                    </div>
                  </div>

                  <div class="col-12 col-md-6 col-lg-6" v-if="trData">
                    <h1 class="_eventLan_regi_price">
                      <!-- {{trData.totalFromPlayer + trData.totalFeeAmount}} kr -->
                      {{clubInfo.currencyCode == "USD" ? '$': ''}} {{trData.totalFromPlayer + trData.totalFeeAmount}} {{clubInfo.currencyCode == 'EUR' ? '€' : clubInfo.currencyCode == "SEK" ? 'kr' : ''}}
                      
                    </h1>
                  </div>
                </div>
              </div>
            </div>
            <p class="help_text" v-show="tab == 'payment'">
            <p class="help_text" v-show="tab=='payment'"><?php echo $settings['paymentHelpTxt'] ?></p>
            </p>
          </div>
        </div>
      </div>
    </div>
</div>

</bracketregisterform>


  <script>
      var baseUrl = 'http://laravel-api.localhost/'
      if(forehandMode=='dev'){
        var stripe = Stripe('pk_test_51HeILUJktwxI8vVn3o92t3vlN9carBdLghhuROdqZjEcfQx7fYeSuTU0mweE45WB6v7yYFmh99ruAZTw5nTgJQfq00AQlnN35n');
      }else{
        var stripe = Stripe('pk_live_51HeILUJktwxI8vVnQOpnSaAdWciOR0bufVorpsDAga3BrC28TUsH4xarmDKnKTYTUDbolFv9XmHxXVEuPrndrtsR00MrfFkHwZ');
         baseUrl = 'https://api.padelcomp.com/'
        //baseUrl = 'https://padel.appifylab.com/'
      }
      var elements = stripe.elements();

  </script>

<script type="text/javascript">
    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
    // eventData = eventData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    // eventData = JSON.parse(eventData)
    // console.log('data',eventData);
    //console.log('set',headerStyleOneSetting);



    var list = document.getElementsByTagName("bracketregisterform");


        for (var j = 0; j < list.length; j++) {
            list[j].setAttribute("id", "bracketregisterform-app-" + j);
            var app = new Vue({
            el: "#bracketregisterform-app-" + j,
                data(){
                    return {

                    trData: null,
                    phone: "Radio option 2",
                    data: {
                      from: 'bracket',
                      player1: {
                          firstName: "",
                          lastName: "",
                          email: "",
                          phone: "",
                          password: "",
                      },
                      player2: {
                          firstName: "",
                          lastName: "",
                          email: "",
                          phone: "",
                          password: "",
                      },
                      team: {
                          teamName: "",
                          className: "",
                          classId: ''
                      },
                      payment: {
                          name: "",
                          email: "",
                      },
                      tr: null,
                    },
                    code: "",
                    firstNameError: false,
                    lastNameError: false,
                    emailError: false,
                    phoneError: false,
                    passError: false,
                    hasForehandId: false,
                    isSending: false,
                    isCoupon: false,
                    showPlayer2: false,
                    card: null,
                    formInfo: null,
                    tab: "player1",
                    classIndex: 0,
                    stripe: null,
                    elements: null,
                    classes: [],
                    clubInfo: null,
                    showFrom: true

                    };
                },
                methods: {
                    async save() {
                      if (this.data.player1.email == "")
                        return this.info("Player 1 email is required");
                      if (this.data.player1.firstName == "")
                        return this.info("Player 1 firstname is required");
                      if (this.data.player1.lastName == "")
                        return this.info("Player 1 lastname is required");
                      if (this.data.player1.password == "")
                        return this.info("Player 1 password is required");
                      if (this.data.player1.password.length < 6)
                        return this.info("Player 1 password must be at least 6 characters long");
                      if (this.data.player1.phone == "")
                        return this.info("Player 1 phone is required");

                      if (this.data.player2.email == "")
                        return this.info("Player 2 email is required");
                      if (this.data.player2.firstName == "")
                        return this.info("Player 2 firstname is required");
                      if (this.data.player2.lastName == "")
                        return this.info("Player 2 lastname is required");
                      if (this.data.player2.password == "")
                        return this.info("Player 2 password is required");
                      if (this.data.player2.password.length < 6)
                        return this.info("Player 2 password must be at least 6 characters long");
                      if (this.data.player2.phone == "")
                        return this.info("Player 2 phone is required");

                      if (this.data.team.teamName == "") return this.info("Teamname is required");

                      if (this.data.team.classId == "") return this.info("Select a class");

                      if(this.trData){
                        if (this.data.payment.name == "")
                          return this.info("Cardholder name is required");
                        if (this.data.payment.email == "")
                          return this.info("Cardholder email is required");

                        if (!this.validateEmail(this.data.payment.email))
                          return this.info("Cardholder email must be a valid email");
                      }



                      this.data.player1.email = this.data.player1.email.replace(/\s+/g, "");
                      this.data.player2.email = this.data.player2.email.replace(/\s+/g, "");
                      if (this.data.player1.email == this.data.player2.email)
                        return this.info(
                          "Both players cannot have same email. Please change email address."
                        );

                      this.saveRegisterInfo();
                    },
                    validateEmail(email) {
                      const re =
                        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                      return re.test(String(email).toLowerCase());
                    },
                    async saveRegisterInfo() {
                      this.data.clubInfo = this.clubInfo
                      this.data.tournament_id = headerStyleOneSetting.qualifier_form_id
                      this.infosSending = true;
                      const res = await this.callApi(
                        "post",
                        `payment/registerBracketSignup`,
                        this.data
                      );
                      let data = res.data
                      if (res.status == 200) {

                        if (this.trData && this.trData.key != "") {
                          if (await this.charge(res.data)) {
                            this.s(res.data.msg);
                            location.reload();
                          } else {
                            alert("We couldn't complete the payment, please try again later.");
                            location.reload();
                          }
                        } else {
                          //this.info(res.data.msg);

                          data.payInfo = {
                            totalFromPlayer: 0,
                            totalFeeAmount: 0
                          }
                          data.paymentIntent = {
                            id: null,
                          }


                          const res = await this.callApi(
                            "post",
                            `payment/confirmSignup`,
                            data
                          );
                          if(res.status == 200){
                            this.s(data.msg);
                            location.reload();
                          }else{
                              alert("We couldn't complete the payment, please try again later.");
                            location.reload();
                          }
                        }
                      } else {
                        this.info(res.data.msg);
                        //location.reload();
                      }
                      this.infosSending = false;
                    },
                    async charge(dataPayload) {
                      const { paymentIntent } = await stripe.confirmCardPayment(
                        this.trData.key,
                        {
                          payment_method: {
                            card: this.card,
                            billing_details: {
                              name: this.data.payment.name,
                              email: this.data.payment.email,
                            },
                          },
                        }
                      );

                      if (paymentIntent && paymentIntent.status == "succeeded") {
                        dataPayload.payInfo = this.trData
                        dataPayload.paymentIntent = paymentIntent


                        const res = await this.callApi(
                          "post",
                          `payment/confirmSignup`,
                          dataPayload
                        );
                        if (res.status == 200) {
                          return true;
                        } else {
                          return false;
                        }
                      } else {


                        return false;
                      }
                    },
                    async testCharge() {
                      const { paymentIntent } = await window.stripe.confirmCardPayment(
                        this.trData.key,
                        {
                          payment_method: {
                            card: this.card,
                            billing_details: {
                              name: 'Sade',
                              email: 'test@gmail.com',
                            },
                          },
                        }
                      );
                      console.log(paymentIntent)
                      if (paymentIntent && paymentIntent.status == "succeeded") {
                        console.log('payment succesed')

                      } else {
                        console.log("start deleting payment");

                        return false;
                      }
                    },

                    showPayment() {
                        this.tab = "payment";
                        if(this.trData.key =='') return 
                        
                        var style = {
                              base: {
                                  'color': "white",
                                  "fontSize": '16px'
                              },
                              hideIcon: true
                          };

                          var card = elements.create("card", { style: style });

                          card.mount("#card-element");
                          this.card = card




                    },

                    async classChanged() {
                      if(!this.trData) return
                      const res = await this.callApi("post", 'payment/getNewBracketSecrate', {
                          id: this.data.team.classId,
                          accountId: this.clubInfo.accountId,
                          from: 'bracket'
                      });
                      if (res.status == 200) {
                        this.trData = res.data
                      } else {
                        alert(res.data.msg);
                      }
                    },
                    async checkCoupon() {
                      if (this.code == "") return this.info("Please add coupon code");
                      this.infosCoupon = true;
                      const res = await this.callApi("post", `payment/checkCoupon`, {
                        id: this.data.team.classId,
                        code: this.code,
                        accountId: this.clubInfo.accountId
                      });
                      if (res.status == 200) {
                        this.trData.totalFeeAmount = res.data.totalFeeAmount;
                        this.trData.totalFromPlayer = res.data.totalFromPlayer;
                        this.trData.key = res.data.key;

                        this.s(`Coupon discount has been applied!`);
                      }
                      this.infosCoupon = false;
                    },
                    async callApi(method, url, dataObj) {
                          try {

                              let data = await axios({
                                  headers: {
                                      'Accept': 'application/json',
                                      'Content-Type': 'application/json',
                                  },
                                  method: method,
                                  url: baseUrl+url,
                                  data: dataObj
                              })
                              return data

                          } catch (e) {

                              return e.response
                          }
                      },

                      info(msg){
                        this.$Notice.info({
                          title: 'Hey!',
                          desc: msg, 
                          duration: 3
                        });
                      },
                      s(msg){
                        this.$Notice.success({
                          title: 'Great!',
                          desc: msg, 
                          duration: 3000
                        });
                      },
                  },
                
                async created() {
                    const res = await this.callApi('get', `payment/getSignupInfoBracket/${headerStyleOneSetting.qualifier_form_id}?league=1&from=bracket`)
                    if(res.status == 200){
                   
                    this.clubInfo = res.data.clubInfo
                    this.trData = res.data.signupInfo

                    this.classes = res.data.classes
                    this.data.team.classId = res.data.classes[0].id
                    if(!res.data.showFrom){
                        return this.showFrom = res.data.showFrom
                    }

                }
            },





            })

        }
 </script>

