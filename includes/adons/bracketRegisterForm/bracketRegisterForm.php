<?php
namespace Elementor;

require_once BASEPLUGIN_INCLUDES . '/common/helperclass.php';

class BracketRegisterForm extends Widget_Base
{
    use Helper;
    public function get_name()
    {
        return 'forehand-bracket-register-form';
    }

    public function get_title()
    {
        return 'BRACKET REGISTER FORM';
    }

    public function get_icon()
    {
        return 'fa fa-calendar';
    }

    public function get_categories()
    {
        return ['Forehand'];
    }

    protected function _register_controls()
    {
        $trs = $this->callApi('bracketTrListsWithClass');
        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Settings', 'elementor'),
            ]
        );
        $optinsAndDefault = $this->getOptionsAndDefaultForTournament($trs);

        $this->add_control(
            'qualifier_form_id',
            [
                'label' => __('Select the tournament', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => $optinsAndDefault[1],
                'options' => $optinsAndDefault[0],
            ]
        );

        // news title
        $this->add_control(
            'title',
            [
                'label' => __('Title', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your title', 'elementor'),
                'default' => 'Tävlingsanmälan',
            ]
        );
        $this->add_control(
            'paymentHelpTxt',
            [
                'label' => __('Payment help text', 'elementor'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'placeholder' => __('Enter your title', 'elementor'),
                'default' => 'Kontakta support@forehand.se om du har frågor eller behöver hjälp med betalningen',
            ]
        );

        $this->end_controls_section();
    }

    // php render.
    protected function render()
    {
        $settings = $this->get_settings_for_display();
        //$eventData = $this->getCalendarData($settings['qualifier_form_id']);

        require 'bracketRegisterFormHtml.php';
    }
    // public function getCalendarData($id)
    // {

    //     $response = wp_remote_request(FOREHAND_API_URL . "/qualiferRegisterForm/$id",
    //         array(
    //             'method' => 'GET',
    //             'headers' => [
    //                 'forehandclub' => FOREHAND_API_KEY,
    //             ],
    //         )
    //     );
    //     $body = wp_remote_retrieve_body($response);

    //     return json_decode($body);
    // }

    // js render

}
