<?php if($settings['imageDirection'] == 'left' ): ?> 
<div class="_mtng_info_sec _desktoponly" style="background: <?php echo $settings['boxBgColor'] ?>">
    	<div class="_mtng_info_lft">
    		<img src="<?php echo $settings['image']['url'] ?>" alt="image">
    	</div>
    	<div class="_mtng_info_r8" 
		style="margin-top: <?php echo $settings['marginTop'] ?>; 
			 padding-left: <?php echo $settings['pl'] ?>; 
			 padding-right: <?php echo $settings['pr'] ?>; 
			 padding-bottom: <?php echo $settings['pb'] ?>; 
			 padding-top: <?php echo $settings['pt'] ?>; 
			 
		">
    		
			<h3 class="_mtng_info_r8_h3" style="color: <?php echo $settings['headingColor'] ?>">
    			<?php echo $settings['heading'] ?>
    		</h3>
    		<p class="_mtng_info_r8_p1">
                <?php echo $settings['text'] ?>
    		</p>
    		<ul class="_mtng_info_r8_ul">
                <?php if($settings['list'] ): ?> 
                    <?php foreach($settings['list'] as $item ): ?>
                        <li style="color: <?php echo $settings['listColor'] ?>">
                            <span style="color: <?php echo $settings['listColor'] ?>"> 
                                <i class="<?php echo $item['icon']?>" aria-hidden="true"></i>
                            </span>
                            <?php echo $item['listTitle'] ?>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
    			
    		</ul>
    		<p class="_mtng_info_r8_p1">
                <?php echo $settings['footerText'] ?>
    		</p>
			
			<?php if($settings['addButton'] == 'yes' ): ?> 
				<div class="forehand_button">
					<a href="<?php echo $settings['buttonLink'] ?>">
						<button class="fbutton"
							style="color:<?php echo $settings['buttonTxtColor'] ?>;background:<?php echo $settings['buttonBg'] ?>;
							border: 1px solid <?php echo $settings['buttonBorderColor'] ?>;border-radius: <?php echo $settings['buttonRds'] ?>;"
						>
							<?php echo $settings['buttonTxt'] ?>
						</button>
					</a>
				</div>
			<?php endif; ?>

    	</div>
</div>
<?php endif; ?>
<?php if($settings['imageDirection'] == 'right' ): ?> 
<div class="_mtng_info_sec _desktoponly" style="background: <?php echo $settings['boxBgColor'] ?>">
    	
    	<div class="_mtng_info_r8" 
		style="margin-top: <?php echo $settings['marginTop'] ?>;
		
		 padding-left: <?php echo $settings['pl'] ?>; 
			 padding-right: <?php echo $settings['pr'] ?>; 
			 padding-bottom: <?php echo $settings['pb'] ?>; 
			 padding-top: <?php echo $settings['pt'] ?>; 
		 
		 ">
    		<h3 class="_mtng_info_r8_h3" style="color: <?php echo $settings['headingColor'] ?>">
    			<?php echo $settings['heading'] ?>
    		</h3>
    		<p class="_mtng_info_r8_p1">
                <?php echo $settings['text'] ?>
    		</p>
    		<ul class="_mtng_info_r8_ul">
                <?php if($settings['list'] ): ?> 
                    <?php foreach($settings['list'] as $item ): ?>
                        <li style="color: <?php echo $settings['listColor'] ?>">
                            <span style="color: <?php echo $settings['listColor'] ?>"> 
                                <i class="<?php echo $item['icon']?>" aria-hidden="true"></i>
                            </span>
                            <?php echo $item['listTitle'] ?>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
    			
    		</ul>
    		<p class="_mtng_info_r8_p1">
                <?php echo $settings['footerText'] ?>
    		</p>
			
			<?php if($settings['addButton'] == 'yes' ): ?> 
				<div class="forehand_button">
					<a href="<?php echo $settings['buttonLink'] ?>">
						<button class="fbutton"
							style="color:<?php echo $settings['buttonTxtColor'] ?>;background:<?php echo $settings['buttonBg'] ?>;
							border: 1px solid <?php echo $settings['buttonBorderColor'] ?>;border-radius: <?php echo $settings['buttonRds'] ?>;"
						>
							<?php echo $settings['buttonTxt'] ?>
						</button>
					</a>
				</div>
			<?php endif; ?>

    	</div>
		<div class="_mtng_info_lft">
    		<img src="<?php echo $settings['image']['url'] ?>" alt="image">
    	</div>
    </div>
<?php endif; ?>




<div class="_mtng_info_sec _onlymobile" style="background: <?php echo $settings['boxBgColor'] ?>">
    
    <div class="_mtng_info_r8" 
    style="margin-top: <?php echo $settings['marginTop'] ?>;
    
     padding-left: <?php echo $settings['pl'] ?>; 
         padding-right: <?php echo $settings['pr'] ?>; 
         padding-bottom: <?php echo $settings['pb'] ?>; 
         padding-top: <?php echo $settings['pt'] ?>; 
     
     ">
        <h3 class="_mtng_info_r8_h3" style="color: <?php echo $settings['headingColor'] ?>">
            <?php echo $settings['heading'] ?>
        </h3>
        <p class="_mtng_info_r8_p1">
            <?php echo $settings['text'] ?>
        </p>
        <ul class="_mtng_info_r8_ul">
            <?php if($settings['list'] ): ?> 
                <?php foreach($settings['list'] as $item ): ?>
                    <li style="color: <?php echo $settings['listColor'] ?>">
                        <span style="color: <?php echo $settings['listColor'] ?>"> 
                            <i class="<?php echo $item['icon']?>" aria-hidden="true"></i>
                        </span>
                        <?php echo $item['listTitle'] ?>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
            
        </ul>
        <p class="_mtng_info_r8_p1">
            <?php echo $settings['footerText'] ?>
        </p>
        
        <?php if($settings['addButton'] == 'yes' ): ?> 
            <div class="forehand_button">
                <a href="<?php echo $settings['buttonLink'] ?>">
                    <button class="fbutton"
                        style="color:<?php echo $settings['buttonTxtColor'] ?>;background:<?php echo $settings['buttonBg'] ?>;
                        border: 1px solid <?php echo $settings['buttonBorderColor'] ?>;border-radius: <?php echo $settings['buttonRds'] ?>;"
                    >
                        <?php echo $settings['buttonTxt'] ?>
                    </button>
                </a>
            </div>
        <?php endif; ?>

    </div>
    <div class="_mtng_info_lft">
        <img src="<?php echo $settings['image']['url'] ?>" alt="image">
    </div>
</div>
