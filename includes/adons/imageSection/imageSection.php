<?php
namespace Elementor;

class ImageSection extends Widget_Base {
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name() {
		return 'forehand-image-section-style-one';
	}
	
	public function get_title() {
		return 'IMAGE SECTION STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-picture-o';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'imageDirection',
			[
				'label' => __( 'Image placement', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left'  => __( 'Left', 'plugin-domain' ),
					'right' => __( 'Right', 'plugin-domain' ),
					
				],
			]
		);
		$this->add_control(
			'heading',
			[
				'label' => __( 'Heading', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your headline', 'elementor' ),
				'default' => "Detta är en intressant rubrik"
			]
		);
		$this->add_control(
			'image', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'text',
			[
				'label' => __( 'Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Text', 'elementor' ),
				'default' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the "
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'listTitle', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'list item' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'icon',
			[
				'label' => __( 'Icon class ', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => 'fa fa-users',
				'label_block' => true,
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'Lists', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [],
				'title_field' => '{{{ listTitle }}}',
			]
		);
		$this->add_control(
			'footerText',
			[
				'label' => __( 'Footer text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Text', 'elementor' ),
				'default' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
			]
		);
		$this->add_control(
			'addButton',
			[
				'label' => __( 'Add a button', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);
		$this->add_control(
			'buttonTxt',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Läs mer'
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonRds',
			[
				'label' => __( 'Button border radius', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '30px'
			]
		);
		$this->add_control(
			'buttonBorderColor',
			[
				'label' => __( 'Button border color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'darkgray'
			]
		);



		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'transparent'
			]
		);
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);





		$this->add_control(
			'marginTop',
			[
				'label' => __( 'Margin top', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Margin top', 'elementor' ),
				'default' => '120px'
			]
		);
		// $this->add_control(
		// 	'marginLeft',
		// 	[
		// 		'label' => __( 'Margin left', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Margin left', 'elementor' ),
		// 		'default' => '45px'
		// 	]
		// );
		$this->add_control(
			'pl',
			[
				'label' => __( 'Padding left', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding left', 'elementor' ),
				'default' => '100px'
			]
		);
		$this->add_control(
			'pr',
			[
				'label' => __( 'Padding right', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding right', 'elementor' ),
				'default' => '100px'
			]
		);
		$this->add_control(
			'pt',
			[
				'label' => __( 'Padding top', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding top', 'elementor' ),
				'default' => '0px'
			]
		);
		$this->add_control(
			'pb',
			[
				'label' => __( 'Padding bottom', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding bottom', 'elementor' ),
				'default' => '0px'
			]
		);





		// $this->add_control(
		// 	'padingBottom',
		// 	[
		// 		'label' => __( 'Padding bottom', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Padding bottom', 'elementor' ),
		// 		'default' => '34px'
		// 	]
		// );
		// $this->add_control(
		// 	'padingLeft',
		// 	[
		// 		'label' => __( 'Padding left', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Padding left', 'elementor' ),
		// 		'default' => '20px'
		// 	]
		// );
		// $this->add_control(
		// 	'textAlign',
		// 	[
		// 		'label' => __( 'Text alignment', 'plugin-domain' ),
		// 		'type' => \Elementor\Controls_Manager::SELECT,
		// 		'default' => 'center',
		// 		'options' => [
		// 			'left'  => __( 'Left', 'plugin-domain' ),
		// 			'center' => __( 'Center', 'plugin-domain' ),
		// 			'right' => __( 'Right', 'plugin-domain' ),
					
		// 		],
		// 	]
		// );


		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		// text color 
		$this->add_control(
			'headingColor',
			[
				'label' => __( 'Heading color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'listColor',
			[
				'label' => __( 'List item color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		// $this->add_control(
		// 	'addButton',
		// 	[
		// 		'label' => __( 'Add a button', 'elementor' ),
		// 		'type' => Controls_Manager::SWITCHER,
		// 		'default' => 'yes'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonTxt',
		// 	[
		// 		'label' => __( 'Button text', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => 'Contact us'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonLink',
		// 	[
		// 		'label' => __( 'Button link', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => ''
		// 	]
		// );
		// $this->add_control(
		// 	'buttonRds',
		// 	[
		// 		'label' => __( 'Button border radius', 'elementor' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'default' => '30px'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonBorderColor',
		// 	[
		// 		'label' => __( 'Button border color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'darkgray'
		// 	]
		// );



		// $this->add_control(
		// 	'buttonBg',
		// 	[
		// 		'label' => __( 'Background color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'transparent'
		// 	]
		// );
		// $this->add_control(
		// 	'buttonTxtColor',
		// 	[
		// 		'label' => __( 'Button text color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => 'black'
		// 	]
		// );



		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('imageSectionHtml.php');
    }
   
	
	


	
	
}