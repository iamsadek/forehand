<?php
// settings to js data
$jsSettings = json_encode($settings);
echo "<script type='text/javascript'>var videoHeaderStyleOne='$jsSettings'</script>";
?>
<videoheaderstyleone>
	
<div class="_vdo_banner_sec" style="background: <?php echo $settings['boxBgColor'] ?>" v-cloak>
			<div class="_vdo_banner_main">
				<div class="_vdo_lft_info">
					<ul class="_vdo_lft_info_ul">
						<li style="color: <?php echo $settings['txtColor'] ?>"><?php echo $settings['date'] ?> <?php echo $settings['month'] ?> </li>
						<li style="color: <?php echo $settings['txtColor'] ?>"><?php echo $settings['weekday'] ?></li>
					</ul>
					<h3 class="_vdo_lft_h3" style="color: <?php echo $settings['txtColor'] ?>">
						 <?php echo $settings['title'] ?>
					</h3>
					<p class="_vdo_lft_plce" style="color: <?php echo $settings['txtColor'] ?>">
						<?php echo $settings['address'] ?>
					</p>
					<p class="_vdo_lft_plce" style="color: <?php echo $settings['txtColor'] ?>">
              <?php echo $settings['regTxt'] ?>
					</p>
					<a class="_vdo_lft_btn" href="<?php echo $settings['readNewsLink']['url'] ?>"
            style="color: <?php echo $settings['buttonColor'] ?>; background:  <?php echo $settings['buttonBg'] ?>;"
          >
              <?php echo $settings['readNewsTxt'] ?>
					</a>
				</div>
				<div class="_vdo_r8" @click="playVideo = true">
					<img :src="currentlyPlaying.image.url" alt="image" v-if="currentlyPlaying">
					<span><i class="fa fa-play"></i></span>
				</div>
			</div>
			<div class="_vdo_btm_lst" style="background: <?php echo $settings['bannerFooter'] ?>">

				<div class="_vdo_btm_lst_lft">
					<p class="_vdo_btm_lst_lft_p">
						   <?php echo $settings['bottomTitle'] ?>
					</p>
				</div>
				<div class="_vdo_btm_lst_r8">
					<ul class="_vdo_btm_lst_r8_ul" v-if="videoHeaderStyleOne.list.length">

            
                
                    <li :class="v.selected? '_vdo_btm_lst_actv_vdo' : ''" v-for="(v, i) in videoHeaderStyleOne.list" @click="changeVideo(v)">
                      	<img :src="v.image.url" alt="image">
                    </li>
                  
                  
                   
					</ul>
				</div>
			</div>
			<div class="_vdo_modal_sec" v-if="playVideo">
				<div class="_vdo_modal_crd">
					<iframe  :src="currentlyPlaying.videoLink.url"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ></iframe>
					<div class="_vdo_modal_icon">
						<span @click="playVideo = false">
							<i class="fa fa-times"></i>
						</span>
					</div>
				</div>
			</div>
		</div>

</videoheaderstyleone>


<script type="text/javascript">
   videoHeaderStyleOne  = JSON.parse(videoHeaderStyleOne)
   var list = document.getElementsByTagName("videoheaderstyleone");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "videoheaderstyleone-app-" + i);
           var app = new Vue({
           el: "#videoheaderstyleone-app-" + i,
               data(){
                   return {
					   videoHeaderStyleOne: videoHeaderStyleOne,
					   currentlyPlaying : false, 
					   playVideo : false
                   }
			   },
			   methods: {
				   changeVideo(v){
					    
						for(let d of this.videoHeaderStyleOne.list){
						   d.selected = false 
						}
						v.selected = true 
						this.currentlyPlaying = v
				   }, 
				  
			   },
			   created(){
				   if(this.videoHeaderStyleOne.list.length){
					   for(let d of this.videoHeaderStyleOne.list){
						   d.selected = false 
					   }
					   this.videoHeaderStyleOne.list[0].selected = true
					   this.currentlyPlaying = this.videoHeaderStyleOne.list[0]
 				   }
				   
			   }

           })
	}
</script>

