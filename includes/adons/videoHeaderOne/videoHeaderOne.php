<?php
namespace Elementor;

class VideoHeaderOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-video-header-1';
	}
	
	public function get_title() {
		return 'VIDEO HEADER 1';
	}
	
	public function get_icon() {
		return 'fas fa-heading';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'txtColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'footerTxtColor',
			[
				'label' => __( 'Footer text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'gray'
			]
		);


		// image 
		$this->add_control(
			'banner',
			[
				'label' => __( 'Choose Banner', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		// background color for footer 
		$this->add_control(
			'bannerFooter',
			[
				'label' => __( 'Banner footer background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#f4f4f4'
			]
		);

		// MONTH
		$this->add_control(
			'month',
			[
				'label' => __( 'Month', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your month', 'elementor' ),
				'default' => 'maj'
			]
		);
		// date
		$this->add_control(
			'date',
			[
				'label' => __( 'Date', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::NUMBER,
				'placeholder' => __( 'Enter your date', 'elementor' ),
				'default' => '12'
			]
		);
		// year
		$this->add_control(
			'weekday',
			[
				'label' => __( 'Year', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your weekday', 'elementor' ),
				'default' => 'Fre'
			]
		);
		// title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Svenska eliten vs Världesliten'
			]
		);
		
		
		$this->add_control(
			'address',
			[
				'label' => __( 'Address', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Stockholm | Globen'
			]
		);
		//registration text
		$this->add_control(
			'regTxt',
			[
				'label' => __( 'Registration text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'En del av Swedish Padel Tour'
			]
		);
		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'buttonColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		$this->add_control(
			'readNewsTxt',
			[
				'label' => __( 'Read news text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Läs mer'
			]
		);

		$this->add_control(
			'readNewsLink',
			[
				'label' => __( 'News link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'default' => [
					'url' => 'https://domain.com',
				],
			]
		);

		$this->add_control(
			'bottomTitle',
			[
				'label' => __( 'Bottom title text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => 'Video'
			]
		);

		//  repeater starts .... 

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'list_title', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'video 1' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'image',
			[
				'label' => __( 'Video image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'videoLink', [
				'label' => __( 'Youtube embed video link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
				'show_external' => true,
				'default' => [
					'url' => 'https://www.youtube.com/embed/DxC7pJevEIw',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		

		$this->add_control(
			'list',
			[
				'label' => __( 'Video lists', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[],
					[],
					[],
				],
				'title_field' => '{{{ list_title }}}',
			]
		);





		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('videoHeaderOneHtml.php');
    }
   
	
	


	
	
}