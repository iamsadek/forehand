<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var bracketShceduleSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageUrl='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsmatchData = json_encode($matchData);
echo "<script type='text/javascript'>var jsmatchData='$jsmatchData'</script>";

?>

<groupplayranks>
	<div v-cloak>
    <div class="_groupTv  groupplayrank">
            <div class="_groupTv_con">
               
                <ul class="_2tree_list _2tree_list2 _2tree_list_rank" v-if="breacketSettings.length">
                   
                    <li
                        :class="currentSettingIndex == i ? '_active' : ''"
                        v-for="(c, i) in breacketSettings"
                        :key="i"
                        @click="bringInNextClassRanks(i)"
                    >
                    {{ c.className }}
                    </li>
                </ul>
                <!-- <h1 v-if="breacketSettings.length" style="color: white" v-for="(c,i) in breacketSettings">{{c.className}}</h1> -->
                <div class="_groupTv_row">
                    <!-- Item -->
                   
                    
                    <div class="_groupTv_col3" v-for="(ranks, r) in allRanks" :delay="2000" v-if="hasRank">
                      <!-- <transition-group name="slide-fade" > -->
                        <div class="_groupTv_card _1bg" :key="r">
                            <p class="_groupTv_card_title">{{ranks[0].div.divisionName}}</p>

                            <div class="_groupTv_card_main">
                                <!-- Team -->
                                <div class="_groupTv_card_items _2bg" v-for="(r, i) in ranks" :key="i" v-if="ranks.length" @click="bringRankHistory(r)">
                                    <div class="_groupTv_card_num">
                                        <p class="_groupTv_card_num_text">{{i+1}}</p>
                                    </div>

                                    <div class="_groupTv_card_pic_group">
                                        <div class="_groupTv_card_pic">
                                            <img class="_groupTv_card_img" :src="imageUrl+r.team.player1.profilePic" alt="" title="">
                                        </div>
                                        <div class="_groupTv_card_pic">
                                            <img class="_groupTv_card_img" :src="imageUrl+r.team.player2.profilePic" alt="" title="">
                                        </div>
                                    </div>

                                    <div class="_groupTv_card_details" >
                                        <p class="_groupTv_card_team">{{r.team.teamName}}</p>

                                        <div class="_groupTv_card_name_group">
                                           <p class="_groupTv_card_name">{{r.team.player1.firstName}}</p>
                                           <p class="_groupTv_card_name _groupTv_card_and"> & </p>
                                           <p class="_groupTv_card_name">{{r.team.player2.firstName}}</p> 
                                        </div>
                                    </div>

                                    <div class="_groupTv_card_point">
                                        <p class="_groupTv_card_point_text">{{r.meta ? r.meta.points : 0}}<sub>{{r.meta ? r.meta.matchePlayed : 0}}</sub></p>
                                    </div>
                                </div>
                                <!-- Team -->
                            </div>
                        </div>
                         <!-- </transition-group> -->
                    </div>
                    <div class="_groupTv_col3" v-for="(s, index) in breacketSettings[0].numberOfDiv" :delay="2000" v-if="!hasRank">
                      <!-- <transition-group name="slide-fade" > -->
                        <div class="_groupTv_card _1bg" :key="index">
                            <p class="_groupTv_card_title">Group {{index+1}}</p>

                            <div class="_groupTv_card_main">
                                <!-- Team -->
                                <div class="_groupTv_card_items _2bg" v-for="(r, i) in breacketSettings[0].numberOfTeams" :key="i" >
                                    <div class="_groupTv_card_num">
                                        <p class="_groupTv_card_num_text">{{i+1}}</p>
                                    </div>

                                    <div class="_groupTv_card_pic_group">
                                        <div class="_groupTv_card_pic">
                                            <img class="_groupTv_card_img" :src="imageUrl+'/uploads/pic.png'" alt="" title="">
                                        </div>
                                        <div class="_groupTv_card_pic">
                                            <img class="_groupTv_card_img" :src="imageUrl+'/uploads/pic.png'" alt="" title="">
                                        </div>
                                    </div>

                                    <div class="_groupTv_card_details" >
                                        <p class="_groupTv_card_team">Team {{i+1}}</p>

                                        <div class="_groupTv_card_name_group">
                                           <p class="_groupTv_card_name">Player1</p>
                                           <p class="_groupTv_card_name _groupTv_card_and"> & </p>
                                           <p class="_groupTv_card_name">Player2</p> 
                                        </div>
                                    </div>

                                    <div class="_groupTv_card_point">
                                        <p class="_groupTv_card_point_text">0<sub>0</sub></p>
                                    </div>
                                </div>
                                <!-- Team -->
                            </div>
                        </div>
                         <!-- </transition-group> -->
                    </div>
                    
                    <!-- Item -->
                   
                </div>
            </div>
            <div class="_tigers_popup_sec" v-if="show">
                <div class="_tigers_popup_sec_main" v-if="rankHistory.length">
                    <div class="_tigers_popup_titl">
                        <h1>{{teamName}}</h1>
                    </div>

                    <!-- CLOSE ICON -->
                    <div class="_tigers_popup_close" @click="show=false">
                        <span><i class="fa fa-times"></i></span>
                    </div>
                    <!-- CLOSE ICON -->

                    <div class="_tigers_popup_card_all">
                        <!-- CARD -->
                        <div class="_tigers_popup_card_main" v-for="(h,ri) in rankHistory">
                            <div class="_tigers_popup_card">
                                <p class="_tigers_popup_vs">v/s</p>
                                <div class="_tigers_popup_crd_dtls">
                                    <div class="_tigers_popup_crd_pic">
                                        <img :src="imageUrl+h.team.player1.profilePic" alt="image" class="">
                                        <img :src="imageUrl+h.team.player2.profilePic" alt="image" class="">
                                    </div>
                                    <p class="_tigers_popup_rslt_titl">{{h.team.teamName}}</p>
                                </div>

                                <div class="_tigers_popup_rslt">
                                    <p class="_tigers_popup_rslt_point">
                                        {{h.homeTeamPoint}}<span> - </span>{{h.awayTeamPoint}}
                                    </p>

                                    <ul>
                                        <li>{{h.playingDate}} | {{h.playingTime}}</li>
                                        
                                    </ul>
                                    <ul>
                                        <li>{{h.courtName || h.court.courtName}}</li>
                                        
                                    </ul>
                                </div>

                                <div class="_tigers_popup_btm_point">
                                    <ul>
                                        <li v-if="h.setOne">
                                            {{h.setOne.split("-")[0]}}<span>-</span>{{h.setOne.split("-")[1]}}
                                        </li>
                                        <li v-if="!h.setOne">-</li>

                                        <li v-if="h.setTwo">
                                            {{h.setTwo.split("-")[0]}}<span>-</span>{{h.setTwo.split("-")[1]}}
                                        </li>
                                        <li v-if="!h.setTwo">-</li>
                                        <li v-if="h.setThree">
                                            {{h.setThree.split("-")[0]}}<span>-</span>{{h.setThree.split("-")[1]}}
                                        </li>
                                        <li v-if="!h.setThree">-</li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- CARD -->

                </div>	
                </div>
	        </div>
        </div>
</groupplayranks>

<script>
settings  = JSON.parse(bracketShceduleSetting)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"

jsmatchData  = JSON.parse(jsmatchData)
var list = document.getElementsByTagName("groupplayranks");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "groupplayranks-app-" + i);
           var app = new Vue({
           el: "#groupplayranks-app-" + i,
                data(){
                    return {
                        allRanks : [], 
                        breacketSettings: [], 
                        groupedDivisions: [], 
                        currentRankIndex : 0, 
                        currentSettingIndex : 0, 
                        rankSwitchTime : 5000,
                        nextClassTimer : 10000,
                        tr: null, 
                        logo: [], 
                        club: null, 
                        show: false,
                        hasRank: false,
                        rankHistory: [],
                        teamName : '',
                        
                    }
                },
                methods: {
                    async bringInNextDivisions(){
                    this.changeRankIndex()
                    const res = await this.callApi('post', 'nextGroupDivisions', {divIds : this.groupedDivisions[this.currentRankIndex]})
                    if(res.status===200){
                        this.allRanks = res.data
                    }
                    setTimeout( () =>{
                        this.bringInNextDivisions()
                    }, this.rankSwitchTime)

                    }, 
                    changeRankIndex(){
                    if(this.currentRankIndex == this.groupedDivisions.length - 1){
                        this.currentRankIndex = 0
                    }else{
                        this.currentRankIndex++
                    }
                    },
                    changeClassIndex(){
                        
                        if(this.currentSettingIndex == this.breacketSettings.length - 1){
                            this.currentSettingIndex = 0
                        }else{
                            this.currentSettingIndex++
                        }
                    },

                    async bringInNextClassRanks(i){
                        this.currentSettingIndex = i
                        console.log(this.breacketSettings[i])
                        const res = await this.callApi('post', `rankForNexClass`, this.breacketSettings[i])
                        if(res.status===200){
                            this.allRanks = res.data.allRanks
                            this.groupedDivisions = res.data.groupedDivisions
                        }
                       

                    },
                    async bringRankHistory(r){
                        this.teamName = r.team.teamName
                        const res = await this.callApi(`get`, `getMatchHistory/${r.team_id}/${r.settingId}`)
                        if(res.status===200){
                            this.rankHistory = res.data
                            this.show = true
                        }else{
                            alert('Something went wrong while loading the data, please try again or contact us!')
                        }
                    },
                    async callApi(method, url, dataObj) {
                        try {

                            let data = await axios({
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                method: method,
                                url: 'https://webapi.forehand.se/api/v1/'+url,
                                data: dataObj
                            })
                            return data

                        } catch (e) {

                            return e.response
                        }
                    },

                },
               

                created(){
                    console.log(jsmatchData)
                    this.breacketSettings = jsmatchData.breacketSettings
                    this.groupedDivisions = jsmatchData.groupedDivisions
                    this.allRanks = jsmatchData.allRanks
                    this.hasRank = jsmatchData.hasRank
                    // if(this.groupedDivisions.length > 1){
                    //     setTimeout( () =>{
                    //         this.bringInNextDivisions()
                    //     }, this.rankSwitchTime)
                        
                    // }
                    // if(this.breacketSettings.length > 1){
                    //     setTimeout( () =>{
                    //         this.bringInNextClassRanks()
                    //     }, this.nextClassTimer)
                        
                    // }
                   
                    
                }


			})

    }
</script>