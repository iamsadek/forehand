<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var knockoutsetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageUrl='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsmatchData = json_encode($matchData);
echo "<script type='text/javascript'>var jsmatchData='$jsmatchData'</script>";

?>

<knockout>
	<div v-cloak>
    <div>
       <!-- Center -->
       
       <div class="_tree_top_center" v-if="brSettings.length">
               
            <div class="_tree_top_main">
              <ul class="_2tree_list class_in_knockout" v-if="brSettings.length > 1">
                  
                  <li :class="currentClassIndex == i?'_active' : ''" v-for="(c, i) in brSettings" :key="i" @click="featchNewClass(c,i)">{{c.className}}</li>
                  
              </ul>
              <!-- <ul class="_2tree_list" v-if="brSettings.length">
                  <li :class="isWinner?'_active' : ''" @click="changeBracket(true)">A-Slutspel</li>
                  <li v-if="brSettings[currentClassIndex].bracketA_knockout > 0 && brSettings[currentClassIndex].bracketB_knockout > 0" :class="bracket == 'BracketA'?'_active' : ''" @click="getBracket('BracketA')">Bracket A</li>
                  <li v-if="brSettings[currentClassIndex].bracketB_knockout > 0" :class="bracket == 'BracketB'?'_active' : ''" @click="getBracket('BracketB')">Bracket B</li>
                  <li v-if="brSettings[currentClassIndex].bracketC_knockout > 0" :class="bracket == 'BracketC'?'_active' : ''" @click="getBracket('BracketC')">Bracket C</li>
                  
              </ul> -->

              <h2 class="_tree_top_title">{{tr ? tr.tournamentName : ''}}</h2>
              <p class="_tree_top_date">{{matches[0][0].playingDate}} | {{matches[0][0].playingTime}}</p>
              <p class="_tree_top_place" >{{brSettings[currentClassIndex].className}} | {{currentBracket | bracketTxt}}</p>
            </div>
          </div>
          <!-- Center -->
        <div class="_2tree" v-if="matches">

            <!-- <ul class="_2tree_list _2tree_list1" v-if="classes[selectedSettingIndex].hasLoserBracket=='yes'">
                <li :class="isWinner?'_active' : ''" @click="changeBracket(true)">A-Slutspel</li>
                <li :class="!isWinner?'_active' : ''" @click="changeBracket(false)">B-Slutspel</li>
            </ul> -->
            <!-- <div class="_2tree_errow">
                <p class="_2tree_errow_icon _pre" style="font-family: Inter, Bangla846, sans-serif;" @click="moveToLeft"><svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.70801 10.585L3.12302 6L7.70801 1.41502L6.29304 -6.18503e-08L0.293038 6L6.29304 12L7.70801 10.585Z" fill="#5D6C71"></path></svg></p>
                <p class="_2tree_errow_icon _next" style="font-family: Inter, Bangla846, sans-serif;" @click="moveToRight"><svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.291992 1.41502L4.87698 6L0.291992 10.585L1.70696 12L7.70696 6L1.70696 8.91824e-07L0.291992 1.41502Z" fill="#FDFDFD"></path></svg></p>
            </div> -->
            <div class="_2tree_main_all deskptioOnly" v-if="matches">
                <!---->
                <div class="_2tree_main" v-for="(matches, i) in matches"  :key="i">
                    <div class="_2tree_pair" v-for="(m, j) in matches" :key="j">
                        <div :class=" (m.homeTeamPoint > m.awayTeamPoint) ?  `_2tree_card  ` : ` _2tree_card  _loser`  ">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">{{m.home.teamName}} <span v-if="m.isHomeQualified" class="qualifed">qualified</span></p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.home.player1.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.home.player1.firstName}} {{m.home.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.home.player1.profilePic"  alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.home.player2.firstName}} {{m.home.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;" >{{m.setOne ? m.setOne.split('-')[0] : ''}}</p>

                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setTwo ? m.setTwo.split('-')[0] : ''}}</p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setThree ? m.setThree.split('-')[0] : ''}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="_2tree_details" v-if="m.round!='1'">
                            <p class="_2tree_details_step" style="font-family: ab, Bangla846, sans-serif;">{{m.roundName}}</p>
                            <p class="_2tree_details_match" style="font-family: ab, Bangla846, sans-serif;">Match {{m.id}}</p>
                            <p class="_2tree_details_match" style="font-family: lato, Bangla846, sans-serif;">{{m.playingDate? m.playingDate.split('T')[0] : ''}} | {{m.playingTime}} <span v-if="m.playingTime">| {{m.courtName}}</span></p>
                        </div>
                        <div :class="(m.awayTeamPoint > m.homeTeamPoint) ?  `_2tree_card  ` : `_2tree_card  _loser ` " v-if="m.round!='1'" >
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">{{m.away.teamName}} <span v-if="m.isAwayQualified" class="qualifed">qualified</span></p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.away.player1.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.away.player1.firstName}} {{m.away.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.away.player2.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.away.player2.firstName}} {{m.away.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;" >{{m.setOne ? m.setOne.split('-')[1] : ''}}</p>

                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setTwo ? m.setTwo.split('-')[1] : ''}}</p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setThree ? m.setThree.split('-')[1] : ''}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!---->


                <!-- FINAL CARD -->
                <div class="_2tree_main" >
                    <div class="_2tree_pair">
                        <!-- if no teams wins -->
                        <div class="_2tree_card _loser" v-if="matches.length > 1 && matches[1][0].isTemp">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">
                                       {{matches[1][0].tempHomeTeam == 'Team not assigned' ? 'Winner' : `Winner of Match ${matches[1][0].id}`}}
                                    </p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img src="https://cdn.forehand.se/uploads/pic.png" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">player1</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img src="https://cdn.forehand.se/uploads/pic.png" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">player2</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                </div>
                            </div>
                        </div>
                        <!-- if home team -->
                        <div class="_2tree_card " v-if="matches.length > 1 && !matches[1][0].isTemp && (matches[1][0].homeTeamPoint > matches[1][0].awayTeamPoint)">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">
                                        {{matches[1][0].home.teamName}}
                                    </p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+matches[1][0].home.player1.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{matches[1][0].home.player1.firstName}} {{matches[1][0].home.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+matches[1][0].home.player1.profilePic"  alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{matches[1][0].home.player2.firstName}} {{matches[1][0].home.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                </div>
                            </div>
                        </div>
                        <!-- if away team -->
                        <div class="_2tree_card " v-if="matches.length > 1 && !matches[1][0].isTemp && (matches[1][0].homeTeamPoint < matches[1][0].awayTeamPoint)">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">
                                        {{matches[1][0].away.teamName}}
                                    </p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+matches[1][0].away.player1.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{matches[1][0].away.player1.firstName}} {{matches[1][0].home.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+matches[1][0].away.player1.profilePic"  alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{matches[1][0].away.player2.firstName}} {{matches[1][0].home.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FINAL CARD -->



            </div>

            
            <p class="thirdMatch" v-if="thirdPlace.length">3rd Place</p>
            <div class="_2tree_main_all deskptioOnly thirdMatchTree" v-if="thirdPlace.length">
               
                <!---->
                
                <div class="_2tree_main " v-for="(matches, i) in thirdPlace"  :key="i">
                
                
                    <div class="_2tree_pair" v-for="(m, j) in matches" :key="j">
                   
                        <div :class=" (m.homeTeamPoint > m.awayTeamPoint) ?  `_2tree_card  ` : ` _2tree_card  _loser`  ">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;"> {{m.home.teamName}} <span v-if="m.isHomeQualified" class="qualifed">qualified</span></p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.home.player1.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.home.player1.firstName}} {{m.home.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.home.player1.profilePic"  alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.home.player2.firstName}} {{m.home.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;" >{{m.setOne ? m.setOne.split('-')[0] : ''}}</p>

                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setTwo ? m.setTwo.split('-')[0] : ''}}</p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setThree ? m.setThree.split('-')[0] : ''}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="_2tree_details" v-if="m.round!='1'">
                            <p class="_2tree_details_step" style="font-family: ab, Bangla846, sans-serif;">{{m.roundName}}</p>
                            <p class="_2tree_details_match" style="font-family: ab, Bangla846, sans-serif;">Match {{m.id}}</p>
                            <p class="_2tree_details_match" style="font-family: lato, Bangla846, sans-serif;">{{m.playingDate? m.playingDate.split('T')[0] : ''}} | {{m.playingTime}} <span v-if="m.playingTime">| {{m.courtName}}</span></p>
                        </div>
                        <div :class="(m.awayTeamPoint > m.homeTeamPoint) ?  `_2tree_card  ` : `_2tree_card  _loser ` " v-if="m.round!='1'" >
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;"> {{m.away.teamName}} <span v-if="m.isAwayQualified" class="qualifed">qualified</span></p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.away.player1.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.away.player1.firstName}} {{m.away.player1.lastName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl+m.away.player2.profilePic" alt="" title="" class="_2tree_card_players_img"></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{m.away.player2.firstName}} {{m.away.player2.lastName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;" >{{m.setOne ? m.setOne.split('-')[1] : ''}}</p>

                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setTwo ? m.setTwo.split('-')[1] : ''}}</p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;">{{m.setThree ? m.setThree.split('-')[1] : ''}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!---->


                <!-- FINAL CARD -->
                <div class="_2tree_main" >
                    <div class="_2tree_pair">
                        <!-- no result yet -->
                        <div class="_2tree_card _loser" v-if="!thirdPlace[0][0].homeTeamPoint">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">
                                        Winner of Match {{thirdPlace[0][0].id}}
                                    </p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img src="https://cdn.forehand.se/uploads/pic.png" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">player1</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img src="https://cdn.forehand.se/uploads/pic.png" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">player2</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                </div>
                            </div>
                        </div>
                        <!-- if home team win -->
                        <div class="_2tree_card " v-if="thirdPlace[0][0].homeTeamPoint && (thirdPlace[0][0].homeTeamPoint > thirdPlace[0][0].awayTeamPoint)">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">
                                        {{thirdPlace[0][0].home.teamName}}
                                    </p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl + thirdPlace[0][0].home.player1.profilePic" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{thirdPlace[0][0].home.player1.firstName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl + thirdPlace[0][0].home.player2.profilePic" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{thirdPlace[0][0].home.player2.firstName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                </div>
                            </div>
                        </div>
                        <!-- if away team win -->
                        <div class="_2tree_card " v-if="thirdPlace[0][0].homeTeamPoint && (thirdPlace[0][0].homeTeamPoint < thirdPlace[0][0].awayTeamPoint)">
                            <div class="_2tree_card_left">
                                <div class="_2tree_card_title">
                                    <p class="_2tree_card_title_text" style="font-family: ab, Bangla846, sans-serif;">
                                        {{thirdPlace[0][0].away.teamName}}
                                    </p>
                                </div>
                                <div class="_2tree_card_players">
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl + thirdPlace[0][0].away.player1.profilePic" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{thirdPlace[0][0].away.player1.firstName}}</p>
                                    </div>
                                    <div class="_2tree_card_players_info">
                                        <div class="_2tree_card_players_pic"><img :src="imageUrl + thirdPlace[0][0].away.player2.profilePic" alt="" title="" class="_2tree_card_players_img" /></div>
                                        <p class="_2tree_card_players_text" style="font-family: lato, Bangla846, sans-serif;">{{thirdPlace[0][0].away.player2.firstName}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="_2tree_card_right">
                                <div class="_2tree_card_point">
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                    <p class="_2tree_card_point_text" style="font-family: lato, Bangla846, sans-serif;"></p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- FINAL CARD -->



            </div>

             
      </div>

    </div>
</knockout>

<script>
settings  = JSON.parse(knockoutsetting)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"

jsmatchData  = JSON.parse(jsmatchData)
console.log('jsmatchData', jsmatchData)
var list = document.getElementsByTagName("knockout");
   for (var j = 0; j < list.length; j++) {
           list[j].setAttribute("id", "knockout-app-" + j);
           var app = new Vue({
           el: "#knockout-app-" + j,
                data(){
                    return {
                        matches : [],
                        thirdPlace : [],
                        brSettings: [],
                        currentClassIndex : 0,
                        currentBracket : 'BracketA',
                        isThreeCols : true,
                        isSixteen : false,
                        isEight : false,
                        classTimer: 6000,
                        brTimer: 2000,
                        club: null,
                        tr: null,
                        logo : [],
                        bracket: 'BracketA'


                    }
                },


               methods: {

                  formateMatchtree(matches, isThird){
                          let i = 0
                          for(let match of matches){

                                for(let d of match){
                                    let tempHome = ''
                                    let tempAway = ''
                                    if(!isThird){
                                        if(d.tempAwayTeam == 'Team not assigned'){
                                            tempHome = 'Winner'
                                            tempAway = 'Winner'
                                        }else{
                                            tempHome = i > 0? `Winner of match ${d.tempHomeTeam}` : d.tempHomeTeam 
                                            tempAway = i > 0? `Winner of match ${d.tempAwayTeam}` : d.tempAwayTeam
                                        }
                                        
                                        
                                    }else{
                                        tempHome = i > 0? `1 Winner of match ${d.tempHomeTeam}` :`Loser of match ${d.tempHomeTeam}`
                                        tempAway = i > 0? `2 Winner of match ${d.tempAwayTeam}` :`Loser of match ${d.tempAwayTeam}`
                                    }

                                    
                                    d.roundName = this.getRoundNames(d.round)
                                    if(d.home){
                                      if(this.isAByTeam(d.home.teamName)){
                                          d.home.teamName = 'BY'
                                          d.home.player1 = {}
                                          d.home.player2 = {}
                                      }

                                    }
                                    if(d.away){
                                      if(this.isAByTeam(d.away.teamName)){
                                          d.away.player1 = {}
                                          d.away.player2 = {}
                                          d.away.teamName = 'BY'
                                      }

                                    }
                                    if(!d.home){
                                        d.isTemp = true
                                        d.home = {
                                            
                                            teamName : tempHome,
                                            player1: {
                                              firstName: 'player1',
                                              lastName: '',
                                              profilePic: '/uploads/pic.png'
                                            },
                                            player2: {
                                              firstName: 'player2',
                                              lastName: '',
                                              profilePic: '/uploads/pic.png'
                                            }
                                        }

                                    }
                                    if(!d.away){
                                        d.isTemp = true
                                        d.away = {
                                            teamName : tempAway,
                                            player1: {
                                            firstName: 'player1',
                                            lastName: '',
                                            profilePic: '/uploads/pic.png'
                                            },
                                            player2: {
                                                firstName: 'player2',
                                                lastName: '',
                                                profilePic: '/uploads/pic.png'
                                                }
                                            }
                                    }

                                    //console.log(d)
                                }
                            i++
                          }
                          if(isThird){
                            console.log('in third')
                            this.thirdPlace = matches
                            console.log('third place is', this.thirdPlace)
                          }else{
                            this.matches = matches
                          }

                          
                          
                        },
                        isAByTeam(name){
                        let teams = ['By 10', 'By 9', 'By 8', 'By 7', 'By 6', 'By 5', 'By 4', 'By 3', 'By 3', 'By 1']
                        return teams.includes(name)
                        },

                        getRoundNames(roundNumber){
                            if(roundNumber >= 16) return `ROUND OF ${roundNumber}`
                            if(roundNumber == 8) return 'QUARTER FINAL'
                            if(roundNumber == 4) return 'SEMI FINAL'
                            if(roundNumber == 2) return 'FINAL'
                            if(roundNumber == 0) return '3rd PLACE'
                        },
                        async featchNewClass(c,i){
                            if(i == this.currentClassIndex) return 
                            this.currentClassIndex = i
                            const res = await this.callApi('post', `${apiUrl}/getKnockoutFixtureForClass/${c.id}/BracketA`, {setting: c})
                            if(res.status==200){
                                if(res.data[0].length == 8){
                                    this.isSixteen = true
                                }else{
                                    this.isEight = true
                                }
                                
                                this.formateMatchtree(res.data, false)
                                //this.formateMatchtree(jsmatchData.thirdPlace, true)
                            }else{
                                console.log('couldnt delete event')
                            }
                        },
                        async callApi(method, url, dataObj) {
                            try {

                                let data = await axios({
                                    headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                    },
                                    method: method,
                                    url: url,
                                    data: dataObj
                                })
                                return data

                            } catch (e) {

                                return e.response
                            }
                        },



                





                    },
                    async created(){
                        this.club = jsmatchData.club
                        this.logo = jsmatchData.logo

                        this.tr = jsmatchData.tr
                        if(jsmatchData.matches[0].length == 8){
                            this.isSixteen = true
                        }else{
                          this.isEight = true
                        }
                        
                        this.formateMatchtree(jsmatchData.matches, false)
                        this.formateMatchtree(jsmatchData.thirdPlace, true)
                        //this.matches = jsmatchData.matches
                        this.brSettings = jsmatchData.brSetting

                        console.log(this.matches)
                        console.log(this.brSettings)




                    },
                    filters : {
                      bracketTxt(text){
                         if(text == 'BracketA') return 'Bracket A'
                         if(text == 'BracketB') return 'Bracket B'
                         if(text == 'BracketC') return 'Bracket C'
                      }
                    }



			})

    }
</script>