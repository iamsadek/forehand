<?php
namespace Elementor;

class SingleRacketStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-single rackets-text-style-one';
	}
	
	public function get_title() {
		return 'SINGLE RACKET STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'sectionBackgroundOne',
			[
				'label' => __( 'Section background one', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'sectionBackgroundTwo',
			[
				'label' => __( 'Section background two', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'image12',
			[
				'label' => __( 'Choose logo image one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$this->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
    
		$this->add_control(
			'textOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Libra' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'textOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textOneBgColor',
			[
				'label' => __( 'Title background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textTwo', [
				'label' => __( 'Text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Racket Name' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'textTwoColor',
			[
				'label' => __( 'Text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textThree', [
				'label' => __( 'Text two ', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Sub line of Racket Name' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'textThreeColor',
			[
				'label' => __( 'Text two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textFour', [
				'label' => __( 'Text three', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'We can provide you with great padel travel packages. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaqu eopsa quae ab illo' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'textFourColor',
			[
				'label' => __( 'Text three color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		
		$this->add_control(
			'buttonText', [
				'label' => __( 'Button text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Explore All Packages' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonTextColor',
			[
				'label' => __( 'Button  text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		$this->add_control(
			'buttonBackground',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		

		$repeater = new \Elementor\Repeater();

        $repeater->add_control(
			'imageLogoTwo',
			[
				'label' => __( 'Choose logo image two', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$repeater->add_control(
			'listBackground',
			[
				'label' => __( 'Box list background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$repeater->add_control(
			'outsideList', [
				'label' => __( 'Outside List ', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Hack' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'outsideListColor',
			[
				'label' => __( 'Outside list color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'white'
			]
		);
		
		
		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],
			
			]
		);

		
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('singleRacketStyleOneHtml.php');
    }
   
	
	


	
	
}