     
   <!-- DO PADEL PAGE-1 SINGEL RACKET -->
	<div class="_do_pdl_racket_sec" style="background-color: <?php echo $settings ['sectionBackgroundOne'] ?>;">
		<div class="_do_pdl_rckt_crd"  style="background-color: <?php echo $settings ['sectionBackgroundTwo'] ?>;">
			<div class="_do_pdl_rckt_crd_lft">
				<div class="_do_pdl_rckt_lft_top">
					<div class="_do_pdl_rckt_lft_top_box">
					<img src="<?php echo $settings['image12']['url'] ?>" alt="image">
					</div>
					<div class="_do_pdl_rckt_lft_top_info">
                              <p class="_do_pdl_rckt_lft_p1" style="color: <?php echo $settings['textOneColor'] ?>
                               ;background:<?php echo $settings['textOneBgColor'] ?>">
                              <?php echo $settings['textOne']?>
                              </p>
						<h3 style="color: <?php echo $settings['textTwoColor'] ?>">
                              <?php echo $settings['textTwo']?>
                              </h3>
						<p class="_do_pdl_rckt_lft_p2" style="color: <?php echo $settings['textThreeColor'] ?>">
                              <?php echo $settings['textThree']?>
                              </p>
					</div>
				</div>
				<div class="_do_pdl_rckt_lft_mdl">
                              <p style="color: <?php echo $settings['textFourColor'] ?>">
                               <?php echo $settings['textFour']?>
                              </p>
                         <a href="<?php echo $settings['buttonLink'] ?>" style="color : <?php echo $settings['buttonTextColor'] ?> ;background: <?php echo $settings['buttonBackground'] ?>">
                         <?php echo  $settings['buttonText']?></a>
				</div>
				<div class="_do_pdl_rckt_lft_btm">
					<ul>
                         <?php if($settings['list'] ): ?> 
                          <?php foreach($settings['list'] as $item ): ?>

						  <li>
							   <div class="_rckt_size_img">
							   <img src="<?php echo $item['imageLogoTwo']['url'] ?>" alt="image">
							  </div> 
						
							<p style="color: <?php echo $item['outsideListColor'] ?>">
                                    <?php echo $item['outsideList']?>
                             </p>
						</li>
						
						  <?php endforeach; ?>
                                <?php endif; ?>
					</ul>
				</div>
			</div>

			<div class="_do_pdl_rckt_crd_r8">
				<div class="_do_pdl_rckt_crd_img">
				
					<img src="<?php echo $settings['image']['url'] ?>" alt="image">
				</div>
			</div>
		</div>
	</div>
	<!-- DO PADEL PAGE-1 SINGEL RACKET END -->