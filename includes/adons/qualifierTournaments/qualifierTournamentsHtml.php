<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;
$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
$eventData = json_encode($eventData);
echo "<script type='text/javascript'>var eventData='$eventData'</script>";

?>



<qualifiertournaments>
<div v-cloak>
<div class="_tour_viaplay">
            <div class="_tour_teams_left">
                <h1 class="_tour_teams_left_title"><?php echo $settings['title'] ?></h1>
                <!-- <p class="_tour_clubs_left_join">turneringar</p> -->

                <p class="_tour_viaplay_eli"><?php echo $settings['subTitle'] ?></p>
            </div> 

            <div class="_tour_teams_right">
            <div class="_tour_teams_errow _pre qft" v-if="trs.length > 6" @click="goLeft"><i class="fas fa-chevron-left"></i></div>
            <div class="_tour_teams_errow _next qft" v-if="trs.length > 6" @click="goRight"><i class="fas fa-chevron-right"></i></div>
             <div class="_tour_viaplay_card_all" :style="{ transform: 'translate3d('+translateValue+'%, 0px, 0px)'  }">
               
                    <!-- Item -->
                    <div class="_tour_viaplay_card" v-for="(t,i) in trs" >
                        <div class="_tour_viaplay_card_pic" @click="goToLanding(t)">
                            <img class="_tour_viaplay_card_img" :src="t.league.standingPic? imageSource+t.league.standingPic : imageSource+t.league.img.image" alt="" title="">
                        </div>

                        <div class="_tour_viaplay_card_details">
                            <h2 class="_tour_viaplay_card_title">{{t.tournamentName}}</h2>
                            <p class="_tour_viaplay_card_date">{{t.city}}</p>
                            <p class="_tour_viaplay_card_date">{{t.dayName}} {{t.dayOfTheMonth}} {{t.monthName}}  </p>
                        </div>

                        <div class="_tour_viaplay_card_bottom" >
                            <div class="_tour_viaplay_card_peo_pic" v-if="t.qfteam">
                                <img class="_tour_viaplay_card_peo_img" :src="imageSource+t.qfteam.team.player1.profilePic" alt="" title="">
                            </div>
                            <div class="_tour_viaplay_card_peo_pic" v-if="t.qfteam">
                                <img class="_tour_viaplay_card_peo_img" :src="imageSource+t.qfteam.team.player2.profilePic" alt="" title="">
                            </div>
                            <template  v-if="t.qfteam">
                                <p class="_tour_viaplay_card_peo" v-if="t.__meta__.qfteam_count > 1"><span>+{{t.__meta__.qfteam_count - 1}}</span> teams signed up</p>
                                <p class="_tour_viaplay_card_peo" v-else><span></span> signed up</p>
                            </template>
                            <template  v-if="!t.qfteam">
                                <p class="_tour_viaplay_card_peo" ><span>Inga</span> anmälningar ännu!</p>
                            </template>
                            
                        </div>
                    </div>
                    <!-- Item -->

                </div>
            </div>
        </div>

</div>
      
</qualifiertournaments>


 
<script type="text/javascript">
    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
    eventData = eventData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    eventData = JSON.parse(eventData)
    console.log(eventData);
    
    
    
    var list = document.getElementsByTagName("qualifiertournaments");
    

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "qualifiertournaments-app-" + i);
            var app = new Vue({
            el: "#qualifiertournaments-app-" + i,
                data(){
                   return {
                        trs : eventData,
                        translateValue: 0,
                        clickNumbers: 1, 
                        pxToChnage: 20,
                        offestToKeep: 7, 
                   }
                },
                methods: {
                    goToLanding(t){
                        window.location = t.landingPage
                    },
                    goLeft(){
                        if(this.clickNumbers == 1) return 
                        let newNumber = this.translateValue + this.pxToChnage
                        this.translateValue = newNumber
                        this.clickNumbers--
                   },
                   goRight(){
                     if(this.clickNumbers == this.trs.length - this.offestToKeep){
                        return

                      } 
                      
                      this.translateValue = -this.clickNumbers*this.pxToChnage
                      this.clickNumbers++
                      
                   },
                }

                


            })

        }
 </script>

