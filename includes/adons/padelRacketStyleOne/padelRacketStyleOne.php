<?php
namespace Elementor;

class PadelRacketStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-padelrackets-text-style-one';
	}
	
	public function get_title() {
		return 'PADEL RACKETS STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		$this->add_control(
			'sectionBackground',
			[
				'label' => __( 'Section background', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		        
    
		$this->add_control(
			'headingOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'VIRGO' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		
		$this->add_control(
			'textOne', [
				'label' => __( 'Text one', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'DOPADEL' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$this->add_control(
			'textOneColor',
			[
				'label' => __( 'Text one Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);
			$this->add_control(
				'textTwo', [
					'label' => __( 'Text two', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Special Edition-2021' , 'plugin-domain' ),
					'label_block' => true,
				]
			);
	
			
	
			$this->add_control(
				'textTwoColor',
				[
					'label' => __( 'Text two Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
				]
				);
				$this->add_control(
					'textThree', [
						'label' => __( 'Text three', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::TEXTAREA,
						'default' => __( 'Precise as a watch and comfortable as a glove. This is the Nox Tempo World Padel Tour 2021 racket, the official racket of the World Padel Tour. Like its predecessor, the Tempo World Padel Tour 2021 racket stands out for its handling, precision and ball exit.' , 'plugin-domain' ),
						'label_block' => true,
					]
				);
		
				
		
				$this->add_control(
					'textThreeColor',
					[
						'label' => __( 'Text three Color', 'plugin-domain' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'default' => '',
					]
					);
					$this->add_control(
						'textFour', [
							'label' => __( 'Text four', 'plugin-domain' ),
							'type' => \Elementor\Controls_Manager::TEXT,
							'default' => __( '2995 kr' , 'plugin-domain' ),
							'label_block' => true,
						]
					);
			
					
			
					$this->add_control(
						'textFourColor',
						[
							'label' => __( 'Text four Color', 'plugin-domain' ),
							'type' => \Elementor\Controls_Manager::COLOR,
							'default' => '',
						]
						);
						$this->add_control(
							'textFive', [
								'label' => __( 'Text five', 'plugin-domain' ),
								'type' => \Elementor\Controls_Manager::TEXT,
								'default' => __( 'Select the picture to enlarge' , 'plugin-domain' ),
								'label_block' => true,
							]
						);
				
						
				
						$this->add_control(
							'textFiveColor',
							[
								'label' => __( 'Text five Color', 'plugin-domain' ),
								'type' => \Elementor\Controls_Manager::COLOR,
								'default' => '',
							]
							);
						$this->add_control(
							'buttonText',
							[
								'label' => __( 'Button text', 'elementor' ),
								'type' => Controls_Manager::TEXT,
								'default' => 'Läs mer'
							]
						);
						
						$this->add_control(
							'buttonLink',
							[
								'label' => __( 'Button link', 'elementor' ),
								'type' => Controls_Manager::TEXT,
								'default' => ''
							]
						);
						$this->add_control(
							'buttonBg',
							[
								'label' => __( 'Button background color', 'elementor' ),
								'type' => Controls_Manager::COLOR,
								'default' => ''
							]
						);
						$this->add_control(
							'buttonTxtColor',
							[
								'label' => __( 'Button text color', 'elementor' ),
								'type' => Controls_Manager::COLOR,
								'default' => ''
							]
						);	
						
         

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$this->add_control(
			'imageSelection',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					
				],
			
			]
		);

		
		

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('padelRacketStyleOneHtml.php');
    }
   
	
	


	
	
}