<?php
namespace Elementor;

class NewsPadelStyleThree extends Widget_Base {
	
	public function get_name() {
		return 'forehand-pnews-text-style-Three-3';
	}
	
	public function get_title() {
		return 'NEWS PADEL STYLE 3';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
		$this->add_control(
			'sectionBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);	        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'News' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
	   $this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$calendarData = $this->getNewsData();
        require('newsPadelStyleThreeHtml.php');
	}
	public function getNewsData(){
        $response = wp_remote_request( FOREHAND_API_URL . "/clubNews?limit=15",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
       
        //var_dump($response);
        //echo  $response;
         $body = wp_remote_retrieve_body($response);
         return json_decode($body);
    }
   
	
	


	
	
}