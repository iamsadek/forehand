<?php
namespace Elementor;

class AnnouncementStyleOne extends Widget_Base {
	//private $baseApi = 'http://backoffice.localhost';
	//private $baseApi = 'https://backoffice.kollol.me';
	public function get_name() {
		return 'forehand-announcement-style-one';
	}
	
	public function get_title() {
		return 'ANNOUNCEMENT STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-info-circle';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'imageDirection',
			[
				'label' => __( 'Image placement', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left'  => __( 'Left', 'plugin-domain' ),
					'right' => __( 'Right', 'plugin-domain' ),
					
				],
			]
		);
		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		
		$this->add_control(
			'sectionPadding',
			[
				'label' => __( 'Section padding', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '60px 60px 60px 60px'
			]
		);
		
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'En fantastisk nyhet'
			]
		);
		$this->add_control(
			'titleColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'heading',
			[
				'label' => __( 'Heading', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Enter your heading', 'elementor' ),
				'default' => 'Nu öppnar vi vår nya hall i Kungsbacka'
			]
		);
		// text
		$this->add_control(
			'location',
			[
				'label' => __( 'Location', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter location', 'elementor' ),
				'default' => "Hede | Kungsbacka"
			]
		);
		// text color 
		$this->add_control(
			'locationColor',
			[
				'label' => __( 'Location text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		$this->add_control(
			'desc',
			[
				'label' => __( 'description', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Enter your description', 'elementor' ),
				'default' => 'Nunc vel mattis nunc, rutrum laoreet felis. Vivamus dignissim cursus ultrices. Aenean euismod ipsum placerat molestie eleifend. Ut varius ex aliquam, tempus orci quis, suscipit sem. Fusce vestibulum molestie pulvinar. In malesuada, orci at mollis tincidunt, justo ante bibendum magna, vel vulputate augue nisl sagittis est.'
			]
		);
		

		$this->add_control(
			'image', [
				'label' => __( 'Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_control(
			'addButton',
			[
				'label' => __( 'Add a button', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);
		$this->add_control(
			'buttonTxt',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Läs mer'
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonRds',
			[
				'label' => __( 'Button border radius', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '30px'
			]
		);
		$this->add_control(
			'buttonBorderColor',
			[
				'label' => __( 'Button border color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'darkgray'
			]
		);



		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'transparent'
			]
		);
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);


		


		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('announcementStyleOneHtml.php');
    }
   
	
	


	
	
}