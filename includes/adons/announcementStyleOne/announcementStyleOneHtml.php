<?php if($settings['imageDirection'] == 'left' ): ?> 
<div class="_anouncmnt_sec" style="background: <?php echo $settings['boxBgColor'] ?>; padding: <?php echo $settings['sectionPadding'] ?>">
    <?php if($settings['title'] != '' ): ?> 
      <h3 class="_anouncmnt_sec_h3" style="color: <?php echo $settings['titleColor'] ?>">
          <?php echo $settings['title'] ?>
      </h3>
    <?php endif; ?>
    <div class="_anouncmnt_sec_main">
      <div class="_anouncmnt_lft">
        <img src="<?php echo $settings['image']['url'] ?>" alt="image">
      </div>
      <div class="_anouncmnt_r8">
        <h3 class="_anouncmnt_r8_h3">
            <?php echo $settings['heading'] ?>
        </h3>
        <p class="_anouncmnt_r8_p" style="color: <?php echo $settings['locationColor'] ?>">
            <?php echo $settings['location'] ?>
        </p>
        <p class="_anouncmnt_r8_txt">
            <?php echo $settings['desc'] ?>
        </p>
        <?php if($settings['addButton'] == 'yes' ): ?> 
				<div class="forehand_button">
					<a href="<?php echo $settings['buttonLink'] ?>">
						<button class="fbutton"
							style="color:<?php echo $settings['buttonTxtColor'] ?>;background:<?php echo $settings['buttonBg'] ?>;
							border: 1px solid <?php echo $settings['buttonBorderColor'] ?>;border-radius: <?php echo $settings['buttonRds'] ?>;"
						>
							<?php echo $settings['buttonTxt'] ?>
						</button>
					</a>
				</div>
			<?php endif; ?>

      </div>
    </div>
</div>
<?php endif; ?>

<?php if($settings['imageDirection'] == 'right' ): ?> 
<div class="_anouncmnt_sec" style="background: <?php echo $settings['boxBgColor'] ?>; padding: <?php echo $settings['sectionPadding'] ?>;">
<?php if($settings['title'] != '' ): ?> 
  <h3 class="_anouncmnt_sec_h3" style="color: <?php echo $settings['titleColor'] ?>">
        <?php echo $settings['title'] ?>
    </h3>
    <?php endif; ?>
   
    <div class="_anouncmnt_sec_main">
      
      <div class="_anouncmnt_r8">
        <h3 class="_anouncmnt_r8_h3">
            <?php echo $settings['heading'] ?>
        </h3>
        <p class="_anouncmnt_r8_p" style="color: <?php echo $settings['locationColor'] ?>">
            <?php echo $settings['location'] ?>
        </p>
        <p class="_anouncmnt_r8_txt">
            <?php echo $settings['desc'] ?>
        </p>
        <?php if($settings['addButton'] == 'yes' ): ?> 
				<div class="forehand_button">
					<a href="<?php echo $settings['buttonLink'] ?>">
						<button class="fbutton"
							style="color:<?php echo $settings['buttonTxtColor'] ?>;background:<?php echo $settings['buttonBg'] ?>;
							border: 1px solid <?php echo $settings['buttonBorderColor'] ?>;border-radius: <?php echo $settings['buttonRds'] ?>;"
						>
							<?php echo $settings['buttonTxt'] ?>
						</button>
					</a>
				</div>
			<?php endif; ?>

      </div>
      <div class="_anouncmnt_lft">
        <img src="<?php echo $settings['image']['url'] ?>" alt="image">
      </div>
    </div>
</div>
<?php endif; ?>