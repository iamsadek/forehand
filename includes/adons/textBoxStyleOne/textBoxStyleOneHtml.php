 <!-- TEXT ARRE -->

 <div class="_trnmnt_txt" style="background: <?php echo $settings['boxBgColor']; ?>; padding-top: <?php echo $settings['padingTop']; ?>; padding-bottom: <?php echo $settings['padingBottom']; ?>;padding-left: <?php echo $settings['padingLeft']; ?>;padding-right: <?php echo $settings['padingRight']; ?>;" >
    <div class="_trnmnt_txt_lft" style="text-align: <?php echo $settings['textAlign']; ?>;" >
        <!-- <p class="center secondary_heading"><?php echo $settings['heading'] ?></p> -->
        <p class="text-center textBoxHeading" 
            style="color: <?php echo $settings['headingColor']; ?>;font-size: <?php echo $settings['headingFontSize']; ?>; padding: <?php echo $settings['headingPadding']; ?>;"
            
            ><?php echo $settings['heading'] ?></p>
        <p class="_trnmnt_txt_p" style="color: <?php echo $settings['textColor']; ?>;font-size: <?php echo $settings['textFontSize']; ?>;line-height: <?php echo $settings['lineHeight']; ?>">
            <?php echo $settings['text'] ?>
        </p>
        <?php if($settings['addButton'] == 'yes' ): ?> 
            <div class="forehand_button">
                <a href="<?php echo $settings['buttonLink'] ?>">
                    <button class="fbutton"
                        style="color:<?php echo $settings['buttonTxtColor'] ?>;background:<?php echo $settings['buttonBg'] ?>;
                        border: 1px solid <?php echo $settings['buttonBorderColor'] ?>;border-radius: <?php echo $settings['buttonRds'] ?>;"
                    >
                        <?php echo $settings['buttonTxt'] ?>
                    </button>
                </a>
            </div>
        <?php endif; ?>
    </div>
    <!-- <div class="_trnmnt_txt_r8">
        <p class="_trnmnt_txt_p">
        To get the next level in tennis, a middle school player, an adult league player, a senior recreational player, and a professional tennis player all need to work on their overall fitness. The use of powerful strokes, the repetitive nature of the game, the various court surfaces, individual game styles, and the variety of movement and stroke patterns and stances in tennis call for a proper tennis workout program.
        </p>
    </div> -->
    </div>
    <!-- TEXT ARRE -->