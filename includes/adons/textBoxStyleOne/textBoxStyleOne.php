<?php
namespace Elementor;

class TextBoxStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-textbox-style-one';
	}
	
	public function get_title() {
		return 'TEXT BOX STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		
		$this->add_control(
			'heading',
			[
				'label' => __( 'Heading', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your headline', 'elementor' ),
				'default' => "Detta är en intressant rubrik"
			]
		);
		$this->add_control(
			'headingPadding',
			[
				'label' => __( 'Top heading', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your headline', 'elementor' ),
				'default' => "0px 0px 0px 0px"
			]
		);
		$this->add_control(
			'text',
			[
				'label' => __( 'Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::WYSIWYG,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the "
			]
		);
		$this->add_control(
			'padingTop',
			[
				'label' => __( 'Padding top', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding top', 'elementor' ),
				'default' => '80px'
			]
		);
		$this->add_control(
			'padingBottom',
			[
				'label' => __( 'Padding bottom', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding bottom', 'elementor' ),
				'default' => '40px'
			]
		);
		$this->add_control(
			'padingLeft',
			[
				'label' => __( 'Padding left', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding left', 'elementor' ),
				'default' => '200px'
			]
		);
		$this->add_control(
			'padingRight',
			[
				'label' => __( 'Padding right', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Padding right', 'elementor' ),
				'default' => '200px'
			]
		);
		$this->add_control(
			'lineHeight',
			[
				'label' => __( 'Line height', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Line height', 'elementor' ),
				'default' => '30px'
			]
		);
		$this->add_control(
			'textFontSize',
			[
				'label' => __( 'Text font size', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'font size', 'elementor' ),
				'default' => '15px'
			]
		);
		$this->add_control(
			'headingFontSize',
			[
				'label' => __( 'Headline font size', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'font size', 'elementor' ),
				'default' => '35px'
			]
		);


		$this->add_control(
			'textAlign',
			[
				'label' => __( 'Text alignment', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'center',
				'options' => [
					'left'  => __( 'Left', 'plugin-domain' ),
					'center' => __( 'Center', 'plugin-domain' ),
					'right' => __( 'Right', 'plugin-domain' ),
					
				],
			]
		);


		// background color 
		$this->add_control(
			'boxBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e5e5e5'
			]
		);
		// text color 
		$this->add_control(
			'headingColor',
			[
				'label' => __( 'Heading color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);
		
		$this->add_control(
			'addButton',
			[
				'label' => __( 'Add a button', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);
		$this->add_control(
			'buttonTxt',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Läs mer'
			]
		);
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonRds',
			[
				'label' => __( 'Button border radius', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '30px'
			]
		);
		$this->add_control(
			'buttonBorderColor',
			[
				'label' => __( 'Button border color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'darkgray'
			]
		);



		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'transparent'
			]
		);
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'black'
			]
		);



		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('textBoxStyleOneHtml.php');
    }
   
	
	


	
	
}