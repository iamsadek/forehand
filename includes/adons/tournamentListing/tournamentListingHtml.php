<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsmatchData = json_encode($matchData);
echo "<script type='text/javascript'>var jsmatchData='$jsmatchData'</script>";

?>

<trlisting>
	<div v-cloak>
  <div class="_padel9_section">
  <div class="_padel9_top_all">

<div class="_padel9_list">
  <ul>
    <li :class="current == 'all' ?  '_padel9_activ' : '' "  @click="changeTr('all')">
      <p><?php echo $settings['allTxt'] ?></p>
    </li>
    <li :class="current == 'Division' ?  '_padel9_activ' : '' " @click="changeTr('Division')">
      <p><?php echo $settings['divTxt'] ?></p>
    </li>
    <li :class="current == 'Bracket' ?  '_padel9_activ' : '' " @click="changeTr('Bracket')">
      <p><?php echo $settings['brTxt'] ?></p>
    </li>
    <li :class="current == 'Americano' ?  '_padel9_activ' : '' " @click="changeTr('Americano')">
      <p><?php echo $settings['amTxt'] ?></p>
    </li>
    <!-- <li>
      <p>mixed</p>
    </li> -->
  </ul>
</div>

</div>
		<div class="_padel9_main">

			<!-- ITEAM -->
			<div class="_padel9_card" v-for="(t, i) in currentTrs" v-if="currentTrs.length" @click="goNext(t)">
            <img :src="imageSource+t.img.image" alt="image">
            <div v-if="t.competitionType !='Divisions'">
               <div class="_padel9_card_top" >
                  <p>{{t.dayName}}</p>
                  <ul>
                     <li>{{t.dayOfTheMonth}} {{t.monthName}}</li>
                     
                  </ul>
               </div>
            </div>
				
            <div class="_padel9_card_top_tab">
               <p style="color : <?php echo $settings['sportTypeTxtColor'] ?>; background : <?php echo $settings['sportTypeTxtBgColor'] ?>;">{{t.competitionType | translateBr}}</p>
            </div>

				<div class="_padel9_card_logo">
					<img :src="imageSource+t.club.logo"  alt="image">
				</div>

				<div class="_padel9_card_btm">
					<div class="_padel9_card_btm_one">
						<p v-if="t.trs.brs.length" v-for="(s, j) in t.trs.brs" :key="j" >{{s.className}}</p>
						
					</div>
					<div class="_padel9_card_btm_two">
						<h3>{{t.leagueName}}</h3>
						<ul>
							<li>{{t.club.clubName}}</li>
							<li>{{t.club.city}}</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- ITEAM -->


		</div>
	</div>
  <div>
</trlisting>


<script>
settings  = JSON.parse(headerStyleOneSetting)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
jsmatchData  = JSON.parse(jsmatchData)
var list = document.getElementsByTagName("trlisting");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "trlisting-app-" + i);
           var app = new Vue({
           el: "#trlisting-app-" + i,
               data(){
                   return {
                      settings : settings,
                      trLists : jsmatchData,
                      imageSource: imageSource,
                      currentTrs : [], 
                      current : 'all', 
                      divs : [], 
                      br : [], 
                      ame: [], 
                      

                   }
               },
			   methods : {
            changeTr(d){
               if(d == 'Bracket'){
                     this.currentTrs = this.br
                  }else if(d == 'Americano'){
                     this.currentTrs = this.ame
                  }else if(d == 'Division'){
                     this.currentTrs = this.divs
                  }
                  else{
                     this.currentTrs = this.trLists
                     
                  }
                  this.current = d
               }, 
               goNext(t){
                  window.location = t.trs.landingPage
               }
			   },

			   created(){
            this.currentTrs = this.trLists
            for(let d of this.trLists){
               if(d.competitionType == 'Bracket'){
                  this.br.push(d)
               }else if(d.competitionType == 'Americano'){
                  this.ame.push(d)
               }else{
                  this.divs.push(d)
               }
            } 
              console.log('trLists is ',this.trLists)
			   }, 
            filters: {
               translateBr(v){
                   if(v=='Bracket') return 'Gruppspel & Slutspel'
                   if(v=='Divisions') return 'Seriespel'
                   return 'Americano'
               }
            }


			})

    }
</script>
