<?php
namespace Elementor;

class TournamentListing extends Widget_Base {
	
	public function get_name() {
		return 'forehand-tournament-listing';
	}
	
	public function get_title() {
		return "TOURNAMENT LISTS 1";
	}
	
	public function get_icon() {
		return 'fa fa-info-circle';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'sectionBg',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#F3F3F324'
			]
		);
		$this->add_control(
			'divTxt',
			[
				'label' => __( 'Division text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => "Seriespel"
			]
		);
		$this->add_control(
			'brTxt',
			[
				'label' => __( 'Group play and bracket text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => "Gruppspel & Slutspel"
			]
		);
		$this->add_control(
			'amTxt',
			[
				'label' => __( 'Americano text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => "Americano"
			]
		);
		$this->add_control(
			'allTxt',
			[
				'label' => __( 'Americano text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => "All"
			]
		);
		$this->add_control(
			'sportTypeTxtColor',
			[
				'label' => __( 'Sport type text color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => "white"
			]
		);
		$this->add_control(
			'sportTypeTxtBgColor',
			[
				'label' => __( 'Sport type background color', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'default' => "black"
			]
		);
		
		
		
		


		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$matchData = $this->getTodaysMatchData();
        require('tournamentListingHtml.php');
	}
	public function getTodaysMatchData(){
		
		$response = wp_remote_request( FOREHAND_API_URL."/getAllTournamtns",
            array(
				'method'     => 'GET', 
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
		$body = wp_remote_retrieve_body($response);
		return json_decode($body);
	}
   
	
	


	
	
}