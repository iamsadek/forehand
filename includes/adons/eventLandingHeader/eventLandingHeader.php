<?php
namespace Elementor;
require_once(BASEPLUGIN_INCLUDES . '/common/helperclass.php');

class EventLandingHeader extends Widget_Base {
	use Helper;
	
	public function get_name() {
		return 'forehand-event-landing-header';
	}
	
	public function get_title() {
		return 'EVENT LANDING HEADER';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		$events = $this->callApi('getClubEvents?timeline=no');
		
		

		$this->eventData = $events;
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$optinsAndDefault = $this->getOptionsAndDefaultValues($events, 'eventName');
		
		$this->add_control(
			'event_landing_id',
			[
				'label' => __( 'Select the event', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => $optinsAndDefault[1],
				'options' => $optinsAndDefault[0],
			]
		);



		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Events'
			]
		);
		$this->add_control(
			'boxBackground',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222222;'
			]
		);
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$eventData = $this->getCalendarData($settings['event_landing_id']);
        require('eventLandingHeaderHtml.php');
	}
	public function getCalendarData($id){
        $response = wp_remote_request( FOREHAND_API_URL."/eventSingle/$id",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
    }

   

	// js render
	
	


	
	
}