<?php
namespace Elementor;


class CalendarStyleFive extends Widget_Base {
	
	public function get_name() {
		return 'forehand-calendar-five';
	}
	
	public function get_title() {
		return 'CALENDAR STYLE 5';
	}
	
	public function get_icon() {
		return 'fa fa-calendar';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {
		
		

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		// news title
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => 'Events'
			]
		);
		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        $calendarData = $this->getCalendarData();
        require('calendarStyleFiveHtml.php');
    }
    public function getCalendarData(){
	
        $response = wp_remote_request( FOREHAND_API_URL."/calendarFive",
            array(
				'method'     => 'GET',
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
        $body = wp_remote_retrieve_body($response);
        return json_decode($body);
	}
	
	
	
	


	
	
}