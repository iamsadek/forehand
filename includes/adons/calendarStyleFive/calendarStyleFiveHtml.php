<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsCalenderData = json_encode($calendarData);
echo "<script type='text/javascript'>var jsCalenderData='$jsCalenderData'</script>";
// print_r($settings['eventsCat']); 
// die('trying to see ');


?>




<calendarstylefive >
    <div v-cloak>
	<div class="_tour_rel">
            <div class="_tour_rel_left">
                <h1 class="_tour_teams_left_title"><?php echo $settings['title'] ?></h1>
                <!-- <a class="_tour_news_see" href="">View All Events</a> -->
            </div>
        
            <div class="_tour_teams_right">
                <div class="_tour_teams_date_all" >
                    <div class="_tour_teams_date_mon" v-for="(calendar, key) in eventDates" v-if="eventDates">
                        <p class="_tour_teams_date_mon_title">{{calendar[0].monthName}}</p>
        
                        <div class="_tour_teams_date_card_all">
                            <!-- Item -->
                            <div :class="monthName==`${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}` ? '_tour_teams_date_card _active' : '_tour_teams_date_card'"  v-for="(c,j) in calendar" v-if="calendar.length"  @click="getData(c, `${calendar[0].monthName}-${c.dayOfTheMonth}-${c.dayName}`)">
                                <p class="_tour_teams_date_card_date">{{c.dayOfTheMonth}}</p>
                                <p class="_tour_teams_date_card_day">{{c.dayName}}</p>
                            </div>
                            <!-- Item -->
							
                        </div>
                    </div>
        
                    
                </div>
        
                <div class="_tour_rel_cards_all">
                    <div class="_tour_rel_cards_group">
                        <!-- Item -->
                        <div class="_tour_rel_cards" v-for="(c, key) in eventData" v-if="eventData.length">
                            <div class="_tour_rel_cards_pic">
                                <img class="_tour_rel_cards_img":src="imageSource+c.eventImg" alt="" title="">
                            </div>
                        
                            <p class="_tour_rel_cards_cal_date">{{c.eventTime}}</p>
                        
                            <div class="_tour_rel_cards_bottom2">
                                <p class="_tour_rel_cards_price">{{c.eventFee == 0 ? 'GRATIS' : c.eventFee }} <span>{{c.eventFee > 0 ? 'kr' : ''}}</span></p>
                                <p class="_tour_rel_cards_title">{{c.eventName}}</p>
                                <p class="_tour_rel_cards_gol">{{c.eventLocation}}</p>
                                <div class="_tour_rel_cards_bottom">
                                    <div class="_tour_rel_cards_peo_pic">
                                        <img class="_tour_rel_cards_peo_img" src="img/captain.png" alt="" title="">
                                    </div>
                                    <div class="_tour_rel_cards_peo_pic">
                                        <img class="_tour_rel_cards_peo_img" src="img/iro3.jpeg" alt="" title="">
                                    </div>
                                    <p class="_tour_rel_cards_peo_text"> {{c.__meta__.singedup_count}}/{{c.eventLimit}} platser kvar</p>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                        
                    </div>
        
                    
                </div>
            </div>
        </div>
    </div>
</calendarstylefive>



<script type="text/javascript">

    axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
    headerStyleOneSetting  = JSON.parse(headerStyleOneSetting)
	
	jsCalenderData = jsCalenderData.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    jsCalenderData = JSON.parse(jsCalenderData)
	
    // eventData  = JSON.parse(eventData)
    // console.log('data is', eventData)
    

    var list = document.getElementsByTagName("calendarstylefive");
    console.log(list)

        for (var i = 0; i < list.length; i++) {
            list[i].setAttribute("id", "calendarstylefive-app-" + i);
            var app = new Vue({
            el: "#calendarstylefive-app-" + i,
                data(){
                    return {
                        headerStyleOneSetting: headerStyleOneSetting,
                        eventDates: jsCalenderData.eventDates,
                        eventData: [],
                        items: [2,2,3,4,5],
                        isModal: false,
                        c : false,
                        imageSource: imageSource,
						index: 0,
						eventItems: [],
						keyName: '',
						prev: 0,
                        nextEl: 0,
                        isDone: false,
						apiUrl : apiUrl,
						catIndex: 0,
						dateIndex : 0, 
						monthName : '', 
						month : '', 
						s: 0,

                    }
                },
                methods: {
					slide(type) {
						let el = this.$refs.sl;
						console.log(el);
						//el.scrollLeft = this.s;
						if (type == "next") {
							this.s += 500;
						} else {
							this.s -= 500;
						}
					},
                    async getData(event, dateString){
						this.monthName = dateString
						this.month = dateString.split("-")[0]
						const res = await axios.get(`${apiUrl}/getEventByDate?date=${event.eventDate}`)
						if(res.status==200){
                            this.eventData = res.data
						}else{
							alert('Something went wrong while loading the data, please try again or contact us!')
						}
					},
                    async changeCat(cat, i){
						this.catIndex = i
						const res = await axios.get(`${apiUrl}/calendarThree?cat=${cat}`)
						if(res.status==200){
                            this.eventDates = res.data.eventDates
                            this.eventData = res.data.eventData
							if(this.eventData.length){
								//let c = this.eventData[0]
								//this.monthName = `${c.monthName}-${c.dayOfTheMonth}-${c.dayName}`
								//this.month = c.monthName
							}
						}else{
							alert('Something went wrong while loading the data, please try again or contact us!')
						}
					},
					goNext(c){
						window.location = c.landingPage
					}

                },
                created(){
					console.log(jsCalenderData)
					this.eventData = jsCalenderData.eventData

					if(this.eventData.length){
						//let c = this.eventData[0]
						//this.monthName = `${c.monthName}-${c.dayOfTheMonth}-${c.dayName}`
						//this.month = c.monthName
					}
                },


            })

        }
 </script>

