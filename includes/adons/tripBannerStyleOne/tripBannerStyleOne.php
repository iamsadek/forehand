<?php
namespace Elementor;

class tripBannerStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-tripBanner-text-style-one';
	}
	
	public function get_title() {
		return 'TRIP BANNER STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
		
		$this->add_control(
			'image',
			[
				'label' => __( 'Choose banner', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

	

		        
    
		$this->add_control(
			'heading', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Premium padel trip to' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headingColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$this->add_control(
			'place', [
				'label' => __( 'Place name', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Mallorca' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'placeColor',
			[
				'label' => __( 'Place name text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$this->add_control(
			'text',
			[
				'label' => __( 'Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => "A completely planned tour, covering everything from"
			]
		);
		$this->add_control(
			'textColor',
			[
				'label' => __( 'Text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textTwo',
			[
				'label' => __( 'Text two', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => "4 day programme"
			]
		);
		$this->add_control(
			'textTwoColor',
			[
				'label' => __( 'Text two color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'textthree',
			[
				'label' => __( 'Text three', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your text', 'elementor' ),
				'default' => "6500KR"
			]
		);
		$this->add_control(
			'textThreeColor',
			[
				'label' => __( 'Text Three color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
	
		
		
		
		$this->add_control(
			'buttonText',
			[
				'label' => __( 'Button text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'BOOK THIS TRIP NOW'
			]
		);
		
		$this->add_control(
			'buttonLink',
			[
				'label' => __( 'Button link', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => ''
			]
		);
	
		$this->add_control(
			'buttonTxtColor',
			[
				'label' => __( 'Button text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'buttonBg',
			[
				'label' => __( 'Button background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);

		
		$repeater = new \Elementor\Repeater();
		
	    $repeater->add_control(
				'imageLogo',
				[
					'label' => __( 'Choose logo image', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::MEDIA,
					'default' => [
						'url' => \Elementor\Utils::get_placeholder_image_src(),
					],
				]
			);
		
		$repeater->add_control(
			'textOne',
			[
				'label' => __( 'Text One', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'default' => "Number of days"
			]
		);
		$repeater->add_control(
			'textOneColor',
			[
				'label' => __( 'Text one color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$repeater->add_control(
			'textOneList',
			[
				'label' => __( 'Text one list', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'default' => "4 Days"
			]
		);
		$repeater->add_control(
			'textOneListColor',
			[
				'label' => __( 'Text one list color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						
					],
					
				],
			]
		);

		$this->end_controls_section();

	}

	
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('tripBannerStyleOneHtml.php');
    }
   
	
	


	
	
}