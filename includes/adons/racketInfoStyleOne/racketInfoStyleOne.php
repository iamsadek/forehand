<?php
namespace Elementor;

class RacketInfoStyleOne extends Widget_Base {
	
	public function get_name() {
		return 'forehand-rinfo-text-style-one';
	}
	
	public function get_title() {
		return 'RACKET INFO STYLE 1';
	}
	
	public function get_icon() {
		return 'fa fa-file-text';
	}
	
	public function get_categories() {
		return [ 'DoPadel' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		
   
		
		$this->add_control(
			'sectionBgColor',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);	        
    
		$this->add_control(
			'headerOne', [
				'label' => __( 'Title', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Additional Information' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'headerOneColor',
			[
				'label' => __( 'Title text color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		$this->add_control(
			'headerOneBGColor',
			[
				'label' => __( 'Title background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => ''
			]
		);
		
	
	
		
		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		

		
		$repeater->add_control(
			'listOne', [
				'label' => __( 'Description', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'The new Hack 02 (2020) from Nox. Adapt the racket to your playing style with custom weights, contains a better grip with innovative Hesacore grip & thanks to the Hybrid Diamond shape you have even more control during your game.' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

		

		$repeater->add_control(
			'listOneColor',
			[
				'label' => __( 'Description text color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
			]
			);

	
		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						
						
					],
					
				],
				
			]
		);
		$this->add_control(
			'image',
			[
				'label' => __( 'Choose second Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
	
	
	$this->add_control(
		'listTwo', [
			'label' => __( 'Description two', 'plugin-domain' ),
			'type' => \Elementor\Controls_Manager::TEXTAREA,
			'default' => __( 'The new Hack 02 (2020) from Nox. Adapt the racket to your playing style with custom weights, contains a better grip with innovative Hesacore grip & thanks to the Hybrid Diamond shape you have even more control during your game.' , 'plugin-domain' ),
			'label_block' => true,
		]
	);

	

	$this->add_control(
		'listTwoColor',
		[
			'label' => __( 'Description text two color', 'plugin-domain' ),
			'type' => \Elementor\Controls_Manager::COLOR,
			'default' => '',
		]
		);
		
		$this->end_controls_section();

	}
	
	// php render. 
	protected function render() {
        $settings = $this->get_settings_for_display();
        require('racketInfoStyleOneHtml.php');
    }
   
	
	


	
	
}