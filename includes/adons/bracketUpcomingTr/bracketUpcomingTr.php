<?php
namespace Elementor;

class BracketUpcomingTr extends Widget_Base {
	
	public function get_name() {
		return 'forehand-bracket-upcoming-tr';
	}
	
	public function get_title() {
		return "BRACKET UPCOMING TOURNAMENTS";
	}
	
	public function get_icon() {
		return 'fa fa-info-circle';
	}
	
	public function get_categories() {
		return [ 'Forehand' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Settings', 'elementor' ),
			]
		);
		$this->add_control(
			'sectionBg',
			[
				'label' => __( 'Section background color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#F3F3F324'
			]
		);
		$this->add_control(
			'trTitle',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'elementor' ),
				'default' => "Kommande tävlingar"
			]
		);
		
		$this->add_control(
			'prizeMoneyTxt',
			[
				'label' => __( 'Title', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your prize money text', 'elementor' ),
				'default' => "Prispengar"
			]
		);
		// $this->add_control(
		// 	'prizeMoney',
		// 	[
		// 		'label' => __( 'Title', 'elementor' ),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __( 'Enter your prize money amount', 'elementor' ),
		// 		'default' => "10 000"
		// 	]
		// );
		
		
		


		$this->end_controls_section();
	}
	
	// php render. 
	protected function render() {
		$settings = $this->get_settings_for_display();
		$matchData = $this->getTodaysMatchData();
        require('bracketUpcomingTrHtml.php');
	}
	public function getTodaysMatchData(){
		
		$response = wp_remote_request( FOREHAND_API_URL."/bracketTournaments?type=upcoming",
            array(
				'method'     => 'GET', 
				'headers' => [
					'forehandclub' => FOREHAND_API_KEY
				]
            )
        );
		$body = wp_remote_retrieve_body($response);
		
		return json_decode($body);
	}
   
	
	


	
	
}