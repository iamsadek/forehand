<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL;
echo "<script type='text/javascript'>var headerStyleOneSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageSource='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";
// calendar data in js var...
$jsmatchData = json_encode($matchData);
echo "<script type='text/javascript'>var jsmatchData='$jsmatchData'</script>";

?>
<bracket>
	<div v-cloak>
	<div class="_trnmnt2_sec">
      <p class="_trnmnt2_sec_p">
        <?php echo $settings['trTitle'] ?>
      </p>
      <div class="_trnmnt2_sec_all" v-if="upcomingMatches.length">
        <!-- LEFT BANNER -->
          

           <!-- RIGHT BANNER -->
      <div  class="_trnmnt2_banner_main" :style="{ transform: 'translate3d('+translateValue+'%, 0px, 0px)'  }">
         <div  :class="j == clickNumbers ? '_trnmnt2_banner sliderLeftActive' : '_trnmnt2_banner'"
                  v-for="(m, j) in upcomingMatches" @click="visitLandingPage(m)"
                >
            <div  class="_trnmnt2_banner_item"  :style="{'background-image': `url(${imageSource+m.trleague.img.image})`}" >
            <div class="_trnmnt2_bnr_logo_top">
                  <img :src="imageSource+m.trleague.club.logo" alt="image">
              </div>

              <div class="_trnmnt2_bnr_logo_btm">
                  <ul>                                  
                    <li v-for="(img, k) in m.trleague.sponsorLogo">
                      <img :src="imageSource+img.logo" v-if="img.logo !='/uploads/white.png'">
                    </li>
                    
                  </ul>
              </div>

                <!-- <div class="_trnmnt2_banner_dte">
                    <p class="_trnmnt2_banner_dte_txt1">{{m.monthName.substring(0,3)}}</p>
                    <p class="_trnmnt2_banner_dte_txt2">{{m.dayOfTheMonth}}</p>
                    <p class="_trnmnt2_banner_dte_txt3">2020</p>
                </div> -->
                <div class="_trnmnt2_bnr_dte">
                    <p class="_trnmnt2_bnr_dte_txt1">{{m.monthName.substring(0,3)}}</p>
                    <p class="_trnmnt2_bnr_dte_txt2">{{m.dayOfTheMonth}}</p>
                    <p class="_trnmnt2_bnr_dte_txt3">{{m.dayName}}</p>
                    <p class="_trnmnt2_bnr_dte_tym">{{m.brs[0].bracketGameTime}}</p>
                  </div>
                <div class="_trnmnt2_banner_mdl">
                  <p class="_trnmnt2_banner_mdl_p1">
                    <?php echo $settings['prizeMoneyTxt'] ?>
                  </p>
                  <p class="_trnmnt2_banner_mdl_p2">
                     {{m.trleague.prizeMoney || 0}} kr
                  </p>
                </div>
                <div class="_trnmnt2_banner_btm">
                  <ul class="_trnmnt2_banner_btm_ul">
                    <li>{{m.tournamentName}}</li>
                    <li>{{m.trleague.club.clubName}}</li>
                  </ul>
                  <div class="_trnmnt2_bnr_tab">
                    <p v-for="(s, i) in m.brs" :key="i">{{s.className}}</p>
                    <!-- <p>dam</p> -->
                  </div>
                  <!-- <p class="_trnmnt2_banner_btm_p">
                    Category <span>Men</span>
                  </p> -->
                </div>
               
              </div>
            </div>

          </div>
           <!-- RIGHT BANNER -->

           <!-- arrow -->
           <div class="_trnmnt2_banner_arrow _banner_arrow_lft" @click="goBack">
              <span>
                <i class="fa fa-arrow-left"></i>
              </span>
            </div>
            <div class="_trnmnt2_banner_arrow _banner_arrow_r8" @click="goNext">
              <span>
                <i class="fa fa-arrow-right"></i>
              </span>
            </div>
           <!-- arrow -->
      </div>
    </div>
	</div>
</bracket>


<script>
settings  = JSON.parse(headerStyleOneSetting)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"
jsmatchData  = JSON.parse(jsmatchData)
var list = document.getElementsByTagName("bracket");
   for (var i = 0; i < list.length; i++) {
           list[i].setAttribute("id", "bracket-app-" + i);
           var app = new Vue({
           el: "#bracket-app-" + i,
               data(){
                   return {
                      settings : settings,
                      upcomingMatches : jsmatchData,
                      imageSource: imageSource, 
                      translateValue: 0, 
                      clickNumbers: 1
                   }
               },
			   methods : {
            goNext(){ // increase 
                if(this.clickNumbers == this.upcomingMatches.length - 1){
                    this.translateValue = 0
                    this.clickNumbers = 1
                    return

                } 
                this.translateValue = -this.clickNumbers*69
                this.clickNumbers++
                
                console.log('increament',this.translateValue)
            },
            goBack(){ // going back
              if(this.clickNumbers == 1) return 
              let newNumber = this.translateValue + 69
              
                this.translateValue = newNumber
                console.log('decreament',this.translateValue)
                this.clickNumbers--
            },
            visitLandingPage(m){
              window.location = m.landingPage
            }
			   },

			   created(){
            console.log(this.upcomingMatches)
            if(this.upcomingMatches.length > 2){
              var repeatedImages = new Array(3).fill(this.upcomingMatches).flat();
              this.upcomingMatches = repeatedImages
              this.upcomingMatches.push(repeatedImages[0])
            }
            
            console.log("this.upcomingMatches", this.upcomingMatches)
			   }


			})

    }
</script>
