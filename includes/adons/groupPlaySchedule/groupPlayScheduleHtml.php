<?php
// settings to js data
$jsSettings = json_encode($settings);
$imageSource = FOREHAND_IMAGE_SOURCE;

$apiUrl = FOREHAND_API_URL_PADELCOMP;
echo "<script type='text/javascript'>var bracketShceduleSetting='$jsSettings'</script>";
echo "<script type='text/javascript'>var imageUrl='$imageSource'</script>";
echo "<script type='text/javascript'>var apiUrl='$apiUrl'</script>";


?>

<bracketschedule>
	<div v-cloak>
        <div>
             
       
      <div class="scheduleTv _tour_teams">
          <ul class="_2tree_list _2tree_list2" v-if="classes.length">
            <li
              :class="selectedSettingIndex == -1 ? '_active' : ''"
              @click="changeClass(-1, -1)"
            >
              All
            </li>
            <li
              :class="selectedSettingIndex == i ? '_active' : ''"
              v-for="(c, i) in classes"
              :key="i"
              @click="changeClass(c, i)"
            >
              {{ c.className }}
            </li>
          </ul>
        <div class="scheduleTv_con">
       
         <div class="switcher">
          <span :class="isFull? '_view_active' : ''" @click="isFull=true">Full view</span> |   <span :class="!isFull? '_view_active' : ''" @click="isFull=false">Short view</span>
               
            </div>
          <div class="scheduleTv_top2">
          <!-- <i-switch v-model="isFull" size="large">
              <span slot="open">Full</span>
              <span slot="close">Short</span>
              
          </i-switch> -->
           
            <div class="allgroups allgroups2" v-if="divs.length">
              <p class="groups">
                <span
                  v-for="(d, i) in divs"
                  :key="i"
                  :class="d.isSelected ? 'groupActive' : ''"
                  @click="showGroup(d)"
                  :style="``"
                  v-if="divId == -1 || d.settingId == divId"
                >
                  {{ d.divisionName }}</span
                >
              </p>
            </div>
          </div>
          <div class="scheduleTv_top2"></div>
          <div class="scheduleTv_main_all">
            <!-- COURT NUMBER -->
            <div class="scheduleTv_main">
              <div
                :class="
                  hasMultiDate
                    ? 'scheduleTv_main_time hasMultiDateForCourt'
                    : 'scheduleTv_main_time'
                "
              ></div>

              <div class="scheduleTv_team_all" v-if="allCourts.length">
                <!-- court -->
                <div
                  class="scheduleTv_team_item"
                  v-for="(c, i) in allCourts"
                  :key="i"
                  :id="isFull ? 'full' : 'short'"
                 
                >
                  <p
                    class="scheduleTv_team_court"
                    v-if="courtEditingIndex != i"
                  >
                    {{ c }}
                  </p>
                 
                </div>
                <!-- court -->
              </div>
            </div>
            <!-- COURT NUMBER -->

            <!-- TEAM WITH TIME -->
            <div
              class="scheduleTv_main"
              v-for="(matches, t) in allMatches"
              :key="t"
            >
              <div
                :class="
                  allMatches[t][0].hasMultiDate
                    ? 'scheduleTv_main_time multiDate'
                    : 'scheduleTv_main_time'
                "
              >
                <p class="scheduleTv_main_time_text">{{ t }}</p>
              </div>

              <div class="scheduleTv_team_all">
                <!-- Team item -->
                <div
                  :class="
                    !m.id
                      ? 'scheduleTv_team_item'
                      : m.isVisible
                      ? 'scheduleTv_team_item '
                      : 'scheduleTv_team_item _opacity'
                  "
                  v-for="(m, i) in matches"
                  :key="i"
                  v-if="!m.fake"
                  :id="isFull ? 'full' : 'short'"
                >
                  <div class="scheduleTv_team">
                    <div
                      :class="`scheduleTv_team_players _over${m.over}`"
                      v-if="isFull"
                    >
                      <p class="scheduleTv_team_name" v-if="m.home">
                        {{ m.id ? m.home.teamName : "-" }}
                        <!-- {{m.courtName}} -->
                      </p>
                      <p class="scheduleTv_team_name" v-if="!m.home && m.id">
                        Team not assigned
                      </p>

                      <div class="scheduleTv_team_vs_main" v-if="m.id">
                        <p class="scheduleTv_team_vs">VS</p>
                      </div>

                      <p class="scheduleTv_team_name right" v-if="m.away">
                        {{ m.id ? m.away.teamName : "-" }}
                        <!-- {{m.courtName}} -->
                      </p>
                      <p class="scheduleTv_team_name" v-if="!m.away && m.id">
                        Team not assigned
                      </p>
                    </div>

                    <div
                      class="scheduleTv_team_group"
                      :style="`background : ${m.colour}`"
                      v-if="m.id"
                    >
                      <p class="scheduleTv_team_group_text _classNameAndPoints">
                        <span class="customClassName" v-if="m.div">
                          {{ m.className }}-{{ m.div.divisionName
                          }}{{ isFull ? `, ${m.courtName}, id-${m.id}` : "" }}
                          <!-- {{m.courtName}} - {{m.id}} -->
                        </span>
                        <span class="customClassName" v-if="!m.div">
                          {{ m.className }}-{{ m.round
                          }}{{ isFull ? `, ${m.courtName}, id-${m.id}` : "" }}
                          <!-- {{m.courtName}} - {{m.id}} -->
                        </span>
                      </p>
                    </div>
                    <div
                      class="scheduleTv_team_group"
                      :style="`background : ${m.colour}`"
                      v-if="!m.id"
                    >
                      <p class="scheduleTv_team_group_text _classNameAndPoints">
                        <span class="customClassName black_txt">No match</span>
                      </p>
                    </div>
                  </div>
                </div>
                <!-- Team item -->
              </div>
            </div>
            <!-- TEAM WITH TIME -->
          </div>
        </div>
      </div>
        
        </div>

    </div>
</bracketschedule>

<script>
settings  = JSON.parse(bracketShceduleSetting)
axios.defaults.headers.common['forehandclub'] ="<?php echo FOREHAND_API_KEY ?>"


var list = document.getElementsByTagName("bracketschedule");
   for (var j = 0; j < list.length; j++) {
           list[j].setAttribute("id", "bracketschedule-app-" + j);
           var app = new Vue({
           el: "#bracketschedule-app-" + j,
              
           data() {
            return {
                allCourts: [],
                allMatches: [],
                hasMultiDate: false,
                divs: [],
                classes: [],
                selectedSettingIndex: 0,
                divId: -1,
                showSwapModal: true,
                isFull: 0,
                showCourtModal: false,
                addCourt: {
                    courtName: "",
                    matchId: null,
                },
                allTeamMatches: [],
                isSaveCourt: false,
                courtName: "",
                courtEditingIndex: -1,

                playingDate: "",
                playingTime: "",
                dateEditingIndex: -1,
                timeKey: -1,
                matchKey: -1,
                newDate: "",
                newTime: "",
                showDateTime: false,
                matchesCount: 0,
                knockoutCount: 0,
            };
           },
               
           methods: {
                async handleDrop(d) {
                // when team is dropped
                let teamOne = this.allMatches[this.timeKey][this.matchKey];
                let teamTwo = d.m;

                // teamOne.courtName = this.allCourts[d.dataMatchKey]
                // teamOne.playingTime = teamTwo.originalTime
                // teamOne.playingDate = teamTwo.playingDate

                // teamTwo.courtName = this.allCourts[this.matchKey]
                // teamTwo.playingTime = teamOne.playingDate
                // teamTwo.playingDate = teamOne.originalTime

                const obj = {
                    team1:
                    teamOne.id == null
                        ? null
                        : {
                            id: teamOne.id,
                            courtName: this.allCourts[d.dataMatchKey],
                            playingDate: teamTwo.playingDate,
                            playingTime: teamTwo.originalTime,
                            isKnockout: teamOne.isKnockout ? true : false,
                        },
                    team2:
                    teamTwo.id == null
                        ? null
                        : {
                            id: teamTwo.id,
                            courtName: this.allCourts[this.matchKey],
                            playingDate: teamOne.playingDate,
                            playingTime: teamOne.originalTime,
                            isKnockout: teamTwo.isKnockout ? true : false,
                        },
                };
                if (obj.team1 && obj.team2) {
                    if (obj.team1.id == obj.team2.id) {
                    return;
                    }
                }
                console.log("teamone", this.allMatches[this.timeKey][this.matchKey]);
                console.log("teamtwo", d.m);

                if (d.m) {
                    d.m.courtName = this.allCourts[this.matchKey];
                }
                if (this.allMatches[this.timeKey][this.matchKey]) {
                    this.allMatches[this.timeKey][this.matchKey].courtName =
                    this.allCourts[d.dataMatchKey];
                }

                Vue.set(
                    this.allMatches[d.dataTimeKey],
                    d.dataMatchKey,
                    this.allMatches[this.timeKey][this.matchKey]
                );
                Vue.set(this.allMatches[this.timeKey], this.matchKey, d.m);

                const res = await this.callApi("post", `bkt/changeMatchPosition`, obj);
                if (res.status == 200) {
                    location.reload();
                }
                },
                dropMatch(m) {
                ///when dragging...
                (this.timeKey = m.timeKey), (this.matchKey = m.matchKey);
                console.log("when dragging", m);
                },
                editDate(t) {
                this.playingDate = this.allMatches[t][0].playingDate;
                this.playingTime = this.allMatches[t][0].originalTime;
                this.dateEditingIndex = t;
                },
                async saveDateChange() {
                if (this.playingDate == "") return this.i("Please add date");
                if (this.playingTime == "") return this.i("Please add time");
                let ids = [];
                let knIds = [];
                for (let d of this.allMatches[this.dateEditingIndex]) {
                    if (d.id == null) continue;
                    console.log(d.id);
                    if (d.isKnockout) {
                    knIds.push(d.id);
                    } else {
                    ids.push(d.id);
                    }
                }
                console.log("old key, ", this.dateEditingIndex);
                if (!ids.length && !knIds.length) {
                    this.newDate = this.playingDate;
                    this.newTime = this.playingTime;
                    delete this.allMatches[this.dateEditingIndex];
                    this.dateEditingIndex = -1;
                    this.addNewDateRow(false);
                    return;
                }

                const res = await this.callApi("post", `bkt/updateScheduleDate`, {
                    ids: ids,
                    knIds: knIds,
                    playingTime: this.playingTime,
                    playingDate: this.playingDate,
                });
                if (res.status == 200) {
                    location.reload();
                } else {
                    this.swr();
                }
                },
                editCourt(i, courtName) {
                this.courtEditingIndex = i;
                this.courtName = courtName;
                },
                async changeCourtName() {
                if (this.courtName == "") return this.i("Please write the court name");
                if (this.allCourts[this.courtEditingIndex] == this.courtName)
                    return this.i("You didn't change the court name");
                let matchIds = [];
                let knckoutmatches = [];
                for (let d of this.allTeamMatches) {
                    if (d.courtName == this.allCourts[this.courtEditingIndex]) {
                    if (d.isKnockout) {
                        knckoutmatches.push(d.id);
                    } else {
                        matchIds.push(d.id);
                    }
                    }
                }

                const res = await this.callApi("post", `bkt/updateScheduleCourt`, {
                    ids: matchIds,
                    knids: knckoutmatches,
                    courtName: this.courtName,
                });
                if (res.status == 200) {
                    location.reload();
                } else {
                    this.swr();
                }
                },
                async saveNewCourt() {
                if (this.addCourt.courtName == "")
                    return this.i("Please add court name first");
                this.allCourts.push(this.addCourt.courtName);
                for (let i in this.allMatches) {
                    this.allMatches[i].push({
                    id: null,
                    isFake: true,
                    colour: "white",
                    playingTime: this.allMatches[i][0].playingTime,
                    isVisible: true,
                    hasMultiDate: true,
                    originalTime: this.allMatches[i][0].originalTime,
                    playingDate: this.allMatches[i][0].playingDate,
                    });
                }
                this.addCourt.courtName = "";
                },

                addNewDateRow(isMsg = true) {
                if (this.newDate == "") return alert("Please select date");
                if (this.newTime == "") return alert("Please select time");

                let monthName = this.getMonthS(
                    moment(new Date(this.newDate)).format("MMM")
                );
                let dayOfTheMonth = moment(new Date(this.newDate)).format("D");
                monthName = monthName.toLowerCase();
                let newDateTime = `${dayOfTheMonth} ${monthName} ${this.newTime}`;
                //check if this time exists
                for (let i in this.allMatches) {
                    if (i == newDateTime) {
                    alert("This date and time already exists");

                    return;
                    }
                }
                let reFormatedMatchesByTime = [];
                let matches = this.allMatches;
                for (let d of this.allCourts) {
                    reFormatedMatchesByTime.push({
                    id: null,
                    isFake: true,
                    colour: "white",
                    playingTime: newDateTime,
                    isVisible: true,
                    hasMultiDate: true,
                    originalTime: this.newTime,
                    playingDate: this.newDate,
                    });
                }
                this.allMatches[newDateTime] = reFormatedMatchesByTime;
                this.$forceUpdate();
                if (isMsg) {
                    alert("Date has been added at the end of the schedule! ");
                }

                console.log("this ran");
                },

                formateMatches(matches) {
                let uniqueDate = _.groupBy(matches, "playingDate");
                console.log("uniqueDate", uniqueDate);
                let isMultiDayTr = Object.keys(uniqueDate).length;

                for (let m of matches) {
                    m.isVisible = true;
                    let monthName = this.getMonthS(
                    moment(new Date(m.playingDate)).format("MMM")
                    );
                    monthName = monthName.toLowerCase();
                    let dayOfTheMonth = moment(new Date(m.playingDate)).format("D");
                    m.originalTime = m.playingTime;
                    m.playingTime = `${dayOfTheMonth} ${monthName} ${m.playingTime}`;

                    m.hasMultiDate = true;
                    // if (m.courtName) {
                    //     if(m.court){
                    //         m.court.courtName = m.courtName;
                    //     }else{
                    //         m.court = {}
                    //         m.court.courtName = m.courtName;
                    //     }

                    // }
                }
                let uniqueCourts = [...new Set(matches.map((item) => item.courtName))];

                // sort the court by name
                uniqueCourts = _.sortBy(uniqueCourts, function (name) {
                    return name;
                });

                //console.log('sort by uniqueCourts', uniqueCourts)
                let courtLength = uniqueCourts.length;
                //console.log('le', courtLength)
                // group matches by time
                matches = _.groupBy(matches, "playingTime");
                //console.log('group bby time', matches)

                let reFormatedMatchesByTime = []; // we must ensure to add some fake matches to fill the spot
                for (let time in matches) {
                    //console.log(time)
                    //console.log('match len', matches[time].length)
                    if (matches[time].length == courtLength) {
                    // console.log('euqal')
                    //continue
                    }
                    for (let c of uniqueCourts) {
                    let found = false;

                    for (let m of matches[time]) {
                        if (m.courtName == c) {
                        //console.log('both courts',m.courtName,c)
                        reFormatedMatchesByTime.push(m);
                        found = true;
                        }
                    }

                    if (!found) {
                        reFormatedMatchesByTime.push({
                        id: null,
                        isFake: true,
                        colour: "white",
                        playingTime: time,
                        isVisible: true,
                        hasMultiDate: isMultiDayTr > 1 ? true : false,
                        originalTime: matches[time][0].originalTime,
                        playingDate: matches[time][0].playingDate,
                        });
                    }
                    }
                    // console.log('time and matches ', time, reFormatedMatchesByTime)
                    matches[time] = reFormatedMatchesByTime;
                    reFormatedMatchesByTime = [];
                }
                this.allCourts = uniqueCourts;
                this.allMatches = matches;
                },
                getMonthS(m) {
                let obj = {
                    Jan: "Jan",
                    Feb: "Feb",
                    Mar: "Mar",
                    Apr: "Apr",
                    May: "Maj",
                    Jun: "Jun",
                    Jul: "Jul",
                    Aug: "Aug",
                    Sep: "Sep",
                    Oct: "Okt",
                    Nov: "Nov",
                    Dec: "Dec",
                };

                return obj[m];
                },
                showGroup(d) {
                d.isSelected = !d.isSelected;
                for (let div of this.divs) {
                    if (d.id == div.id) continue;
                    console.log(div.id);
                    div.isSelected = false;
                }
                for (let i in this.allMatches) {
                    for (let m of this.allMatches[i]) {
                    if (d.isSelected) {
                        // user selected this division, mute all except this div
                        if (m.division_id == d.id) {
                        m.isVisible = true;
                        } else {
                        m.isVisible = false;
                        }
                    } else {
                        // user unselected it. so open all
                        console.log("here");
                        m.isVisible = true;
                    }
                    }
                }
                },
                changeClass(c, i) {
                if (c == -1) {
                    this.divId = -1;
                    this.selectedSettingIndex = -1;
                    return;
                }
               
                this.divId = c.id;
                this.selectedSettingIndex = i;
                },
                async callApi(method, url, dataObj) {
                      try {

                          let data = await axios({
                              headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              method: method,
                              url: apiUrl+url,
                              data: dataObj
                          })
                          return data

                      } catch (e) {

                          return e.response
                      }
                  },
            },

            async created() {
                console.log(settings)
                const res = await this.callApi(
                "get",
                `tournament/getBracketScheduleByTr/${settings.trId}`
                );
                if (res.status == 200) {
                this.knockoutCount = res.data.knockoutCount;
                this.matchesCount = res.data.matchesCount;
                this.allTeamMatches = res.data.allMatches;
                //console.log('total matches', this.allTeamMatches.length)
                this.formateMatches(res.data.allMatches);
                console.log(res.data.divs[0].settingId)
                if(res.data.divs.length){
                  this.divId = res.data.divs[0].settingId
                }
                console.log(this.divId)
                this.divs = res.data.divs;
                
                this.classes = res.data.classes;
                }
            },
                

		})

    }
</script>