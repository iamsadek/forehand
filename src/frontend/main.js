import Vue from 'vue'
import App from './App.vue'
import router from './router'
window.axios = require('axios');
/*custom common methods*/
import common from './common';
Vue.mixin(common);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#vue-frontend-app',
  router,
  render: h => h(App)
})
