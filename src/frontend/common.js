export default {
	data() {
		return {

			mobile: 'https://mobile.padelmatch.se/uploads',
			//siteUri: 'https://backoffice.forehand.se',
			siteUri: 'http://backoffice.localhost',
			
			apiUrl: 'https://padelmatch.se/api/'
		}
	},
	methods: {
		async callApi(method, url, dataObj) {
            console.log("axios is")
            console.log(axios)
			try {

				let data = await axios({
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
					},
					method: method,
					url: url,
					data: dataObj
				})
				return data

			} catch (e) {

				return e.response
			}
		},


	}
}