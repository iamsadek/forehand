<?php
/*
Plugin Name: Forehand
Plugin URI: https://forehand.se/
Description: This plugin containts all the widgets, themes and templates and all..
Version: 0.204
Author: Sadek
Author URI: https://forehand.se/
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: baseplugin
Domain Path: /languages
 */

/**
 * Copyright (c) YEAR Your Name (email: Email). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Base_Plugin class
 *
 * @class Base_Plugin The class that holds the entire Base_Plugin plugin
 */

$plugin_data = get_file_data(__FILE__, array('Version' => 'Version'), false);
$plugin_version = $plugin_data['Version'];
define('BASEPLUGIN_VERSION', $plugin_version);
define('FOREHAND_MODE', 'devd');
final class Base_Plugin
{

    /**
     * Plugin version
     *
     * @var string
     */
    public $version = '0.1.0';

    /**
     * Holds various class instances
     *
     * @var array
     */
    private $container = array();

    /**
     * Constructor for the Base_Plugin class
     *
     * Sets up all the appropriate hooks and actions
     * within our plugin.
     */
    public function __construct()
    {

        $this->define_constants();

        register_activation_hook(__FILE__, array($this, 'activate'));
        register_deactivation_hook(__FILE__, array($this, 'deactivate'));

        add_action('plugins_loaded', array($this, 'init_plugin'));
        add_action('elementor/init', array($this, 'elementorAdd'));

    }

    /**
     * Initializes the Base_Plugin() class
     *
     * Checks for an existing Base_Plugin() instance
     * and if it doesn't find one, creates it.
     */
    public static function init()
    {
        static $instance = false;

        if (!$instance) {
            $instance = new Base_Plugin();
        }

        return $instance;
    }
    public function elementorAdd()
    {
        //include 'includes/source.php';

        // Unregister source with closure binding, thank Steve.
        // $unregister_source = function($id) {
        //     unset( $this->_registered_sources[ $id ] );
        // };
        // $unregister_source->call( \Elementor\Plugin::instance()->templates_manager, 'remote');
        // \Elementor\Plugin::instance()->templates_manager->register_source( 'Elementor\TemplateLibrary\Source_Custom' );
    }
    // public function wpml_widgets_to_translate_filter( $widgets ) {

    //     $widgets[ 'forehand-about-style-one' ] = [
    //         'conditions' => [ 'widgetType' => 'forehand-about-style-one' ],
    //         'fields'     => [
    //            [
    //               'field'       => 'trTitle',
    //               'type'        => __( 'trTitle', 'seelos-elementor' ),
    //               'editor_type' => 'LINE'
    //            ],
    //         ],
    //      ];

    //     return $widgets;
    //   }

    /**
     * Magic getter to bypass referencing plugin.
     *
     * @param $prop
     *
     * @return mixed
     */
    public function __get($prop)
    {
        if (array_key_exists($prop, $this->container)) {
            return $this->container[$prop];
        }

        return $this->{$prop};
    }

    /**
     * Magic isset to bypass referencing plugin.
     *
     * @param $prop
     *
     * @return mixed
     */
    public function __isset($prop)
    {
        return isset($this->{$prop}) || isset($this->container[$prop]);
    }

    /**
     * Define the constants
     *
     * @return void
     */
    public function define_constants()
    {
        // define plugin constant...
        $this->definePluginVars('dev');

        define('BASEPLUGIN_FILE', __FILE__);
        define('BASEPLUGIN_PATH', dirname(BASEPLUGIN_FILE));
        define('BASEPLUGIN_INCLUDES', BASEPLUGIN_PATH . '/includes');
        define('BASEPLUGIN_URL', plugins_url('', BASEPLUGIN_FILE));
        define('BASEPLUGIN_ASSETS', BASEPLUGIN_URL . '/assets');

        $key = get_option('forehand_api_key');
        define('FOREHAND_API_KEY', $key);

    }
    public function definePluginVars($mode)
    {
        if (FOREHAND_MODE == 'dev') {
            define('FOREHAND_API_URL', 'http://127.0.0.1:3333/api/v1');
            define('FOREHAND_API_URL_PADELCOMP', 'http://laravel-api.localhost/');
            //define('FOREHAND_IMAGE_SOURCE', 'http://backoffice.localhost');
        } else {
            define('FOREHAND_API_URL', 'https://webapi.forehand.se/api/v1');
             define('FOREHAND_API_URL_PADELCOMP', 'https://api.padelcomp.com/');
            //define('FOREHAND_API_URL_PADELCOMP', 'https://padel.appifylab.com/');
            //define('FOREHAND_IMAGE_SOURCE', 'http://backoffice.forehand.se');
            //define('FOREHAND_IMAGE_SOURCE', 'http://backoffice.kollol.me');
        }
        define('FOREHAND_IMAGE_SOURCE', 'https://cdn.forehand.se');
    }

    /**
     * Load the plugin after all plugis are loaded
     *
     * @return void
     */
    public function init_plugin()
    {
        $this->includes();
        $this->init_hooks();
    }

    /**
     * Placeholder for activation function
     *
     * Nothing being called here yet.
     */
    public function activate()
    {

        $installed = get_option('baseplugin_installed');

        if (!$installed) {
            update_option('baseplugin_installed', time());
        }

        update_option('baseplugin_version', BASEPLUGIN_VERSION);
    }

    /**
     * Placeholder for deactivation function
     *
     * Nothing being called here yet.
     */
    public function deactivate()
    {

    }

    /**
     * Include the required files
     *
     * @return void
     */
    public function includes()
    {

        require_once BASEPLUGIN_INCLUDES . '/Assets.php';

        if ($this->is_request('admin')) {
            require_once BASEPLUGIN_INCLUDES . '/Admin.php';
        }

        require_once BASEPLUGIN_INCLUDES . '/Frontend.php';

        if ($this->is_request('ajax')) {
            // require_once BASEPLUGIN_INCLUDES . '/class-ajax.php';
        }

        require_once BASEPLUGIN_INCLUDES . '/Api.php';
    }

    /**
     * Initialize the hooks
     *
     * @return void
     */
    public function init_hooks()
    {
        
        add_action('init', array($this, 'init_classes'));

        // Localize our plugin
        add_action('init', array($this, 'localization_setup'));
        
    }

    /**
     * Instantiate the required classes
     *
     * @return void
     */
    public function init_classes()
    {

        if ($this->is_request('admin')) {
            $this->container['admin'] = new App\Admin();
        }

        // if ( $this->is_request( 'frontend' ) ) {
        //     $this->container['frontend'] = new App\Frontend();

        // }

        if ($this->is_request('ajax')) {
            // $this->container['ajax'] =  new App\Ajax();
        }

        $this->container['api'] = new App\Api();
        $this->container['assets'] = new App\Assets();
    }

    /**
     * Initialize plugin for localization
     *
     * @uses load_plugin_textdomain()
     */
    public function localization_setup()
    {
        load_plugin_textdomain('baseplugin', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    }

    /**
     * What type of request is this?
     *
     * @param  string $type admin, ajax, cron or frontend.
     *
     * @return bool
     */
    private function is_request($type)
    {
        switch ($type) {
            case 'admin':
                return is_admin();

            case 'ajax':
                return defined('DOING_AJAX');

            case 'rest':
                return defined('REST_REQUEST');

            case 'cron':
                return defined('DOING_CRON');

            case 'frontend':
                return (!is_admin() || defined('DOING_AJAX')) && !defined('DOING_CRON');
        }
    }

} // Base_Plugin

$baseplugin = Base_Plugin::init();
function add_elementor_widget_categories($elements_manager)
{

    $elements_manager->add_category(
        'Forehand',
        [
            'title' => __('Forehand', 'plugin-name'),
            'icon' => 'fa fa-plug',
        ]

    );
    $elements_manager->add_category(
        'DoPadel',
        [
            'title' => __('DoPadel', 'plugin-name'),
            'icon' => 'fa fa-plug',
        ]

    );

    $elements_manager->add_category(
        'Matchi',
        [
            'title' => __('Matchi', 'plugin-name'),
            'icon' => 'fa fa-plug',
        ]

    );

}
add_action('elementor/elements/categories_registered', 'add_elementor_widget_categories');
add_action('init', 'my_elementor_init');
require BASEPLUGIN_PATH . '/updated_checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/iamsadek/forehand',
    __FILE__,
    'unique-plugin-or-theme-slug'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('fQLXosNqNksfBbeHa19o');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');
function my_elementor_init()
{
    new App\Frontend();
}
